import openpyxl
import time

from DataBase.DBController import DBController
from DataBase.Upload import Upload
from DataBase.Call import Call
from DataBase.Project import Project

from CallCenter.Skorozvon import Skorozvon

project = Project.get_by_name("Действующие точка")
project_params = project.params()
bank = project.bank()

book = openpyxl.load_workbook("res.xlsx")
sheet = book.get_active_sheet()

data = sheet.values

good_list = []
bad_list = []

for row in data:

    if row[0] == 'None':
        continue

    if row[1] == 'True':
        good_list.append(row[0])
    else:
        bad_list.append(row[0])

controller = DBController()
#upload = Upload.create(controller, "Из файла", "Руками")
upload = Upload.find_by_id(6388, controller)

#for org_id in bad_list:
#    call = Call.create(controller, org_id, bank.id(), project.id(), state="ОДП при подгрузке")

#    upload.link_to_call_and_organization(call.id(), org_id)

cur = controller.get_cursor()

SQL = """SELECT Organization.ID, Organization.Name as OrgName, Organization.INN, Organization.Address,
                ContactPerson.Surname, ContactPerson.Name as CName, ContactPerson.MiddleName,
                ContactInfo.Value as Phone
         FROM Organization
         INNER JOIN ContactPerson ON ContactPerson.ID = Organization.LastContactPerson
         INNER JOIN ContactInfo ON ContactInfo.ID = Organization.LastPhoneInfo
         WHERE Organization.ID in ({})""".format(",".join(good_list))

cur.execute(SQL)
org_rec_list = cur.fetchall()

upload_list = []

for org_rec in org_rec_list:
    upload_list.append(
        {
            "external_id": org_rec["ID"],
            "name": "{} {} {}".format(org_rec["Surname"], org_rec["CName"], org_rec["MiddleName"]),
            "phones": [org_rec["Phone"]],
            "address": org_rec["Address"],
            "post": "Директор",
            "city": org_rec["OrgName"],
            "tags": ["Подгрузка {}".format(upload.id())]
        }
    )

skorozvon = Skorozvon()
part_len = 1000

for i in range(1, len(upload_list) // part_len + 1):

    print("Обрабатываем контакты с {} по {}".format(i * part_len, (i + 1) * part_len))
    part = upload_list[i * part_len:(i + 1) * part_len]

    wait = True

    while wait:
        try:
            print("Отправляем контакты на массовую загрузку")
            ext_upload_id = skorozvon.mass_load(part, project_params["Скорозвон"]["project_id"])
            wait = False
        except:
            continue

    wait = True

    while wait:
        print("Ожидаем загрузки контактов скорозвоном")
        if skorozvon.is_import_complete(ext_upload_id):
            wait = False
            continue

        time.sleep(5)

    # попробуем еще раз, если поучили ошибку
    try:
        print("Загрузили, получаем обратную связь")
        ext_org_list = skorozvon.get_ext_org_list(ext_upload_id)
    except:
        print("Ошибка получения результата массовой загрузки {}".format(ext_upload_id))
        ext_org_list = skorozvon.get_ext_org_list(ext_upload_id)

    count = 0

    for ext_org in ext_org_list:
        count += 1
        print("Создаем звонок {} из {}".format(count, len(ext_org_list)))

        call = Call.create(controller, ext_org["external_id"], bank.id(), project.id(), ext_org["id"])
        upload.link_to_call_and_organization(call.id(), ext_org["external_id"])
