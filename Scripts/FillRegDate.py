import sys

sys.path.append("..")

from datetime import datetime
from DataBase.Organization import Organization
from DataBase.DBController import DBController

controller = DBController()

cur = controller.get_cursor()

cur.execute("""SELECT *
               FROM Organization 
               WHERE LastRegInfo IS NULL""")

org_rec_list = cur.fetchall()

total_count = len(org_rec_list)

count = 0
log = open("log_fill_org_reg_info.txt", "w")
start_dt = datetime.now()

for org_rec in org_rec_list:

    org = Organization(org_rec, controller)
    reg_id = org.set_reg_info(org.open_date(), org.ogrn())
    org.set_last_reg_info(reg_id)

    count += 1
    duration = datetime.now() - start_dt
    one_org_duration = duration/count

    log_message = "{} из {} Организация {} среднее время {} осталось {} прогноз {}"\
                   .format(count, total_count, org.id(), one_org_duration, one_org_duration*(total_count-count),
                           datetime.now() + one_org_duration*(total_count-count))
    print(log_message)
    log.write(log_message + "\n")
    log.flush()

#total_count = cur.fetchone()["CNT"]

#cur.execute("""SELECT MAX(Organization.ID) as MID
#               FROM Organization
#               LEFT JOIN RegInfo ON RegInfo.ID = Organization.LastRegInfo
#               WHERE RegInfo.ID IS NOT NULL""")

#start_id = cur.fetchone()["MID"]

#count = 0
#log = open("log_fill_org_reg_info.txt", "w")
#start_dt = datetime.now()


#def set_last_reg_info(org):
#    global count

#    reg_id = org.set_reg_info(org.open_date(), org.ogrn())
#    org.set_last_reg_info(reg_id)

#    count += 1
#    duration = datetime.now() - start_dt
#    one_org_duration = duration/count


#    log_message = "{} из {} Организация {} среднее время {} осталось {} прогноз {}"\
#        .format(count, total_count, org.id(), one_org_duration, one_org_duration*(total_count-count),
#                datetime.now() + one_org_duration*(total_count-count))
#    print(log_message)
#    log.write(log_message + "\n")
#    log.flush()


#Organization.for_all_organizations(set_last_reg_info, start_id=start_id)
