import requests
import json
import time
import holidays

from datetime import datetime
from datetime import timedelta

from DataBase.DBController import DBController
from DataBase.Project import Project
from DataBase.Task import Task

class Skorozvon:

    def __init__(self):
        self.grant_type = "password"
        self.username = "zakaz@rt-voice.com"
        self.api_key = "ed122f83c633b8f7bc799577c55e6d31e6fb37d1e96ef8826668b23abac94926"
        self.client_id = "29055bf486467ffb99159edf3c21881d8ec4349ee1eb61c0b172364bbcc623b7"
        self.client_secret = "172f48c27f7eb1c2322526b8f92d5b25dcc9cbc8785f137a428795b3f4a4cb2a"

        auth_url = "https://app.skorozvon.ru/oauth/token"
        auth = {
            "grant_type": self.grant_type,
            "username": self.username,
            "api_key": self.api_key,
            "client_id": self.client_id,
            "client_secret": self.client_secret
        }

        r = requests.post(auth_url, params=auth)
        res = json.loads(r.text)

        self.auth_token = res['access_token']
        self.project_id = 30000000022
        self.project_ids = {"Тинькофф": 30000000022, "Сбербанк": 30000000024, "ВТБ24": 30000000023, "МТС": 30000000025, "ТЕСТ": 20000000574}

    def re_auth(self):
        self.grant_type = "password"
        self.username = "zakaz@rt-voice.com"
        self.api_key = "ed122f83c633b8f7bc799577c55e6d31e6fb37d1e96ef8826668b23abac94926"
        self.client_id = "29055bf486467ffb99159edf3c21881d8ec4349ee1eb61c0b172364bbcc623b7"
        self.client_secret = "172f48c27f7eb1c2322526b8f92d5b25dcc9cbc8785f137a428795b3f4a4cb2a"

        auth_url = "https://app.skorozvon.ru/oauth/token"
        auth = {
            "grant_type": self.grant_type,
            "username": self.username,
            "api_key": self.api_key,
            "client_id": self.client_id,
            "client_secret": self.client_secret
        }

        r = requests.post(auth_url, params=auth)
        res = json.loads(r.text)

        self.auth_token = res['access_token']
        self.project_id = 30000000022
        self.project_ids = {"Тинькофф": 30000000022, "Сбербанк": 30000000024, "ВТБ24": 30000000023, "МТС": 30000000025,
                            "ТЕСТ": 20000000574}

    def set_odp_state(self, contact):

        test = open("del_number.txt", "a")

        headers = {'Authorization': 'Bearer ' + self.auth_token}

        url = "https://app.skorozvon.ru/api/v2/leads/{}".format(contact["ExtID"])

        test.write(url + "\n" + str(headers) + "\n")

        r = requests.delete(url, headers=headers)

        test.write(r.text + "\n\n")

        return r.text

    def remove_org(self, id):

        test = open("del_number.txt", "a")

        headers = {'Authorization': 'Bearer ' + self.auth_token}

        url = "https://app.skorozvon.ru/api/v2/leads/{}".format(id)

        test.write(url + "\n" + str(headers) + "\n")

        r = requests.delete(url, headers=headers)

        test.write(r.text + "\n\n")

        return r.text

    def get_org_data(self, id):
        url = "https://app.skorozvon.ru/api/v2/leads/{}".format(id)

        headers = {'Authorization': 'Bearer ' + self.auth_token}

        r = requests.get(url, headers=headers)
        print(r.text)

    def send_data(self, org_list):

        #org_data = []
        #for org in org_list:
        #    org_data.append({
        #        "id": org["id"],
        #        "name": org["name"],
        #        #"post": org["post"],
        #        "phones": [org["phone"]],
        #        #"address": org["address"],
        #        #"external_id": org["external_id"],
        #        "business": org["business"]
        #    })

        url = "https://app.skorozvon.ru/api/v2/leads/import"

        headers = {'Authorization': 'Bearer ' + self.auth_token}

        data = {
            "call_project_id": 20000003128,
            "data": org_list
        }

        r = requests.post(url, json=data, headers=headers)
        print(r.text)

    def append_to_project(self, id, project):
        request = json.loads(
            """{"limit":"100","page":0,"ids":[20003881696],"all":false,"order":{"column":"name","direction":0},"project_id":20000000574,"excluded_ids":[],"filter":{"tags_condition":"or","managers":"all","scenario_id":"","call_result":"","business":"","firm_name":"","file_id":"","city":"","tags":"all","used_by":"user","term":"СУШАНЛО","project_id":20000000574,"result_by":"client","tags_exclusion":null},"authenticity_token":"f4zWKL3f1NnbuNLuOZby9txkj876dmIPe+Q4NxgUI1c=","utf8":"✓"}""")
        url = "https://shard2-lb1.skorozvon.ru/settings/contacts"

        request["ids"] = [id]
        request["project_id"] = project

        headers = {'Authorization': 'Bearer ' + self.auth_token}

        r = requests.put(url, json=request, headers=headers)
        print(r.text)

    def mass_append_to_project(self, ids, project):
        request = json.loads(
            """{"limit":"100","page":0,"ids":[20003881696],"all":false,"order":{"column":"name","direction":0},"project_id":20000000574,"excluded_ids":[],"filter":{"tags_condition":"or","managers":"all","scenario_id":"","call_result":"","business":"","firm_name":"","file_id":"","city":"","tags":"all","used_by":"user","term":"СУШАНЛО","project_id":20000000574,"result_by":"client","tags_exclusion":null},"authenticity_token":"f4zWKL3f1NnbuNLuOZby9txkj876dmIPe+Q4NxgUI1c=","utf8":"✓"}""")
        url = "https://shard2-lb1.skorozvon.ru/settings/contacts"

        request["ids"] = ids
        request["project_id"] = project

        headers = {'Authorization': 'Bearer ' + self.auth_token}

        r = requests.put(url, json=request, headers=headers)
        print(r.text)

    def is_contacts_in_project(self, project, downloads):

        url = "https://shard2-lb1.skorozvon.ru/bootstrap"

        headers = {'Authorization': 'Bearer ' + self.auth_token}

        r = requests.get(url, headers=headers)
        res = json.loads(r.text)

        tag_ids = []

        for tag in res["tags"]:

            for download in downloads:
                if tag["title"] == "Загрузка {}".format(download):
                    tag_ids.append(tag["id"])

        for tag_id in tag_ids:
            url = "https://shard2-lb1.skorozvon.ru/settings/contacts?limit=30&page=0&order%5Bcolumn%5D=name&order%5Bdirection%5D=1&filter%5Btags_condition%5D=or&filter%5Bmanagers%5D=all&filter%5Bscenario_id%5D=&filter%5Bcall_result%5D=&filter%5Bbusiness%5D=&filter%5Bfirm_name%5D=&filter%5Bfile_id%5D=&filter%5Bcity%5D=&filter%5Btags%5D%5B%5D={}&filter%5Bused_by%5D=user&filter%5Bproject_id%5D={}&filter%5Bresult_by%5D=client&filter%5Btags_exclusion%5D=&filter%5Bevent_type%5D="
            url = url.format(tag_id, project)

            r = requests.get(url, headers=headers)
            res = json.loads(r.text)

            if len( res["data"]["leads"] ) > 0:
                return True

        return False

        print(r.text)

    def get_last_status(self, id):

        res_dict = {
            20000014496: "Больше не звоним",
            20000014502: "Занято (системный)",
            20000014499: "Лид",
            20000014495: "Не взяли трубку",
            20000014511: "Неопознанная ошибка (системный)",
            20000014506: "Несуществующий номер",
            20000014505: "Нет ответа (системный)",
            20000017464: "Номер из черного списка (системный)",
            20000014501: "Обнаружен автоответчик (системный)",
            20000014510: "Оператор занят (системный)",
            20000014507: "Оператор не принял вызов (системный)",
            20000014498: "Отказ",
            20000014508: "Отклонен оператором (системный)",
            20000014509: "Отклонен (системный)",
            20000014513: "Отложен (системный)",
            20000014494: "Перезвонить",
            20000014514: "Повторный вызов (системный)",
            20000014500: "Потерян (cистемный)",
            20000014512: "Системная ошибка (системный)",
            20000014503: "Соединен (системный)",
            20000014504: "Сообщение не проиграно (системный)",
            20000014497: "Уже звонили"
        }

        url = "https://app.skorozvon.ru/api/v2/leads/{}/results".format(id)

        headers = {'Authorization': 'Bearer ' + self.auth_token}

        r = requests.get(url, headers=headers)

        res = json.loads(r.text)

        if len(res["data"]) == 0:
            return None

        last_call = res["data"][0]

        url = "https://app.skorozvon.ru/api/v2/scenarios/{}/results".format(last_call["scenario_id"])

        headers = {'Authorization': 'Bearer ' + self.auth_token}

        r = requests.get(url, headers=headers)

        res = json.loads(r.text)

        status = None

        for scen_res in res["data"]:

            if scen_res["id"] == last_call["result_id"]:
                status = scen_res["name"]
                break

        return status

    def get_ext_org_list_impl(self, import_id, page):
        url = "https://app.skorozvon.ru/api/v2/leads?stored_file_id={}&page={}".format(import_id, page)

        headers = {'Authorization': 'Bearer ' + self.auth_token}

        r = requests.get(url, headers=headers)
        res = json.loads(r.text)

        return res

    def get_ext_org_list(self, import_id):

        result = []
        page = 1
        is_complete = False

        while not is_complete:
            try:
                qres = self.get_ext_org_list_impl(import_id, page)

                result.extend(qres["data"])

                if qres["pagination"]["total"] > len(result):
                    page += 1
                else:
                    is_complete = True
            except:
                pass

        return result

    def is_import_complete(self, import_id):
        url = "https://app.skorozvon.ru/api/v2/leads/import/{}".format(import_id)

        headers = {'Authorization': 'Bearer ' + self.auth_token}

        r = requests.get(url, headers=headers)

        print(r.text)
        res = json.loads(r.text)

        return res["state"] == 'loaded'

    def mass_load(self, upload_list, project_id):
        url = "https://app.skorozvon.ru/api/v2/leads/import"

        headers = {'Authorization': 'Bearer ' + self.auth_token}

        r = requests.post(url, json={"call_project_id": project_id, "data": upload_list}, headers=headers)

        print(r.text)
        res = json.loads(r.text)

        return res["id"]

    def send_org(self, org, id, pl=None):

        if pl is not None:
            pl.start_operation("Загрузка данных")

        org_data = {
            "name": org["name"],
            "post": org["post"],
            "phones": [org["phone"]],
            "address": org["address"],
            "external_id": org["external_id"],
            "city": org["business"],
            "tags": [
                "Загрузка " + str(id)
            ]
        }

        url = "https://app.skorozvon.ru/api/v2/leads"

        headers = {'Authorization': 'Bearer ' + self.auth_token}

        r = requests.post(url, json=org_data, headers=headers)
        print(r.text)
        res = json.loads(r.text)

        org_id = res["id"]

        if pl is not None:
            pl.end_operation()

        #pl.start_operation("Добавление в проект")

        #self.append_to_project(org_id, self.project_ids[project])

        #pl.end_operation()

        return org_id

    def get_scenarios_info(self):

        url = "https://app.skorozvon.ru/api/v2/scenarios?page=3"
        headers = {'Authorization': 'Bearer ' + self.auth_token}

        r = requests.get(url, headers=headers)

        return json.loads(r.text)

    def get_scenarios_res(self, scenario_id):

        url = "https://app.skorozvon.ru/api/v2/scenarios/{}/results".format(scenario_id)
        headers = {'Authorization': 'Bearer ' + self.auth_token}

        r = requests.get(url, headers=headers)

        return json.loads(r.text)

    def get_contractor_info(self, ext_id):

        headers = {'Authorization': 'Bearer ' + self.auth_token}
        url = "https://shard3-lb1.skorozvon.ru/leads/{}".format(ext_id)

        r = requests.get(url, headers=headers)

        return json.loads(r.text)

    def get_list(self, start_date, end_date, results_ids, scenario_ids):
        start_date = start_date[6:] + "-" + start_date[3:5] + "-" + start_date[:2] + "T00:00:00.000+02:00"

        end_date = datetime.strptime(end_date, "%d.%m.%Y")
        #end_date = end_date - timedelta(days=1)
        end_date = end_date.strftime("%Y-%m-%d")
        end_date = end_date + "T23:59:59.999+02:00"

        url = "https://shard3-lb1.skorozvon.ru/reports.json"

        headers = {'Authorization': 'Bearer ' + self.auth_token}

        res = []

        for scenario_id in scenario_ids:
            is_complete = False
            offset = 0
            limit = 100

            while not is_complete:
                data = {"order": {
                            "column": "start",
                            "direction": "1"
                        },
                        "name": "calls_results",
                        "filter": {
                            "range": [start_date, end_date],
                            "results_ids": results_ids,
                            "scenario_id": scenario_id
                        },
                        "settings": {
                            "filter": {
                                "range": [start_date, end_date]}
                        },
                        "limit": limit,
                        "page": offset}

                r = requests.post(url, json=data, headers=headers)
                res_info = json.loads(r.text)

                res += res_info['reports'][0]['data']['calls']

                if len(res_info['reports'][0]['data']['calls']) < limit:
                    is_complete = True
                else:
                    offset += 1

        return res

    def get_project_source(self, projct_id, all_data=False):

        headers = {'Authorization': 'Bearer ' + self.auth_token}

        LIMIT = 100
        page = 0

        url = "https://shard3-lb1.skorozvon.ru/settings/contacts?limit={}&\
page={}&\
order%5Bcolumn%5D=name&\
order%5Bdirection%5D=1&\
filter%5Btags_condition%5D=or&\
filter%5Bmanagers%5D=all&\
filter%5Bscenario_id%5D=&\
filter%5Bcall_result%5D=&\
filter%5Bbusiness%5D=&\
filter%5Bfirm_name%5D=&\
filter%5Bfile_id%5D=&\
filter%5Bcity%5D=&\
filter%5Btags%5D=all&\
filter%5Bused_by%5D=all&\
filter%5Bproject_id%5D={}&\
filter%5Bproject_state%5D=incomplete&\
filter%5Bresult_by%5D=scenario&\
filter%5Btags_exclusion%5D=&\
filter%5Bevent_type%5D="

        r = requests.get(url.format(LIMIT, page, projct_id), headers=headers)

        if not all_data:
            return json.loads(r.text)["data"]

        res = json.loads(r.text)["data"]

        while len(res["leads"]) == (page + 1) * LIMIT:
            page += 1

            r = requests.get(url.format(LIMIT, page, projct_id), headers=headers)

            res["leads"].extend(json.loads(r.text)["data"]["leads"])

        return res

    def get_status_by_project(self, scenario_id):

        headers = {'Authorization': 'Bearer ' + self.auth_token}
        url = "https://shard3-lb1.skorozvon.ru/reports.json"

        range = "today"
        #range = "yesterday"
        limit = 100
        page = 0

        is_not_full = True
        res = []

        while is_not_full:
            query = {"name": "calls_results",
                     "filter": {
                         "range": "today",
                         "results_ids": "all",
                         "scenario_id": scenario_id,
                         "users_ids": "all",
                         "tags_ids": "all",
                         "result_by": "client"
                     },
                     "settings": {"favorites": [], "filter": {"range": range}},
                     "limit": limit,
                     "order": {
                         "column": "start",
                         "direction": 1
                     },
                     "page": page,
                     "utf8": "✓"
            }

            r = requests.post(url, json=query, headers=headers)

            sub_res = json.loads(r.text)["reports"][0]["data"]["calls"]

            if len(sub_res) < limit:
                is_not_full = False

            page += 1
            res += sub_res

        return res

    def add_tag(self, tag_name):

        headers = {'Authorization': 'Bearer ' + self.auth_token}
        url = "https://shard3-lb1.skorozvon.ru/settings/tags"
        data = {"title": tag_name, "utf8": "✓"}

        raw_tag_info = requests.post(url, json=data, headers=headers)

        tag_info = json.loads(raw_tag_info.text)

        return tag_info["data"]["id"]

    def add_contact_to_project(self, tag_id, project_id):
        headers = {'Authorization': 'Bearer ' + self.auth_token}
        url = "https://shard3-lb1.skorozvon.ru/settings/contacts"

        data = {
            "limit": "30",
            "page": 0,
            "ids": [],
            "all": True,
            "order": {
                "column": "name",
                "direction": 1},
            "project_id": project_id,
            "excluded_ids": [],
            "project_mode": "save_and_transfer",
            "edited_project_id": "",
            "slices": [],
            "filter": {
                "tags_condition": "or",
                "managers": "all",
                "scenario_id": "",
                "call_result": "",
                "business": "",
                "firm_name": "",
                "file_id": "",
                "city": "",
                "tags": [tag_id],
                "used_by": "all",
                "project_id": "",
                "result_by": "scenario",
                "tags_exclusion": None,
                "event_type": None,
                "time_range": []
            },
            "utf8": "✓"}

        return requests.put(url, json=data, headers=headers)

    def add_tag_to_contacts(self, tag_id, project_id, scenario_id, result_list, date, project_state=None):

        if date:
            date = date.strftime("%d.%m.%Y")

        headers = {'Authorization': 'Bearer ' + self.auth_token}
        url = "https://shard3-lb1.skorozvon.ru/settings/contacts"

        data = {
            "limit": "30",
            "page": 0,
            "ids": [],
            "all": True,
            "tags": [tag_id],
            "order": {
                "column": "name",
                "direction": 1
            },
            "excluded_ids": [],
            "slices": [],
            "filter": {
                "tags_condition": "or",
                "managers": "all",
                "scenario_id": "",
                "call_result": "",
                "business": "",
                "firm_name": "",
                "file_id": "",
                "city": "",
                "tags": "all",
                "scenario": scenario_id,
                "results": result_list,
                "latest_call_start": date,
                "latest_call_end": date,
                "lead_create_start": date,
                "lead_create_end": date,
                "used_by": "all",
                "project_id": project_id,
                "project_state": project_state,
                "result_by": "scenario",
                "tags_exclusion": None,
                "event_type": None,
                "time_range": []
            },
            "utf8": "✓"}

        return requests.put(url, json=data, headers=headers)

    def delete_contacts_for_project(self, project_id, scenario_id, tag_id):

        headers = {'Authorization': 'Bearer ' + self.auth_token,
                   "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"}

        url = "https://shard3-lb1.skorozvon.ru/settings/contacts"
        data = "all=true&\
filter%5Btags_condition%5D=or&\
filter%5Bmanagers%5D=all&\
filter%5Bscenario_id%5D=&\
filter%5Bcall_result%5D=&\
filter%5Bbusiness%5D=&\
filter%5Bfirm_name%5D=&\
filter%5Bfile_id%5D=&\
filter%5Bcity%5D=&\
filter%5Btags%5D%5B%5D={}&\
filter%5Bscenario%5D={}&\
filter%5Bresults%5D=all&\
filter%5Bused_by%5D=all&\
filter%5Bproject_id%5D={}&\
filter%5Bresult_by%5D=scenario&\
filter%5Btags_exclusion%5D=&\
filter%5Bevent_type%5D=&\
limit=30&\
page=0&\
project_mode=remove_from_project&\
edited_project_id={}".format(tag_id, scenario_id, project_id, project_id)

        return requests.delete(url, data=data, headers=headers)

    def get_project_state(self, project_id):

        headers = {'Authorization': 'Bearer ' + self.auth_token}
        url = "https://shard3-lb2.skorozvon.ru/call_projects/{}".format(project_id)

        raw_project_state = requests.get(url, headers=headers)

        return json.loads(raw_project_state.text)


def check_project_count(params, set_res):

    try:
        client = Skorozvon()

        project = Project.get_by_name(params["ProjectName"])
        project_params = project.params()

        try:
            info = client.get_project_source(project_params["Скорозвон"]["project_id"])
        except:
            return {"sleep": 1, "params": params}

        set_res({"Количество": info["total"]})
    except:
        return {"sleep": 1, "params": params}

    return {"sleep": int(params["Periodicity"])*60, "params": params}


def re_add_contacts(params, set_res):

    # проверим, что сегодня не праздничный день и не воскресенье
    ru_holidays = holidays.CountryHoliday('RU')
    ru_holidays.append(["2020-02-24", "2020-03-09", "2020-05-04", "2020-05-05", "2020-05-11", "2020-03-09"])

    if datetime.now() in ru_holidays or datetime.now().weekday() == 6:
        set_res({"Не рабочее время": "Выходной или праздник"})
        # откладываем до следующего дня
        suspend = datetime.now() + timedelta(days=1)

        suspend = datetime.strptime("{} {}".format(suspend.strftime("%Y-%m-%d"), params["StartTime"]),
                                    "%Y-%m-%d %H:%M:%S")
        #suspend = suspend.replace(hour=5, minute=0, second=0)

        return {"suspend": suspend}

    # Берем вчерашнюю дату
    date = datetime.now() - timedelta(days=1)

    is_work_date = False

    while not is_work_date:
        if date in ru_holidays or date.weekday() == 6 or date.weekday() == 5:
            date = date - timedelta(days=1)
            continue

        is_work_date = True

    project = Project.get_by_name(params["ProjectName"])
    project_params = project.params()

    project_id = project_params["Скорозвон"]["project_id"]
    scenario_id = project_params["Скорозвон"]["scenario_id"]
    result_list = []

    result_name_list = params["ResultName"].split(",")

    for result_name in result_name_list:
        is_find = False
        result_name = result_name.strip()

        for result in project_params["Скорозвон"]["results"]:
            if project_params["Скорозвон"]["results"][result]["name"] == result_name:
                result_list.append(result)
                is_find = True

        if not is_find:
            set_res({"Ошибка": "В проекте не найден результат с названием '{}'".format(result_name)})
            return

    client = None

    for i in range(3):
        try:
            client = Skorozvon()
        except:
            pass

        if client:
            break

    if not client:
        set_res({"Ошибка": "Не удалось создать подключение к скорозвону"})
        return {"sleep": 120}

    project_state = client.get_project_state(project_id)

    if project_state["data"]["dialing_state"] == "active":
        set_res({"Состояние": "Проект скорозвона активен"})
        controller = DBController()
        Task.set_task_impl(controller, "Оповещение проблем",
                           json.dumps({"message": "Проект скорозвона {} активен, перенос невозможен".format(params["ProjectName"])}))
        suspend = datetime.now() + timedelta(days=1)
        suspend = datetime.strptime("{} {}".format(suspend.strftime("%Y-%m-%d"), params["StartTime"]),
                                    "%Y-%m-%d %H:%M:%S")
        return {"suspend": suspend}

    unprocessed_count_before = project_state["data"]["cases_count"] - project_state["data"]["completed_cases_count"] - project_state["data"]["failed_cases_count"]

    res = {
        "Не обработанных контактов до начала процесса": unprocessed_count_before,
        "Операция": ""
    }

    res["Операция"] = "Пометка остатков тегом"
    set_res(res)

    # Создаем тег для оставщихся в проекте документноа
    tag_name = "{} {} остатки".format(params["ProjectName"], datetime.now().strftime("%d.%m.%Y"))
    tag_id = client.add_tag(tag_name)

    client.add_tag_to_contacts(tag_id, project_id, None, None, None, "incomplete")
    time.sleep(30)

    res["Операция"] = "Удаление остатков из проекта"
    set_res(res)

    client.delete_contacts_for_project(project_id, scenario_id, tag_id)
    time.sleep(30)

    old_tag_id = tag_id

    res["Операция"] = "Создание тега"
    set_res(res)

    # Создаем тег
    tag_name = "{} {} недозвоны".format(params["ProjectName"], datetime.now().strftime("%d.%m.%Y"))
    #tag_name = "Тест переноса5"
    tag_id = client.add_tag(tag_name)

    res["Операция"] = "Добавление тега на контакты"
    set_res(res)

    # Добавляем тег на контакты
    #for i in range(3):
    client.add_tag_to_contacts(tag_id, project_id, scenario_id, result_list, date)
    time.sleep(30)

    res["Операция"] = "Удаление контактов из проекта"
    set_res(res)

    # Удаляем контакты из проекта
    #for i in range(3):
    client.delete_contacts_for_project(project_id, scenario_id, tag_id)
    time.sleep(30)

    res["Операция"] = "Добавление контактов в проект"
    set_res(res)

    #Добавляем контакты в проект

    #for i in range(3):
    add_raw_res = client.add_contact_to_project(tag_id, project_id)
    time.sleep(30)

    client.add_contact_to_project(old_tag_id, project_id)
    time.sleep(30)

    add_res = json.loads(add_raw_res.text)

    res["Перенесено контактов"] = add_res["data"]["total"]
    res["Операция"] = "Завершено"
    set_res(res)

    # откладываем до следующего дня
    suspend = datetime.now() + timedelta(days=1)
    suspend = datetime.strptime("{} {}".format(suspend.strftime("%Y-%m-%d"), params["StartTime"]),
                                "%Y-%m-%d %H:%M:%S")
    return {"suspend": suspend}


if __name__ == "__main__":
    #client = Skorozvon()

    #client.set_odp_state({"ExtID": 30126168516})

    #def test(a):
    #    return a

    #re_add_contacts({"ProjectName": "Новореги Сбербанк", "ResultName": "Перезвонить"}, test)

    client = Skorozvon()

    info = client.get_scenarios_info()
    info = client.get_scenarios_res(30000004804)

    i = 0

