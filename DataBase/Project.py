from DataBase.DBController import DBController
from DataBase.Region import Region
from Bank.BankList import BankList
import json


class Project:

    @staticmethod
    def get_by_id(id):
        controller = DBController()
        cur = controller.get_cursor()

        sql = """SELECT * FROM Project WHERE ID = {};""".format(id)

        cur.execute(sql)

        project_rec_list = cur.fetchall()

        if len(project_rec_list) == 0:
            return None

        return Project(project_rec_list[0], controller)

    @staticmethod
    def get_by_name(name):
        controller = DBController()
        cur = controller.get_cursor()

        sql = """SELECT * FROM Project WHERE Name = '{}';""".format(name)

        cur.execute(sql)

        project_rec_list = cur.fetchall()

        if len(project_rec_list) == 0:
            return None

        return Project(project_rec_list[0], controller)

    @staticmethod
    def list_in_work():
        return Project.list(in_work=True)

    @staticmethod
    def list(in_work=False):
        controller = DBController()
        cur = controller.get_cursor()

        sql = """SELECT * FROM Project """

        if in_work:
            sql += "WHERE State = 'Включен'"

        sql += ";"

        cur.execute(sql)

        project_rec_list = cur.fetchall()

        res = []

        for project_rec in project_rec_list:
            res.append(Project(project_rec, controller))

        return res

    def __init__(self, rec, controller=None, bank=None):
        self.rec = rec
        self._bank = None

        if controller is None:
            self.controller = DBController()
        else:
            self.controller = controller

        if bank is not None:
            self._bank = bank

        cur = self.controller.get_cursor()

        cur.execute("""SELECT Region.* 
                       FROM Region 
                       INNER JOIN Project_Region ON Project_Region.Region = Region.ID
                       WHERE Project_Region.Project = %s;""", (self.rec["ID"],))

        region_list = cur.fetchall()
        self.regions = []

        for region_rec in region_list:
            self.regions.append(Region(region_rec))

    def id(self):
        return self.rec["ID"]

    def name(self):
        return self.rec["Name"]

    def state(self):
        return self.rec["State"]

    def bank(self):

        if self._bank:
            return self._bank

        cur = self.controller.get_cursor()

        cur.execute("""SELECT * FROM Bank WHERE ID = {}""".format(self.rec["Bank"]))

        bank_rec_list = cur.fetchall()

        if len(bank_rec_list) == 0:
            return None

        self._bank = BankList.get_by_name(bank_rec_list[0]["Name"])

        return self._bank

    def is_allow_uncorrect_address(self):
        return True

    def is_region_allow(self, address, inn=None):

        address = Region.normalize_address(address)

        if address is not None and address != "":
            if not Region.is_adress_correct(address):
                if self.is_allow_uncorrect_address():
                    return True
                else:
                    return False

            for region in self.regions:
                if region.address_in_region(address):
                    return True

            return False

        if inn is not None:

            region_id = inn[:2]

            for region in self.regions:
                if region.get_number() == region_id:
                    return True

        return False

    def params(self):
        try:
            return json.loads(self.rec["Params"])
        except:
            pass

        return None
