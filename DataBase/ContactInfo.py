import re

from DataBase.DBController import DBController


class ContactInfo:

    @staticmethod
    def normalize_phone(phone_original):

        phone = phone_original

        if not phone:
            return phone_original

        phone = re.sub(r'[^0-9]+', r'', phone)

        if phone[0] == "8" or phone[0] == "7":
            phone = "+7" + phone[1:]

        if phone[0] == "9":
            phone = "+7" + phone

        if len(phone) == 12:
            return phone

        return phone_original

    @staticmethod
    def phone_is_invalid(phone):

        if not phone:
            return True

        if phone == 'None':
            return True

        phone = re.sub(r'[^0-9]+', r'', phone)

        if not phone:
            return True

        if phone[0] == "8" or phone[0] == "7":
            phone = "+7" + phone[1:]

        if phone[0] == "9":
            phone = "+7" + phone

        if len(phone) == 12:
            return False

        return True

    @staticmethod
    def phone_is_notsot(phone):

        if not phone:
            return True

        if phone == 'None':
            return True

        phone = re.sub(r'[^0-9]+', r'', phone)

        if not phone:
            return True

        if phone[0] == "8" or phone[0] == "7":
            phone = "+7" + phone[1:]

        if phone[0] == "9":
            phone = "+7" + phone

        if len(phone) == 12 and phone[:3] == "+79":
            return False

        return True

    @staticmethod
    def create(controller, contact_person, type, value):
        cur = controller.get_cursor()

        cur.execute("INSERT INTO ContactInfo SET ContactPerson = {}, Type = '{}', Value = '{}'".
                    format(contact_person.id(), type, value))

        sql = """SELECT * FROM ContactInfo WHERE ID=%s"""
        cur.execute(sql, (cur.lastrowid,))
        res = cur.fetchall()

        if len(res) == 0:
            return None

        return ContactInfo(res[0], controller)


    @staticmethod
    def find(controller, contact_person, type, value):
        cur = controller.get_cursor()

        cur.execute("""SELECT ID FROM ContactInfo 
                       WHERE ContactPerson = '{}' AND Type = '{}' AND Value = '{}';""".
                    format(contact_person.id(), type, value))
        res = cur.fetchall()

        if len(res) == 0:
            return None

        return ContactInfo(res[0], controller)

    @staticmethod
    def get_by_id(controller, id):
        cur = controller.get_cursor()
        cur.execute("""SELECT * FROM ContactInfo WHERE ID = {};""".format(id))

        res = cur.fetchall()

        if len(res) == 0:
            return None

        return ContactInfo(res[0], controller)

    def __init__(self, rec, controller=None):
        self.rec = rec

        if controller is None:
            self.controller = DBController()
        else:
            self.controller = controller

    def id(self):
        return self.rec["ID"]

    def value(self):
        return self.rec["Value"]
