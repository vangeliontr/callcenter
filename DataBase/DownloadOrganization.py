import json


class DownloadOrganization:

    def __init__(self, controller, download, organization):

        self.rec = {
            "Download": download.id(),
            "Organization": organization.id(),
            "Operation": "",
            "State": {}
        }

        self.controller = controller
        self.download = download
        self.organization = organization

    def set_res(self, name, value):

        self.rec["State"][name] = value

    def get_res(self, name):
        return self.rec["State"][name]

    def set_operation(self, operation):
        self.rec["Operation"] = operation

    def save(self):

        sql = """INSERT INTO Download_Organization 
                 SET Organization = '{}', Download = '{}', 
                     State = '{}', Operation = '{}';""".format(self.rec["Organization"],
                                                               self.rec["Download"],
                                                               json.dumps(self.rec["State"]),
                                                               self.rec["Operation"])

        cur = self.controller.get_cursor()

        cur.execute(sql)
