from DataBase.DBController import DBController


class Region:

    @staticmethod
    def get_by_number(controller, number):

        cur = controller.get_cursor()

        cur.execute("SELECT * FROM Region Where Number = {}".format(number))

        region_list = cur.fetchall()

        if len(region_list) == 0:
            return None

        return Region(region_list[0])

    @staticmethod
    def get_by_address(controller, address):

        cur = controller.get_cursor()

        try:
            index_prefix = int(address[0:3])
        except ValueError:
            return None

        cur.execute("""SELECT * FROM Region Where StartIndexPrefix <= {} AND
                           EndIndexPrefix >= {};""".format(index_prefix, index_prefix))

        region_list = cur.fetchall()

        if len(region_list) == 0:
            return None

        return Region(region_list[0])

    @staticmethod
    def detect_region(controller, address, inn=None, ogrn=None):

        if ogrn is not None:
            try:
                region_id = ogrn[3:5]

                region = Region.get_by_number(controller, region_id)
                region.set_source("ОГРН")

                if region is not None:
                    return region
            except:
                pass

        try:
            address = Region.normalize_address(address)

            if address is not None and address != "":
                region = Region.get_by_address(controller, address)

                if region is not None:
                    region.set_source("Адрес")
                    return region
        except:
            pass

        try:
            if inn is not None:

                region_id = inn[:2]

                region = Region.get_by_number(controller, region_id)

                if region is not None:
                    region.set_source("ИНН")
                    return region
        except:
            pass

        return None

    @staticmethod
    def is_adress_correct(address):
        try:
            index_prefix = int(address[0:3])
        except ValueError:
            return False

        return True

    @staticmethod
    def normalize_address(address):

        if address is None:
            return None

        if address[0] == ",":
            address = address[1:]

        if address[0] == " ":
            address = address[1:]

        return address

    def __init__(self, rec):
        self.rec = rec
        self.source = ""

    def id(self):
        return self.rec["ID"]

    def set_source(self, source):
        self.source = source

    def source(self):
        return self.source

    def get_number(self):
        return self.rec["Number"]

    def get_name(self):
        return self.rec["Name"]

    def address_in_region(self, address):

        try:
            index_prefix = int(address[0:3])
        except ValueError:
            return False

        try:
            if self.rec["StartIndexPrefix"] <= index_prefix <= self.rec["EndIndexPrefix"]:
                return True
        except:
            return False

        return False

    @staticmethod
    def find_region_by_address(address):
        controller = DBController()
        cur = controller.get_cursor()

        cur.execute("""SELECT * FROM Region;""")

        all_regions = cur.fetchall()

        for region_rec in all_regions:
            region = Region(region_rec)

            if region.address_in_region(address):
                return region

        return None

