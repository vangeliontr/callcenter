from DataBase.DBController import DBController
from datetime import datetime, timedelta
import requests
import json
import os


class Task:

    @staticmethod
    def get_stat(controller):

        sql = """SELECT count(*) as cnt, Type, State, Suspend > NOW() as Suspended 
                 FROM Task 
                 GROUP BY Type, State, Suspend > NOW();"""

        cur = controller.get_cursor()

        cur.execute(sql)
        return cur.fetchall()

    @staticmethod
    def get_by_reestr(controller, reestr):
        sql = """SELECT * FROM Task WHERE Reestr = '{}' ORDER BY ID DESC;""".format(reestr)

        cur = controller.get_cursor()

        cur.execute(sql)
        task_rec_list = cur.fetchall()

        res = []

        for task_rec in task_rec_list:
            res.append(Task(task_rec, controller))

        return res

    @staticmethod
    def get_by_type(controller, type, start_date=None, end_date = None):
        sql = """SELECT * FROM Task WHERE Type = '{}' ORDER BY ID DESC;""".format(type)

        if start_date:
            sql = sql.replace("WHERE", "WHERE Suspend > '{}' AND".format(start_date))

        if end_date:
            sql = sql.replace("WHERE", "WHERE Suspend < '{}' AND".format(end_date))

        cur = controller.get_cursor()

        cur.execute(sql)
        task_rec_list = cur.fetchall()

        res = []

        for task_rec in task_rec_list:
            res.append(Task(task_rec, controller))

        return res

    @staticmethod
    def is_in_work(controller, task_id):
        sql = """SELECT State FROM Task WHERE ID = {}""".format(task_id)
        cur = controller.get_cursor()
        cur.execute(sql)

        res_list = cur.fetchall()

        if len(res_list) == 0:
            return None

        return res_list[0]["State"] == 1

    @staticmethod
    def get_list(controller, limit, task_type_filter=None, state_filter=None, is_duration_filter=None):
        cur = controller.get_cursor()

        WHERE = []

        if task_type_filter:
            WHERE.append("Type = '{}'".format(task_type_filter))

        if state_filter:
            WHERE.append("State = {}".format(state_filter))

        if is_duration_filter is not None:
            if is_duration_filter:
                WHERE.append("Suspend > NOW()")
            else:
                WHERE.append("Suspend < NOW()")

        if WHERE:
            cur.execute("SELECT * FROM Task WHERE {} ORDER BY ID DESC LIMIT {};".format(" AND ".join(WHERE), limit))
        else:
            cur.execute("SELECT * FROM Task ORDER BY ID DESC LIMIT {};".format(limit))

        res = []
        task_list = cur.fetchall()

        for task in task_list:
            res.append(Task(task, controller))

        return res

    @staticmethod
    def get_by_id(controller, id):
        cur = controller.get_cursor()
        cur.execute("SELECT * FROM Task WHERE ID = {};".format(id))

        res = cur.fetchall()

        if len(res) == 0:
            return None

        return Task(res[0], controller)

    @staticmethod
    def get_unprocessed_task(controller):
        cur = controller.get_cursor()

        sql = """UPDATE Task SET State = 1, ID = (SELECT @update_id := ID) 
                 WHERE ID = (SELECT * FROM (SELECT ID FROM Task WHERE State = 0 AND 
                 ( Suspend < CURRENT_TIMESTAMP() OR Suspend is NULL ) ORDER BY ID ASC LIMIT 1 FOR UPDATE) as q );"""

        cur.execute("SET @update_id := 0;")
        cur.execute(sql)
        cur.execute("SELECT @update_id;")
        res = cur.fetchall()
        res = res[0]
        controller.save_changes()

        if res["@update_id"] == 0:
            return None

        return Task.get_by_id(controller, res["@update_id"])

    @staticmethod
    def get_unprocessed_task_list(controller):
        cur = controller.get_cursor()

        sql = """SELECT * FROM Task WHERE State = 0 AND 
                 ( Suspend < CURRENT_TIMESTAMP() OR Suspend is NULL ) ORDER BY ID ASC;"""

        cur.execute(sql)
        res = cur.fetchall()

        if len(res) == 0:
            return res

        ids = []

        for task_rec in res:
            ids.append(str(task_rec["ID"]))

        sql = """UPDATE Task SET State = 1 WHERE ID in ({});""".format(", ".join(ids))

        cur.execute(sql )

        return res

    @staticmethod
    def find_by_full_data(controller, type, data, delta_hours=5):
        cur = controller.get_cursor()
        date = datetime.now() - timedelta(hours=delta_hours)
        date = date.strftime("%Y-%m-%d %H:%M:%S")

        sql = """SELECT * FROM Task 
                           WHERE Type = '{}' AND Data = '{}' AND Suspend > '{}'
                           ORDER BY Suspend DESC
                           LIMIT 1;""".format(type, json.dumps(data).encode("unicode-escape").decode(), date)

        cur.execute(sql)

        task_list = cur.fetchall()

        if len(task_list) == 0:
            return None

        task = Task(task_list[0], controller)
        res = task.get_res()

        if "Ошибка" in res:
            return None

        return task_list[0]["ID"]

    @staticmethod
    def find_by_data(controller, type, data, delta_hours=5):
        cur = controller.get_cursor()
        date = datetime.now() - timedelta(hours=delta_hours)
        date = date.strftime("%Y-%m-%d %H:%M:%S")

        cur.execute("""SELECT * FROM Task 
                       WHERE Type = '{}' AND Data like '%{}%' AND Suspend > '{}'
                       ORDER BY Suspend DESC
                       LIMIT 1;""".format(type, data, date))

        task_list = cur.fetchall()

        if len(task_list) == 0:
            return None

        task = Task(task_list[0], controller)
        res = task.get_res()

        if "Ошибка" in res:
            return None

        return task_list[0]["ID"]

    @staticmethod
    def set_task_impl(controller, type, obj, suspend=None):
        cur = controller.get_cursor()

        if suspend is None:
            cur.execute("INSERT INTO Task SET State = 0, Type = %s, Data = %s", (type, obj))
        else:
            cur.execute("INSERT INTO Task SET State = 0, Type = %s, Data = %s, Suspend = %s", (type, obj, suspend))

        task_id = cur.lastrowid
        controller.save_changes()

        return task_id

    @staticmethod
    def set_task(type, obj):

        params_file_name = os.path.dirname(__file__) + "/task.dat"
        str_params = open(params_file_name, "r").read()
        params = json.loads(str_params)

        url = params["url"]

        res = requests.post(url, json={"params": obj, "name": type})
        res = json.loads(res.text)

        return res["task_id"]

    def __init__(self, rec, controller=None):
        self.rec = rec

        if controller is None:
            self.controller = DBController()
        else:
            self.controller = controller

    def id(self):
        return self.rec["ID"]

    def type(self):
        return self.rec["Type"]

    def suspend(self):
        return self.rec["Suspend"]

    def state(self):

        if self.rec["State"] == 0:
            return "Ожидает в очереди"

        if self.rec["State"] == 1:
            return "Выполняется"

        if self.rec["State"] == 2:
            return "Завершена"

        return "Неизвестное состояник"

    def get_params(self):
        return json.loads(self.rec["Data"])

    def get_res(self):

        if self.rec["Result"] is None:
            return {}

        return json.loads(self.rec["Result"])

    def restart(self):
        cur = self.controller.get_cursor()
        cur.execute("UPDATE Task SET State = 0, Suspend = NOW() WHERE ID = {}".format(self.rec["ID"]))
        self.controller.save_changes()

    def stop(self):
        cur = self.controller.get_cursor()
        cur.execute("UPDATE Task SET State = 2 WHERE ID = {}".format(self.rec["ID"]))
        self.controller.save_changes()

    def delay(self, minutes):
        cur = self.controller.get_cursor()
        cur.execute("UPDATE Task SET Suspend = Suspend + INTERVAL {} MINUTE WHERE ID = {}".format(minutes, self.rec["ID"]))
        self.controller.save_changes()

    def set_res(self, res):
        cur = self.controller.get_cursor()
        cur.execute("UPDATE Task SET Result = %s WHERE ID = %s", (json.dumps(res), self.rec["ID"]))
        self.controller.save_changes()

    def set_params(self, params):
        cur = self.controller.get_cursor()
        cur.execute("UPDATE Task SET Data = %s WHERE ID = %s", (json.dumps(params), self.rec["ID"]))
        self.controller.save_changes()

    def set_done(self):
        cur = self.controller.get_cursor()
        cur.execute("UPDATE Task SET State = 2 WHERE ID = {}".format(self.rec["ID"]))
        self.controller.save_changes()

    def set_sleep(self, sleep_sec):
        cur = self.controller.get_cursor()
        cur.execute("UPDATE Task SET Suspend = DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL {} SECOND) WHERE ID = {}".
                    format(sleep_sec, self.rec["ID"]))
        self.controller.save_changes()

    def set_suspend(self, suspend):
        cur = self.controller.get_cursor()
        cur.execute("UPDATE Task SET Suspend = '{}' WHERE ID = {}".
                    format(suspend.strftime("%Y-%m-%d %H:%M:%S"), self.rec["ID"]))
        self.controller.save_changes()

    def set_on_work(self):
        cur = self.controller.get_cursor()
        cur.execute("UPDATE Task SET State = 0 WHERE ID = {}".format(self.rec["ID"]))
        self.controller.save_changes()

    def set_get_state(self):
        cur = self.controller.get_cursor()
        cur.execute("UPDATE Task SET State = 1 WHERE ID = {}".format(self.rec["ID"]), multi=True)
        self.controller.save_changes()


if __name__ == "__main__":
    controller = DBController()
    #task_id = Task.find_by_full_data(contr, "Проверка на ОДП", {"Bank": "ВТБ24", "INN": "790153353218"})
    #pass

    Task.set_task_impl(controller, "testSuspend", "{}",
                       (datetime.now() + timedelta(minutes=30)).strftime("%Y-%m-%d %H:%M:%S"))
