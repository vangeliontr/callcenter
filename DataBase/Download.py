from DataBase.DBController import DBController
from DataBase.Organization import Organization

class DownloadFile:

    def __init__(self, rec, controller=None):
        self.rec = rec

        if controller is None:
            self.controller = DBController()
        else:
            self.controller = controller

    def name(self):
        return self.rec["Name"]

    def id(self):
        return self.rec["ID"]

    def download_id(self):
        return self.rec["Download"]

    def source(self):
        return self.rec["Source"]

    def download(self):
        cur = self.controller.get_cursor()

        cur.execute("""SELECT * FROM Download
                       WHERE ID = {};""".format(self.download_id()))

        download_rec = cur.fetchall()

        if len(download_rec) == 0:
            return None

        return Download(download_rec[0], self.controller)

    def set_state(self, state):
        cur = self.controller.get_cursor()

        sql = "Update DownloadFile SET State=%s WHERE ID = %s;"
        cur.execute(sql, (state, self.rec["ID"]))

        self.controller.save_changes()

class Download:

    @staticmethod
    def get_unprocessed_download_files(controller):
        cur = controller.get_cursor()

        cur.execute("""SELECT DownloadFile.*, Download.RegDate, Download.Source FROM DownloadFile
                       INNER JOIN Download ON Download.ID = DownloadFile.Download
                       WHERE DownloadFile.State in ('Ожидает загрузки');""")

        download_recs = cur.fetchall()
        res = []

        if len(download_recs) == 0:
            return res

        for download_rec in download_recs:
            res.append(DownloadFile(download_rec, controller))

        return res

    @staticmethod
    def find_download(controller, source, receive_date_time):
        cur = controller.get_cursor()

        sql = """SELECT * FROM Download WHERE Source = %s AND ReceiveDateTime = %s;"""
        cur.execute(sql, (source, receive_date_time))

        res = cur.fetchall()

        if len(res) == 0:
            return None

        return Download(res[0], controller)

    @staticmethod
    def get_by_id(controller, id):
        cur = controller.get_cursor()

        sql = """SELECT * FROM Download WHERE ID = %s;"""
        cur.execute(sql, (id,))

        res = cur.fetchall()

        if len(res) == 0:
            return None

        return Download(res[0], controller)

    @staticmethod
    def find_download_by_state(controller, state):
        cur = controller.get_cursor()

        sql = """SELECT ID FROM Download WHERE State = %s;"""
        cur.execute(sql, (state,))

        res = cur.fetchall()

        if len(res) == 0:
            return None

        return res[0]["ID"]

    @staticmethod
    def find_download_by_filename(controller, file_name):
        cur = controller.get_cursor()

        sql = """SELECT Download FROM DownloadFile WHERE Name = %s;"""
        cur.execute(sql, (file_name,))

        res = cur.fetchall()

        if len(res) == 0:
            return None

        sql = """SELECT * FROM Download WHERE ID = %s;"""
        cur.execute(sql, (res[0]["Download"],))

        res = cur.fetchall()

        return Download(res[0], controller)

    @staticmethod
    def create_fake(controller, state):
        cur = controller.get_cursor()

        sql = """INSERT INTO Download (State)
                         VALUES (%s)"""

        cur.execute(sql, (state,))

        download_id = cur.lastrowid

        controller.save_changes()

        return download_id

    @staticmethod
    def create(controller=None, file_list=[], source=None, reg_date=None, receive_date=None,
               type=None, state="Ожидает загрузки", comment=None):

        if controller is None:
            controller = DBController()

        cur = controller.get_cursor()

        sql = """INSERT INTO Download (State, Source, RegDate, ReceiveDateTime, Type, Comment)
                 VALUES (%s, %s, %s, %s, %s, %s)"""

        cur.execute(sql, (state, source, reg_date, receive_date, type, comment))

        download_id = cur.lastrowid

        for file_name in file_list:
            sql = """INSERT INTO DownloadFile (Name, State, Download) VALUES (%s, %s, %s);"""
            cur.execute(sql, (file_name, state, download_id))

        controller.save_changes()

        sql = "SELECT * FROM Download WHERE ID = {}".format(download_id)
        cur.execute(sql)
        download_rec_list = cur.fetchall()

        if len(download_rec_list) == 0:
            return None

        return Download(download_rec_list[0], controller)

    def __init__(self, rec, controller=None):
        self.rec = rec

        if controller is None:
            self.controller = DBController()
        else:
            self.controller = controller

    def id(self):
        return self.rec["ID"]

    def add_organization(self, organization_id, operation, phone_validation, first_valid_number):
        cur = self.controller.get_cursor()

        if first_valid_number:
            first_valid_number = "Да"
        else:
            first_valid_number = None

        sql = """INSERT INTO Download_Organization SET Download = %s, Organization = %s, Operation = %s, 
                                                           PhoneValidate = %s, FirstNumber = %s;"""

        cur.execute(sql, (self.rec["ID"], organization_id, operation, phone_validation, first_valid_number))

        self.controller.save_changes()

    def set_finish(self, duration):
        cur = self.controller.get_cursor()

        cur.execute("Update Download SET State='Успешно загружено', Duration = {} WHERE ID = {};".
                         format(duration, self.rec["ID"]))

        self.controller.save_changes()

    def set_state_full(self, state, comment):
        cur = self.controller.get_cursor()

        sql = "Update Download SET State=%s, Comment = %s WHERE ID = %s;"
        cur.execute(sql, (state, comment, self.rec["ID"]))

        sql = "Update DownloadFile SET State=%s WHERE Download = %s;"
        cur.execute(sql, (state, self.rec["ID"]))

        self.controller.save_changes()

    def set_state(self, state, comment):
        cur = self.controller.get_cursor()

        sql = "Update Download SET State=%s, Comment = %s WHERE ID = %s;"
        cur.execute(sql, (state, comment, self.rec["ID"]))

        self.controller.save_changes()

    def set_error(self, comment):
        self.set_state('Ошибка', comment)

    def get_file_list(self):
        cur = self.controller.get_cursor()

        sql = "SELECT Name FROM DownloadFile WHERE Download = %s;"
        cur.execute(sql, (self.rec["ID"],))

        return cur.fetchall()

    def reg_date(self):
        return self.rec["RegDate"]

    def append_error(self, comment):
        cur = self.controller.get_cursor()

        sql = "INSERT INTO DownloadError (Download, Text) VALUES(%s,%s);"
        cur.execute(sql, (self.rec["ID"], comment))
        self.controller.save_changes()

    def organization_list(self):

        cur = self.controller.get_cursor()

        sql = "SELECT * FROM Organization WHERE Download = %s;"
        cur.execute(sql, (self.rec["ID"],))

        org_list = cur.fetchall()

        res = []

        for org in org_list:
           res.append(Organization(org, self.controller))

        return res
