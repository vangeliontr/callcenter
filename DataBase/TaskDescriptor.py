from DataBase.DBController import DBController
from DataBase.Task import Task
import json

class TaskDescriptor:

    @staticmethod
    def get_list(controller):
        res = []
        cur = controller.get_cursor()

        sql = """SELECT * FROM TaskDescriptor"""

        cur.execute(sql)
        desc_recs = cur.fetchall()

        for desc_rec in desc_recs:
            res.append(TaskDescriptor(desc_rec, controller))

        return res

    @staticmethod
    def get_by_task(controller, task_id):
        cur = controller.get_cursor()

        sql = """SELECT TaskDescriptor.* FROM Task 
                 INNER JOIN TaskDescriptor ON TaskDescriptor.Name = Task.Type
                 WHERE Task.ID = {};""".format(task_id)

        cur.execute(sql)
        res = cur.fetchall()

        if len(res) == 0:
            return None

        return TaskDescriptor(res[0], controller)

    @staticmethod
    def get_by_name(controller, name):
        cur = controller.get_cursor()

        sql = """SELECT * FROM TaskDescriptor
                 WHERE Name = '{}';""".format(name)

        cur.execute(sql)
        res = cur.fetchall()

        if len(res) == 0:
            return None

        return TaskDescriptor(res[0], controller)

    @staticmethod
    def get_by_type(controller, type):
        cur = controller.get_cursor()

        sql = """SELECT * FROM TaskDescriptor
                     WHERE Type = '{}';""".format(type)

        cur.execute(sql)
        rec_list = cur.fetchall()

        res = []

        for rec in rec_list:
            res.append(TaskDescriptor(rec, controller))

        return res

    def __init__(self, rec, controller=None):
        self.rec = rec

        if controller is None:
            self.controller = DBController()
        else:
            self.controller = controller

    def name(self):
        return self.rec["Name"]

    def get_handle_params(self):
        return json.loads(self.rec["Handler"])

    def get_params(self):
        return json.loads(self.rec["Data"])

    def get_task_params(self):
        return json.loads(self.rec["TaskParams"])

    def get_task_list(self):
        cur = self.controller.get_cursor()

        cur.execute("""SELECT * FROM Task WHERE Type = '{}';""".format(self.name()))

        res = []

        task_rec_list = cur.fetchall()

        for task_rec in task_rec_list:
            res.append(Task(task_rec, self.controller))

        return res

    def handle(self, task):
        handle_params = self.get_handle_params()

        module = __import__(handle_params["import_path"])
        params = {}

        params.update(task.get_params())
        params.update(json.loads(self.rec["Data"]))
        params["TaskID"] = task.id()

        res = getattr(getattr(module, handle_params["module"]), handle_params["method"])(params, task.set_res)

        if res is not None:
            if "params" in res:
                task.set_params(res["params"])

            if "sleep" in res:
                task.set_sleep(res["sleep"])

            if "suspend" in res:
                task.set_suspend(res["suspend"])

            return "WORK"
