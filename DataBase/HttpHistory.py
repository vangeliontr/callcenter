
class HttpHistory:

    @staticmethod
    def get_stat(controller, start_date=None, end_date=None, site=None, ip=None, method=None,
                 path=None, user=None, h_stat=None, v_stat=None):

        cur = controller.get_cursor()

        WHERE = []

        if start_date:
            start_date = "{}-{}-{} {}".format(start_date[6:10], start_date[3:5], start_date[0:2], start_date[11:])
            WHERE.append("DateTime > '{}'".format(start_date))

        if end_date:
            end_date = "{}-{}-{} {}".format(end_date[6:10], end_date[3:5], end_date[0:2], end_date[11:])
            WHERE.append("DateTime < '{}'".format(end_date))

        if site:
            WHERE.append("Site = '{}'".format(site))

        if ip:
            WHERE.append("ClientIP = '{}'".format(ip))

        if method:
            WHERE.append("Method = '{}'".format(method))

        if path:
            WHERE.append("URL like '%{}%'".format(path))

        if user:
            WHERE.append("User like '%{}%'".format(user))

        if not WHERE:
            WHERE = ""
        else:
            WHERE = "WHERE {}".format(" AND ".join(WHERE))

        GROUP = []

        if h_stat:
            GROUP.append(h_stat)

        if v_stat:
            GROUP.append(v_stat)

        SELECT = ""

        if GROUP:
            SELECT = ", {}".format(",".join(GROUP))
            GROUP = "GROUP BY {}".format(",".join(GROUP))
        else:
            GROUP = ""

        SQL = """SELECT count(*) as Count{} FROM HTTPLog {} {}""".format(SELECT, WHERE, GROUP)
        cur.execute(SQL)

        return cur.fetchall()

    @staticmethod
    def log_list(controller, limit=100, offset=0, start_date=None, end_date=None, site=None, ip=None, method=None,
                 path=None, user=None):
        cur = controller.get_cursor()

        WHERE = []

        if start_date:
            start_date = "{}-{}-{} {}".format(start_date[6:10], start_date[3:5], start_date[0:2], start_date[11:])
            WHERE.append("DateTime > '{}'".format(start_date))

        if end_date:
            end_date = "{}-{}-{} {}".format(end_date[6:10], end_date[3:5], end_date[0:2], end_date[11:])
            WHERE.append("DateTime < '{}'".format(end_date))

        if site:
            WHERE.append("Site = '{}'".format(site))

        if ip:
            WHERE.append("ClientIP = '{}'".format(ip))

        if method:
            WHERE.append("Method = '{}'".format(method))

        if path:
            WHERE.append("URL like '%{}%'".format(path))

        if user:
            WHERE.append("User like '%{}%'".format(user))

        if not WHERE:
            WHERE = ""
        else:
            WHERE = "WHERE {}".format(" AND ".join(WHERE))

        SQL = """SELECT * FROM HTTPLog {} ORDER BY ID DESC LIMIT %s OFFSET %s""".format(WHERE)
        cur.execute(SQL, (limit, offset*limit))

        return cur.fetchall()

    def __init__(self, controller):
        self.controller = controller

    def add_log_data(self, method, url, client_ip, site, user):
        cur = self.controller.get_cursor()

        SQL = """INSERT INTO HTTPLog SET Method = %s, URL = %s, ClientIP = %s, Site = %s, User = %s;"""
        cur.execute(SQL, (method, url, client_ip, site, user))
