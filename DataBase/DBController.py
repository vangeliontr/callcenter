import mysql.connector
import json
import os


class DBController:

    def __init__(self, params=None):
        self.cnx = None

        params_file_name = os.path.dirname(__file__) + "/connection.dat"

        if params is None:
            while True:
                try:
                    with open(params_file_name, "r") as f:
                        str_params = f.read()
                        break
                except Exception as e:
                    print(e)

            #with open(params_file_name, "r") as f:
            #    str_params = f.read()


            self.params = json.loads(str_params)

        else:
            self.params = params

        self.cnx = mysql.connector.connect(user=self.params["user"], password=self.params["password"],
                                           port=self.params["port"], host=self.params["host"],
                                           database=self.params["database"])

        self.cnx.autocommit = True
        self.cursor = None

    def get_cursor(self):

        try:
            if self.cursor is not None:
                self.cursor.close()
        except:
            pass

        self.cursor = self.cnx.cursor(dictionary=True)
        return self.cursor

    def save_changes(self):
        self.cnx.commit()

    def __del__(self):

        try:
            if self.cursor is not None:
                self.cursor.close()
        except:
            pass

        if self.cnx:
            self.cnx.close()
