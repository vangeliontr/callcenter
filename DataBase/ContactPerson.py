from DataBase.DBController import DBController
from DataBase.ContactInfo import ContactInfo


class ContactPerson:

    @staticmethod
    def find(controller, surname, name, middle_name, birth_date):
        cur = controller.get_cursor()

        if surname is None:
            surname = ""

        if name is None:
            name = ""

        if middle_name is None:
            middle_name = ""

        cur.execute("""SELECT * FROM ContactPerson 
                       WHERE Surname = %s AND Name = %s AND MiddleName = %s AND BirthDate = %s;""",
                    (surname, name, middle_name, birth_date))

        res = cur.fetchall()

        if len(res) == 0:
            return None

        return ContactPerson(res[0], controller)

    @staticmethod
    def get_by_id(controller, id):
        cur = controller.get_cursor()
        cur.execute("""SELECT * FROM ContactPerson WHERE ID = {};""".format(id))

        res = cur.fetchall()

        if len(res) == 0:
            return None

        return ContactPerson(res[0], controller)

    @staticmethod
    def find_with_org(controller, surname, name, middle_name, org_id):
        cur = controller.get_cursor()

        cur.execute("""SELECT ContactPerson.*
                       FROM ContactPerson_Organization
                       INNER JOIN ContactPerson ON ContactPerson_Organization.ContactPerson = ContactPerson.ID
                       WHERE
                       ContactPerson_Organization.Organization = %s AND
                       ContactPerson.Surname = %s AND
                       ContactPerson.Name = %s AND
                       ContactPerson.MiddleName = %s;
                       """, (org_id, surname, name, middle_name))

        res = cur.fetchall()

        if len(res) == 0:
            return None

        return ContactPerson(res[0], controller)

    @staticmethod
    def create(controller, surname, name, middle_name, birth_date, doc_type=None, doc_num=None, inn=None):
        cur = controller.get_cursor()

        if surname is None:
            surname = ""

        if name is None:
            name = ""

        if middle_name is None:
            middle_name = ""

        cur.execute("""INSERT INTO ContactPerson 
                       SET Surname = %s, Name = %s, MiddleName = %s, BirthDate = %s, 
                           DocumentType = %s, DocumentNumber = %s, INN = %s;""",
                    (surname, name, middle_name, birth_date, doc_type, doc_num, inn))

        controller.save_changes()
        cur.execute("""SELECT * FROM ContactPerson WHERE ID = %s;""", (cur.lastrowid,))
        res = cur.fetchall()

        if len(res) == 0:
            return None

        return ContactPerson(res[0], controller)

    def __init__(self, rec, controller=None):
        self.rec = rec

        if controller is None:
            self.controller = DBController()
        else:
            self.controller = controller

    def id(self):
        return self.rec["ID"]

    def name(self):
        return str(self.rec["Name"])

    def surname(self):
        return str(self.rec["Surname"])

    def middlename(self):
        return str(self.rec["MiddleName"])

    def find_contact_info(self, contact_type, value):
        cur = self.controller.get_cursor()

        value = value.replace("'", "")

        cur.execute("SELECT * FROM ContactInfo WHERE ContactPerson = %s AND Type = %s AND Value = %s;",
                    (self.rec["ID"], contact_type, value))

        res = cur.fetchall()

        if len(res) == 0:
            return None

        return ContactInfo(res[0], self.controller)

    def have_valid_contact_info(self, phone_is_invalid):
        cur = self.controller.get_cursor()

        cur.execute("""SELECT * FROM ContactInfo WHERE ContactPerson = {} AND Type = 'Телефон';""".
                    format(self.rec["ID"]))

        res = cur.fetchall()

        if len(res) == 0:
            return False

        for info in res:
            if not phone_is_invalid(info["Value"]):
                return True

        return False

    def get_all_valid_phones(self, phone_is_invalid):
        cur = self.controller.get_cursor()

        cur.execute("""SELECT * FROM ContactInfo WHERE ContactPerson = {} AND Type = 'Телефон';""".
                    format(self.rec["ID"]))

        res = cur.fetchall()

        result = []

        for info in res:
            if not phone_is_invalid(info["Value"]):
                result.append(ContactInfo(info, self.controller))

        return result

    def create_contact_info(self, contact_type, value):
        cur = self.controller.get_cursor()

        value = value.replace("'", "")

        cur.execute("INSERT INTO ContactInfo SET ContactPerson = %s, Type = %s, Value = %s;",
                    (self.rec["ID"], contact_type, value))

        self.controller.save_changes()

        cur.execute("""SELECT * FROM ContactInfo WHERE ID = %s;""", (cur.lastrowid,))
        res = cur.fetchall()

        if len(res) == 0:
            return None

        return ContactInfo(res[0], self.controller)

        return ContactInfo(res[0], self.controller)
