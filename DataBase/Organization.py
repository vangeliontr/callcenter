from datetime import datetime, timedelta

from DataBase.DBController import DBController
from DataBase.ContactPerson import ContactPerson
from DataBase.ContactInfo import ContactInfo
from DataBase.Call import Call

class Organization:

    @staticmethod
    def for_all_organizations(func, controller=None, limit=1000, offset=0, start_id=None):

        if controller is None:
            controller = DBController()

        SQL = """SELECT * FROM Organization {} ORDER BY ID ASC LIMIT {} OFFSET {}"""

        WHERE = ""

        if start_id:
            WHERE = "WHERE ID > {} ".format(start_id)

        org_list = [1]

        while org_list:
            cur = controller.get_cursor()

            cur.execute(SQL.format(WHERE, limit, offset))
            org_list = cur.fetchall()

            for org_rec in org_list:
                func(Organization(org_rec, controller))

            offset += limit

    @staticmethod
    def find_by_inn_open_date(controller, inn, create_date):
        cur = controller.get_cursor()
        sql = """SELECT * FROM Organization WHERE INN = %s ORDER BY ID DESC;"""

        cur.execute(sql, (inn,))
        res = cur.fetchall()

        if len(res) == 0:
            return None

        for org_data in res:

            try:
                delta = datetime.strptime(create_date, "%Y-%m-%d").date() - org_data['OpenDate']

                if timedelta(-30) < delta < timedelta(30):
                    return Organization(res[0], controller)
            except:
                pass

        return None

    @staticmethod
    def get_by_id(controller, id):
        cur = controller.get_cursor()
        sql = """SELECT * FROM Organization WHERE ID = {}""".format(id)
        cur.execute(sql)
        res = cur.fetchall()

        if len(res) == 0:
            return None

        return Organization(res[0], controller)

    @staticmethod
    def create(controller, org_type, name, inn, open_date, address, ogrn, download_id):
        cur = controller.get_cursor()

        sql = """INSERT INTO Organization SET Type = %s, Name = %s, INN = %s, OpenDate = %s, Address = %s, 
                 OGRN = %s, Download = %s;"""

        cur.execute(sql, (org_type, name, inn, open_date, address, ogrn, download_id))

        controller.save_changes()
        org_id = cur.lastrowid

        #sql = """INSERT INTO RegInfo SET OpenDate = %s, Confirm = 0, Organization = %s, OGRN = %s;"""

        #try:
        #    cur.execute(sql, (open_date, org_id, ogrn))
        #except:
        #    pass

        #last_reg_info_id = cur.lastrowid

        sql = """SELECT * FROM Organization WHERE ID=%s"""
        cur.execute(sql, (org_id,))
        res = cur.fetchall()

        if len(res) == 0:
            return None

        #sql = "UPDATE Organization SET LastRegInfo = {} WHERE ID = {};".format(last_reg_info_id, org_id)
        #cur.execute(sql)
        #controller.save_changes()

        return Organization(res[0], controller)

    @staticmethod
    def get_by_call(controller, call):
        cur = controller.get_cursor()
        sql = """SELECT * FROM Organization 
                 INNER JOIN Call_Organization ON Organization.ID = Call_Organization.Organization
                 WHERE Call_Organization.Call_ = {}""".format(call.id())

        cur.execute(sql)
        res = cur.fetchall()

        if len(res) == 0:
            return None

        return Organization(res[0], controller)

    def __init__(self, rec, controller=None):
        self.rec = rec

        if controller is None:
            self.controller = DBController()
        else:
            self.controller = controller

    def is_already_loaded(self, download_id):
        cur = self.controller.get_cursor()

        cur.execute("SELECT * FROM Download_Organization WHERE Download = {} AND Organization = {}".
                    format(download_id, self.rec["ID"]))

        res = cur.fetchall()

        if len(res) > 0:
            return True

        return False

    def id(self):
        return self.rec["ID"]

    def download(self):
        return self.rec["Download"]

    def address(self):
        return self.rec["Address"]

    def name(self):
        return self.rec["Name"]

    def inn(self):
        return self.rec["INN"]

    def open_date(self):
        return self.rec["OpenDate"]

    def ogrn(self):
        return self.rec["OGRN"]

    def egrul_check(self):
        return self.rec["EGRULCheck"]

    def is_closed(self):
        return self.rec["IsClosed"]

    def set_reg_info(self, open_date, ogrn):

        cur = self.controller.get_cursor()

        field_list = []

        field_list.append("Organization = {}".format(self.id()))

        if open_date:
            field_list.append("OpenDate = '{}'".format(open_date))
        else:
            field_list.append("OpenDate = '{}'".format(self.rec["Created"]))

        if ogrn and ogrn.isdigit():
            field_list.append("OGRN = {}".format(ogrn))

        SQL = """SELECT * FROM RegInfo WHERE {}""".format(" AND ".join(field_list))
        cur.execute(SQL)

        exist_reg_info = cur.fetchone()

        if exist_reg_info:
            return exist_reg_info["ID"]

        SQL = """INSERT INTO RegInfo SET {}""".format(", ".join(field_list))
        cur.execute(SQL)
        return cur.lastrowid

    def set_last_reg_info(self, reg_info_id):

        if not self.rec["LastRegInfo"]:
            cur = self.controller.get_cursor()

            SQL = """UPDATE Organization SET LastRegInfo = {} WHERE ID = {}"""

            cur.execute(SQL.format(reg_info_id, self.id()))

    def last_contact_person(self):
        return ContactPerson.get_by_id(self.controller, self.rec["LastContactPerson"])

    def last_contact_info(self):
        return ContactInfo.get_by_id(self.controller, self.rec["LastPhoneInfo"])

    def time_zone(self):

        cur = self.controller.get_cursor()

        cur.execute("""SELECT Region.TimeZone + 3 as TZ 
                       FROM Organization_Region
                       INNER JOIN Region ON Organization_Region.Region = Region.ID
                       WHERE Organization_Region.Organization = {};""".format(self.rec["ID"]))

        res = cur.fetchall()

        if len(res) > 0:
            return res[0]["TZ"]

        return 3

    def last_call_to_bank(self, bank):

        cur = self.controller.get_cursor()

        cur.execute("""SELECT Call_.*
                       FROM Call_Organization
                       INNER JOIN Call_ ON Call_Organization.Call_ = Call_.ID
                       WHERE Call_Organization.Organization = {} AND
                       Call_.Bank = {};""".format(self.rec["ID"], bank.id()))

        cal_list = cur.fetchall()

        if len(cal_list) == 0:
            return None

        return Call(cal_list[0], self.controller)

    def link_organization_to_region(self, region):

        cur = self.controller.get_cursor()

        sql = """INSERT INTO Organization_Region SET Organization = %s, Region = %s, Comment = %s;"""

        cur.execute(sql, (self.id(), region.id(), region.source))

    def link_contact(self, contact_person, phone_contact_info, position='Директор'):

        try:
            cur = self.controller.get_cursor()

            sql = """INSERT INTO ContactPerson_Organization 
                             SET ContactPerson = {}, Organization = {}, Position = '{}';""".format(contact_person.id(),
                                                                                                   self.id(),
                                                                                                   position)

            cur.execute(sql)
        except Exception as e:
            print(str(e))

        try:
            sql = """UPDATE Organization 
                     SET LastContactPerson = %s
                     WHERE ID = %s;"""

            if phone_contact_info:
                sql = sql.replace("SET", "SET LastPhoneInfo = {}, ".format(phone_contact_info.id()))

            cur.execute(sql, (contact_person.id(), self.id()))
        except Exception as e:
            print(str(e))

    def call_list(self):

        cur = self.controller.get_cursor()

        sql = """SELECT Call_.* 
                 FROM Call_Organization
                 INNER JOIN Call_ ON Call_.ID = Call_Organization.Call_
                 WHERE Call_Organization.Organization = {};""".format(self.id())

        cur.execute(sql)
        call_rec_list = cur.fetchall()

        res = []

        for call_rec in call_rec_list:
            res.append(Call(call_rec, self.controller))

        return res
