from DataBase.DBController import DBController


class Call:

    @staticmethod
    def get_last_list(controller, limit=100):
        sql = """SELECT Call_.ID as 'Идентификатор', 
                        Call_.ExtID as 'ИдентификаторСкорозвона', 
                        Call_.Created as 'Поступил', 
                        Call_.Result as 'Результат',
                        Call_.Comment as 'Комментарий',
                        Call_.Bank,
                        Call_.Project
                 FROM Call_ 
                 ORDER BY Call_.Created DESC 
                 LIMIT {};""".format(limit)

        cur = controller.get_cursor()
        cur.execute(sql)

        return cur.fetchall()


    @staticmethod
    def create(controller, org_id, bank_id, project_id, ext_id=None, state=None, task_id=None):
        cur = controller.get_cursor()

        sql = "INSERT INTO Call_ SET Bank = %s, Project = %s, ExtID = %s;"

        if state:
            sql = sql.replace("SET ", "SET Result = '{}',".format(state))

        if task_id:
            sql = sql.replace("SET ", "SET TaskID = {},".format(task_id))

        cur.execute(sql, (bank_id, project_id, ext_id))

        call_id = cur.lastrowid

        controller.save_changes()
        cur.execute("""SELECT * FROM Call_ WHERE ID = %s;""", (call_id,))
        res = cur.fetchall()

        cur.execute("INSERT INTO Call_Organization SET Call_ = %s, Organization = %s;",
                    (call_id, org_id))
        controller.save_changes()

        if len(res) == 0:
            return None

        return Call(res[0], controller)

    @staticmethod
    def by_id(controller, call_id):
        cur = controller.get_cursor()

        cur.execute("""SELECT * FROM Call_ WHERE ID = %s;""", (call_id,))
        res = cur.fetchall()

        if len(res) == 0:
            return None

        return Call(res[0], controller)

    @staticmethod
    def by_ext_id(controller=None, ext_call_id=None):

        if ext_call_id is None:
            return None

        if controller is None:
            controller = DBController()

        cur = controller.get_cursor()

        cur.execute("""SELECT * FROM Call_ WHERE ExtID = %s;""", (ext_call_id,))
        res = cur.fetchall()

        if len(res) == 0:
            return None

        return Call(res[0], controller)

    @staticmethod
    def by_ext_ids(controller=None, ext_call_ids=None):

        if ext_call_ids is None:
            return None

        if controller is None:
            controller = DBController()

        cur = controller.get_cursor()

        cur.execute("""SELECT * FROM Call_ WHERE ExtID in ({});""".format(', '.join(ext_call_ids)))
        res = cur.fetchall()

        if len(res) == 0:
            return None

        result = []

        for call_rec in res:
            result.append(Call(call_rec, controller))

        return result

    def __init__(self, rec, controller=None):
        self.rec = rec

        if controller is None:
            self.controller = DBController()
        else:
            self.controller = controller

    def id(self):
        return self.rec["ID"]

    def ext_id(self):
        return self.rec["ExtID"]

    def result(self):
        return self.rec["Result"]

    def set_result(self, res, done_state_list=None):
        cur = self.controller.get_cursor()

        SQL = "UPDATE Call_ SET Result = '{}' WHERE ID = {}".format(res, self.id())

        if done_state_list:
            SQL += " AND Result not in ('{}')".format("', '".join(done_state_list))

        cur.execute(SQL)
        self.controller.save_changes()
