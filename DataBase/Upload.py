from DataBase.DBController import DBController
from DataBase.Call import Call


class Upload:

    @staticmethod
    def find_by_id(upload_id, controller=None):
        if controller is None:
            controller = DBController()

        cur = controller.get_cursor()

        SQL = """SELECT * FROM Upload WHERE ID = {};""".format(upload_id)
        cur.execute(SQL)
        upload_rec = cur.fetchone()

        if not upload_rec:
            return None

        return Upload(upload_rec, controller)

    @staticmethod
    def list(controller=None, limit=50, offset=0, start_date=None, end_date=None):

        if controller is None:
            controller = DBController()

        cur = controller.get_cursor()

        sql = """SELECT * FROM Upload"""

        where = ""

        if start_date:
            start_date = start_date[6:] + "-" + start_date[3:5] + "-" + start_date[:2] + " 00:00:00+03:00"
            where = " WHERE DateTime > '{}'".format(start_date)

        if end_date:
            end_date = end_date[6:] + "-" + end_date[3:5] + "-" + end_date[:2] + " 23:59:59+03:00"

            if where:
                where += " AND DateTime < '{}'"
            else:
                where = " WHERE DateTime < '{}'"

            where = where.format(end_date)

        sql += where

        if limit:
            sql += " LIMIT {}".format(limit)

        if offset:
            sql += " OFFSET {}".format(offset)

        cur.execute(sql)
        upload_rec_list = cur.fetchall()

        res = []

        for upload_rec in upload_rec_list:
            res.append(Upload(upload_rec, controller))

        return res

    @staticmethod
    def create(controller, type, reason=None):

        cur = controller.get_cursor()

        sql = """INSERT INTO Upload (State, Type, Reason)
                     VALUES (%s, %s, %s)"""

        cur.execute(sql, ('В процессе', type, reason))

        upload_id = cur.lastrowid
        controller.save_changes()

        cur.execute("SELECT * FROM Upload WHERE ID = {}".format(upload_id))
        uoload_list = cur.fetchall()

        return Upload(uoload_list[0], controller)

    def __init__(self, rec, controller=None):
        self.rec = rec

        if controller is None:
            self.controller = DBController()
        else:
            self.controller = controller

        cur = self.controller.get_cursor()

        cur.execute("""SELECT * FROM Upload_Organization WHERE Upload = {};""".format(self.rec["ID"]))
        self.organization_link_rec_list = cur.fetchall()

    def id(self):
        return self.rec["ID"]

    def state(self):
        return self.rec["State"]

    def datetime(self):
        return self.rec["DateTime"]

    def contact_count(self):
        return len(self.organization_link_rec_list)

    def params(self):
        return self.rec["Params"]

    def set_state(self, state):
        cur = self.controller.get_cursor()
        cur.execute("""UPDATE Upload SET State = '{}' WHERE ID = {}""".format(state, self.id()))
        self.controller.save_changes()

    def call_list(self, filter=[]):

        cur = self.controller.get_cursor()

        where = ""

        for elem in filter:
            where += " AND {} {} {}".format(elem["field"], elem["operation"], elem["value"])

        cur.execute("""SELECT Call_.* 
                       FROM Upload_Organization 
                       INNER JOIN Call_ ON Call_.ID = Upload_Organization.`Call` 
                       WHERE Upload = {}{};""".format(self.rec["ID"], where))

        call_rec_list = cur.fetchall()

        res = []

        for call_rec in call_rec_list:
            res.append(Call(call_rec, self.controller))

        return res

    def call_report(self):
        cur = self.controller.get_cursor()

        sql = """SELECT Call_.Result, count(*) as CNT
                 FROM Upload_Organization
                 INNER JOIN Call_ ON Call_.ID = Upload_Organization.`Call`
                 WHERE Upload_Organization.Upload = {}
                 GROUP BY Call_.Result;""".format(self.id())

        cur.execute(sql)

        return cur.fetchall()

    def time_zone_report(self):
        cur = self.controller.get_cursor()

        sql = """SELECT Region.TimeZone + 3 as TZ, COUNT(*) AS CNT
                 FROM Upload_Organization
                 INNER JOIN Organization ON Organization.ID = Upload_Organization.Organization
                 INNER JOIN Organization_Region ON Organization_Region.Organization = Organization.ID
                 INNER JOIN Region ON Organization_Region.Region = Region.ID
                 WHERE
                 Upload_Organization.Upload = {}
                 GROUP BY Region.TimeZone;""".format(self.id())

        cur.execute(sql)

        return cur.fetchall()

    def link_to_call_and_organization(self, call_id, org_id):
        cur = self.controller.get_cursor()

        sql = """INSERT INTO Upload_Organization (`Call`, Organization, Upload)
                             VALUES ({}, {}, {})""".format(call_id, org_id, self.id())

        cur.execute(sql)

        self.controller.save_changes()


