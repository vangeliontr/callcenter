#!/usr/bin/env python3

import sys
import json
import time

sys.path.append("../")

from Tools.Daemon import Daemon
from DataBase.DBController import DBController
from DataBase.Task import Task
from DataBase.Call import Call


def check_db_to_odp():

    controller = DBController()

    #START_ID = 1248973

    START_ID = -1

    with open("START_ID", "r") as f:
        START_ID = int(f.read())

    print("Стартовая позиция {}".format(START_ID))

    is_continue = True

    in_opd = 0
    not_in_odp = 0

    while is_continue:

        cur = controller.get_cursor()

        with open("START_ID", "w") as f:
            f.write(str(START_ID))
            print("Записали позицию {}".format(START_ID))

        print("Получаем порцию в 1000 контактов, начиная с {}".format(START_ID))

        SQL = """
            SELECT * FROM Organization
            WHERE LastContactPerson is not NULL and LastPhoneInfo is not null AND OpenDate < '2020-04-20'
            ORDER BY Organization.ID DESC 
            LIMIT 1000
        """

        SQL = SQL.replace("WHERE ", "WHERE ID < {} AND ".format(START_ID))

        cur.execute(SQL)
        org_list = cur.fetchall()

        check_list = []

        for org in org_list:

            cur = controller.get_cursor()

            if org["ID"] < START_ID:
                START_ID = org["ID"]

            SQL = """
                SELECT * FROM Call_Organization
                INNER JOIN Call_ ON Call_.ID = Call_Organization.Call_
                WHERE Call_Organization.Organization = {} AND ( Call_.Result like '%Отправлено%' OR Call_.Result like '%Лид%' OR Call_.Result like '%ОДП%')
            """.format(org["ID"])

            cur.execute(SQL)
            call_list = cur.fetchall()

            if len(call_list) != 0:
                print("Активен")
                continue
            else:
                print("Неактивен")

            check_list.append(org)

        print("На проверку {}".format(len(check_list)))
        odp_task_list = {}

        count = 0

        for org_rec in check_list:

            count += 1

            print("Постановка на проверку {} из {}".format(count, len(check_list)))
            odp_task_list[org_rec["INN"]] = Task.set_task_impl(controller, "Проверка на ОДП",
                                                               json.dumps({"Bank": "Россельхозбанк",
                                                                           "INN": org_rec["INN"]}))

        for org_rec in check_list:

            print("Проверяем {}".format(org_rec))

            task_state = "Выполняется"

            while task_state != "Завершена":
                task = Task.get_by_id(controller, odp_task_list[org_rec["INN"]])
                task_state = task.state()

                if task_state != "Завершена":
                    print("Ожидаем проверку организации с ИНН {} на ОДП, задача {}".
                          format(org_rec["INN"], odp_task_list[org_rec["INN"]]))
                    time.sleep(5)

            odp_state = task.get_res()

            if odp_state and "Состояние" in odp_state and odp_state["Состояние"] == "ОДП":

                try:
                    Call.create(controller, org_rec["ID"], 13, None, state="ОДП")
                    in_opd += 1
                except Exception as e:
                    print(e)
            else:
                not_in_odp += 1

            print("В ОДП {}, не в ОДП {}".format(in_opd, not_in_odp))


conf = json.loads(open("rshb_odp_daemon_settings.txt", "r").read())
Daemon.run(check_db_to_odp, conf)
