from DataBase.DBController import DBController
from Bank.BankList import BankList
from DataBase.Call import Call
import time

rshb_bank = BankList.get_by_name("РосСельхозБанк")
psb_bank = BankList.get_by_name("ПромСвязьБанк")

controller = DBController()
cur = controller.get_cursor()

START_ID = 1248973

is_continue = True

in_opd = 0
not_in_odp = 0

while is_continue:

    SQL = """
        SELECT * FROM Organization
        WHERE LastContactPerson is not NULL and LastPhoneInfo is not null AND OpenDate < '2020-04-20'
        ORDER BY Organization.ID DESC 
        LIMIT 1000
    """

    if START_ID != -1:
        SQL = SQL.replace("WHERE ", "WHERE ID < {} AND ".format(START_ID))

    cur.execute(SQL)
    org_list = cur.fetchall()

    for org in org_list:

        cur = controller.get_cursor()

        print(org)
        print("В ОДП {}, не в ОДП {}".format(in_opd, not_in_odp))

        if START_ID == -1:
            START_ID = org["ID"]

        if org["ID"] < START_ID:
            START_ID = org["ID"]

        SQL = """
            SELECT * FROM Call_Organization
            INNER JOIN Call_ ON Call_.ID = Call_Organization.Call_
            WHERE Call_Organization.Organization = {} AND ( Call_.Result like '%Отправлено%' OR Call_.Result like '%Лид%' OR Call_.Result like '%ОДП%')
        """.format(org["ID"])

        cur.execute(SQL)
        call_list = cur.fetchall()

        if len(call_list) != 0:
            print("Активен")
            continue

        is_in_odp = False

        if rshb_bank.is_in_odp(org["INN"]):
            Call.create(controller, org["ID"], 13, None, state="ОДП")
            is_in_odp = True

        if not is_in_odp:

            if psb_bank.is_in_odp(org["INN"]):
                Call.create(controller, org["ID"], 12, None, state="ОДП")
                is_in_odp = True

            time.sleep(1)

        if is_in_odp:
            in_opd += 1
        else:
            not_in_odp += 1
