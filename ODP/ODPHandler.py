import openpyxl
import os
import re
import time
import traceback
import json
import holidays

from datetime import datetime, timedelta, date
from threading import Thread
from Bank.BankList import BankList
from DataBase.Organization import Organization
from DataBase.DBController import DBController
from DataBase.Download import Download
from DataBase.Task import Task
from DataBase.Call import Call
from DataBase.Project import Project
from CallCenter.Skorozvon import Skorozvon
from ODP.ODPException import ODPDelayException

import sys
sys.path.append("..")


def phone_is_invalid(phone):

    if not phone:
        return True

    if phone[0] == "8" or phone[0] == "7":
        phone = "+7" + phone[1:]

    if phone[0] == "9":
        phone = "+7" + phone

    if phone[0:2] == "+7":
        phone = "+7" + re.sub(r'[^0-9]+', r'', phone[2:])

        if len(phone) == 12:
            return False

    return True


def check_project_to_odp(params, set_res):

    project = Project.get_by_name(params["Project"])

    project_params = project.params()

    done_state_list = []

    for result in project_params["Скорозвон"]["results"]:
        if "IsDoneStatus" in project_params["Скорозвон"]["results"][result] and \
            project_params["Скорозвон"]["results"][result]["IsDoneStatus"]:

            done_state_list.append(project_params["Скорозвон"]["results"][result]["name"])

    # Логирование
    daily_log_path = "Протоколы проверок/{}/{}".format(project.name(),
                                                       datetime.now().strftime("%Y.%m.%d"))

    res = {}
    ru_holidays = holidays.CountryHoliday('RU')
    ru_holidays.append(["2020-02-24", "2020-03-09", "2020-05-04", "2020-05-05", "2020-05-11", "2020-03-09"])

    if datetime.now() in ru_holidays or datetime.now().weekday() == 6:

        res["Не рабочее время"] = "Выходной день"
        res["Протоколы проверок"] = """<a href="../../{}">{}</a>""".format(daily_log_path,
                                                                           datetime.now().strftime("%Y.%m.%d"))
        set_res(res)
        return {"sleep": params["Periodicity"] * 60, "params": params}

    if datetime.now().hour < 6 or datetime.now().hour >= 18:

        res["Не рабочее время"] = "Не работаем c 18:00 до 6:00"
        res["Протоколы проверок"] = """<a href="../../{}">{}</a>""".format(daily_log_path,
                                                                           datetime.now().strftime("%Y.%m.%d"))
        set_res(res)
        return {"sleep": params["Periodicity"] * 60, "params": params}

    #Информирование
    res = {"Состояние": "Получение информации о проекте"}
    set_res(res)

    controller = DBController()
    bank = project.bank()
    skz_client = None

    try:
        skz_client = Skorozvon()
    except:
        return {"sleep": 5}

    if not os.path.exists("../Admin/{}".format(daily_log_path)):
        os.makedirs("../Admin/{}".format(daily_log_path))

    log = open("../Admin/{}/{}".format(daily_log_path,
                                       datetime.now().strftime("%H.%M.txt")), "w", encoding="1251")

    # Информирование
    res["Состояние"] = "Запрос списка контактов"
    res["Протоколы проверок"] = """<a href="../../{}">{}</a>""".format(daily_log_path, datetime.now().strftime("%Y.%m.%d"))
    set_res(res)

    project_params = project.params()
    skz_org_list = skz_client.get_project_source(project_params["Скорозвон"]["project_id"], True)["leads"]

    # Логирование
    log.write("Контактов в скорозвоне: {}\n".format(len(skz_org_list)))

    # Информирование
    res["Состояние"] = "Поиск контактов в БД"
    res["Контактов в скорозвоне"] = len(skz_org_list)
    set_res(res)

    skz_ext_ids = []
    for skz_org in skz_org_list:
        skz_ext_ids.append(str(skz_org["id"]))

    if len(skz_ext_ids) == 0:
        return {"sleep": params["Periodicity"] * 60, "params": params}

    try:
        call_list = Call.by_ext_ids(controller, skz_ext_ids)
    except Exception as e:

        Task.set_task("Оповещение проблем",
                      {"message": "Ошибка в задаче проверки на ОДП по проекту {}".format(params["Project"])})

        res["Ошибка"] = str(e)
        res["Список Скорозвона"] = str(skz_ext_ids)
        set_res(res)

        return {"sleep": params["Periodicity"] * 60, "params": params}

    org_list = {}
    count = 0

    for call in call_list:

        org = Organization.get_by_call(controller, call)
        org_list[org.inn()] = {"org": org, "call": call}

        # Информирование
        count += 1
        res["Прогресс поиска"] = "{} из {}".format(count, res["Контактов в скорозвоне"])
        set_res(res)

    # Логирование
    log.write("Контактов в БД: {}\n".format(len(org_list)))

    if "ОДП" in project_params and "СтратегияПроверки" in project_params["ОДП"] and \
        project_params["ОДП"]["СтратегияПроверки"] == "Последователная":

        # Информирование
        res["Состояние"] = "Последовательная проверка проекта на ОДП"
        res["Контактов в БД"] = len(org_list)
        res["Количество в ОДП"] = 0
        res["Количество не в ОДП"] = 0
        res["Ошибки при проверке на ОДП"] = ""
        set_res(res)
        count = 0


        for inn in org_list:
            count += 1
            res["Прогресс проверки"] = "{} из {}".format(count, res["Контактов в БД"])
            set_res(res)

            start_dt = datetime.now()

            try:
                in_odp = bank.is_in_odp(inn)
            except:
                try:
                    bank.odp_delay()
                except:
                    pass

                try:
                    in_odp = bank.is_in_odp(inn)
                except Exception as e:
                    res["Ошибки при проверке на ОДП"] += str(e)

            end_dt = datetime.now()

            log.write("{}: Проверка контакта {} началась в {} закончилась в {}, длилась {} \n".
                      format(datetime.now(), inn, start_dt, end_dt, end_dt-start_dt))

            if in_odp:
                org_list[inn]["call"].set_result("ОДП")
                del_res = skz_client.set_odp_state({"ExtID": org_list[inn]["call"].ext_id()})

                if del_res == "Internal Server Error":
                    skz_client.re_auth()
                    del_res = skz_client.set_odp_state({"ExtID": org_list[inn]["call"].ext_id()})

                # Логирование
                log.write("{}: Контакт {} в ОДП, результат удаления {}\n".format(datetime.now(), inn, del_res))

                # Информирование
                res["Количество в ОДП"] += 1
            else:
                # Логирование
                log.write("{}: Контакт {} не в ОДП\n".format(datetime.now(), inn))

                # Информирование
                res["Количество не в ОДП"] += 1

            try:
                bank.odp_delay()
            except:
                pass


    else:
        # Информирование
        res["Состояние"] = "Постановка задач проверки ОДП"
        res["Контактов в БД"] = len(org_list)
        set_res(res)
        count = 0

        check_list = {}

        for inn in org_list:

            check_list[inn] = Task.set_task_impl(controller, "Проверка на ОДП", json.dumps({"Bank": bank.name(), "INN": inn}))

            try:
                bank.odp_delay()
            except:
                pass

            # Информирование
            count += 1
            res["Прогресс постановки"] = "{} из {}".format(count, res["Контактов в БД"])
            set_res(res)

        # Логирование
        log.write("Задач проверки: {}\n".format(len(check_list)))

        # Информирование
        res["Состояние"] = "Ожидание проверки на ОДП"
        res["Задач проверки"] = len(check_list)
        res["Количество в ОДП"] = 0
        res["Количество не в ОДП"] = 0
        res["Отсутствие состояния проверки ОДП"] = 0
        res["Ошибки при проверке на ОДП"] = ""
        set_res(res)
        count = 0

        for inn in check_list:

            task_state = "Выполняется"

            while task_state != "Завершена":
                task = Task.get_by_id(controller, check_list[inn])
                task_state = task.state()

                if task_state != "Завершена":
                    time.sleep(5)

            task_res = task.get_res()

            try:

                if "Состояние" not in task_res:
                    res["Отсутствие состояния проверки ОДП"]
                elif task_res["Состояние"] == "ОДП":

                    org_list[inn]["call"].set_result("ОДП", done_state_list)
                    del_res = skz_client.set_odp_state({"ExtID": org_list[inn]["call"].ext_id()})

                    if del_res == "Internal Server Error":
                        skz_client.re_auth()
                        del_res = skz_client.set_odp_state({"ExtID": org_list[inn]["call"].ext_id()})

                    # Логирование
                    log.write("{}: Контакт {} в ОДП, результат удаления {}\n".format(datetime.now(), inn, del_res))

                    # Информирование
                    res["Количество в ОДП"] += 1
                else:
                    # Логирование
                    log.write("{}: Контакт {} не в ОДП\n".format(datetime.now(), inn))

                    # Информирование
                    res["Количество не в ОДП"] += 1
            except Exception as e:
                res["Ошибки при проверке на ОДП"] += "{}\n".format(str(e)[:50])

            log.flush()

            # Информирование
            count += 1
            res["Прогресс проверки"] = "{} из {}".format(count, res["Контактов в БД"])
            set_res(res)

    # Логирование
    log.write("Количество в ОДП: {}\n".format(res["Количество в ОДП"]))
    log.write("Количество не в ОДП: {}\n".format(res["Количество не в ОДП"]))

    # Информирование
    res["Состояние"] = "Завершено"
    set_res(res)

    return {"sleep": params["Periodicity"] * 60, "params": params}


def check_to_odp(params, set_res):
    first_row = int(params["first_row"])
    inn_col = int(params["inn_col"]) - 1
    phone_col = int(params["phone_col"]) - 1
    date_col = int(params["date_col"]) - 1

    controller = DBController()

    if not os.path.isfile("../Files/" + params["file"]["Идентификатор"] + ".xlsx"):
        os.rename("../Files/" + params["file"]["Идентификатор"],
                  "../Files/" + params["file"]["Идентификатор"] + ".xlsx")

    wb = openpyxl.load_workbook(filename="../Files/" + params["file"]["Идентификатор"] + ".xlsx")
    sheet = wb.get_active_sheet()
    max_col = sheet.max_column
    data = sheet.values

    set_res({
        "Состояние": "Подготовка данных",
        "Всего контактов": sheet.max_row - first_row + 1
    })

    bank_llist = BankList.get(controller)
    precheck_list = {}

    sheet.cell(row=1, column=max_col + 2).value = "Валидность телефона"
    sheet.cell(row=1, column=max_col + 3).value = "Уже есть в БД"

    cnt = 0

    for bank in bank_llist:
        cnt += 1
        precheck_list[bank.name()] = {}
        sheet.cell(row=1, column=max_col + 3 + cnt).value = "ОДП " + bank.name()

    cnt = 0

    for row in data:

        inn = row[inn_col]

        cnt += 1

        if cnt < first_row:
            continue

        for bank in bank_llist:
            is_precheck_odp = False

            try:
                is_precheck_odp = bank.is_pre_check_odp()
            except:
                pass

            if is_precheck_odp:
                precheck_list[bank.name()][inn] = bank.get_score_id(inn)

        set_res({
            "Состояние": "Предпроверка по ОДП (Загрузка в Тинькофф)",
            "Предпроверено": cnt - first_row + 1,
            "Всего контактов": sheet.max_row - first_row + 1
        })

    data = sheet.values
    cnt = 0

    set_res({
        "Состояние": "Проверка на ОДП (Ожидаем ТФ)",
        "Обработано": cnt,
        "Всего контактов": sheet.max_row - first_row + 1
    })

    for row in data:

        cnt += 1

        if cnt < first_row:
            continue

        phone = row[phone_col]
        inn = row[inn_col]
        date = row[date_col]

        p_is_invalid = phone_is_invalid(phone)
        org = Organization.find_by_inn_open_date(controller, inn, date)

        if p_is_invalid:
            sheet.cell(row=cnt, column=max_col + 2).value = "НЕТ"
        else:
            sheet.cell(row=cnt, column=max_col + 2).value = "ДА"

        if org is None:
            sheet.cell(row=cnt, column=max_col + 3).value = "НЕТ"
        else:
            sheet.cell(row=cnt, column=max_col + 3).value = "ДА"

        if org is not None:
            is_in_db = True

        odp = {}

        bank_cnt = 0

        for bank in bank_llist:
            bank_cnt += 1
            is_precheck_odp = False

            try:
                is_precheck_odp = bank.is_pre_check_odp()
            except:
                pass

            if is_precheck_odp:
                odp[bank.name()] = bank.get_odp_status(precheck_list[bank.name()][inn]) != "APPROVED"
            elif bank.is_in_odp(inn):
                odp[bank.name()] = True
            else:
                odp[bank.name()] = False

            if odp[bank.name()]:
                sheet.cell(row=cnt, column=max_col + 3 + bank_cnt).value = "ДА"
            else:
                sheet.cell(row=cnt, column=max_col + 3 + bank_cnt).value = "НЕТ"

        set_res({
            "Состояние": "Проверка на ОДП",
            "Обработано": cnt - first_row + 1,
            "Всего контактов": sheet.max_row - first_row + 1
        })

    wb.save("../Admin/{}.xlsx".format(params["file"]["Идентификатор"]))

    set_res({
        "Результат": '<a href="../../{}.xlsx">скачать</a>'.format(params["file"]["Идентификатор"]),
        "Обработано": cnt - first_row + 1,
        "Всего контактов": sheet.max_row - first_row + 1
    })


def check_download_to_odp(params, set_res):

    controller = DBController()
    skorozvon = Skorozvon()
    download = Download.get_by_id(controller, params["id"])
    bank = BankList.get_by_name(params["bank_name"])

    organization_list = download.organization_list()
    check_list = {}
    call_to_check_list = []

    res = {}
    res["На проверку"] = 0

    for organization in organization_list:
        call = organization.last_call_to_bank(bank)

        if call is None:
            continue

        if call.result() in ('ОДП', 'Больше не звоним', 'Отказ', 'ОтправленоГорячий', 'ОтправленоХолодный',
             'Уже звонили', 'Отправлено', 'Лид (горячий)', 'Лид (КП)'):
            continue

        res["На проверку"] += 1
        set_res(res)

        call_to_check_list.append(call)
        check_list[call.id()] = Task.set_task("Проверка на ОДП", {"Bank": params["bank_name"], "INN": organization.inn()})

    res["Проверено"] = 0
    res["В ОПД"] = 0

    for call in call_to_check_list:

        task_state = "Выполняется"

        while task_state != "Завершена":
            task = Task.get_by_id(controller, check_list[call.id()])
            task_state = task.state()

            if task_state != "Завершена":
                time.sleep(5)

        task_res = task.get_res()

        if task_res["Состояние"] == "ОДП":
            call.set_result("ОДП")
            skorozvon.set_odp_state(call.rec)

            res["В ОПД"] += 1
            set_res(res)

        res["Проверено"] += 1
        set_res(res)


def check_to_odp(params, set_res):

    log = open("odp.log", "a")
    bank_name = params["Bank"]
    bank = BankList.get_by_name(bank_name)

    if bank is None:
        set_res({"Ошибка": "Не найден банк с названием {}".format(bank_name)})
        return

    if "BankTaskID" in params:

        if params["BankTaskID"] is None:
            set_res({"Состояние": "Не в ОДП"})
            return

        try:
            status = bank.get_odp_status(params["BankTaskID"], False)
        except Exception as e:
            Task.set_task("Оповещение проблем",
                          {"message": "При получении состояния задачи {} на ОДП по банку {}: {}".format(params["TaskID"], bank_name, e)})

            return {"sleep": 5, "params": params}

        log.write("{}: Статус проверки ИНН {} по ИД {}: {}\n".
                  format(datetime.now(), params["INN"], params["BankTaskID"], status))

        if status == "APPROVED":
            set_res({"Состояние": "Не в ОДП"})
            return
        elif status == "ON_WORK":
            set_res({"Состояние": "Ожидаем проверки в банке"})
            return {"sleep": 5, "params": params}
        else:
            set_res({"Состояние": "ОДП"})
            return


    inn = params["INN"]

    if len(inn) == 11:
        inn = "0" + inn

    phone = None

    if "Phone" in params:
        phone = params["Phone"]

    is_precheck_odp = False

    try:
        is_precheck_odp = bank.is_pre_check_odp()
    except:
        pass

    if is_precheck_odp:
        try:
            params["BankTaskID"] = bank.get_score_id(inn, phone)

            if params["BankTaskID"] == "ОДП":
                set_res({"Состояние": "ОДП"})
                return

            log.write("{} Запустили проверку ИНН {} ИД: {}\n".
                      format(datetime.now(), params["INN"], params["BankTaskID"]))
            set_res({"Состояние": "Ожидаем проверки в банке"})
        except Exception:
            pass

        return {"sleep": 5, "params": params}
    else:
        try:
            if bank.is_in_odp(inn):
                set_res({"Состояние": "ОДП"})
                return
        except ODPDelayException as e:
            set_res({"Приостановлено": str(e)})
            return {"sleep": 5}

        set_res({"Состояние": "Не в ОДП"})


def check_db_to_odp():

    last_hour = -1
    last_part = None

    ru_holidays = holidays.CountryHoliday('RU')

    while True:

        if datetime.now() in ru_holidays:
            print(str(datetime.now()) + " Не работаем в праздничные дни")
            time.sleep(5 * 60)
            continue

        if datetime.now().hour < 6 or datetime.now().hour >= 18:
            print(str(datetime.now()) + " не работаем c 18:00 до 6:00")
            time.sleep(5 * 60)
            continue

        now_hour = datetime.now().hour
        now_part = datetime.now().minute < 30

        # запускаем алгоритм каждый час
        if now_hour != last_hour or last_part != now_part:
            last_hour = now_hour
            last_part = now_part

            check_odp_impl()
        else:
            time.sleep(60)


def check_odp_impl():
    # запустим алгоритм для всех банков

    partner = Skorozvon()
    bank_list = BankList.get()

    thread_list = []
    last_thread = None

    for bank in bank_list:

        if bank.name() == "МТС" or bank.name() == "ПромСвязьБанк" or bank.name() == "Тинькофф":
            continue

        last_thread = Thread(target=check_odp_impl_for_bank, args=(bank, partner))
        thread_list.append(last_thread)

    for thread in thread_list:
        thread.start()

    last_thread.join()


def check_odp_impl_for_bank(bank, partner):

    #bank = BankList.get_by_name(bank.name())

    log = open("log_{}_{}.txt".format(bank.name(), datetime.now()), "w")
    #log = open("123.txt", "w")

    try:
        controller = DBController()
        cur = controller.get_cursor()

        # надо проверять все, что поступило за сегодня, вчера и позавчера, не учитывая выходные
        now_dt = datetime.now()
        one_day_ago = None
        two_day_ago = None

        if now_dt.weekday() == 6:
            log.write("В воскресенье не работаем")
            return
        elif now_dt.weekday() == 0:
            one_day_ago = datetime.now() - timedelta(3)
            two_day_ago = datetime.now() - timedelta(4)
        elif now_dt.weekday() == 1:
            one_day_ago = datetime.now() - timedelta(1)
            two_day_ago = datetime.now() - timedelta(4)
        else:
            one_day_ago = datetime.now() - timedelta(1)
            two_day_ago = datetime.now() - timedelta(2)

        date_condition = ""

        for date in (now_dt, one_day_ago, two_day_ago):

            if date is not None:

                if len(date_condition) != 0:
                    date_condition += ", "

                date_condition += "DATE('{}')".format(date.strftime('%Y-%m-%d'))

        date_condition = "DATE(Created) in ( " + date_condition + " )"

        SQL = """SELECT * FROM Download WHERE {} ORDER BY ID DESC LIMIT 30""".format(date_condition)

        log.write("Запрашиваем кол-во загрузок\n")
        log.flush()

        cur.execute(SQL)

        download_list = cur.fetchall()

        full_dwn_cnt = len(download_list)

        log.write("Обрабатываем {} загрузок\n".format(full_dwn_cnt))
        log.flush()

        d_cur = 0
        for download in download_list:

            log.write("Запрашиваем кол-во контактов загрузки {}\n".format(download["ID"]))
            log.flush()

            contact_list = get_contact_list_to_check(cur, download["ID"], bank.id())

            d_cur += 1

            full_cont_cnt = len(contact_list)

            log.write("Обрабатываем {} контактов\n".format(full_cont_cnt))
            log.flush()

            if not bank.is_multithread_odp():
                check_conrtact_list(partner, contact_list, log, d_cur, full_dwn_cnt, full_cont_cnt, bank, cur, controller, -1, -1, None)
            else:

                thread_list = []
                last_thread = None

                acc_list = bank.acc_list()

                i = 0

                for acc in acc_list:

                    last_thread = Thread(target=check_conrtact_list, args=(partner, contact_list, log, d_cur, full_dwn_cnt, full_cont_cnt, bank, cur, controller, i, len(acc_list), acc))
                    thread_list.append(last_thread)
                    i += 1

                for thread in thread_list:
                    thread.start()

                last_thread.join()


    except Exception as e:
        log.write("Авариное завершение поиска ОДП: " + str(e) + "\n" + traceback.format_exc() + "\n")
        return

    log.write("Штатное завершение поиска ОДП")


def get_contact_list_to_check(cur, download_id, bank_id):
    SQL = """SELECT Organization.ID, Organization.`Name`, Organization.INN, Call_.Result, ContactInfo.`Value`,
                Call_.ExtID, Call_.ID as CID
             FROM Organization
             INNER JOIN Call_Organization ON Call_Organization.Organization = Organization.ID
             INNER JOIN Call_ ON Call_Organization.Call_ = Call_.ID
             LEFT JOIN ContactPerson ON Call_.ContactPerson = ContactPerson.ID
             LEFT JOIN ContactInfo ON ContactInfo.ContactPerson = ContactPerson.ID
             WHERE Organization.Download = {} AND Call_.Bank = {} AND (
             Result not in ('ОДП', 'ОДП при подгрузке', 'Больше не звоним', 'Отказ', 'ОтправленоГорячий', 
             'ОтправленоХолодный', 'Уже звонили', 'Отправлено', 'Лид (горячий)', 'Лид (КП)', 'Лид' ) OR Result is NULL ) ORDER BY Call_.ID DESC
             """.format(download_id, bank_id)

    cur.execute(SQL)
    return cur.fetchall()


def check_conrtact_list(partner, contact_list, log, d_cur, full_dwn_cnt, full_cont_cnt, bank, cur, controller, num, num_len, acc):

    is_precheck_odp = False

    try:
        is_precheck_odp = bank.is_pre_check_odp()
    except:
        pass

    precheck_list = {}

    c_cur = 0

    if is_precheck_odp:
        for contact in contact_list:
            c_cur += 1
            precheck_list[contact["INN"]] = bank.get_score_id(contact["INN"])

            log.write("{}: Препроверка загрузка {} из {}, контакт {} {} из {} \n".
                      format(datetime.now(), d_cur, full_dwn_cnt, contact["INN"], c_cur, full_cont_cnt))

            log.flush()

    c_cur = 0
    # 2. Проверим все контакты на ОДП
    for contact in contact_list:
        try:
            c_cur += 1

            if num != -1:
                if c_cur % num_len != num:
                    continue

            #bank.odp_delay()

            log.write("{}: загрузка {} из {}, контакт {} {} из {} \n".
                      format(datetime.now(), d_cur, full_dwn_cnt, contact["INN"], c_cur, full_cont_cnt))
            log.flush()

            res = None

            count = 0

            while res is None:
                count += 1

                if count >= 10:
                    raise Exception("Ошибка при проверке на ОДП более 10 раз подряд")

                try:
                    if is_precheck_odp:

                        if precheck_list[contact["INN"]] is None:
                            continue

                        res = bank.get_odp_status(precheck_list[contact["INN"]]) != "APPROVED"
                    else:
                        res = bank.is_in_odp_full(contact["INN"], contact["Value"], acc)
                except Exception as e:
                    log.write("Ошибка при запросе ОДП: {}\n".format(str(e)))
                    log.flush()
                    pass

            SQL = """SELECT Call_.Result
                                     FROM Organization
                                     INNER JOIN Call_Organization ON Call_Organization.Organization = Organization.ID
                                     INNER JOIN Call_ ON Call_Organization.Call_ = Call_.ID
                                     WHERE Organization.ID = {} AND Call_.Bank = {}""".format(contact["ID"],
                                                                                              bank.id())

            cur.execute(SQL)
            call_res = cur.fetchall()[0]["Result"]

            if call_res in ('ОДП', 'ОДП при подгрузке', 'Больше не звоним', 'Отказ', 'ОтправленоГорячий', 'ОтправленоХолодный',
                            'Уже звонили', 'Отправлено', 'Лид'):
                log.write("Состояние контакта изменилось на {} - не будем проверять контакт\n".format(call_res))
                log.flush()
                continue

            if res:
                set_odp_to_contact(partner, controller, cur, contact)
                log.write("{}: загрузка {} из {}, контакт {} {} из {} в ОДП\n".
                          format(datetime.now(), d_cur, full_dwn_cnt, contact["INN"], c_cur, full_cont_cnt))
            else:
                log.write("{}: загрузка {} из {}, контакт {} {} из {} не ОДП\n".
                          format(datetime.now(), d_cur, full_dwn_cnt, contact["INN"], c_cur, full_cont_cnt))

            log.flush()
        except Exception as e:
            log.write("Ошибка при обработке контакта: {}\n".format(str(e)))


def set_odp_to_contact(partner, controller, cur, contact):

    res = partner.set_odp_state(contact)

    if res == "Internal Server Error":
        partner.re_auth()
        partner.set_odp_state(contact)

    SQL = """UPDATE Call_ SET Result = 'ОДП' WHERE ID = """ + str(contact["CID"]) + """ AND (
             Result not in ('ОДП', 'ОДП при подгрузке', 'Больше не звоним', 'Отказ', 'ОтправленоГорячий', 
             'ОтправленоХолодный', 'Уже звонили', 'Отправлено', 'Лид') OR Result is NULL )"""
    cur.execute(SQL)
    controller.save_changes()


if __name__ == "__main__":

    def set_res(test):
        return test

    check_project_to_odp({"Project": "Новореги ТФ"}, set_res)

    #check_db_to_odp()
