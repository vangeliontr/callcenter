#!/usr/bin/env python3

import sys
import json

sys.path.append("../")

from ODPHandler import check_db_to_odp
from Tools.Daemon import Daemon


conf = json.loads(open("odp_daemon_settings.txt", "r").read())
Daemon.run(check_db_to_odp, conf)
