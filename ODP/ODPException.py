
class ODPException (Exception):
    pass


class ODPDelayException(ODPException):
    pass
