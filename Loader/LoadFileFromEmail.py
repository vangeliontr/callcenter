import imaplib
import json
import email
import os
import zipfile
import traceback
import shutil
from email.header import decode_header
from datetime import datetime
import time
from rarfile import RarFile
from datetime import timezone
from DataBase.Download import Download
from DataBase.DBController import DBController

pre_path = "../../PersonalOperatorAcc/auto_load/"

def get_from_email(server, in_email, source):

    res, incoming_list = server.search(None, '(UNSEEN FROM "' + in_email + '")')
    incoming_list = incoming_list[0]

    for email_id in incoming_list.split():
        res, data = server.fetch(email_id, "(RFC822)")

        mail = email.message_from_bytes(data[0][1])

        if not mail.is_multipart():
            continue

        print("{} Получение письма от {}".format(datetime.now(), in_email))

        # Получим дату приема письма
        raw_date = mail.get("date")
        raw_date = raw_date.replace(" (UTC)", "")
        receive_date = datetime.strptime(raw_date, '%a, %d %b %Y %H:%M:%S %z')
        receive_date = receive_date.replace(tzinfo=timezone.utc).astimezone(tz=None)

        print("{} Письмо получено на email {}".format(datetime.now(), receive_date))
        controller = DBController()

        # Возможно мы уже пытались принять письмо, попробуем найти его загрузку
        download = Download.find_download(controller, source, datetime.strftime(receive_date, "%Y-%m-%d %H:%M:%S"))

        # Если уже была попытка загрузки, то больше ничего не делаем
        if download is not None:

            print("{} Уже была попытка принять письмо, перезапустим его загрузку".format(datetime.now()))

            download.set_state_full("Ожидает загрузки", "")

            file_list = download.get_file_list()

            for file in file_list:
                filename = file["Name"]

                try:
                    shutil.move(pre_path + "Успешно обработано/" + filename, pre_path + filename)
                except:
                    pass

                try:
                    shutil.move(pre_path + "Ошибки/" + filename, pre_path + filename)
                except:
                    pass

            return

        print("{} Письмо новое, скачаем вложения")

        # Дату регистации определим позже
        reg_date = None
        type = None

        # Получим заголовок письма
        subject = None

        try:
            subject = email.header.decode_header(mail.get("subject"))[0][0].decode()
        except:
            pass

        if subject is not None:
            try:
                reg_date = datetime.strptime(subject[0:10], "%d.%m.%Y")
                reg_date = datetime.strftime(reg_date, "%Y-%m-%d")
            except:
                pass

            try:
                type = subject[10:]
            except:
                pass

        # Список файлов, которые получили в итоге
        file_list = []

        # Пробегаемся по всем файлам архива
        for part in mail.walk():
            email_filename = part.get_filename()

            if email_filename:
                # Декодируем название файла
                if decode_header(email_filename)[0][1] is not None:
                    email_filename = (decode_header(email_filename)[0][0]).decode(decode_header(email_filename)[0][1])

                print("{} Получен файл {}".format(datetime.now(), email_filename))

                if reg_date is None:

                    if source == "Поставщик телефонов":

                        try:
                            reg_date = datetime.strptime(email_filename[0:10], "%d.%m.%Y")
                        except Exception as e:
                            print(e)
                            try:
                                reg_date = datetime.strptime(email_filename[0:8], "%d.%m.%y")
                            except:
                                pass

                        if reg_date is not None:
                            reg_date = datetime.strftime(reg_date, "%Y-%m-%d")
                            print("{} Определили дату регистрации: {}".format(datetime.now(), reg_date))

                first, second = os.path.splitext(email_filename)
                arch_filename = first + "_" + datetime.strftime(receive_date, "%d.%m %H.%M") + second

                arch_path = './Архивы/' + arch_filename

                with open(arch_path, 'wb') as new_file:
                    new_file.write(part.get_payload(decode=True))

                if second == ".zip":
                    zip = zipfile.ZipFile(arch_path)

                    for file in zip.infolist():
                        file_list.append(file.filename)

                    zip.extractall(pre_path, None, b"7790")
                    zip.close()
                else:
                    rar = RarFile(arch_path)

                    for file in rar.infolist():

                        file_list.append(file.filename)

                    rar.extractall(path=pre_path, pwd="7790")
                    rar.close()

        Download.create(controller, file_list, source, reg_date,
                        datetime.strftime(receive_date, "%Y-%m-%d %H:%M:%S"), type)


def load_db_from_email():
    while True:

        email_settings = json.loads(open("email_settings.txt", "r").read())

        #if datetime.now().hour < 4 or datetime.now().hour >= 22:
        #    print(str(datetime.now()) + " не работаем c 22:00 до 5:00")
        #    time.sleep(5*60)
        #    continue

        print("{} Подключение к почте".format(datetime.now()))

        try:
            server = imaplib.IMAP4_SSL(email_settings["imap_ssl_host"], email_settings["imap_ssl_port"])
            server.login(email_settings["username"], email_settings["password"])

            while True:
                print("{} Проверка входящих документов".format(datetime.now()))

                server.select('INBOX')

                for provider in email_settings["providers"]:
                    get_from_email(server, provider["email"], provider["type"])

                time.sleep(20)

        except Exception as e:
            print(datetime.strftime(datetime.now(),"%Y.%m.%d %H:%M:%S") +
                  " ошибка " + str(e))
            print(traceback.format_exc())

        finally:
            server.logout()

        time.sleep(5)


if __name__ == "__main__":
    load_db_from_email()
