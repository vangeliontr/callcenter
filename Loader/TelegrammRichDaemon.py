#!/usr/bin/env python3

import sys
import json
import functools

sys.path.append("../")

from TelegrammHandler import bot_handler_loop
from Tools.Daemon import Daemon

conf = json.loads(open("telegramm_rich_daemon_settings.txt", "r").read())
rich_handler_loop_with_id = functools.partial(bot_handler_loop, conf["bot_id"])

Daemon.run(rich_handler_loop_with_id, conf)
