import time
import json
import sys
sys.path.append("..")

from datetime import datetime
from DataBase.DBController import DBController
from DataBase.Upload import Upload
from DataBase.Organization import Organization
from CallCenter.Skorozvon import Skorozvon
from DataBase.Call import Call
from DataBase.Project import Project
from DataBase.Task import Task
from Bank.BankList import BankList
from DataBase.ContactInfo import ContactInfo


def load_from_db_to_bank(params, set_res):
    controller = DBController()

    if "Проект" not in params:
        raise Exception("Загружать в банк можно только по определенному проекту")

    project = Project.get_by_name(params["Проект"])

    if project is None:
        raise Exception(f"Не найден проект с названием {params['Проект']}")

    bank = project.bank()

    SQL = f"""SELECT 
                     ContactPerson.`Name` as CName,
                     ContactPerson.Surname,
                     ContactPerson.MiddleName,
                     Organization.ID,
                     Organization.`Name`,
                     Organization.INN,
                     Organization.Address,
                     ContactInfo.Value
              FROM Organization 
              INNER JOIN Download_Organization ON Download_Organization.Organization = Organization.ID
              INNER JOIN ContactPerson ON ContactPerson.ID = Organization.LastContactPerson
              INNER JOIN ContactInfo ON ContactInfo.ID = Organization.LastPhoneInfo
              WHERE Download_Organization.Download = {params['Загрузка']} AND
              Download_Organization.Operation = '{params['Операция']}' AND
              Organization.LastPhoneInfo is not NULL;"""

    cur = controller.get_cursor()
    cur.execute(SQL)
    org_list = cur.fetchall()

    reason = """Задача подгрузки <a href="../task/task.py?id={}">{}</a>""".format(params["TaskID"], params["TaskID"])
    upload = Upload.create(controller, "", reason)

    res = {
        "Контактов на проверку ОДП": len(org_list),
        "Номер подгрузки": upload.id()
    }

    set_res(res)
    cnt = 0
    odp_task_list = {}

    for org in org_list:

        cnt += 1
        res["Запуск проверки на ОДП"] = "{} из {}".format(cnt, len(org_list))

        # Записываем результат каждой 100й организации, иначе сильно накладно
        # вообще надо заменить записью раз в секунду
        if cnt % 100 == 0:
            set_res(res)

        odp_task_list[org["INN"]] = Task.set_task_impl(controller, "Проверка на ОДП",
                                                       json.dumps({"Bank": bank.name(),
                                                                   "INN": org["INN"],
                                                                   "Phone": org["Value"]}))

    res["Контактов в ОДП"] = 0
    res["Загружено"] = 0
    res["Ошибки при загрузке"] = ""
    set_res(res)
    cnt = 0
    to_load_list = []

    for org in org_list:

        cnt += 1
        res["Проверка на ОДП"] = "{} из {}".format(cnt, len(org_list))

        # Записываем результат каждой 100й организации, иначе сильно накладно
        # вообще надо заменить записью раз в секунду
        if cnt % 100 == 0:
            set_res(res)

        task_state = "Выполняется"

        while task_state != "Завершена":
            task = Task.get_by_id(controller, odp_task_list[org["INN"]])
            task_state = task.state()

            if task_state != "Завершена":
                print("Ожидаем проверку организации с ИНН {} на ОДП, задача {}".
                      format(org["INN"], bank.name(), odp_task_list[org["INN"]]))
                time.sleep(5)

        odp_state = task.get_res()

        if odp_state and "Состояние" in odp_state and odp_state["Состояние"] == "ОДП":

            try:
                call = Call.create(controller, org["ID"], bank.id(), project.id(), state="ОДП при подгрузке",
                                   task_id=params["TaskID"])

                upload.link_to_call_and_organization(call.id(), org["ID"])
            except Exception as e:
                print(e)

            res["Контактов в ОДП"] = res["Контактов в ОДП"] + 1
            set_res(res)

            print("Организация {} в ОПД".format(org["INN"]))
            continue

        org_info = {
            "Имя": org["CName"],
            "Отчество": org["MiddleName"],
            "Фамилия": org["Surname"],
            "Телефон": org["Value"],
            "Горячий": False,
            "Название": org["Name"],
            "ИНН": org["INN"],
            "Адрес": org["Address"],
            "Комментарий": "",
            "temperature": "COLD"
        }

        if org_info["Телефон"][0:2] == "79":
            org_info["Телефон"] = "+" + org_info["Телефон"]

        if bank.is_mass_load():
            to_load_list.append(org_info)
            continue

        log = open(f"send/{org['INN']} to {project.name()}", "w")
        res_list = bank.send_org(org_info, log, project_params=project.params())

        for m_res in res_list:
            if m_res is not None and 'error' in m_res:
                res["Ошибки при загрузке"] += org['INN'] + ": " + m_res['error'][:20] + "<br/>"

        res["Загружено"] += 1
        set_res(res)

    if len(to_load_list):
        bank.mass_send(to_load_list, upload.id())


def load_from_db(params, set_res):

    controller = DBController()
    our_project_id = None
    bank_id = None
    project_name = ""

    if "Проект" in params:
        project = Project.get_by_name(params["Проект"])
        bank = project.bank()
        region_list = project.regions
        our_project_id = project.id()
        bank_id = bank.id()
        project_name = params["Проект"]
    elif "Банк" in params:
        bank = BankList.get_by_name(params["Банк"])
        region_list = bank.regions
        bank_id = bank.id()
        project_name = params["Банк"]

    res = {}
    is_diagnistic_only = False

    if "Загружать на уровне" in params:

        task_list = Task.get_list(controller, 100, "Оставшиеся контакты по проекту")

        monitoring_task = None

        for task in task_list:
            task_params = task.get_params()

            if task_params["ProjectName"] == params["Проект"]:
                monitoring_task = task
                break

        if monitoring_task is None:
            res["Мониторинг"] = "Не найдена задача мониторинга проекта " + params["Проект"]
        else:
            task_res = monitoring_task.get_res()

            # Если поля количество нет - значит задача в работе
            if "Количество" not in task_res:
                res["Мониторинг"] = "Задача мониторинга проекта {} в работе".format(params["Проект"])
                set_res(res)
                return {"sleep": 60, "params": params}

            if int(task_res["Количество"]) > int(params["Загружать на уровне"]):
                is_diagnistic_only = True
                res["Мониторинг"] = "Уровень контактов {}, загружать будем на уровне {}".\
                    format(task_res["Количество"], params["Загружать на уровне"])

        set_res(res)

    sql = """SELECT * FROM (
             SELECT MAX(Organization.ID) AS ID, Organization.INN, count(*) FROM Organization
             LEFT JOIN Organization_Region ON Organization_Region.Organization = Organization.ID
             {}
             WHERE {}
             GROUP BY Organization.INN
             ORDER BY MAX(Organization.ID) {} ) as origin
             WHERE origin.INN not in (
             SELECT Organization.INN
             FROM Call_
             INNER JOIN Call_Organization ON Call_Organization.Call_ = Call_.ID
             INNER JOIN Organization ON Call_Organization.Organization = Organization.ID
             WHERE Call_.TaskID = {} )"""

    WHERE = []
    JOIN = []
    ORDER = "ASC"

    WHERE.append("INN is not NULL")
    WHERE.append("Type is not NULL")
    WHERE.append("LastPhoneInfo is not NULL")
    WHERE.append("LastContactPerson is not NULL")

    #if "Дата открытия старше" in params:
    #    WHERE.append("OpenDate > '{}'".format(params["Дата открытия старше"]))

    #if "Дата открытия младше" in params:
    #    WHERE.append("OpenDate < '{}'".format(params["Дата открытия младше"]))

    if "Тип организации" in params:
        if params["Тип организации"] == "ООО":
            WHERE.append("LENGTH(INN) = 10")
        elif params["Тип организации"] == "ИП":
            WHERE.append("LENGTH(INN) = 12")

    if "Дата регистрации с" in params:
        date = params["Дата регистрации с"]
        WHERE.append("OpenDate >= '{}-{}-{}'".format(date[6:10], date[3:5], date[0:2]))

    if "Дата регистрации по" in params:
        date = params["Дата регистрации по"]

        if date == "Прошлого года":
            WHERE.append("OpenDate < '{}-01-01'".format(datetime.now().year))
        else:
            WHERE.append("OpenDate < '{}-{}-{}'".format(date[6:10], date[3:5], date[0:2]))

    if "Статусы" in params:
        JOIN.append("INNER JOIN Call_Organization ON Call_Organization.Organization = Organization.ID")
        JOIN.append("INNER JOIN Call_ ON Call_.ID = Call_Organization.Call_")

        status_list = params["Статусы"]

        adapted_status_list = []

        for status in status_list:

            if status == "NULL":
                adapted_status_list.append("Call_.Result is NULL")
            else:
                adapted_status_list.append("Call_.Result like '%{}%'".format(status))

        WHERE.append("({})".format(" OR ".join(adapted_status_list)))

    if "Статусы банка" in params:
        JOIN.append("INNER JOIN Call_Organization ON Call_Organization.Organization = Organization.ID")
        JOIN.append("INNER JOIN Call_ ON Call_.ID = Call_Organization.Call_")

        status_list = params["Статусы банка"]

        status_where = ""

        for status in status_list:

            if status_where:
                status_where += ", "

            status_where += "'{}'".format(status)

        if status_where:
            where = """Call_.Bank = {} AND Call_.Result in ({})""".format(bank.id(), status_where)
        else:
            where = ""

        if "Статусы других банков" in params:
            status_list = params["Статусы других банков"]

            status_where = ""

            for status in status_list:

                if status_where:
                    status_where += " OR "

                status_where += "Call_.Result like '%{}%'".format(status)

            if where:
                where += " OR "

            where += """ Call_.Bank <> {} AND ({})""".format(bank.id(), status_where)

        WHERE.append("({})".format(where))

    if "Регионы" in params:

        cond = "in"

        if params["Регионы"] == "Не проекта":
            cond = "not in"

        region_where = ""

        for region in region_list:

            if region_where:
                region_where += ", "

            region_where += str(region.id())

        region_where = "({})".format(region_where)

        WHERE.append("Organization_Region.Region {} {}".format(cond, region_where))

    if "Не загружать регионы" in params:
        WHERE.append("(Organization_Region.Region not in ({}) OR Organization_Region.Region is NULL)".
                     format(",".join(params["Не загружать регионы"])))

    if "Загрузка" in params and "Операция" not in params:
        WHERE.append("Organization.Download = {}".format(params["Загрузка"]))
        WHERE.append("Organization.LastPhoneInfo is not NULL")

    if "Операция" in params:
        WHERE.append("Download_Organization.Operation = '{}'".format(params["Операция"]))
        WHERE.append("Download_Organization.Download = '{}'".format(params["Загрузка"]))
        JOIN.append("INNER JOIN Download_Organization ON Download_Organization.Organization = Organization.ID")
        WHERE.append("Organization.LastPhoneInfo is not NULL")

    if "Срок жизни организации" in params:
        WHERE.append("DATEDIFF(NOW(), DATE(Organization.OpenDate)) > {}".format(params["Срок жизни организации"]))

    if "Порядок" in params and params["Порядок"] == "От новых к старым":
        ORDER = "DESC"


    WHERE_STR = ""

    for cond in WHERE:

        if WHERE_STR:
            WHERE_STR += " AND "

        WHERE_STR += cond

    JOIN_STR = ""

    for cond in JOIN:

        if JOIN_STR:
            JOIN_STR += "\n"

        JOIN_STR += cond

    sql = sql.format(JOIN_STR, WHERE_STR, ORDER, params["TaskID"])

    if "Размер порции контактов" in params:
        test = params["Размер порции контактов"]
        test = int(test)
        sql += " LIMIT {}".format(test)

    res["Состояние"] = "Выполняем запрос данных"
    set_res(res)

    cur = controller.get_cursor()
    cur.execute(sql)

    if cur.rowcount == 0:
        res["Не подгружаем"] = "Нет ни одного контакта для подгрузки"
        set_res(res)
        return

    res["Осталось контактов"] = cur.rowcount
    set_res(res)

    #if "Размер порции контактов" in params:
    #    test = params["Размер порции контактов"]
    #    test = int(test)
    #    org_rec_list = cur.fetchmany(size=test)
    #    res["Осталось контактов"] -= test
    #else:

    org_rec_list = cur.fetchall()

    cnt = 0
    org_count = len(org_rec_list)
    odp_task_list = {}

    if org_count == 0:
        return

    if is_diagnistic_only:

        res["Регионы"] = {}

        for org_rec in org_rec_list:
            cnt += 1

            res["Анализ регионов"] = "{} из {}".format(cnt, org_count)

            org = Organization(org_rec, controller)

            tz = org.time_zone()

            if tz not in res["Регионы"]:
                res["Регионы"][tz] = 0

            res["Регионы"][tz] += 1

            set_res(res)

        return {"sleep": 15 * 60, "params": params}

    new_org_rec_list = []
    count = 0

    res["Состояние"] = "Проверка на закрытие организаций {} из {}".format(count, len(org_rec_list))
    res["Закрыто организаций"] = 0
    res["Проверено в ЕГРЮЛ"] = 0
    set_res(res)

    try:
        cur.close()
    except:
        pass

    cur = controller.get_cursor()

    for org_rec in org_rec_list:
        count += 1
        res["Состояние"] = "Проверка на закрытие организаций {} из {}".format(count, len(org_rec_list))
        set_res(res)

        org_rec["org_obj"] = Organization.get_by_id(controller, org_rec["ID"])

        if org_rec["org_obj"].egrul_check() != None:
            res["Проверено в ЕГРЮЛ"] += 1

        if org_rec["org_obj"].is_closed() != None:
            res["Закрыто организаций"] += 1

            try:
                Call.create(
                    controller,
                    org_rec["ID"],
                    bank_id,
                    our_project_id,
                    state="Организация закрыта",
                    task_id=params["TaskID"]
                )

            except Exception as e:
                print(e)

            cur = controller.get_cursor()
            continue

        new_org_rec_list.append(org_rec)

    res.pop("Состояние")

    set_res(res)
    org_rec_list = new_org_rec_list

    up_type = ""

    if "Тип организации" in params:
        up_type = params["Тип организации"]

    reason = """Задача подгрузки <a href="../task/task.py?id={}">{}</a>""".format(params["TaskID"], params["TaskID"])

    upload = Upload.create(controller, up_type, reason)
    res["Номер подгрузки"] = upload.id()
    upload_list = []
    cnt = 0

    res["Состояние"] = "Проверка на сотовые номера {} из {}".format(count, len(org_rec_list))
    res["Нет ни одного номера сотового"] = 0
    set_res(res)

    new_org_rec_list = []
    count = 0

    for org_rec in org_rec_list:
        count += 1
        res["Состояние"] = "Проверка на сотовые номера {} из {}".format(count, len(org_rec_list))

        org_id = org_rec["ID"]

        org = Organization.get_by_id(controller, org_id)
        last_contact_person = org.last_contact_person()
        phone_list = last_contact_person.get_all_valid_phones(ContactInfo.phone_is_notsot)

        if len(phone_list) == 0:
            res["Нет ни одного номера сотового"] += 1

            try:
                call = Call.create(controller, org_rec["ID"], bank_id, our_project_id,
                                   state="Нет ни одного номера сотового",
                                   task_id=params["TaskID"])

                upload.link_to_call_and_organization(call.id(), org_rec["ID"])
            except Exception as e:
                print(e)

            continue

        new_org_rec_list.append(org_rec)

    org_rec_list = new_org_rec_list

    if "Постепенная подгрузка" in params and params["Постепенная подгрузка"]:
        Task.set_task("Оповещение подгрузки",
                      {"message": "Началась постепенная подгрузка {} по проекту {}".format(upload.id(),
                                                                                           project_name)})
    else:
        try:

            type = ""

            try:
                if "Операция" in params:
                    type = params["Операция"]
            except:
                pass

            message = "Началась подгрузка {} по проекту {} {}. Проверено в ЕГРЮЛ {} закрыто {}". \
                format(upload.id(), project_name, type, res["Проверено в ЕГРЮЛ"], res["Закрыто организаций"])

            Task.set_task("Оповещение подгрузки",
                          {"message": message})
        except:
            pass

    checked_org_list = []

    if "Проект" in params and "ОДП" in project.params() and "СтратегияПроверки" in project.params()["ОДП"] and \
            project.params()["ОДП"]["СтратегияПроверки"] == "Последователная":

        res["Контактов в ОДП"] = 0

        for org_rec in org_rec_list:

            #cnt += 1
            #res["Проверка на ОДП"] = "{} из {}".format(cnt, org_count)
            #set_res(res)

            #try:
            #    in_odp = bank.is_in_odp(org_rec["INN"])
            #except:
            #    try:
            #        bank.odp_delay()
            #    except:
            #        pass

            #    try:
            #        in_odp = bank.is_in_odp(org_rec["INN"])
            #    except Exception as e:
            #        res["Ошибки при проверке на ОДП"] += str(e)

            #if in_odp:

            #    try:
            #        call = Call.create(controller, org_rec["ID"], bank_id, our_project_id, state="ОДП при подгрузке",
            #                           task_id=params["TaskID"])

            #        upload.link_to_call_and_organization(call.id(), org_rec["ID"])
            #    except Exception as e:
            #        print(e)

            #    res["Контактов в ОДП"] = res["Контактов в ОДП"] + 1
            #    set_res(res)

            #    print("Организация {} в ОПД".format(org_rec["INN"]))
            #    continue

            org = org_rec["org_obj"]

            last_contact_person = org.last_contact_person()
            last_contact_info = org.last_contact_info()

            checked_org_list.append({
                "rec": org_rec,
                "obj": org,
                "last_contact_person": last_contact_person,
                "last_contact_info": last_contact_info,
                "contact_info_list": last_contact_person.get_all_valid_phones(ContactInfo.phone_is_notsot)[:3]
            })

            #try:
            #    bank.odp_delay()
            #except:
            #    pass

    else:

        for org_rec in org_rec_list:

            cnt += 1
            res["Запуск проверки на ОДП"] = "{} из {}".format(cnt, len(org_rec_list))
            set_res(res)

            #task_id = Task.find_by_full_data(controller, "Проверка на ОДП", {"Bank": bank.name(), "INN": org_rec["INN"]})
            task_id = None

            if task_id:
                odp_task_list[org_rec["INN"]] = task_id
                continue

            last_contact_info = org_rec["org_obj"].last_contact_info()

            odp_task_list[org_rec["INN"]] = Task.set_task_impl(
                controller,
                "Проверка на ОДП",
                json.dumps({
                    "Bank": bank.name(),
                    "INN": org_rec["INN"],
                    "Phone": last_contact_info.value()
                })
            )

        res["Контактов в ОДП"] = 0
        cnt = 0
        set_res(res)

        for org_rec in org_rec_list:

            cnt += 1
            res["Проверка на ОДП"] = "{} из {}".format(cnt, len(org_rec_list))
            set_res(res)

            task_state = "Выполняется"

            while task_state != "Завершена":
                task = Task.get_by_id(controller, odp_task_list[org_rec["INN"]])
                task_state = task.state()

                if task_state != "Завершена":
                    print("Ожидаем проверку организации с ИНН {} на ОДП, задача {}".
                          format(org_rec["INN"], bank.name(), odp_task_list[org_rec["INN"]]))
                    time.sleep(5)

            odp_state = task.get_res()

            if odp_state and "Состояние" in odp_state and odp_state["Состояние"] == "ОДП":

                try:
                    call = Call.create(controller, org_rec["ID"], bank_id, our_project_id, state="ОДП при подгрузке",
                                       task_id=params["TaskID"])

                    upload.link_to_call_and_organization(call.id(), org_rec["ID"])
                except Exception as e:
                    print(e)

                res["Контактов в ОДП"] = res["Контактов в ОДП"] + 1
                set_res(res)

                print("Организация {} в ОПД".format(org_rec["INN"]))
                continue

            org_id = org_rec["ID"]

            org = Organization.get_by_id(controller, org_id)

            last_contact_person = org.last_contact_person()
            last_contact_info = org.last_contact_info()

            checked_org_list.append({
                "rec": org_rec,
                "obj": org,
                "last_contact_person": last_contact_person,
                "last_contact_info": last_contact_info,
                "contact_info_list": last_contact_person.get_all_valid_phones(ContactInfo.phone_is_notsot)[:3]
            })

    if "Блокировать контакты" in params and params["Блокировать контакты"]:

        cnt = 0
        org_count = len(checked_org_list)
        block_task_list = {}

        for org in checked_org_list:

            cnt += 1
            res["Запуск блокировки контактов"] = "{} из {}".format(cnt, org_count)
            set_res(res)

            inn = org["obj"].inn()
            block_task_list[inn] = Task.set_task_impl(controller, "Блокировка контактов в сбербанке",
                                                      json.dumps({"INN": inn,
                                                                  "Phone": org["last_contact_info"].value()}))
        cnt = 0
        blocked_org_list = []

        res["Заблокировано другим партнером"] = 0
        set_res(res)

        for org in checked_org_list:

            cnt += 1
            res["Проверка блокировки"] = "{} из {}".format(cnt, org_count)
            set_res(res)

            task_state = "Выполняется"
            inn = org["obj"].inn()

            while task_state != "Завершена":
                task = Task.get_by_id(controller, block_task_list[inn])
                task_state = task.state()

                if task_state != "Завершена":
                    print("Ожидаем блокировку организации с ИНН {}, задача {}".
                          format(inn, bank.name(), odp_task_list[inn]))
                    time.sleep(5)

            block_res = task.get_res()

            if "result" in block_res and block_res["result"] != "Заблокирован":

                try:
                    call = Call.create(controller, org["rec"]["ID"], bank_id, our_project_id, state="ЗДП при подгрузке",
                                       task_id=params["TaskID"])

                    upload.link_to_call_and_organization(call.id(), org["rec"]["ID"])
                except Exception as e:
                    print(e)

                res["Заблокировано другим партнером"] = res["Заблокировано другим партнером"] + 1
                set_res(res)

                continue

            org["block_res"] = block_res["data"]

            blocked_org_list.append(org)

        checked_org_list = blocked_org_list

    res["Невалидных телефонов"] = 0

    for org in checked_org_list:

        if ContactInfo.phone_is_invalid(org["last_contact_info"].value()):
            call = Call.create(controller, org["obj"].id(), bank_id, our_project_id, state="Невалидный телефон",
                               task_id=params["TaskID"])

            upload.link_to_call_and_organization(call.id(), org_rec["ID"])
            res["Невалидных телефонов"] += 1
            continue

        phones = []

        if "contact_info_list" in org:
            for contact_info in org["contact_info_list"]:
                phones.append(ContactInfo.normalize_phone(contact_info.value()))
        else:
            phones.append(ContactInfo.normalize_phone(org["last_contact_info"].value()))

        upload_list.append(
            {
                "external_id": org["obj"].id(),
                "name": org["last_contact_person"].surname() + " " + org["last_contact_person"].name() + " " +
                        org["last_contact_person"].middlename(),
                "phones": phones,
                "address": org["obj"].address(),
                "post": "Директор",
                "city": org["obj"].name(),
                "tags": ["Подгрузка {} {}".format(upload.id(), up_type)]
            }
        )

    if "Постепенная подгрузка" in params and params["Постепенная подгрузка"]:
        for org in upload_list:
            call = Call.create(controller, org["external_id"], bank_id, our_project_id,
                               task_id=params["TaskID"])
            upload.link_to_call_and_organization(call.id(), org["external_id"])

        return

    skorozvon = Skorozvon()

    part_len = 1000

    project_id = None

    if "Проект" in params:
        project_params = project.params()

        if "Скорозвон" in project_params and "project_id" in project_params["Скорозвон"]:
            project_id = project_params["Скорозвон"]["project_id"]

    if project_id is None:
        Task.set_task("Оповещение проблем", {"message": "Не удалось определить идентификатор проекта скорозвона"})
        raise Exception("Не удалось определить идентификатор проекта скорозвона")

    count = 0

    upload_log = open("upload_log.txt", "w")

    for i in range(0, len(upload_list) // part_len + 1):

        part = upload_list[i * part_len:(i + 1) * part_len]

        wait = True

        while wait:
            try:

                try:
                    open("mass_load/{}_{}.txt".format(upload.id(), up_type), "a").write(
                        datetime.strftime(datetime.now(), "%Y.%m.%d %H.%M.%S") + str(part))
                except:
                    pass

                ext_upload_id = skorozvon.mass_load(part, project_id)
                wait = False
            except:
                continue

        wait = True

        while wait:
            if skorozvon.is_import_complete(ext_upload_id):
                wait = False
                continue

            time.sleep(5)

        # попробуем еще раз, если поучили ошибку
        try:
            ext_org_list = skorozvon.get_ext_org_list(ext_upload_id)
        except:
            print("Ошибка получения результата массовой загрузки {}".format(ext_upload_id))
            ext_org_list = skorozvon.get_ext_org_list(ext_upload_id)

        count += len(ext_org_list)

        upload_log.write("uploaded:\n")

        for ext_org in ext_org_list:

            upload_log.write("{}\n".format(ext_org["external_id"]))

            call = Call.create(controller, ext_org["external_id"], bank_id, our_project_id, ext_org["id"],
                               task_id=params["TaskID"])
            upload.link_to_call_and_organization(call.id(), ext_org["external_id"])

            if "Блокировать контакты" in params and params["Блокировать контакты"]:
                for org in checked_org_list:

                    if str(org["obj"].id()) == str(ext_org["external_id"]):
                        cur = controller.get_cursor()

                        SQL = "INSERT INTO Call_Bank SET Call_ = {}, Data = '{}', Bank = {}". \
                            format(call.id(), str(org["block_res"]).replace("'", '"'), bank.id())
                        cur.execute(SQL)
                        controller.save_changes()

    upload_log.write("to upload:\n")

    for org in upload_list:
        upload_log.write("{}\n".format(org["external_id"]))

    upload_log.close()

    res["Подгружено в скорозвон"] = "{} из {}".format(count, len(upload_list))
    set_res(res)

    try:
        type = ""

        try:
            if "Операция" in params:
                type = params["Операция"]
        except:
            pass

        Task.set_task("Оповещение подгрузки",
                      {"message": "Подгрузка {} закончена, проект {} {}".format(upload.id(), project_name, type)})
    except:
        pass

    upload.set_state("Завершено")

    if "Размер порции контактов" in params:
        return {"sleep": 1*60, "params": params}

