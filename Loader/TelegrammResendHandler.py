import telebot


def bot_handler_loop(bot_id):

    while True:

        try:
            bot = telebot.TeleBot(bot_id)

            @bot.message_handler(commands=['chat_id'])
            def chat_id(message):
                bot.send_message(message.from_user.id, str(message))

            @bot.message_handler(content_types=['document'])
            def add_file_to_load(message):
                bot.send_message(message.from_user.id, "Получили файл {}".format(message.document.file_name))

                file_info = bot.get_file(message.document.file_id)
                downloaded_file = bot.download_file(file_info.file_path)
                bot.send_document(401560380, (message.document.file_name, downloaded_file))
                bot.send_document(171495334, (message.document.file_name, downloaded_file))

            bot.polling(interval=2)

        except Exception as e:
            print(str(e))
