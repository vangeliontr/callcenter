import openpyxl
import os
import re

from datetime import datetime

from Tools.settings import GLOBAL_URL
from Loader.FormatDetector import FormatDetector, EmpiricalFormatDetector
from DataBase.DBController import DBController
from DataBase.Download import Download
from DataBase.Organization import Organization
from DataBase.DownloadOrganization import DownloadOrganization
from DataBase.Region import Region
from DataBase.ContactPerson import ContactPerson
from DataBase.ContactInfo import ContactInfo
from DataBase.Task import Task


def normalize_phone(phone_original):

    phone = phone_original

    if not phone:
        return phone_original

    phone = re.sub(r'[^0-9]+', r'', phone)

    if phone[0] == "8" or phone[0] == "7":
        phone = "+7" + phone[1:]

    if phone[0] == "9":
        phone = "+7" + phone

    if len(phone) == 12:
        return phone

    return phone_original


def phone_is_invalid(phone):

    if not phone:
        return True

    if phone == 'None':
        return True

    phone = re.sub(r'[^0-9]+', r'', phone)

    if not phone:
        return True

    if phone[0] == "8" or phone[0] == "7":
        phone = "+7" + phone[1:]

    if phone[0] == "9":
        phone = "+7" + phone

    if len(phone) == 12:
        return False

    return True


def is_empty_row(row):

    is_empty = True

    for elem in row:
        if elem is not None:
            is_empty = False
            break

    return is_empty


def load_file_to_db(params, set_res):

    controller = DBController()

    # Если не поменять расширение, то библиотека Excel будет писать ошибку
    if not os.path.isfile("../Files/{}.xlsx".format(params["file"]["Идентификатор"])):
        os.rename("../Files/{}".format(params["file"]["Идентификатор"]),
                  "../Files/{}.xlsx".format(params["file"]["Идентификатор"]))

    wb = openpyxl.load_workbook(filename="../Files/{}.xlsx".format(params["file"]["Идентификатор"]))
    sheet = wb.get_active_sheet()
    data = sheet.values

    reg_date = None
    download_type = None
    download_comment = None

    if "source" not in params:
        params["source"] = None

    row_count = sheet.max_row

    try:
        if params["is_rich"]:
            format = FormatDetector.detect_format(data)
            row_count -= 1
        else:

            reg_date = params["file"]["Имя"][3:8]

            if reg_date[3:5] == "12":
                reg_date = "2019-{}-{}".format(reg_date[3:5], reg_date[0:2])
            else:
                reg_date = "2020-{}-{}".format(reg_date[3:5], reg_date[0:2])

            if params["file"]["Имя"][:2] == 'ip':
                download_type = 'ИП'
            else:
                download_type = 'ООО'

            download_comment = params["file"]["Имя"][8:params["file"]["Имя"].rfind(".")]

            format = FormatDetector.detect_format(data, reg_date)
            #format = EmpiricalFormatDetector.detect_format(data, reg_date)

            if "ResetData" in format and format["ResetData"]:
                data = sheet.values
            else:
                row_count -= 1
    except Exception as e:

        task_link = f"{GLOBAL_URL}/cgi-bin/task/task.py?id={params['TaskID']}"

        Task.set_task("Оповещение загрузки",
                      {"message": "Ошибка определения формата файла {}: {}\n{}".format(params["file"]["Имя"],
                                                                                       str(e),
                                                                                       task_link)})

        raise e

    row_num = 0
    res = {}

    # Создадим загрузку
    download = Download.create(controller=controller, file_list=[params["file"]["Имя"]], state="Загрузка",
                               reg_date=reg_date, receive_date=datetime.now(), type=download_type,
                               comment=download_comment, source=params["source"])

    res["Номер загрузки"] = download.id()
    set_res(res)

    try:
        task_link = f"{GLOBAL_URL}/cgi-bin/task/task.py?id={params['TaskID']}"

        download_type = "без обогащения"

        if params["is_rich"]:
            download_type = "с обогащением"

        #Task.set_task("Оповещение загрузки",
        #              {"message": "Старт процесса загрузки {} {} {}".
        #              format(download.id(), download_type, task_link)})
    except:
        pass

    upload_safe_numbers = False

    for row in data:

        row_num += 1
        res["Обрабатывается"] = "Строка {} из {}".format(row_num, row_count)
        set_res(res)

        if is_empty_row(row):

            if "Пустых строк" not in res:
                res["Пустых строк"] = 0

            res["Пустых строк"] += 1

            continue

        # договорились пока работать только с директорами - это Должностное лицо
        if format["Должность"](row) and format["Должность"](row) != 'Должностное лицо' and \
                format["Должность"](row) != 'Директор':

            if "Не должностных лиц" not in res:
                res["Не должностных лиц"] = 0

            res["Не должностных лиц"] += 1

            continue

        #В случае когда нам приходят ООО - нужно грузить собственные номера, понять это можно по совпадению ИНН ЮЛ и ФЛ
        if not upload_safe_numbers and format["ИНН"](row) != format["ИННФЛ"](row):
            upload_safe_numbers = True

        inn = format["ИНН"](row).replace('.0', '')

        if len(inn) == 11 or len(inn) == 9:
            inn = "0" + inn

        org_type = "ИП"

        if len(inn) == 10:
            org_type = "ООО"

        create_date = format["ДатаОткрытия"](row)

        if create_date is not None:
            create_date = create_date[:10]

        organization = Organization.find_by_inn_open_date(controller, inn, create_date)

        region = None

        try:
            region = Region.detect_region(controller, format["Адрес"](row), inn, format["ОГРН"](row))
        except:
            pass

        if organization is not None:
            download_organization = DownloadOrganization(controller, download, organization)

            if organization.download() == download.id():
                download_organization.set_res("Повтор", "Дубль")
            else:
                download_organization.set_res("Повтор", "Не уникальный")
        else:

            organization = Organization.create(controller, org_type, format["Название"](row), inn, create_date,
                                               format["Адрес"](row), format["ОГРН"](row), download.id())

            try:
                reg_id = organization.set_reg_info(create_date, format["ОГРН"](row))
                organization.set_last_reg_info(reg_id)
            except:
                pass

            if region:
                organization.link_organization_to_region(region)

            download_organization = DownloadOrganization(controller, download, organization)
            download_organization.set_res("Повтор", "Уникальный")

        birth_date = format["ДатаРождения"](row)
        contact_person = None

        try:
            format["Фамилия"](row)
            format["Имя"](row)
            format["Отчество"](row)
        except:
            if "Пустых контактных лиц" not in res:
                res["Пустых контактных лиц"] = 0

            res["Пустых контактных лиц"] += 1

            continue


        if birth_date is not None:
            birth_date = create_date[:10]

            contact_person = ContactPerson.find(controller, format["Фамилия"](row), format["Имя"](row),
                                                format["Отчество"](row), birth_date)

        if contact_person is None:
            contact_person = ContactPerson.find_with_org(controller, format["Фамилия"](row), format["Имя"](row),
                                                         format["Отчество"](row), organization.id())

        if contact_person is None:
            contact_person = ContactPerson.create(controller, format["Фамилия"](row), format["Имя"](row),
                                                  format["Отчество"](row), birth_date, format["ТипДокумента"](row),
                                                  format["НомерДокумента"](row), format["ИННФЛ"](row))

        phone_list = format["Телефон"](row).split("\n")

        for phone in phone_list:
            #phone = format["Телефон"](row)
            phone_contact_info = None

            if phone[len(phone) - 2: len(phone)] == ".0":
                phone = phone[:len(phone) - 2]

            if phone_is_invalid(phone):

                if str(phone) == "":
                    download_organization.set_res("Телефон", "Пустой")
                else:
                    download_organization.set_res("Телефон", "Невалидный")

            else:
                phone = normalize_phone(phone)
                download_organization.set_res("Телефон", "Валидный")

                phone_contact_info = ContactInfo.find(controller, contact_person, "Телефон", phone)

                if phone_contact_info is None:

                    # Если не "пустышка", то попробуем осознать не первый ли валидный номер нам пришел,
                    # если контакт уже есть в БД
                    if not params["is_rich"] and download_organization.get_res("Повтор") == "Не уникальный":
                        if not contact_person.have_valid_contact_info(phone_is_invalid):
                            download_organization.set_res("Повтор", "Первый валидный телефон")

                    phone_contact_info = ContactInfo.create(controller, contact_person, "Телефон", phone)

            organization.link_contact(contact_person, phone_contact_info)

        if download_organization.get_res("Повтор") == "Не уникальный":
            if "Не уникальных" not in res:
                res["Не уникальных"] = 0

            res["Не уникальных"] += 1
        elif download_organization.get_res("Повтор") == "Дубль":
            if "Дублей" not in res:
                res["Дублей"] = 0

            res["Дублей"] += 1
        elif download_organization.get_res("Повтор") == "Первый валидный телефон":
            if "Первый валидный телефон" not in res:
                res["Первый валидный телефон"] = 0

            res["Первый валидный телефон"] += 1

            download_organization.set_operation("Подгрузка")
        else:
            if download_organization.get_res("Телефон") == "Валидный":
                if "На подгрузку" not in res:
                    res["На подгрузку"] = 0

                res["На подгрузку"] += 1

                download_organization.set_operation("Подгрузка")
            else:
                if params["is_rich"]:

                    if "На обогащение" not in res:
                        res["На обогащение"] = 0

                    res["На обогащение"] += 1

                    download_organization.set_operation("Обогащение")
                else:

                    if "Не валидный телефон" not in res:
                        res["Не валидный телефон"] = 0

                    res["Не валидный телефон"] += 1

        download_organization.save()
        controller.save_changes()

    res["Задачи подгрузки"] = ""

    #if not params["is_rich"]:
    #upload_safe_numbers = True

    #if upload_safe_numbers:

        # СБЕР
        #sber_task_id = Task.set_task("Подгрузка", {"Загрузка": download.id(),
        #                                           "Проект": "Новореги Сбербанк",
        #                                           "Регионы": "Проекта",
        #                                           "Операция": "Подгрузка",
        #                                           "Блокировать контакты": True})
        #res["Задачи подгрузки"] += """, <a href="./task.py?id={}">Сбер {}</a>""".format(sber_task_id, sber_task_id)

        # ПСБ
        #psb_uniq_task_id = Task.set_task("Подгрузка", {"Загрузка": download.id(),
        #                                               "Проект": "Новореги ПромСвязьБанк",
        #                                               "Регионы": "Проекта",
        #                                               "Операция": "Подгрузка"})
        #res["Задачи подгрузки"] += """, <a href="./task.py?id={}">ПромСвязьБанк {}</a>""".format(psb_uniq_task_id,
        #                                                                                         psb_uniq_task_id)

    # РСХБ
    #rshb_uniq_task_id = Task.set_task("Подгрузка", {"Загрузка": download.id(),
    #                                                "Проект": "Новореги РосСельхозБанк",
    #                                                "Регионы": "Проекта",
    #                                                "Операция": "Подгрузка"})
    #res["Задачи подгрузки"] += """, <a href="./task.py?id={}">РосСельхозБанк {}</a>""".format(rshb_uniq_task_id,
    #                                                                                          rshb_uniq_task_id)

    # Холодные ТФ
    #ctf_uniq_task_id = Task.set_task("Подгрузка в банк", {
    #    "Загрузка": download.id(),
    #    "Проект": "Холодные ТФ",
    #    "Операция": "Подгрузка"
    #})

    #res["Задачи подгрузки"] += """<a href="./task.py?id={}">Холодные ТФ {}</a>"""\
    #                           .format(ctf_uniq_task_id, ctf_uniq_task_id)

    # ТФ
    #tf_task_id = Task.set_task("Подгрузка", {"Загрузка": download.id(),
    #                                         "Проект": "Новореги ТФ",
    #                                         "Регионы": "Проекта",
    #                                         "Операция": "Подгрузка"})
    #res["Задачи подгрузки"] += """<a href="./task.py?id={}">Новореги ТФ {}</a>""".format(tf_task_id, tf_task_id)

    # ВТБ
    #vtb_task_id = Task.set_task("Подгрузка", {"Загрузка": download.id(),
    #                                          "Проект": "Новореги ВТБ",
    #                                          "Регионы": "Проекта",
    #                                          "Операция": "Подгрузка"})

    #res["Задачи подгрузки"] += """, <a href="./task.py?id={}">Новореги ВТБ {}</a>""".format(vtb_task_id, vtb_task_id)

    # Точка
    tochka_task_id = Task.set_task("Подгрузка", {"Загрузка": download.id(),
                                                 "Проект": "Действующие точка",
                                                 "Регионы": "Проекта",
                                                 "Размер порции контактов": 1000,
                                                 "Операция": "Подгрузка"})

    res["Задачи подгрузки"] += """, <a href="./task.py?id={}">Точка {}</a>""".format(tochka_task_id, tochka_task_id)

    #if params["is_rich"]:
    #    rich_task_id = Task.set_task("Обогащение загрузки", {"Download": download.id()})
    #    res["Задача обогащения"] = """<a href="./task.py?id={}">Обогащение {}</a>""".format(rich_task_id, rich_task_id)

    set_res(res)
    download.set_state("Завершено", download_comment)


if __name__ == "__main__":

    load_file_to_db({
        "file": {"Идентификатор": "123", "Имя": "test"},
        "is_rich": True
    }, lambda x: None)
