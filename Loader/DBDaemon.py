#!/usr/bin/env python3

import sys
import json

sys.path.append("../")

from LoadFileToDB import load_data_to_db
from Tools.Daemon import Daemon

conf = json.loads(open("db_daemon_settings.txt", "r").read())

Daemon.run(load_data_to_db, conf)
