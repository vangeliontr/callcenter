import requests
import telebot
import uuid

from io import BytesIO
from DataBase.Task import Task


def send_message(params, _set_res):

    url = "https://api.telegram.org/bot{}/sendMessage?chat_id={}&text={}".format(params["bot_id"],
                                                                                 params["chat_id"],
                                                                                 params["message"])

    requests.post(url)

def send_document(params, _set_res):
    url = f"https://api.telegram.org/bot{params['bot_id']}/sendDocument"

    #file_obj = BytesIO(params["document"].encode("cp1251"))
    #file_obj.name = params["caption"]
    
    requests.post(url, params={"chat_id": params["chat_id"]},
                  files={'document': params["document"]})

def send_message_to_user(params, _set_res):

    url = "https://api.telegram.org/bot{}/sendMessage?chat_id={}&text={}".format(params["bot_id"],
                                                                                 params["user"],
                                                                                 params["message"])

    requests.post(url)


def bot_handler_loop(bot_id, is_rich=True):

    pre_path = "../Files/"

    while True:

        try:
            bot = telebot.TeleBot(bot_id)

            @bot.message_handler(commands=['chat_id'])
            def chat_id(message):
                bot.send_message(message.from_user.id, str(message))

            @bot.message_handler(content_types=['document'])
            def add_file_to_load(message):
                bot.send_message(message.from_user.id, "Получили файл {} на загрузку".format(message.document.file_name))
                file_info = bot.get_file(message.document.file_id)
                downloaded_file = bot.download_file(file_info.file_path)

                file_id = str(uuid.uuid4())

                open(pre_path + file_id, "wb").write(downloaded_file)

                source = message.from_user.username

                if not source:
                    source = message.from_user.first_name

                link = "../download/get_download_file.py?id={}&name={}".format(file_id, message.document.file_name)

                Task.set_task("Загрузка файла в БД", {"file": {"Идентификатор": file_id,
                                                               "Имя": message.document.file_name,
                                                               "Скачать": '<a href="{}">{}</a>'.format(link, message.document.file_name)},
                                                      "is_rich": is_rich,
                                                      "source": source})

            bot.polling(interval=2)

        except Exception as e:
            print(str(e))
