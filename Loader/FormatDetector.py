import re
from datetime import datetime


class FormatDetector:

    MAP = {
        "Фамилия": [["Название компании", {"Тип": "Функция", "Значение": "Взять вторую часть"}],
                    ["ФИО руководителя", {"Тип": "Функция", "Значение": "Взять первую часть"}],
                    "Фамилия"],
        "Имя": [["Название компании", {"Тип": "Функция", "Значение": "Взять третью часть"}],
                ["ФИО руководителя", {"Тип": "Функция", "Значение": "Взять вторую часть"}],
                "Имя"],
        "Отчество": [["Название компании", {"Тип": "Функция", "Значение": "Взять четвертую часть"}],
                     ["ФИО руководителя", {"Тип": "Функция", "Значение": "Взять третью часть"}],
                     "Отчество"],
        "ИНН": ["ИНН", "ИНН компании"],
        "ИННФЛ": ["ИНН", "ИНН физика", "ИНН ФЛ", "ИННФЛ руководителя"],
        "ДатаОткрытия": ["Дата присвоения ОГРНИП", "Дата статуса ЮЛ", "Дата регистрации"],
        "ДатаРождения": ["Дата рождения"],
        "ТипДокумента": ["Документ, удостоверяющий личность", "Вид документа, удостоверяющего личность"],
        "НомерДокумента": [["Серия документа", "Номер документа", {"Тип": "Функция", "Значение": "Удалить пробелы"}],
                           ["Серия", "Номер", {"Тип": "Функция", "Значение": "Удалить пробелы"}],
                           ["Серия документа, удостоверяющего личность",
                            "Номер документа, удостоверяющего личность",
                            {"Тип": "Функция", "Значение": "Удалить пробелы"}]],
        "Адрес": ["Адрес", "Адрес места жительства", "Адрес регистрации", "Юридический адрес",
                  ["Индекс", "Регион", "Район", "Город", "Населенный пункт", "Улица", "Дом", "Корпус", "Квартира"]],
        "ОГРН": ["ОГРН", "ОГРНИП", "ОГРН ЮЛ"],
        "Название": [[{"Тип": "Строка", "Значение": "ИП"}, "Фамилия", "Имя", "Отчество"], "Наименование ЮЛ",
                     "Наименование", "Название компании", "Название"],
        "Телефон": ["Номер телефона", "Телефон", "Телефон ФЛ", "Мобильный телефон", "phone"],
        "Должность": ["Вид объекта к которому относятся сведения"]
    }

    CRITICAL_ELEMS = ["Фамилия", "Имя", "Отчество", "ИНН", "Телефон"]

    @staticmethod
    def get_value_safe(value):

        if value is None:
            return ""

        return str(value)

    @staticmethod
    def get_safe_normilize_part(value, part_num):

        value = re.compile('[^а-яА-Я ]').sub('', value)

        value = value.split(" ")

        if len(value) > part_num:
            res = value[part_num]
            return res[:1].upper() + res[1:].lower()

        return ""

    @staticmethod
    def detect_format(data, open_date=None):

        first_row = ()

        for row in data:

            for elem in row:
                if elem is None:
                    first_row = first_row + ("",)
                else:
                    first_row = first_row + (elem.strip(),)

            break

        return FormatDetector.detect_format_impl(first_row, open_date)

    @staticmethod
    def detect_format_impl(first_row, open_date=None):

        res = {}

        for elem in FormatDetector.MAP:
            res[elem] = None

        for elem in FormatDetector.MAP:

            for name in FormatDetector.MAP[elem]:

                if isinstance(name, str):

                    num = 0

                    for col in first_row:

                        if name == col:
                            res[elem] = lambda source, num=num: FormatDetector.get_value_safe(source[num])
                            break

                        num += 1

                if isinstance(name, list):
                    name_list = name

                    res_list = []
                    func_list = []

                    for name in name_list:

                        if isinstance(name, str):

                            num = 0

                            for col in first_row:

                                if name == col:
                                    res_list.append(lambda source, num=num: FormatDetector.get_value_safe(source[num]))
                                    break

                                num += 1

                        if isinstance(name, dict):

                            if name["Тип"] == "Строка":
                                data = name["Значение"]
                                res_list.append(lambda source, data=data: data)
                            elif name["Тип"] == "Функция":
                                func_list.append(name["Значение"])

                    if len(name_list) == len(res_list) + len(func_list):

                        res_lambda = lambda source, res_list=res_list: ' '.join([res(source) for res in res_list])

                        for func in func_list:

                            if func == "Удалить пробелы":
                                res_lambda = lambda source, res_lambda=res_lambda: res_lambda(source).replace(" ", "")

                            if func == "Взять первую часть":
                                res_lambda = lambda source, res_lambda=res_lambda: res_lambda(source).split(" ")[0]

                            if func == "Взять вторую часть":
                                res_lambda = lambda source, res_lambda=res_lambda: res_lambda(source).split(" ")[1]

                            if func == "Взять третью часть":
                                res_lambda = lambda source, res_lambda=res_lambda: res_lambda(source).split(" ")[2]

                            if func == "Взять четвертую часть":
                                res_lambda = lambda source, res_lambda=res_lambda: res_lambda(source).split(" ")[3] if len(res_lambda(source).split(" ")) > 3 else ''

                        res[elem] = res_lambda

        for elem in res:
            if res[elem] is None:

                if elem == "ДатаОткрытия" and open_date is not None:
                    res[elem] = lambda source: open_date
                else:
                    res[elem] = lambda source: ""

                if elem in FormatDetector.CRITICAL_ELEMS:
                    raise Exception("Ошибка при определении формата: не определено поле '{}'".format(elem))

        return res


class EmpiricalFormatDetector:

    @staticmethod
    def detect_format(data, open_date):

        file_format = []

        first_row = ()

        while not file_format:
            row = next(data)

            is_all_null = True

            for elem in row:

                if elem is not None:
                    is_all_null = False
                    break

            if is_all_null:
                continue

            for col in row:

                first_row = first_row + (str(col).strip(),)

                if isinstance(col, int):
                    col = str(col)

                if isinstance(col, str):

                    if col.isdigit() and (len(col) == 10 or len(col) == 12 or len(col) == 15):
                        file_format.append("ИНН")
                    elif col.isdigit() and len(col) == 13:
                        file_format.append("ОГРН")
                    elif len(col.split(',')) == 10:
                        file_format.append("АдресКЛАДР")
                    elif col[0:2].isdigit() and col[2:5] == " - ":
                        file_format.append("Адрес")
                    elif len("".join(re.findall('\d+', col))) > 6:
                        file_format.append("Телефон")
                    else:
                        file_format.append("Строка")

                elif isinstance(col, float):
                    file_format.append("Дата")
                elif isinstance(col, datetime):
                    file_format.append("Дата")
                else:
                    file_format.append("Строка")

        is_format_discription_exist = True

        for elem in file_format:

            if elem != 'Строка':
                is_format_discription_exist = False
                break

        if is_format_discription_exist:
            auto_detected_format = FormatDetector.detect_format_impl(first_row)
            auto_detected_format["ДатаОткрытия"] = lambda source: open_date
            return auto_detected_format

        if file_format[:6] == ['Телефон', 'ИНН', 'Строка', 'Строка', 'Строка', 'АдресКЛАДР']:
            return {
                "Фамилия": (lambda source: FormatDetector.get_value_safe(source[2])),
                "Имя": (lambda source: FormatDetector.get_value_safe(source[3])),
                "Отчество": (lambda source: FormatDetector.get_value_safe(source[4])),
                "ИНН": (lambda source: FormatDetector.get_value_safe(source[1])),
                "ИННФЛ": (lambda source: FormatDetector.get_value_safe(source[1])),
                "ДатаОткрытия": (lambda source: open_date),
                "ДатаРождения": (lambda source: None),
                "ТипДокумента": (lambda source: None),
                "НомерДокумента": (lambda source: None),
                "Адрес": (lambda source: FormatDetector.get_value_safe(source[5])),
                "ОГРН": (lambda source: None),
                "Название": (lambda source: "ИП {} {} {}".format(FormatDetector.get_value_safe(source[2]),
                                                                 FormatDetector.get_value_safe(source[3]),
                                                                 FormatDetector.get_value_safe(source[4]))),
                "Телефон": (lambda source: FormatDetector.get_value_safe(source[0])),
                "Должность": (lambda source: "Директор"),
                "ResetData": True
            }
        elif file_format[:5] == ['Телефон', 'ИНН', 'Строка', 'Строка', 'Строка']:
            return {
                "Фамилия": (lambda source: FormatDetector.get_value_safe(source[2])),
                "Имя": (lambda source: FormatDetector.get_value_safe(source[3])),
                "Отчество": (lambda source: FormatDetector.get_value_safe(source[4])),
                "ИНН": (lambda source: FormatDetector.get_value_safe(source[1])),
                "ИННФЛ": (lambda source: FormatDetector.get_value_safe(source[1])),
                "ДатаОткрытия": (lambda source: open_date),
                "ДатаРождения": (lambda source: None),
                "ТипДокумента": (lambda source: None),
                "НомерДокумента": (lambda source: None),
                "Адрес": (lambda source: None),
                "ОГРН": (lambda source: None),
                "Название": (lambda source: "ИП {} {} {}".format(FormatDetector.get_value_safe(source[2]),
                                                                 FormatDetector.get_value_safe(source[3]),
                                                                 FormatDetector.get_value_safe(source[4]))),
                "Телефон": (lambda source: FormatDetector.get_value_safe(source[0])),
                "Должность": (lambda source: "Директор"),
                "ResetData": True
            }
        elif file_format[:6] == ['Строка', 'ИНН', 'Телефон']:
            return {
                "Фамилия": (lambda source: FormatDetector.get_safe_normilize_part(source[0], 0)),
                "Имя": (lambda source: FormatDetector.get_safe_normilize_part(source[0], 1)),
                "Отчество": (lambda source: FormatDetector.get_safe_normilize_part(source[0], 2)),
                "ИНН": (lambda source: FormatDetector.get_value_safe(source[1])),
                "ИННФЛ": (lambda source: FormatDetector.get_value_safe(source[1])),
                "ДатаОткрытия": (lambda source: open_date),
                "ДатаРождения": (lambda source: None),
                "ТипДокумента": (lambda source: None),
                "НомерДокумента": (lambda source: None),
                "Адрес": (lambda source: None),
                "ОГРН": (lambda source: None),
                "Название": (lambda source: "ИП {} {} {}".format(FormatDetector.get_safe_normilize_part(source[0], 0),
                                                                 FormatDetector.get_safe_normilize_part(source[0], 1),
                                                                 FormatDetector.get_safe_normilize_part(source[0], 2))),
                "Телефон": (lambda source: FormatDetector.get_value_safe(source[2])),
                "Должность": (lambda source: "Директор"),
                "ResetData": True
            }
        elif file_format[:6] == ['ИНН', 'Строка', 'Строка', 'Строка', 'Телефон']:
            return {
                "Фамилия": (lambda source: FormatDetector.get_value_safe(source[1])),
                "Имя": (lambda source: FormatDetector.get_value_safe(source[2])),
                "Отчество": (lambda source: FormatDetector.get_value_safe(source[3])),
                "ИНН": (lambda source: FormatDetector.get_value_safe(source[0])),
                "ИННФЛ": (lambda source: None),
                "ДатаОткрытия": (lambda source: open_date),
                "ДатаРождения": (lambda source: None),
                "ТипДокумента": (lambda source: None),
                "НомерДокумента": (lambda source: None),
                "Адрес": (lambda source: None),
                "ОГРН": (lambda source: None),
                "Название": (lambda source: "ИП {} {} {}".format(FormatDetector.get_value_safe(source[1]),
                                                                 FormatDetector.get_value_safe(source[2]),
                                                                 FormatDetector.get_value_safe(source[3]))),
                "Телефон": (lambda source: FormatDetector.get_value_safe(source[4])),
                "Должность": (lambda source: "Директор"),
                "ResetData": True
            }
        elif file_format[:13] == ['Телефон', 'Строка', 'Строка', 'Строка', 'Строка', 'Телефон', 'Строка', 'Строка',
                                  'Строка', 'Строка', 'Строка', 'Строка', 'Строка'] or\
             file_format[:13] == ['ИНН', 'Строка', 'Строка', 'Строка', 'Строка', 'Телефон', 'Строка', 'Строка',
                                  'Строка', 'Строка', 'Строка', 'Строка', 'Строка']:
            return {
                "Фамилия": (lambda source: FormatDetector.get_value_safe(source[2])),
                "Имя": (lambda source: FormatDetector.get_value_safe(source[3])),
                "Отчество": (lambda source: FormatDetector.get_value_safe(source[4])),
                "ИНН": (lambda source: FormatDetector.get_value_safe(source[0])),
                "ИННФЛ": (lambda source: None),
                "ДатаОткрытия": (lambda source: open_date),
                "ДатаРождения": (lambda source: None),
                "ТипДокумента": (lambda source: None),
                "НомерДокумента": (lambda source: None),
                "Адрес": (lambda source: "{}, {}, {}, {}, {}, {}, {}".format(FormatDetector.get_value_safe(source[6]),
                                                                             FormatDetector.get_value_safe(source[7]),
                                                                             FormatDetector.get_value_safe(source[8]),
                                                                             FormatDetector.get_value_safe(source[9]),
                                                                             FormatDetector.get_value_safe(source[10]),
                                                                             FormatDetector.get_value_safe(source[11]),
                                                                             FormatDetector.get_value_safe(source[12]))),
                "ОГРН": (lambda source: None),
                "Название": (lambda source: FormatDetector.get_value_safe(source[1])),
                "Телефон": (lambda source: FormatDetector.get_value_safe(source[5])),
                "Должность": (lambda source: "Директор"),
                "ResetData": True
            }
        elif file_format[:7] == ['Телефон', 'Строка', 'Строка', 'Строка', 'Строка', 'Телефон', 'Телефон'] or\
             file_format[:7] == ['ИНН', 'Строка', 'Строка', 'Строка', 'Строка', 'Телефон', 'Телефон']:
            return {
                "Фамилия": (lambda source: FormatDetector.get_value_safe(source[2])),
                "Имя": (lambda source: FormatDetector.get_value_safe(source[3])),
                "Отчество": (lambda source: FormatDetector.get_value_safe(source[4])),
                "ИНН": (lambda source: FormatDetector.get_value_safe(source[0])),
                "ИННФЛ": (lambda source: None),
                "ДатаОткрытия": (lambda source: open_date),
                "ДатаРождения": (lambda source: None),
                "ТипДокумента": (lambda source: None),
                "НомерДокумента": (lambda source: None),
                "Адрес": (lambda source: FormatDetector.get_value_safe(source[6])),
                "ОГРН": (lambda source: None),
                "Название": (lambda source: FormatDetector.get_value_safe(source[1])),
                "Телефон": (lambda source: FormatDetector.get_value_safe(source[5])),
                "Должность": (lambda source: "Директор"),
                "ResetData": True
            }
        else:
            raise Exception("Не нашли формат для файла")





