import re
import openpyxl
import os
import json
import time
import shutil
import traceback
import uuid

from datetime import datetime, timedelta
from DataBase.DBController import DBController
from DataBase.Organization import Organization
from CallCenter.Skorozvon import Skorozvon
from Bank.BankList import BankList
from DataBase.Task import Task
from DataBase.Download import Download
from DataBase.ContactInfo import ContactInfo
from DataBase.ContactPerson import ContactPerson
from DataBase.Project import Project
from DataBase.Upload import Upload
from DataBase.Call import Call


def normalize_phone(phone_original):

    phone = phone_original

    if not phone:
        return phone_original

    if phone[0] == "8" or phone[0] == "7":
        phone = "+7" + phone[1:]

    if phone[0] == "9":
        phone = "+7" + phone

    if phone[0:2] == "+7":
        phone = "+7" + re.sub(r'[^0-9]+', r'', phone[2:])

        if len(phone) == 12:
            return phone

    return phone_original


def phone_is_invalid(phone):

    if not phone:
        return True

    if phone[0] == "8" or phone[0] == "7":
        phone = "+7" + phone[1:]

    if phone[0] == "9":
        phone = "+7" + phone

    if phone[0:2] == "+7":
        phone = "+7" + re.sub(r'[^0-9]+', r'', phone[2:])

        if len(phone) == 12:
            return False

    return True


def load_exist_to_project(params, set_res):

    controller = DBController()
    skorozvon = Skorozvon()

    file_content = open("../Files/" + params["file"]["Идентификатор"], "rb").read()
    file_content = file_content.decode("utf-8")

    lines = file_content.split("\n")
    load_id = str(datetime.now())
    load_id = "ТФ POS c ИНН " + load_id[:19]

    line_num = 0
    ids = []
    invalid_phone_num = 0

    for line in lines:

        line_num += 1

        if line_num == 1 or line == "":
            continue

        set_res({
            "Обработано": "{} из {}".format(line_num - 1, len(lines) - 2),
            "Невалидный номер": "{} контактов".format(invalid_phone_num)
        })

        cols = line.split(";")

        phone = cols[7]

        if phone_is_invalid(phone):
            invalid_phone_num += 1
            continue

        phone = normalize_phone(phone)

        inn = cols[1]
        reg_date = cols[2]
        adr = cols[3]

        if adr == 'None':
            adr = None

        f = cols[4]
        i = cols[5]
        o = cols[6]

        if f == 'None':
            f = ''

        if i == 'None':
            i = ''

        if o == 'None':
            o = ''

        fio = f + " " + i + " " + o

        organization = Organization.find_by_inn_open_date(controller, inn,
                                                          reg_date[6:10] + "-" + reg_date[3:5] + "-" + reg_date[:2])

        if organization is None:
            continue

        name = cols[0]

        if params["allow_inn"]:
            name = inn + " " + name

        if params["allow_okved"]:
            name = cols[8] + " " + name

        ids.append(skorozvon.send_org({"name": fio,
                                       "post": "Директор",
                                       "phone": phone,
                                       "address": adr,
                                       "external_id": organization.id(),
                                       "business": name}, load_id))

    success = False

    while not success:
        try:
            success = True

            skorozvon.mass_append_to_project(ids, params["skorozvon_project_id"])

            if not skorozvon.is_contacts_in_project(ids, load_id):
                success = False

        except Exception as e:
            skorozvon.re_auth()
            print("Ошибка при добавалении в проект " + str(e))


def write_to_log(log, message):
    log.write("{} {}\n".format(datetime.now(), message))
    log.flush()


def send_file_to_load(controller, log, params, file_path, comment):

    try:
        pre_path = params["pre_path"]

        reg_date = datetime.now().strftime("%d.%m.%Y")
        filename = reg_date[:5] + comment + ".xlsx"
        reg_date = datetime.now().strftime("%Y-%m-%d")

        shutil.copy(file_path, pre_path + "/" + filename)

        Download.create(controller, [filename], "Поставщик_ПД", reg_date,
                        datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"),
                        filename[5:].replace(".xlsx", ""))

    except Exception as e:
        write_to_log(log, "Ошибка: {}\n{}".format(e, traceback.format_exc()))


def delay_upload(params, set_res):
    try:
        project = Project.get_by_name(params["ProjectName"])
        bank = project.bank()
        skz_client = Skorozvon()

        # Логирование
        daily_log_path = "Постепенные подгрузки/{}/{}".format(project.name(),
                                                              datetime.now().strftime("%Y.%m.%d"))

        if not os.path.exists("../Admin/{}".format(daily_log_path)):
            os.makedirs("../Admin/{}".format(daily_log_path))

        log = open("../Admin/{}/{}".format(daily_log_path,
                                           datetime.now().strftime("%H.%M.txt")), "w", encoding="1251")

        res = {}
        res["Протоколы проверок"] = """<a href="../../{}">{}</a>""".format(daily_log_path,
                                                                           datetime.now().strftime("%Y.%m.%d"))

        set_res(res)

        controller = DBController()
        cur = controller.get_cursor()

        cur.execute("""SELECT Call_Bank.Data, Organization.INN, ContactInfo.Value as Phone, Call_.ID, 
                             Call_Bank.ID as BlockID, Call_.ExtID
                       FROM Call_Bank
                       INNER JOIN Call_ ON Call_.ID = Call_Bank.Call_
                       INNER JOIN Call_Organization ON Call_Organization.Call_ = Call_.ID
                       INNER JOIN Organization ON Call_Organization.Organization = Organization.ID
                       INNER JOIN ContactInfo ON Organization.LastPhoneInfo = ContactInfo.ID
                       WHERE Call_.Project = {} AND Call_.Result is NULL""".format(project.id()))

        block_rec_list = cur.fetchall()

        for block_rec in block_rec_list:

            # Проверить, что время блокировка контакта не вышла
            if datetime.strptime(json.loads(block_rec["Data"])['block_until'][:19],
                                 "%Y-%m-%dT%H:%M:%S") > datetime.now():
                log.write("{} Блокировка по контакту {} не закончилась: {}\n".
                          format(datetime.now().strftime("%H:%M:%S"), block_rec["INN"],
                                 block_rec["Data"]))
                log.flush()
                continue

            lock_res = None

            # Если вышло - попробуем еще раз заблокировать
            try:
                lock_res = bank.lock_org(block_rec["INN"], block_rec["Phone"])
            except Exception as e:
                log.write("{} Ошибка при блокировке организации с ИНН {}: {}\n".
                          format(datetime.now().strftime("%H:%M:%S"), block_rec["INN"]), e)
                log.flush()
                continue

            if lock_res == "limit out":
                SQL = "UPDATE Call_ SET Result = 'Закончились попытки блокировки' WHERE ID = {};".format(block_rec["ID"])
                cur.execute(SQL)
                controller.save_changes()
                skz_client.remove_org(block_rec["ExtID"])

                log.write("{}: Организация с ИНН {} заблокирована больше допустимого количества раз\n".
                              format(datetime.now().strftime("%H:%M:%S"), block_rec["INN"]))
                log.flush()
                continue

            if lock_res is None:
                SQL = "UPDATE Call_ SET Result = 'Заблокирован другим партнером' WHERE ID = {};". \
                        format(block_rec["ID"])
                cur.execute(SQL)
                controller.save_changes()
                skz_client.remove_org(block_rec["ExtID"])

                log.write("{}: Организация с ИНН {} заблокирован другим партнером\n".
                              format(datetime.now().strftime("%H:%M:%S"), block_rec["INN"]))
                log.flush()
                continue

            log.write("{}: Организация с ИНН {} успешно заблокирована: {}\n".
                      format(datetime.now().strftime("%H:%M:%S"), block_rec["INN"], lock_res))
            log.flush()

            SQL = "UPDATE Call_Bank SET Data = '{}' WHERE ID = {}".format(str(lock_res).replace("'", '"'), block_rec["ID"])
            cur.execute(SQL)
            controller.save_changes()
    except:
        pass

    return {"sleep": 10}

    # Подготовка
    controller = DBController()
    skz_client = Skorozvon()
    project = Project.get_by_name(params["ProjectName"])
    bank = project.bank()
    res = {}

    project_params = project.params()
    project_id = project_params["Скорозвон"]["project_id"]

    # проверим, что надо начинать работать
    project_state = skz_client.get_project_state(project_id)

    if project_state["data"]["dialing_state"] != "active":
        set_res({"Состояние": "Проект скорозвона не активен"})
        return {"sleep": 10}

    # Логирование
    daily_log_path = "Постепенные подгрузки/{}/{}".format(project.name(),
                                                          datetime.now().strftime("%Y.%m.%d"))

    if not os.path.exists("../Admin/{}".format(daily_log_path)):
        os.makedirs("../Admin/{}".format(daily_log_path))

    log = open("../Admin/{}/{}".format(daily_log_path,
                                       datetime.now().strftime("%H.%M.txt")), "w", encoding="1251")

    try:

        res["Протоколы проверок"] = """<a href="../../{}">{}</a>""".format(daily_log_path,
                                                                           datetime.now().strftime("%Y.%m.%d"))

        # 1. Смотрим что осталось в проекта
        res["Этап обработки"] = "Оценка остатков в проекте"
        set_res(res)

        skz_org_list = skz_client.get_project_source(project_id, True)["leads"]

        log.write("{}: Контактов осталось в скорозвоне {}\n".format(datetime.now().strftime("%H:%M:%S"), len(skz_org_list)))
        log.flush()

        if skz_org_list:
            # 2. Проверяем блокировку оставшихся в проекте
            res["Этап обработки"] = "Проверка блокировки остатков"
            set_res(res)

            ext_id_org_list = [str(skz_org["id"]) for skz_org in skz_org_list]

            SQL = """SELECT Call_Bank.ID as BlockID, Call_.ID, Call_.ExtID, Call_Bank.Data
                     FROM Call_ 
                     LEFT JOIN Call_Bank ON Call_Bank.Call_ = Call_.ID
                     WHERE Call_.ExtID in ({});""".format(",".join(ext_id_org_list))

            cur = controller.get_cursor()
            cur.execute(SQL)
            bank_org_data_list = cur.fetchall()

            i = 0
            removed_count = 0

            for skz_org in skz_org_list:

                log.write("\n{}: Анализ контакта: {}\n".format(datetime.now().strftime("%H:%M:%S"), skz_org))
                log.flush()

                i += 1
                res["Прогресс"] = "{} из {}".format(i, len(skz_org_list))

                skz_bank_org_data = None

                for bank_org_data in bank_org_data_list:

                    if bank_org_data["ExtID"] == str(skz_org["id"]):

                        if skz_bank_org_data is None:
                            skz_bank_org_data = bank_org_data
                        elif skz_bank_org_data["BlockID"] < bank_org_data["BlockID"]:
                            skz_bank_org_data = bank_org_data


                if skz_bank_org_data is None:
                    log.write("{}: Не найдена информация о звонке, удаляем из скорозвона\n".
                              format(datetime.now().strftime("%H:%M:%S")))
                    log.flush()

                    skz_client.remove_org(skz_org["id"])
                    removed_count += 1

                    continue

                # Контакт вообще никогда не был заблокирован
                if skz_bank_org_data["Data"] is None:
                    log.write("{}: Организация никогда не была заблокирована, удаляем из скорозвона\n".
                              format(datetime.now().strftime("%H:%M:%S")))
                    log.flush()

                    skz_client.remove_org(skz_org["id"])
                    removed_count += 1
                    continue

                # Проверить, что время блокировки контакта не вышло
                if datetime.strptime(json.loads(skz_bank_org_data["Data"])['block_until'][:19], "%Y-%m-%dT%H:%M:%S") < datetime.now():
                    log.write("{}: Срок блокировки организация вышел, удаляем из скорозвона\n".
                              format(datetime.now().strftime("%H:%M:%S")))
                    log.flush()

                    skz_client.remove_org(skz_org["id"])
                    removed_count += 1

                    SQL = """UPDATE Call_ SET Result = 'Не успели обработать' WHERE ID = {}""".format(skz_bank_org_data["ID"])
                    cur.execute(SQL)
                    controller.save_changes()
                    continue

                log.write("{}: Срок блокировки актуальный, ждем состояния\n".
                          format(datetime.now().strftime("%H:%M:%S")))
                log.flush()


            # После очистки заново запросим список контактов и оценим кол-во
            skz_org_list = skz_client.get_project_source(project_id, True)["leads"]

            log.write("{}: Контактов осталось в скорозвоне после очистки {}\n".
                      format(datetime.now().strftime("%H:%M:%S"), len(skz_org_list)))
            log.flush()

        org_limit = 20

        # посчитаем кол-во контактов в проекте от кол-ва операторов
        if "OrgLimitForOper" in params:
            org_limit = params["OrgLimitForOper"] * len(project_state["data"]["user_ids"])
            res["Уровень контактов"] = "{} по {} штук на {} операторов".format(org_limit, params["OrgLimitForOper"],
                                                                               len(project_state["data"]["user_ids"]))
            set_res(res)
        elif "OrgLimit" in params:
            org_limit = params["OrgLimit"]
            res["Уровень контактов"] = "Константа {}".format(params["OrgLimit"])
            set_res(res)

        # Если в скорозвоне на обработке контактов больше, чем лимит - не догружаем
        if len(skz_org_list) >= org_limit:
            return {"sleep": 10}

        count_to_load = params["OrgLimit"] - len(skz_org_list)

        # 3. Сформируем выборку контактов на прозвон
        org_to_load_list = []
        org_id_to_call_id = {}

        res["Этап обработки"] = "Загрузка в проект {} контактов".format(count_to_load)
        res["Прогресс"] = "{} из {}".format(len(org_to_load_list), count_to_load)
        set_res(res)

        while count_to_load > len(org_to_load_list):

            # TODO: добавить часовые пояса
            SQL = """SELECT Organization.ID, Organization.INN, ContactInfo.Value as Phone, ContactPerson.Name,
                            ContactPerson.MiddleName, ContactPerson.Surname, Organization.Address, 
                            Organization.Name as OrgName, Call_.ID as CallID
                     FROM Call_ 
                     INNER JOIN Call_Organization ON Call_Organization.Call_ = Call_.ID
                     INNER JOIN Organization ON Call_Organization.Organization = Organization.ID
                     INNER JOIN ContactInfo ON Organization.LastPhoneInfo = ContactInfo.ID
                     INNER JOIN ContactPerson ON Organization.LastContactPerson = ContactPerson.ID
                     WHERE Result is NULL and Project = {} AND Call_.ExtID is NULL AND (RecallDateTime < NOW() OR RecallDateTime is NULL)
                     AND Organization.Download <> 2372
                     ORDER BY Call_.RecallDateTime, Call_.ID DESC;""".format(project.id())

            cur = controller.get_cursor()
            cur.execute(SQL)
            org_try_to_load_list = cur.fetchall()

            for org_to_load in org_try_to_load_list:

                res["Прогресс"] = "{} из {}".format(len(org_to_load_list), count_to_load)
                set_res(res)

                try:
                    # а. Проверим на ОДП
                    if bank.is_in_odp(org_to_load["INN"]):
                        SQL = "UPDATE Call_ SET Result = 'ОДП' WHERE ID = {};".format(org_to_load["CallID"])
                        cur.execute(SQL)
                        controller.save_changes()

                        log.write("{} Организация с ИНН {} находится в ОДП\n".
                                  format(datetime.now().strftime("%H:%M:%S"), org_to_load["INN"]))
                        log.flush()
                        continue
                except Exception as e:
                    log.write("{} Ошибка при проверке ОДП организации с ИНН {}: {}\n".
                              format(datetime.now().strftime("%H:%M:%S"), org_to_load["INN"]), e)
                    log.flush()
                    continue

                # б. Попробуем заблокировать контакт
                try:
                    lock_res = bank.lock_org(org_to_load["INN"], org_to_load["Phone"])
                except Exception as e:
                    log.write("{} Ошибка при блокировке организации с ИНН {}: {}\n".
                              format(datetime.now().strftime("%H:%M:%S"), org_to_load["INN"]), e)
                    log.flush()
                    continue

                if lock_res == "limit out":
                    SQL = "UPDATE Call_ SET Result = 'Закончились попытки блокировки' WHERE ID = {};". \
                        format(org_to_load["CallID"])
                    cur.execute(SQL)
                    controller.save_changes()

                    log.write("{}: Организация с ИНН {} заблокирована больше допустимого количества раз\n".
                              format(datetime.now().strftime("%H:%M:%S"), org_to_load["INN"]))
                    log.flush()
                    continue

                if lock_res is None:
                    SQL = "UPDATE Call_ SET Result = 'Заблокирован другим партнером' WHERE ID = {};".\
                        format(org_to_load["CallID"])
                    cur.execute(SQL)
                    controller.save_changes()

                    log.write("{}: Организация с ИНН {} заблокирован другим партнером\n".
                              format(datetime.now().strftime("%H:%M:%S"), org_to_load["INN"]))
                    log.flush()
                    continue

                SQL = "INSERT INTO Call_Bank SET Call_ = {}, Data = '{}', Bank = {}".\
                    format(org_to_load["CallID"], str(lock_res).replace("'", '"'), bank.id())
                cur.execute(SQL)
                controller.save_changes()

                # в. все ок, грузим контакт в скорозвон
                org_to_load_list.append(
                    {
                        "external_id": org_to_load["ID"],
                        "name": "{} {} {}".format(org_to_load["Surname"], org_to_load["Name"], org_to_load["MiddleName"]),
                        "phones": [org_to_load["Phone"]],
                        "address": org_to_load["Address"],
                        "post": "Директор",
                        "city": org_to_load["OrgName"]
                        #"tags": ["Подгрузка {} {}".format(upload.id(), up_type)]
                    }
                )

                org_id_to_call_id[str(org_to_load["ID"])] = org_to_load["CallID"]

                if count_to_load <= len(org_to_load_list):
                    break

            break

        if org_to_load_list:
            ext_upload_id = skz_client.mass_load(org_to_load_list, project_id)

            wait = True

            while wait:
                if skz_client.is_import_complete(ext_upload_id):
                    wait = False
                    continue

                time.sleep(5)

            ext_org_list = skz_client.get_ext_org_list(ext_upload_id)

            for ext_org in ext_org_list:

                log.write("\n{}: Загрузили организацию {}\n".format(datetime.now().strftime("%H:%M:%S"), ext_org))
                log.flush()

                SQL = "UPDATE Call_ SET ExtID = '{}' WHERE ID = {}".\
                    format(ext_org["id"], org_id_to_call_id[ext_org["external_id"]])
                cur.execute(SQL)

                controller.save_changes()
    except Exception as e:
        log.write("{}: Необработанное исключение при загрузке {} {}\n".
                  format(datetime.now().strftime("%H:%M:%S"), e, traceback.format_exc()))
        log.flush()

    return {"sleep": 10}


def rich_download(params, set_res):

    res = {}
    controller = DBController()

    sql = """SELECT Organization.ID, Organization.INN, Organization.LastContactPerson, 
                    ContactPerson.DocumentType, ContactPerson.DocumentNumber, Organization_Region.Region,
                    ContactPerson.INN AS CPINN
             FROM Organization
             LEFT JOIN Organization_Region ON Organization_Region.Organization = Organization.ID
             INNER JOIN Download_Organization ON Download_Organization.Organization = Organization.ID
             INNER JOIN ContactPerson ON ContactPerson.ID = Organization.LastContactPerson
             WHERE Organization.Download = {} AND 
                   Download_Organization.Operation = 'Обогащение'""".format(params["Download"])

    cur = controller.get_cursor()
    cur.execute(sql)

    org_list = cur.fetchall()
    res["Всего контактов"] = len(org_list)
    res["Обогащено из БД"] = 0
    set_res(res)

    to_doc_analize_org_list = []
    count = 0

    # Обогащение из собственных данных
    for org in org_list:

        #to_doc_analize_org_list.append(org)
        #continue

        cur = controller.get_cursor()

        count += 1
        res["Собственное обогащение"] = "{} из {}".format(count, res["Всего контактов"])
        set_res(res)

        cur.execute("""SELECT * FROM Organization WHERE INN = '{}' AND ID <> {} AND INN is not NULL AND INN <> '' ORDER BY ID DESC""".
                    format(org["CPINN"], org["ID"]))

        same_inn_org_list = cur.fetchall()

        if len(same_inn_org_list) == 0:
            to_doc_analize_org_list.append(org)
            continue

        phone = None

        for same_inn_org in same_inn_org_list:

            cur.execute("""SELECT ContactInfo.Value 
                               FROM ContactPerson_Organization
                               INNER JOIN ContactInfo ON ContactInfo.ContactPerson = ContactPerson_Organization.ContactPerson
                               WHERE ContactPerson_Organization.Organization = {} AND ContactInfo.Type = 'Телефон'""".
                        format(same_inn_org["ID"]))

            org_phone_list = cur.fetchall()

            for org_phone in org_phone_list:

                if not phone_is_invalid(org_phone["Value"]):
                    phone = normalize_phone(org_phone["Value"])
                    break

        if phone is not None:
            contact_person = ContactPerson.get_by_id(controller, org["LastContactPerson"])
            contact_info = ContactInfo.create(controller, contact_person, "Телефон", phone)

            organization = Organization.get_by_id(controller, org["ID"])

            organization.link_contact(contact_person, contact_info, "Директор")

            res["Обогащено из БД"] += 1
        else:
            to_doc_analize_org_list.append(org)


    to_check_org_list = []

    res["Контактов не с паспортом РФ"] = 0
    count = 0

    for org in to_doc_analize_org_list:
        count += 1

        res["Анализ документов"] = "{} из {}".format(count, len(to_doc_analize_org_list))
        set_res(res)

        if org["DocumentType"] not in ("Паспорт гражданина Российской Федерации", "ПасРФ"):
            res["Контактов не с паспортом РФ"] += 1
            set_res(res)
            continue

        to_check_org_list.append(org)

    #bank_list = BankList.get(controller)

    #unfiltred_bank_list = BankList.get(controller)

    #bank_list = []

    #for bank in unfiltred_bank_list:

    #    if bank.name() != "Тинькофф":
    #        continue

        #if bank.name() == "ПромСвязьБанк":
        #    continue

        #if bank.name() == "МТС":
        #    continue

    #    bank_list.append(bank)

    #precheck_list = {}

    #for bank in bank_list:
    #    precheck_list[bank.name()] = {}

    #res["Контактов на проверку"] = len(to_check_org_list)
    #res["По региону не подходит ни одному банку"] = 0
    #res["В ОДП"] = 0
    #set_res(res)

    #count = 0

    #for org in to_check_org_list:

    #    count += 1

    #    res["Запуск проверки на ОДП"] = "{} из {}".format(count, len(to_check_org_list))
    #    set_res(res)

    #    for bank in bank_list:

    #        if not bank.in_regions(org["Region"]):
    #            continue

            #task_id = Task.find_by_full_data(controller, "Проверка на ОДП", {"Bank": bank.name(), "INN": org["INN"]})
    #        task_id = None

    #        if task_id is None:
    #            task_id = Task.set_task_impl(controller, "Проверка на ОДП",
    #                                                             json.dumps({"Bank": bank.name(), "INN": org["INN"]}))

    #        precheck_list[bank.name()][org["INN"]] = task_id

    to_rich_org_list = []
    #count = 0

    for org in to_check_org_list:
    #    count += 1
    #    in_odp_count = 0
    #    allow_to_one = False

    #    res["Анализ результата проверки на ОДП"] = "{} из {}".format(count, len(to_check_org_list))
    #    set_res(res)

    #    for bank in bank_list:

    #        if not bank.in_regions(org["Region"]):
    #            continue

    #        in_odp = False
    #        allow_to_one = True

    #        task_state = "Выполняется"

    #        while task_state != "Завершена":
    #            task = Task.get_by_id(controller, precheck_list[bank.name()][org["INN"]])
    #            task_state = task.state()

    #            if task_state != "Завершена":
    #                time.sleep(5)

    #        odp_state = task.get_res()

    #        if odp_state and "Состояние" in odp_state and odp_state["Состояние"] == "ОДП":
    #            in_odp = True

    #        if in_odp:
    #            in_odp_count += 1

    #    if not allow_to_one:
    #        res["По региону не подходит ни одному банку"] += 1
    #        set_res(res)

    #        continue

    #    if in_odp_count > 0:
    #        res["В ОДП"] += 1
    #        set_res(res)

    #        continue

        #task_id = Task.find_by_full_data(controller, "Обогащение МТС", {"passport": org["DocumentNumber"]})

        #if task_id is None:
        task_id = Task.set_task_impl(controller, "Обогащение МТС", json.dumps({"passport": org["DocumentNumber"]}))

        org["rich_task_id"] = task_id
        to_rich_org_list.append(org)

    res["Контактов на обогащение"] = len(to_check_org_list)
    res["Обогащено"] = 0

    set_res(res)
    count = 0

    for org in to_rich_org_list:
        count += 1

        res["Анализ результатов обогащения"] = "{} из {}".format(count, len(to_rich_org_list))
        set_res(res)

        task_state = "Выполняется"

        while task_state != "Завершена":
            task = Task.get_by_id(controller, org["rich_task_id"])
            task_state = task.state()

            if task_state != "Завершена":
                time.sleep(5)

        numbers = task.get_res()

        try:
            if numbers[0] == 'PASSPORT_NOT_FOUND':
                continue
        except:
            pass

        phone = None

        try:
            for number in numbers:
                if numbers[number]:
                    phone = number
                    break
        except:
            pass

        if phone is None:
            continue

        contact_person = ContactPerson.get_by_id(controller, org["LastContactPerson"])
        contact_info = ContactInfo.create(controller, contact_person, "Телефон", phone)

        organization = Organization.get_by_id(controller, org["ID"])

        organization.link_contact(contact_person, contact_info, "Директор")

        res["Обогащено"] += 1

        set_res(res)

    res["Задачи подгрузки"] = ""

    # ТФ
    tf_task_id = Task.set_task("Подгрузка", {"Загрузка": params["Download"],
                                             "Проект": "Новореги ТФ",
                                             "Регионы": "Проекта",
                                             "Операция": "Обогащение"})
    res["Задачи подгрузки"] += """<a href="./task.py?id={}">Новореги ТФ {}</a>""".format(tf_task_id, tf_task_id)

    # СБЕР
    #sber_task_id = Task.set_task("Подгрузка", {"Загрузка": params["Download"],
    #                                           "Проект": "Новореги Сбербанк",
    #                                           "Регионы": "Проекта",
    #                                           "Операция": "Обогащение",
    #                                           "Блокировать контакты": True})
    #res["Задачи подгрузки"] += """, <a href="./task.py?id={}">Сбер {}</a>""".format(sber_task_id, sber_task_id)

    # ВТБ
    vtb_task_id = Task.set_task("Подгрузка", {"Загрузка": params["Download"],
                                              "Проект": "Новореги ВТБ",
                                              "Регионы": "Проекта",
                                              "Операция": "Обогащение"})
    res["Задачи подгрузки"] += """, <a href="./task.py?id={}">ВТБ {}</a>""".format(vtb_task_id, vtb_task_id)

    # ПСБ
    #psb_uniq_task_id = Task.set_task("Подгрузка", {"Загрузка": params["Download"],
    #                                               "Проект": "Новореги ПромСвязьБанк",
    #                                               "Регионы": "Проекта",
    #                                               "Операция": "Обогащение"})
    #res["Задачи подгрузки"] += """, <a href="./task.py?id={}">ПромСвязьБанк {}</a>""".format(psb_uniq_task_id,
    #                                                                                         psb_uniq_task_id)

    # РСХБ
    #rshb_uniq_task_id = Task.set_task("Подгрузка", {"Загрузка": params["Download"],
    #                                                "Проект": "Новореги РосСельхозБанк",
    #                                               "Регионы": "Проекта",
    #                                                "Операция": "Обогащение"})
    #res["Задачи подгрузки"] += """, <a href="./task.py?id={}">РосСельхозБанк {}</a>""".format(rshb_uniq_task_id,
    #                                                                                          rshb_uniq_task_id)
    #

    # Холодные ТФ
    #ctf_task_id = Task.set_task("Подгрузка в банк", {"Загрузка": params["Download"],
    #                                                 "Проект": "Холодные ТФ",
    #                                                 "Операция": "Обогащение",
    #                                                 "Проверять на ОДП": "НЕТ"})

    #res["Задачи подгрузки"] += """<a href="./task.py?id={}">Холодные ТФ {}</a>""".format(ctf_task_id, ctf_task_id)

    # Холодные точка
    #ctf_task_id = Task.set_task_impl(
    #    controller,
    #    "Подгрузка в банк",
    #    json.dumps({
    #        "Загрузка": params["Download"],
    #        "Проект": "Холодные точка",
    #        "Операция": "Обогащение",
    #        "Проверять на ОДП": "НЕТ"
    #    }),
    #    (datetime.now() + timedelta(minutes=30)).strftime("%Y-%m-%d %H:%M:%S")
    #)

    #res["Задачи подгрузки"] += """, <a href="./task.py?id={}">Холодные точка {}</a>""".format(ctf_task_id, ctf_task_id)

    set_res(res)


def pre_load_handle(params, set_res):

    log = open("rich.log", "w")

    controller = DBController()

    if not os.path.isfile("../Files/" + params["file"]["Идентификатор"] + ".xlsx"):
        os.rename("../Files/" + params["file"]["Идентификатор"],
                  "../Files/" + params["file"]["Идентификатор"] + ".xlsx")

    write_to_log(log, "Открывем таблицу")

    wb = openpyxl.load_workbook(filename="../Files/" + params["file"]["Идентификатор"] + ".xlsx")
    sheet = wb.get_active_sheet()
    data = sheet.values

    write_to_log(log, "Таблица открыта")

    same_wb = openpyxl.Workbook()
    same_ws = same_wb.active
    same_ws.title = "Данные"
    cnt = 1
    all_cnt = 0
    data_len = sheet.max_row

    write_to_log(log, "Ищем контакты с собственными номерами")

    for row in data:

        all_cnt += 1

        set_res({
            "Состояние": "Анализ на собственные номера {} из {}".format(all_cnt, data_len)
        })

        if phone_is_invalid(row[14]):
            write_to_log(log, "Строка {} без валидного номера".format(all_cnt))
            continue

        write_to_log(log, "Строка {} с валидным номером".format(all_cnt))

        cnt += 1

        same_ws.cell(row=cnt, column=1).value = row[0]
        same_ws.cell(row=cnt, column=2).value = row[1]
        same_ws.cell(row=cnt, column=3).value = row[2]
        same_ws.cell(row=cnt, column=4).value = row[3]
        same_ws.cell(row=cnt, column=5).value = row[4]
        same_ws.cell(row=cnt, column=6).value = row[5]
        same_ws.cell(row=cnt, column=7).value = row[13]
        same_ws.cell(row=cnt, column=8).value = normalize_phone(row[14])

    same_wb.save("../Admin/{}_same.xlsx".format(params["file"]["Идентификатор"]))

    send_file_to_load(controller, log, params, "../Admin/{}_same.xlsx".format(params["file"]["Идентификатор"]), "Собственные номера")

    set_res({
        "Состояние": "Проверка и обогощение",
        "Собственные номера телефонов": '<a href="../../{}_same.xlsx">скачать</a>'.format(params["file"]["Идентификатор"])
    })

    STEP = 300
    offset = 0
    pre_cnt = 0
    cnt = 0
    rich_count = 0

    bank_llist = BankList.get(controller)
    precheck_list = {}

    mts_wb = openpyxl.Workbook()
    mts_ws = mts_wb.active
    mts_ws.title = "Данные"
    mts_cnt = 1

    for bank in bank_llist:
        precheck_list[bank.name()] = {}

    write_to_log(log, "Начинаем процесс обогащения")

    # Основной цикл работы с шагом offset
    while offset * STEP < data_len:
        data = sheet[STEP*offset + 1: STEP*offset + STEP]

        offset += 1

        write_to_log(log, "Предпроверка контактов на шаге {}".format(offset))

        # цикл предпроверки
        for row in data:

            inn = row[3].value

            if inn is None:
                continue

            pre_cnt += 1

            set_res({
                "Состояние": "Проверка и обогощение",
                "Предпроверка": pre_cnt,
                "Проверка": cnt,
                "Всего": data_len,
                "Обогащено": mts_cnt - 1,
                "Собственные номера телефонов": '<a href="../../{}_same.xlsx">скачать</a>'.format(
                    params["file"]["Идентификатор"]),
                "Обогащенные номера телефонов": '<a href="../../{}_mts.xlsx">скачать</a>'.format(
                    params["file"]["Идентификатор"])
            })

            if pre_cnt == 1:
                continue

            write_to_log(log, "Предпроверка строки {}".format(pre_cnt))

            if not phone_is_invalid(row[14].value):
                write_to_log(log, "Строка {} с валидным номером".format(pre_cnt))
                continue

            write_to_log(log, "Строка {} без валидного номера".format(pre_cnt))

            for bank in bank_llist:

                write_to_log(log, "Строка {} предпроверка по банку {}".format(pre_cnt, bank.name()))

                if not bank.is_region_allow(row[13].value, inn):
                    write_to_log(log, "Строка {} не подходит по региону для банка {}".format(pre_cnt, bank.name()))
                    continue

                precheck_list[bank.name()][inn] = Task.set_task_impl(controller, "Проверка на ОДП",
                                                                     json.dumps({"Bank": bank.name(), "INN": inn}))

        write_to_log(log, "Обогащение контактов на шаге {}".format(offset))

        check_org_list = []

        #цикл проверки
        for row in data:

            inn = row[3].value

            if inn is None:
                continue

            cnt += 1

            set_res({
                "Состояние": "Проверка",
                "Предпроверка": pre_cnt,
                "Проверка": cnt,
                "Всего": data_len,
                "Обогащено": mts_cnt - 1,
                "Собственные номера телефонов": '<a href="../../{}_same.xlsx">скачать</a>'.format(
                    params["file"]["Идентификатор"]),
                "Обогащенные номера телефонов": '<a href="../../{}_mts.xlsx">скачать</a>'.format(
                    params["file"]["Идентификатор"])
            })

            if cnt == 1:
                continue

            write_to_log(log, "Обогащение строки {}".format(cnt))

            if not phone_is_invalid(row[14].value):
                write_to_log(log, "Строка {} с валидным номером".format(cnt))
                continue

            write_to_log(log, "Строка {} без валидного номера".format(cnt))

            if row[6].value != "Паспорт гражданина Российской Федерации":
                write_to_log(log, "Строка {} бещ паспорта гражданина РФ".format(cnt))
                continue

            date = row[4].value

            org = Organization.find_by_inn_open_date(controller, inn, date)

            if org is not None:
                write_to_log(log, "Строка {} уже есть в БД".format(cnt))
                continue

            in_odp_count = 0

            allow_to_one = False

            for bank in bank_llist:

                if bank.name() != 'Сбербанк' and bank.name() != 'МТС':
                    continue

                write_to_log(log, "Проверка строка {} на ОДП для банка {}".format(cnt, bank.name()))

                if not bank.is_region_allow(row[13].value, inn):
                    write_to_log(log, "Строка {} не подходит по региону для банка {}".format(cnt, bank.name()))
                    continue

                in_odp = False
                allow_to_one = True

                task_state = "Выполняется"

                while task_state != "Завершена":
                    task = Task.get_by_id(controller, precheck_list[bank.name()][inn])
                    task_state = task.state()

                    if task_state != "Завершена":
                        write_to_log(log, "Ожидаем проверку строки {} на ОДП по банку {}, задача {}".
                                     format(cnt, bank.name(), precheck_list[bank.name()][inn]))
                        time.sleep(5)

                odp_state = task.get_res()

                if odp_state and "Состояние" in odp_state and odp_state["Состояние"] == "ОДП" :
                    in_odp = True

                if in_odp:
                    in_odp_count += 1
                    write_to_log(log, "Строка {} в ОДП по банку {}".format(cnt, bank.name()))
                else:
                    write_to_log(log, "Строка {} не в ОДП по банку {}".format(cnt, bank.name()))

            if not allow_to_one:
                write_to_log(log, "Строка {} не подходит ни для одного банка".format(cnt))
                continue

            if in_odp_count > 1:
                write_to_log(log, "Строка {} в ОДП более чем в 2х банках".format(cnt))
                continue

            passport = str(row[7].value) + str(row[8].value)
            passport = passport.replace(" ", "")
            passport = passport.replace('.0', '')

            task_id = Task.set_task_impl(controller, "Обогащение МТС", json.dumps({"passport": passport}))

            check_org_list.append({"task_id": task_id, "cnt": cnt, "data": [
                row[0].value,
                row[1].value,
                row[2].value,
                row[3].value,
                row[4].value,
                row[5].value,
                row[13].value,
            ]})

        for org_data in check_org_list:

            set_res({
                "Состояние": "Обогащение",
                "Обогащение": org_data["cnt"],
                "Предпроверка": pre_cnt,
                "Проверка": cnt,
                "Всего": data_len,
                "Обогащено": mts_cnt - 1,
                "Собственные номера телефонов": '<a href="../../{}_same.xlsx">скачать</a>'.format(
                    params["file"]["Идентификатор"]),
                "Обогащенные номера телефонов": '<a href="../../{}_mts.xlsx">скачать</a>'.format(
                    params["file"]["Идентификатор"])
            })

            task_state = "Выполняется"

            while task_state != "Завершена":
                task = Task.get_by_id(controller, org_data["task_id"])
                task_state = task.state()

                if task_state != "Завершена":
                    write_to_log(log, "Ожидаем обогащение строки {} на ОДП по банку задача {}".
                                 format(org_data["cnt"], org_data["task_id"]))
                    time.sleep(5)

            numbers = task.get_res()

            #numbers = mts_bank.extend_passport_data(passport)
            #numbers = {"79146344535": True}

            try:
                if numbers[0] == 'PASSPORT_NOT_FOUND':
                    write_to_log(log, "Строка {} номера в МТС не найдены".format(org_data["cnt"]))
                    continue
            except:
                pass

            phone = None

            try:
                for number in numbers:
                    if numbers[number]:
                        phone = number
                        break
            except:
                pass

            if phone is None:
                write_to_log(log, "Строка {} номера в МТС не найдены".format(org_data["cnt"]))
                continue

            write_to_log(log, "Строка {} найден номер {}".format(org_data["cnt"], phone))

            mts_cnt += 1

            mts_ws.cell(row=mts_cnt, column=1).value = org_data["data"][0]
            mts_ws.cell(row=mts_cnt, column=2).value = org_data["data"][1]
            mts_ws.cell(row=mts_cnt, column=3).value = org_data["data"][2]
            mts_ws.cell(row=mts_cnt, column=4).value = org_data["data"][3]
            mts_ws.cell(row=mts_cnt, column=5).value = org_data["data"][4]
            mts_ws.cell(row=mts_cnt, column=6).value = org_data["data"][5]
            mts_ws.cell(row=mts_cnt, column=7).value = org_data["data"][6]
            mts_ws.cell(row=mts_cnt, column=8).value = "+" + phone

        mts_wb.save("../Admin/{}_mts.xlsx".format(params["file"]["Идентификатор"]))

    send_file_to_load(controller, log, params, "../Admin/{}_mts.xlsx".format(params["file"]["Идентификатор"]), "Обогащенные номера")

    set_res({
        "Состояние": "Проверка и обогощение",
        "Предпроверка": pre_cnt,
        "Проверка": cnt,
        "Всего": data_len,
        "Обогащено": mts_cnt - 1,
        "Собственные номера телефонов": '<a href="../../{}_same.xlsx">скачать</a>'.format(
            params["file"]["Идентификатор"]),
        "Обогащенные номера телефонов": '<a href="../../{}_mts.xlsx">скачать</a>'.format(
            params["file"]["Идентификатор"])
    })


def get_org_phones(cur, org_id):
    cur.execute("""SELECT ContactInfo.Value 
                   FROM ContactPerson_Organization
                   INNER JOIN ContactInfo ON ContactInfo.ContactPerson = ContactPerson_Organization.ContactPerson
                   WHERE ContactPerson_Organization.Organization = {} AND ContactInfo.Type = 'Телефон'""".
                format(org_id))

    return cur.fetchall()


def upload_list(params, set_res):
    okved_list = params["OKVEDList"].split(" ")
    except_project = Project.get_by_name(params["ExceptProject"])
    controller = DBController()
    cur = controller.get_cursor()
    res = {}

    if params["ExceptProject"] and except_project is None:
        raise Exception("Неверно указано название проекта")

    SQL = """SELECT Organization.ID, Organization.INN, ContactInfo.Value 
             FROM Organization 
             INNER JOIN ContactPerson ON Organization.LastContactPerson = ContactPerson.ID 
             INNER JOIN ContactInfo ON Organization.LastPhoneInfo = ContactInfo.ID 
             WHERE ContactInfo.`Value` is not NULL AND ContactInfo.`Value` <> '' AND {} 
             ORDER BY Organization.ID DESC 
             LIMIT {}"""

    WHERE = []

    if okved_list:
        WHERE.append("OKVED in ('{}')".format("','".join(okved_list)))

    if except_project:
        WHERE.append("""Organization.ID not in (
                        SELECT Call_Organization.Organization FROM Call_
                        INNER JOIN Call_Organization ON Call_Organization.Call_ = Call_.ID 
                        WHERE Project = {}
                    )""".format(except_project.id()))

    if params["DaysCount"]:
        open_dt = datetime.now() - timedelta(days=int(params["DaysCount"]))
        WHERE.append("OpenDate < '{}'".format(open_dt.strftime("%Y-%m-%d")))

    SQL = SQL.format(" AND ".join(WHERE), params["Count"])
    cur.execute(SQL)

    org_list = cur.fetchall()

    wb = None
    count = 0
    file_num = 1
    res["Результат"] = ""
    row_num = 0

    for org in org_list:

        if phone_is_invalid(org["Value"]):
            continue

        count += 1
        row_num += 1
        print(count)

        if wb is None:
            wb = openpyxl.Workbook()
            ws = wb.active
            ws.title = "Список"

        ws.cell(row=row_num, column=1, value=org["ID"])
        ws.cell(row=row_num, column=2, value=org["INN"])
        ws.cell(row=row_num, column=3, value=normalize_phone(org["Value"]))

        if count == file_num * int(params["FileCount"]):
            file_id = uuid.uuid1()
            wb.save("../Files/{}".format(file_id))

            res["Результат"] += """<br/><a href="../download/get_download_file.py?id={}&name={}">{}</a>""". \
                format(file_id, "Файл {}.xlsx".format(file_num), "Файл {}.xlsx".format(file_num))
            set_res(res)

            wb = None
            file_num += 1
            row_num = 0

    if wb is not None:
        file_id = uuid.uuid1()
        wb.save("../Files/{}".format(file_id))

        res["Результат"] += """<a href="../download/get_download_file.py?id={}&name={}">{}</a>""".\
            format(file_id, "Файл {}.xlsx".format(file_num), "Файл {}.xlsx".format(file_num))

        set_res(res)


def upload_cheked_file(params, set_res):

    project = Project.get_by_name(params["ProjectName"])

    if project is None:
        raise Exception(f"Не найден проект с названием {params['ProjectName']}")

    project_params = project.params()
    bank = project.bank()

    bad_list = []

    # Если не поменять расширение, то библиотека Excel будет писать ошибку
    if not os.path.isfile("../Files/{}.xlsx".format(params["OriginalFile"]["Идентификатор"])):
        os.rename("../Files/{}".format(params["OriginalFile"]["Идентификатор"]),
                  "../Files/{}.xlsx".format(params["OriginalFile"]["Идентификатор"]))

    book = openpyxl.load_workbook(filename="../Files/{}.xlsx".format(params["OriginalFile"]["Идентификатор"]))
    sheet = book.get_active_sheet()
    data = sheet.values

    for row in data:
        bad_list.append(str(row[0]))

    # Если не поменять расширение, то библиотека Excel будет писать ошибку
    if not os.path.isfile("../Files/{}.xlsx".format(params["File"]["Идентификатор"])):
        os.rename("../Files/{}".format(params["File"]["Идентификатор"]),
                  "../Files/{}.xlsx".format(params["File"]["Идентификатор"]))

    book = openpyxl.load_workbook(filename="../Files/{}.xlsx".format(params["File"]["Идентификатор"]))
    sheet = book.get_active_sheet()

    data = sheet.values

    good_list = []

    res = {}
    count = 0

    for row in data:
        count += 1

        if count == 1:
            continue

        #set_res(res)

        good_list.append(str(row[2]))

        try:
            bad_list.remove(str(row[2]))
        except:
            pass

    controller = DBController()
    upload = Upload.create(controller, "Из файла", "Руками")

    res["В ОДП"] = str(len(bad_list))
    res["Не в ОДП"] = str(len(good_list))
    res["Подгрузка"] = upload.id()
    set_res(res)

    for org_id in bad_list:
        call = Call.create(controller, org_id, bank.id(), project.id(), state="ОДП при подгрузке")

        upload.link_to_call_and_organization(call.id(), org_id)

    cur = controller.get_cursor()

    SQL = """SELECT Organization.ID, Organization.Name as OrgName, Organization.INN, Organization.Address,
                    ContactPerson.Surname, ContactPerson.Name as CName, ContactPerson.MiddleName,
                    ContactInfo.Value as Phone
             FROM Organization
             INNER JOIN ContactPerson ON ContactPerson.ID = Organization.LastContactPerson
             INNER JOIN ContactInfo ON ContactInfo.ID = Organization.LastPhoneInfo
             WHERE Organization.ID in ({})""".format(",".join(good_list))

    cur.execute(SQL)
    org_rec_list = cur.fetchall()

    upload_list = []

    for org_rec in org_rec_list:
        upload_list.append(
            {
                "external_id": org_rec["ID"],
                "name": "{} {} {}".format(org_rec["Surname"], org_rec["CName"], org_rec["MiddleName"]),
                "phones": [org_rec["Phone"]],
                "address": org_rec["Address"],
                "post": "Директор",
                "city": org_rec["OrgName"],
                "tags": ["Подгрузка {}".format(upload.id())]
            }
        )

    skorozvon = Skorozvon()
    part_len = 1000

    for i in range(0, len(upload_list) // part_len + 1):

        res["Загрузка в Скорозвон"] = "Обрабатываем контакты с {} по {}".format(i * part_len, (i+1) * part_len)
        set_res(res)

        part = upload_list[i * part_len:(i + 1) * part_len]

        wait = True

        while wait:
            try:
                res["Состояние загрузки"] = "Отправляем контакты на массовую загрузку"
                set_res(res)

                ext_upload_id = skorozvon.mass_load(part, project_params["Скорозвон"]["project_id"])
                wait = False
            except:
                continue

        wait = True

        while wait:
            res["Состояние загрузки"] = "Ожидаем загрузки контактов скорозвоном"
            set_res(res)

            if skorozvon.is_import_complete(ext_upload_id):
                wait = False
                continue

            time.sleep(5)

        # попробуем еще раз, если поучили ошибку
        try:
            res["Состояние загрузки"] = "Загрузили, получаем обратную связь"
            set_res(res)

            ext_org_list = skorozvon.get_ext_org_list(ext_upload_id)
        except:

            ext_org_list = skorozvon.get_ext_org_list(ext_upload_id)

        count = 0

        for ext_org in ext_org_list:
            count += 1

            res["Состояние загрузки"] = "Создаем звонок {} из {}".format(count, len(ext_org_list))
            set_res(res)

            call = Call.create(controller, ext_org["external_id"], bank.id(), project.id(), ext_org["id"])
            upload.link_to_call_and_organization(call.id(), ext_org["external_id"])
