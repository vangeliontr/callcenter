from datetime import datetime
from DataBase.Download import Download
from DataBase.DBController import DBController

import telebot


def send_file_to_db(controller, message, bot):
    exists_download = Download.find_download_by_filename(controller, message.document.file_name)

    if exists_download is not None:
        bot.send_message(message.from_user.id,
                         "Повторяем загрузку {}".format(exists_download.id()))
        exists_download.set_download_state("Ожидает загрузки", "")

        return

    reg_date = datetime.strptime(message.document.file_name[:5] + ".2019", "%d.%m.%Y")
    Download.create(controller, [message.document.file_name], "Поставщик телефонов", reg_date,
                    datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"),
                    message.document.file_name[5:].replace(".xlsx", ""))


def load_db_from_telegramm(bot_id, pre_path):
    while True:

        try:
            bot = telebot.TeleBot(bot_id)

            @bot.message_handler(content_types=['document'])
            def get_text_messages(message):
                bot.send_message(message.from_user.id, "Получили файл {} на загрузку".format(message.document.file_name))
                file_info = bot.get_file(message.document.file_id)
                downloaded_file = bot.download_file(file_info.file_path)
                open(pre_path + message.document.file_name, "wb").write(downloaded_file)

                controller = DBController()
                send_file_to_db(controller, message, bot)

            bot.polling(interval=2)

        except Exception as e:
            print(str(e))


if __name__ == "__main__":
    load_db_from_telegramm("896335873:AAH0aSqFCiXJUPg0yKvL1TVJ-a92ZV8F-cg", "./")
