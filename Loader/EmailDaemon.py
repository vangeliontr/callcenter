#!/usr/bin/env python3

import sys
import json

sys.path.append("../")

from LoadFileFromEmail import load_db_from_email
from Tools.Daemon import Daemon

conf = json.loads(open("email_daemon_settings.txt", "r").read())

Daemon.run(load_db_from_email, conf)
