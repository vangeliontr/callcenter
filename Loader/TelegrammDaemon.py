#!/usr/bin/env python3

import sys
import json
import functools

sys.path.append("../")

from TelegrammHandler import bot_handler_loop
from Tools.Daemon import Daemon

conf = json.loads(open("telegramm_daemon_settings.txt", "r").read())
load_db_from_telegramm_with_id = functools.partial(bot_handler_loop, conf["bot_id"], False)

Daemon.run(load_db_from_telegramm_with_id, conf)
