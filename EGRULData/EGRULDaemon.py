#!/usr/bin/env python3

import sys
import json

sys.path.append("../")

from Loader import main_loop
from Tools.Daemon import Daemon


conf = json.loads(open("egrul_daemon_settings.txt", "r").read())
Daemon.run(main_loop, conf)
