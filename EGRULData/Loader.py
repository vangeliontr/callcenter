import json
import fitz
import time
import sys

sys.path.append("../")

from datetime import datetime
from Tools.ProxyHab import ProxyHub
from DataBase.DBController import DBController


def db_format_date(date_str):
    return "'{}-{}-{}'".format(date_str[6:10], date_str[3:5], date_str[0:2])


def is_date_less(one_dt_str, two_dt_str):
    one_dt = datetime.strptime(one_dt_str, "%d.%m.%Y")
    two_dt = datetime.strptime(two_dt_str, "%d.%m.%Y")

    return one_dt < two_dt


def get_egrul_data_by_inn(params, set_res):

    #hub = ProxyHub()
    hub = params["proxy_hub"]

    inn_list = params["inn_list"]
    query = "%2C+".join(inn_list)

    url = "https://egrul.nalog.ru/"
    data = "vyp3CaptchaToken=&query={}&region=&PreventChromeAutocomplete=".format(query)
    headers = {"Content-Type": "application/x-www-form-urlencoded"}

    try_count = 0
    is_success = False

    while not is_success:

        try_count += 1

        r = hub.post(url, data=data, headers=headers)

        try:
            res = json.loads(r.text)
        except:
            continue

        if "ERRORS" in res:
            set_res({"Состояние": "Ошибка запроса по ИНН номер {}: {}".format(r.text, try_count)})
            time.sleep(3)
            continue

        is_success = True

    status = "wait"

    while status == "wait":
        r = hub.get("https://egrul.nalog.ru/search-result/" + res["t"])

        try:
            res = json.loads(r.text)
        except:
            continue

        if "rows" in res:
            status = "ready"
            check_list = res["rows"]
        #else:
        #    set_res({"Состояние": "Ошибка при получении информации по ИНН"})
        #    return

    res = {}

    for org_data in check_list:

        if not org_data["i"] in res:
            res[org_data["i"]] = []

        res[org_data["i"]].append({
            "Дата открытия": org_data["r"],
            "Дата закрытия": org_data["e"] if "e" in org_data else None,
            "ОГРН": org_data["o"],
            "Токен": org_data["t"]
        })

    set_res(res)


def get_egrul_report_data(params, set_res):
    hub = ProxyHub()

    is_success = False
    try_count = 0
    url = "https://egrul.nalog.ru/vyp-request/{}".format(params["Токен"])

    while not is_success:

        try_count += 1

        r = hub.get(url)

        try:
            res = json.loads(r.text)
        except:
            continue

        if "ERRORS" in res:
            set_res({"Состояние": "Ошибка запроса выписки номер {}: {}".format(r.text, try_count)})
            time.sleep(3)
            continue

        is_success = True

    id = res["t"]

    status = None

    while status != "ready":
        url = "https://egrul.nalog.ru/vyp-status/{}".format(id)
        r = hub.get(url)

        try:
            res = json.loads(r.text)
            status = res["status"]
        except:
            time.sleep(1)


    status = None

    while status != "ready":
        url = "https://egrul.nalog.ru/vyp-download/{}".format(id)
        r = hub.get(url)

        pdf_file = b''

        for chunk in r:
            pdf_file += chunk

        try:
            reader = fitz.open(stream=pdf_file, filetype="application/pdf")
            status = "ready"
        except:
            pass

    contents = ""

    for page in reader:
        contents += page.getText()

    start_pos = contents.find("Код и наименование вида деятельности\n")
    start_pos += len("Код и наименование вида деятельности\n")
    end_pos = contents.find(" ", start_pos)

    okved = contents[start_pos:end_pos]

    set_res({"ОКВЭД": okved})


def read_egrul_info(params, set_res):

    conttroller = DBController()
    cur = conttroller.get_cursor()

    SQL = """SELECT ID FROM (
             SELECT MAX(Organization.ID) AS ID, Organization.INN, count(*) FROM Organization
             LEFT JOIN Organization_Region ON Organization_Region.Organization = Organization.ID
             WHERE INN is not NULL AND Type is not NULL AND LastPhoneInfo is not NULL AND LastContactPerson is not NULL AND OpenDate < '2018-09-01' AND (Organization_Region.Region not in (70,39) OR Organization_Region.Region is NULL) AND EGRULCheck is NULL
             GROUP BY Organization.INN
             ORDER BY MAX(Organization.ID) DESC ) as origin
             WHERE origin.INN not in (
             SELECT Organization.INN
             FROM Call_
             INNER JOIN Call_Organization ON Call_Organization.Call_ = Call_.ID
             INNER JOIN Organization ON Call_Organization.Organization = Organization.ID
             WHERE Call_.TaskID = 159933 ) LIMIT 1000 """

    cur.execute(SQL)
    org_list = cur.fetchall()

    if len(org_list) == 0:
        SQL = """SELECT * FROM Organization
                 WHERE
                    (DATEDIFF(NOW(), DATE(OpenDate)) > {}   OR OpenDate   is NULL) AND
                    (DATEDIFF(NOW(), DATE(EGRULCheck)) > {} OR EGRULCheck is NULL) AND
                    OpenDate < '2019-06-01'
                 ORDER BY Organization.ID DESC
                 LIMIT 100;""".format(params["Срок жизни больше"], params["Период проверки"])

        cur.execute(SQL)
        org_list = cur.fetchall()
    else:

        org_id_list = []

        for org in org_list:
            org_id_list.append(str(org["ID"]))

        SQL = "SELECT * FROM Organization WHERE ID in ({})".format(",".join(org_id_list))
        cur.execute(SQL)
        org_list = cur.fetchall()

    part_len = 5

    for i in range(0, len(org_list) // part_len + 1):
        part = org_list[i * part_len:(i + 1) * part_len]

        if len(part) == 0:
            break

        read_egrul_info_impl(part, params, cur)


def read_egrul_info_impl(org_list, params, cur):

    org_dict = {}
    inn_list = []

    for org_data in org_list:
        org_dict[org_data["INN"]] = org_data
        inn_list.append(org_data["INN"])

    egrul_org_list_info = {}

    def set_get_egrul_data_by_inn_res(f_res):
        egrul_org_list_info.update(f_res)

    get_egrul_data_by_inn({"inn_list": inn_list, "proxy_hub": params["proxy_hub"]}, set_get_egrul_data_by_inn_res)

    for inn in org_dict:
        db_org_data = org_dict[inn]

        if inn not in egrul_org_list_info:
            SQL = """UPDATE Organization 
                             SET EGRULCheck = NOW()
                             WHERE ID = {}
                  """.format(db_org_data["ID"])

            cur.execute(SQL)
            print("Организация {} ИНН {} не найденва в ЕГРЮЛ".format(db_org_data["ID"], inn))
            continue

        egrul_org_data = egrul_org_list_info[inn]

        SQL = "UPDATE Organization SET LastRegInfo = NULL WHERE ID = {}".format(db_org_data["ID"])
        cur.execute(SQL)

        SQL = "DELETE FROM RegInfo WHERE Organization = {}".format(db_org_data["ID"])
        cur.execute(SQL)

        last_org_reg_info = None

        if len(egrul_org_data) > 1:
            i = 0

        for org_reg_info in egrul_org_data:
            SQL = """INSERT INTO RegInfo 
                     SET 
                        OpenDate = {}, 
                        CloseDate = {}, 
                        Confirm = 1, 
                        Organization = {}, 
                        OGRN = '{}';""".format(
                db_format_date(org_reg_info["Дата открытия"]),
                db_format_date(org_reg_info["Дата закрытия"]) if org_reg_info["Дата закрытия"] else "NULL",
                db_org_data["ID"],
                org_reg_info["ОГРН"]
            )

            cur.execute(SQL)
            org_reg_info["ID"] = cur.lastrowid

            if last_org_reg_info is None:
                last_org_reg_info = org_reg_info

            if is_date_less(last_org_reg_info["Дата открытия"], org_reg_info["Дата открытия"]):
                last_org_reg_info = org_reg_info

        #ogrn = None

        #if last_org_reg_info:

        #    def set_get_egrul_report_data_info(reg_data):
        #        ogrn = reg_data["ОГРН"]

        #    if org_reg_info["Дата закрытия"] is not None:
        #        get_egrul_report_data({"Токен": org_reg_info["Токен"]}, set_get_egrul_report_data_info)

        print("{} {} {}".format(db_org_data["INN"], db_org_data["OpenDate"], "closed" if last_org_reg_info["Дата закрытия"] else 'open'))

        SQL = """UPDATE Organization 
                 SET
                    LastRegInfo = {},
                    EGRULCheck = NOW(),
                    IsClosed = {}
                 WHERE
                    ID = {}
                    """.format(
            last_org_reg_info["ID"] if last_org_reg_info["ID"] else 'NULL',
            "1" if last_org_reg_info["Дата закрытия"] else 'NULL',
            db_org_data["ID"]
        )

        cur.execute(SQL)


def main_loop():

    def set_res(r):
        pass

    proxy_hub = ProxyHub()
    #proxy_hub = None
    count = 0

    start_dt = datetime.now()

    while True:

        try:
            read_egrul_info({
                "Срок жизни больше": 30,
                "Период проверки": 90,
                "proxy_hub": proxy_hub
            }, set_res)
        except Exception as e:
            pass

        count += 100
        delta = datetime.now() - start_dt
        print(f"Проверено {count} за {delta} один контакт проверяется в среднем за {delta/count}")


if __name__ == "__main__":
    org_id_list = ["498531", "502445"]

    conttroller = DBController()
    cur = conttroller.get_cursor()

    SQL = "SELECT * FROM Organization WHERE ID in ({})".format(",".join(org_id_list))
    cur.execute(SQL)
    org_list = cur.fetchall()


    proxy_hub = ProxyHub()

    read_egrul_info_impl(org_list, {
                "Срок жизни больше": 30,
                "Период проверки": 90,
                "proxy_hub": proxy_hub
            }, cur)
