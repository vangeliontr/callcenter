from Bank.Bank import Bank
from datetime import datetime

import json
import requests

from openpyxl import Workbook
from Loader.TelegrammHandler import send_document


class TochkaBank(Bank):

    def __init__(self, rec):
        super().__init__(rec)

        try:
            ad = rec["AuthData"]

            if ad is not None:
                self.auth_data = json.loads(ad)
        except Exception as e:
            raise e

    def is_in_odp_full(self, inn, phone, acc_data):

        return self.is_in_odp(inn)

    def is_multithread_odp(self):
        return True

    def is_pre_check_odp(self):
        return True

    def get_odp_status(self, score_id, wait_res):

        request_date = datetime.now()
        res = requests.post("https://z.tochka.com/api/v1/callcenter-gateway/tasks", json=[
            score_id
        ], headers={
            "Authorization": self.auth_data["id"]
        })

        if res.status_code != 200:
            return "ON_WORK"

        try:
            result = json.loads(res.text)
        except:
            raise Exception("Ошибка при разборе ответа '{}' по идентификатору {} запроса от {}".format(
                res.text,
                score_id,
                request_date
            ))

        try:
            if not result["success"]:
                return "ON_WORK"
            
            if result["result"][0]["status"] == "Error":
                return "REJECTED"
            
            if result["result"][0]["status"] != "Completed":
                return "ON_WORK"

            if result["result"][0]["approved"]:
                return "APPROVED"
        except:
            raise Exception("Не смогли обработать ответ {}".format(result))

        return "REJECTED"

    def get_score_id(self, inn, phone):

        res = requests.post("https://z.tochka.com/api/v1/callcenter-gateway/check_companies", json=[
            {
                "inn": inn,
                "phone": phone
            }
        ], headers={
            "Authorization": self.auth_data["id"]
        })

        result = json.loads(res.text)

        return result["result"][0]["task_id"]

    def is_in_odp_full(self, inn, _phone, _acc_data):
        return self.is_in_odp(inn)

    def is_in_odp(self, inn):

        return False

    def is_mass_load(self):
        return True

    def mass_send(self, org_list, upload_num):

        wb = Workbook()
        ws = wb.active
        ws.title = "Список"

        row = 1

        ws.cell(row=row, column=1, value="Имя")
        ws.cell(row=row, column=2, value="Отчество")
        ws.cell(row=row, column=3, value="Фамилия")
        ws.cell(row=row, column=4, value="Телефон")
        ws.cell(row=row, column=5, value="Название")
        ws.cell(row=row, column=6, value="ИНН")

        for org in org_list:

            row += 1

            ws.cell(row=row, column=1, value=org["Имя"])
            ws.cell(row=row, column=2, value=org["Отчество"])
            ws.cell(row=row, column=3, value=org["Фамилия"])
            ws.cell(row=row, column=4, value=org["Телефон"])
            ws.cell(row=row, column=5, value=org["Название"])
            ws.cell(row=row, column=6, value=org["ИНН"])

        wb.save(f'up_tochka_{upload_num}.xlsx')

        send_document({
            "chat_id": "-407225582",
            "bot_id": "1223151642:AAEsV14bpRu-qcSl_58yiJswzTzvhehUO_U",
            "document": open(f'up_tochka_{upload_num}.xlsx', "rb"),
            "caption": f"{upload_num}.xlsx"
        }, None)


    def send_org(self, org_data, log, project_params):

        url = "https://open.tochka.com:3000/rest/v1/request/new"

        data = {
            "token": self.auth_data["token"],
            "workMode": 1,
            "request": {
                "telephone": org_data["Телефон"].split("|")[0],
                "inn": org_data["ИНН"],
                "name": org_data["Название"],
                "last_name": org_data["Фамилия"],
                "first_name": org_data["Имя"],
                "second_name": org_data["Отчество"],
                "comment": org_data["Комментарий"],
                "address": ",".join(org_data["Адрес"].split(",")[:5]) if org_data["Адрес"] else None
            }
        }

        try:
            log.write("{}: Send POST {} {}".format(datetime.now(), url, data))
            res = requests.post(url, json=data)

            res_data = json.loads(res.text)
            log.write("{}: Response {} {}".format(datetime.now(), res.status_code, res_data))

            try:
                if res_data[0].find("Ошибка") != -1:
                    return [{"error": res_data[0]}]
            except:
                pass
        except Exception as e:
            return [{"error": str(e)}]

        return []
