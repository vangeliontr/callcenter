from DataBase.DBController import DBController


class BankList:

    @staticmethod
    def get(cnt=None):

        if cnt is None:
            controller = DBController()
        else:
            controller = cnt

        cur = controller.get_cursor()

        cur.execute("""SELECT * From Bank;""")

        bank_list = cur.fetchall()

        res = []

        for bank_rec in bank_list:
            module = __import__("Bank." + bank_rec["Module"])
            res.append(getattr(getattr(module, bank_rec["Module"]), bank_rec["Module"])(bank_rec))

        return res

    @staticmethod
    def get_by_name(name):
        controller = DBController()
        cur = controller.get_cursor()

        cur.execute("""SELECT * From Bank WHERE Name = %s;""", (name,))

        bank_list = cur.fetchall()

        if len(bank_list) == 0:
            return None

        bank_rec = bank_list[0]

        module = __import__("Bank." + bank_rec["Module"])
        return getattr(getattr(module, bank_rec["Module"]), bank_rec["Module"])(bank_rec)


def block_org_by_sber(params, set_res):
    sberbank = BankList.get_by_name("Сбербанк")

    try:
        block_data = sberbank.lock_org(params["INN"], params["Phone"])
    except Exception as e:
        print(e)
        return {"sleep": 60}

    res = {"result": "",
           "data": None}

    if block_data is None:
        res["result"] = "Заблокирован другим партнером"
    elif block_data == "limit out":
        res["result"] = "Превышен лимит блокировок"
    else:
        res["result"] = "Заблокирован"
        res["data"] = block_data

    set_res(res)

def rich_org(params, set_res):
    mts_bank = BankList.get_by_name("МТС")

    try:
        numbers = mts_bank.extend_passport_data(params["passport"])
    except Exception as e:
        print(e)
        return {"sleep": 60}
    #numbers = {"79146344535": True}

    set_res(numbers)

def rich_org(params, set_res):
    mts_bank = BankList.get_by_name("МТС")

    try:
        numbers = mts_bank.extend_passport_data(params["passport"])
    except Exception as e:
        print(e)
        return {"sleep": 60}
    #numbers = {"79146344535": True}

    set_res(numbers)

if __name__ == "__main__":

    rich_org({"passport": 6909329012}, lambda test: test)

    #for bank in BankList.get():
    #    res = bank.is_region_allow("629320,ЯМАЛО-НЕНЕЦКИЙ АВТОНОМНЫЙ ОКРУГ, ,НОВЫЙ УРЕНГОЙ ГОРОД, ,МИРА ПРОСПЕКТ,ДОМ 32, ,КВАРТИРА 129")
    #    bank.is_in_odp("390507916036")
