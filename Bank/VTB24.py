from Bank.Bank import Bank
from CallCenter.Skorozvon import Skorozvon
from DataBase.DBController import DBController
from datetime import datetime, timedelta
import hashlib
import json
import requests
from urllib.parse import quote
from openpyxl import load_workbook


class VTB24(Bank):

    def __init__(self, rec):
        super().__init__(rec)

        ad = json.loads(self.auth_data())

        self.key1 = ad["key1"]
        self.key2 = ad["key2"]

        self.Token = None

        if "Token" in ad and ad["Token"]:
            self.Token = ad["Token"]


    def auth(self):
        now = datetime.now()
        date = now.strftime('%a, %d %b %Y %H:%M:%S Europe/Moscow')
        hash = hashlib.sha256((self.key2 + hashlib.sha256((self.key1 + date).encode()).hexdigest()).encode()).hexdigest()
        Token = "#" + self.key1 + "#" + hash

        url = "https://mb-partner.bm.ru/auth/ident"

        res = requests.post(url, headers={'Token': Token, "Date": date}, verify=False)

        response = json.loads(res.text)

        ad = {
            "Token": response["access_token"]["token"],
            "key1": self.key1,
            "key2": self.key2
        }

        self.save_auth_data(json.dumps(ad))
        return response["access_token"]["token"]

    def is_allow_uncorrect_address(self):
        return True

    def is_in_odp(self, inn):

        def impl():
            if self.Token is None:
                self.Token = self.auth()

            url = "https://mb-partner.bm.ru//anketa/anketa_exists_inn?inn=" + inn

            res = requests.get(url, headers={'Token': self.Token}, verify=False)

            if res.status_code != 200:
                raise Exception("Возврат не 200")

            response = json.loads(res.text)

            if response["status_code"] == '13':
                return False

            return True

        try:
            return impl()
        except:
            self.Token = None
            return impl()

    def send_org(self, org, log, project_params=None):

        if self.is_in_odp(org["ИНН"]):
            log.write("На момент отправки клиент в ОДП")
            return "На момент отправки клиент в ОДП"

        region = None
        city = None
        office = None

        if org["Комментарий"] is None or org["Комментарий"].find("#ОфисБанка:") == -1:
            log.write("Нет информации об отделении банка")
            return "Нет информации об отделении банка"

        address = org["Комментарий"]
        start_pos = address.find("#ОфисБанка:")
        address = org["Комментарий"][start_pos + 1:]
        end_pos = address.find("#")

        address = address[:end_pos]

        fields = address.split(":")

        region = fields[2]
        city = fields[3]
        office = fields[4]

        if region is None or city is None or office is None:
            raise Exception("Не удалось определить офис банка")

        if self.Token is None:
            self.Token = self.auth()

        url = "https://mb-partner.bm.ru/anketa/add"

        res = requests.post(url, headers={'Token': self.Token}, verify=False)
        add_response = json.loads(res.text)

        body = {
            "inn": org["ИНН"],
            "org_name": org["Название"],
            "contact_phone": org["Телефон"].split("|")[0],
            "fio": org["Фамилия"] + ' ' + org["Имя"] + ' ' + org["Отчество"],
            "region": region,
            "city": city,
            "branch": int(office),
            "agreement": "1",
            "add_info": "Профит Сейл"
        }

        url = "https://mb-partner.bm.ru/anketa/" + add_response["id_anketa"] + "/edit"

        log.write("Запрос\n")
        log.write(str(body))

        res = requests.post(url,
                            data="anketaid=" + add_response["id_anketa"] + "&anketadata=" + quote(json.dumps(body)),
                            headers={'Token': self.Token, 'Content-Type': 'application/x-www-form-urlencoded'},
                            verify=False)

        url = "https://mb-partner.bm.ru/anketa/" + add_response["id_anketa"] + "/apply"

        res = requests.post(url, data="id=" + add_response["id_anketa"],
                            headers={'Token': self.Token, 'Content-Type': 'application/x-www-form-urlencoded'},
                            verify=False)

        log.write("Результат" + str(res))
        log.write(res.text)

        try:
            res = json.loads(res.text)
            if "status_code" in res and res["status_code"] == '8':
                return res["info"]
        except:
            pass

    def get_work_region_list(self):

        def impl():
            if self.Token is None:
                self.Token = self.auth()

            url = "https://mb-partner.bm.ru/misc/regions"

            res = requests.get(url, headers={'Token': self.Token}, verify=False)
            response = json.loads(res.text)

            res = []

            for region in response["region_list"]:
                res.append({'ID': region['value'], 'name': region['name']})

            return res

        try:
            return impl()
        except:
            self.Token = None
            return impl()

    def get_work_region_city_list(self, region, _region_name):
        if self.Token is None:
            self.Token = self.auth()

        url = "https://mb-partner.bm.ru/misc/cities?region_code=" + region

        res = requests.get(url, headers={'Token': self.Token}, verify=False)
        response = json.loads(res.text)

        res = []

        for city in response["city_list"]:
            res.append({'ID': city['id']['value'], 'name': city['name']['value']})

        return res

    def get_work_region_city_office_list(self, region, city):

        #if region == '50' or region == '77':
        #    return [{"ID": '4327', "name": 'м. Кузнецкий мост, ул. Пушечная, д.5'}]

        if self.Token is None:
            self.Token = self.auth()

        url = "https://mb-partner.bm.ru//misc/branch/" + region

        if city:
            url += "?city_id=" + city

        res = requests.get(url, headers={'Token': self.Token}, verify=False)
        response = json.loads(res.text)

        res = []

        for branch in response["branch_list"]["list"]:

            if branch['name']['value'] == 'Колл-центр':
                continue

            res.append({'ID': branch['id']['value'], 'name': branch['name']['value']})

        return res

    def is_in_odp_full(self, inn, phone, acc_data):

        return self.is_in_odp(inn)

    def get_org_list(self, from_date, to_date):

        to_date = datetime.strptime(to_date, "%d.%m.%Y")
        to_date = to_date + timedelta(days=1)
        to_date = to_date.strftime("%d.%m.%Y")

        if self.Token is None:
            self.Token = self.auth()

        is_complete = False

        result = []
        offset = 0
        limit = 200

        while not is_complete:
            url = "https://mb-partner.bm.ru/anketa?date_from={}&date_to={}&limit={}&start={}".format(from_date, to_date, limit, offset)

            r = requests.get(url, headers={'Token': self.Token}, verify=False)
            #print(r.text)
            res = json.loads(r.text)

            for org in res["anketa_list"]['list']:
                if org["fields"]["status_web"]["value"] != 11:
                    result.append(org)

            if len(res["anketa_list"]['list']) < limit:
                is_complete = True
            else:
                offset += limit


        return result

    def get_fio_by_contact(self, org, cur):
        return org['fields']['fio']['value']

    def get_results_ids(self):
        return [20000018436]

    def get_scenario_id(self):
        return [20000001410]

    def is_multithread_odp(self):
        return False

    def odp_delay(self):
        pass

    def get_month_report(self, month, year):

        wb = load_workbook(filename="report_template.xlsx")

        controller = DBController()
        cur = controller.get_cursor()

        SQL = """SELECT Call_.Created, Call_.ExtID, Organization.INN
                 FROM Organization
                 INNER JOIN Call_Organization ON Call_Organization.Organization = Organization.ID
                 INNER JOIN Call_ ON Call_Organization.Call_ = Call_.ID
                 WHERE Call_.Bank = {} AND (Call_.Result = 'Отправлено' OR Call_.Result = 'Лид') AND 
                       DATE(Call_.Created) >= '2020-{}-01' AND DATE(Call_.Created) < '2020-{}-01';""".\
            format(self.id(), month, int(month) + 1)

        cur.execute(SQL)
        our_org_list = cur.fetchall()

        skorozvon = Skorozvon()

        #wb = Workbook()
        ws = wb.active
        ws.title = "Отчет"

        #ws.cell(row=1, column=1, value="ИНН")
        #ws.cell(row=1, column=2, value="Телефон")
        #ws.cell(row=1, column=3, value="Дата")
        #ws.cell(row=1, column=4, value="Время")
        #s.cell(row=1, column=5, value="Длительност")

        total_cnt = len(our_org_list)
        cnt = 0
        row = 1
        test = False

        for our_org in our_org_list:

            if test:
                break

            cnt += 1

            print("""{} из {}""".format(cnt, total_cnt))
            print(str(our_org))

            in_process = True
            error_cnt = 0

            while in_process:
                try:
                    info = skorozvon.get_contractor_info(our_org["ExtID"])
                except Exception as e:
                    #error_cnt += 1

                    #if error_cnt < 10:
                    #    print("Ошибка")
                    #    continue
                    pass

                try:

                    call_info = None

                    call_date_list = info["data"]["history"]["data"]

                    for sk_call_date in call_date_list:
                        if sk_call_date['result'] == 'Лид':
                            call_info = sk_call_date

                    if call_info is None:
                        call_info = {"connected_at": our_org["Created"], "seconds": 0, "phone": ""}

                    call_date = call_info["connected_at"]

                    call_time = call_date[11:]
                    call_date = call_date[:10]

                    call_duration = call_info["seconds"]
                    call_phone = call_info["phone"]

                    row += 1

                    ws.cell(row=row, column=1, value=our_org["INN"])
                    ws.cell(row=row, column=2, value=call_phone)
                    ws.cell(row=row, column=3, value="{} {}".format(call_date, call_time))
                    ws.cell(row=row, column=4, value="{} {}".format(call_date, call_time))
                    ws.cell(row=row, column=5, value=call_duration)

                except:
                    pass

                in_process = False

        return wb


if __name__ == "__main__":
    from DataBase.DBController import DBController
    controller = DBController()
    cur = controller.get_cursor()

    cur.execute("""SELECT * From Bank WHERE Name = %s;""", ("ВТБ24",))

    bank_list = cur.fetchall()

    bank_rec = bank_list[0]

    bank = VTB24(bank_rec)
    bank.is_in_odp("310262282808")