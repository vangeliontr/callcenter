from Bank.Bank import Bank
import json
import requests
import time

from datetime import datetime
from datetime import timedelta


class TinkoffBank(Bank):

    def __init__(self, rec, test = None):
        super().__init__(rec)

        try:
            ad = rec["AuthData"]

            if ad is not None:
                self.auth_data = json.loads(ad)
        except Exception as e:
            raise e

        self.headers = {'Authorization': """Partner-Basic api-key="{}", api-secret="{}", agent-id="{}" """.
                        format(self.auth_data["api-key"], self.auth_data["api-secret"], self.auth_data["agent-id"])}

    def is_allow_uncorrect_address(self):
        return True

    def is_in_odp(self, _inn):

        return False

    def is_mass_load(self):
        return False

    def send_org(self, org, log, product="РКО", project_params=None):

        if project_params and "Банк" in project_params:
            bank_params = project_params["Банк"]

            if "api-key" in bank_params and "api-secret" in bank_params and "agent-id" in bank_params:
                self.headers = {
                    'Authorization': """Partner-Basic api-key="{}", api-secret="{}", agent-id="{}" """.
                        format(bank_params["api-key"], bank_params["api-secret"], bank_params["agent-id"])}

        params = None

        comment = org["Комментарий"]
        start_pos = comment.find("#")

        if start_pos != -1:
            comment = comment[start_pos + 1:]
            end_pos = comment.find("#")

            if end_pos != -1:
                comment = comment[:end_pos]
                params = json.loads(comment)

        #org["Промокод"] = "psoldreg"

        if params is None or product != "РКО":
            return [self._send_org(org, log, product)]
        else:
            org["Комментарий"] = params["Комментарий"]

            res_list = []

            if "Время звонка" in params:
                org["Время звонка"] = "{}-{}-{}T{}:00.000".format(params["Время звонка"][6:10],
                                                           params["Время звонка"][3:5],
                                                           params["Время звонка"][0:2],
                                                           params["Время звонка"][11:])

            if "Предыдущий банк" in params and params["Предыдущий банк"] != "Прочее":
                org["Комментарий"] = "Сейачс обслуживается в {}; {}".format(params["Предыдущий банк"],
                                                                            org["Комментарий"])

            #if "Промокод" in params:
            #    org["Промокод"] = params["Промокод"]

            if "Аренда торговый эквайринг" in params["Список продуктов"] and \
                "РКО" in params["Список продуктов"]:
                params["Список продуктов"].remove("РКО")

            for prod in params["Список продуктов"]:

                if prod == "Аренда торговый эквайринг":
                    res_list.append(self._send_org(org, log, "РКО"))

                res_list.append(self._send_org(org, log, prod))

            return res_list

    def _send_org(self, org, log, product="РКО"):
        url = "https://origination.tinkoff.ru/api/v1/public/partner/createApplication"

        phones = org["Телефон"].split("|")

        if len(org["ИНН"]) == 11 or len(org["ИНН"]) == 9:
            org["ИНН"] = "0" + org["ИНН"]

        data = {
            "product": product,
            "source": "Федеральные партнеры",
            "subsource": "API",
            "firstName": org["Имя"],
            "middleName": org["Отчество"],
            "lastName": org["Фамилия"],
            "phoneNumber": phones[0],
            "isHot": org["Горячий"],
            "companyName": org["Название"],
            "innOrOgrn": org["ИНН"],
            "comment": org["Комментарий"],
            "temperature": org["temperature"]
        }

        if "Время звонка" in org:
            data["callTime"] = org["Время звонка"]

        #if "Промокод" in org:
        #    data["promocode"] = org["Промокод"]

        if product == "Оборотный кредит" and org["Комментарий"] is not None:
            comment = org["Комментарий"]
            start_pos = comment.find("#")
            comment = comment[start_pos + 1:]
            end_pos = comment.find("#")

            comment = comment[:end_pos]
            comment = json.loads(comment)

            data["products"] = [{
                "partNumber": "TFLL1.1",
                "term": comment["СрокКредита"],
                "termType": "Месяцы",
                "currency": "RUR",
                "paymentInfo": {
                    "clientAmount": comment["СуммаКредита"]
                }
            }]

            data["comment"] = ""

        log.write(str(data) + "\n")
        log.flush()

        r = requests.post(url, json=data, headers=self.headers)
        log.write(r.text + "\n")

        if r.status_code != 200:
            res = json.loads(r.text)

            return {'error': r.text}

        res = json.loads(r.text)

        try:
            return {'ID': res['result']['applicationId']}
        except:
            pass

        if 'errorCode' in res:
            return {'error': res['errorMessage']}

        return None

    def get_score_id(self, inn, _phone=None):

        if len(inn) == 9:
            inn = "0" + inn

        url = "https://origination.tinkoff.ru/api/v1/public/partner/innScoring"

        r = requests.post(url, json={"inn": inn}, headers=self.headers, timeout=20)

        res = json.loads(r.text)

        if "errorMessage" in res and res["errorMessage"] == "Неверный ИНН":
            return None

        return res["result"]["scoreId"]

    def get_odp_status(self, score_id, wait_res=True):

        is_ready = False

        while not is_ready:
            url = "https://origination.tinkoff.ru/api/v1/public/partner/innScoring/{}".format(score_id)

            open("log_score.txt", "a").write("{} {}\n".format(datetime.now(), url))

            try:
                r = requests.get(url, headers=self.headers, timeout=20)
                res = json.loads(r.text)

                is_ready = res['result']['isReady']

                if is_ready:
                    return res['result']['result']
                elif not wait_res:
                    return "ON_WORK"
            except:
                if not wait_res:
                    return "ON_WORK"

    def is_in_odp_full(self, inn, phone, acc_data):

        #acc_data = super().get_acc_data(7)

        #auth_data = json.loads(acc_data["data"])
        auth_data = acc_data["data"]

        print(auth_data["Название"])

        headers = {'Authorization': """Partner-Basic api-key="{}", api-secret="{}", agent-id="{}" """.
            format(auth_data["api-key"], auth_data["api-secret"], auth_data["agent-id"])}

        url = "https://origination.tinkoff.ru/api/v1/public/partner/scoring"
        data = {"inn": inn}

        proxies = {'https': auth_data["Proxy"]}

        try:
            r = requests.post(url, json=data, headers=headers, proxies=proxies)
        except Exception as e:
            raise Exception("\nПрокси: " + auth_data["Proxy"] + "\n" + str(e))

        res = {}

        #super().set_using_acc_data(acc_data)

        try:
            res = json.loads(r.text)
        except Exception as e:
            print(e)
            print(r.text)
            return None

        if "result" not in res:
            return True

        if res["result"] != "OK":
            return True

        url = "https://origination.tinkoff.ru/api/v1/public/partner/checkPhone"
        data = {"phoneNumber": phone}

        r = requests.post(url, json=data, headers=self.headers)
        res = json.loads(r.text)

        if "result" not in res:
            return True

        if "permissions" not in res["result"]:
            return True

        if res["result"]["permissions"] != "ANY":
            return True

        return False

    def get_org_list_old(self, start_date, end_date):

        start_date = start_date[6:] + "-" + start_date[3:5] + "-" + start_date[:2]
        end_date = end_date[6:] + "-" + end_date[3:5] + "-" + end_date[:2]

        return None

    def get_org_list(self, start_date, end_date):

        start_date = datetime.strptime(start_date, "%d.%m.%Y")
        start_date = start_date - timedelta(days=1)
        start_date = start_date.strftime("%Y-%m-%d") + "T21:00:00Z"

        end_date = datetime.strptime(end_date, "%d.%m.%Y")
        end_date = end_date - timedelta(days=1)
        end_date = end_date.strftime("%Y-%m-%d") + "T21:00:00Z"

        url = "https://origination.tinkoff.ru/api/v1/public/partner/applications?from={}&till={}".format(start_date, end_date)

        r = requests.get(url, headers=self.headers)

        res = json.loads(r.text)

        return res['result']

    def get_fio_by_contact(self, org, cur):

        cur.execute("""SELECT ContactPerson.* FROM Call_Bank 
                       INNER JOIN Call_ ON Call_.ID = Call_Bank.Call_
                       INNER JOIN ContactPerson ON Call_.ContactPerson = ContactPerson.ID 
                       WHERE Call_Bank.ExtID = '{}'""".format(org["id"]))

        res = cur.fetchall()

        if len(res) == 0:
            return org['id']

        org = res[0]

        return org["Surname"] + " " + org["Name"] + " " + org["MiddleName"]

    def get_results_ids(self):
        return [20000010368, 20000010367, 20000023505]

    def get_scenario_id(self):
        return [20000000783, 20000001779]

    def is_multithread_odp(self):
        return False

    def is_pre_check_odp(self):
        return True

    def pre_check_odp(self):
        pass

    def odp_delay(self):
        pass
