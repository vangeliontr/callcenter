#!/usr/bin/env python3

import sys

sys.path.append("../")
from Tools.AuthServer import start_server
from Tools.BufferedCGIHTTPRequestHandler import BufferedCGIHTTPRequestHandler


def run_server(port=None):

    if port is None:
        port = int(sys.argv[6])

    start_server("0.0.0.0", port, "SkzAPI")


if __name__ == "__main__":
    run_server(8101)
