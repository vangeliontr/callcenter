#!/usr/bin/python3

import os
import sys

sys.path.append("../")

import json
from datetime import datetime
from Bank.BankList import BankList
from DataBase.DBController import DBController
from DataBase.Project import Project
from DataBase.Task import Task
from DataBase.Organization import Organization
from DataBase.ContactPerson import ContactPerson
from DataBase.ContactInfo import ContactInfo
from DataBase.Organization import Organization
from CallCenter.Skorozvon import Skorozvon


try:
    controller = DBController()
    cur = controller.get_cursor()


    def send_to_telega(message):
        Task.set_task_impl(controller, "Оповещение проблем", json.dumps({"message": message}))
        pass

    res = {}

    log_file_name = "log_" + datetime.strftime(datetime.now(), "%Y.%m.%d %H.%M.%S.%f") + ".txt"
    log = open("./log/" + log_file_name, "w", encoding="cp1251")
    log_file_name = log_file_name.replace(" ", "%20")

    content_len = int(os.environ["CONTENT_LENGTH"])
    req_body = sys.stdin.read(content_len)

    request_file_name = "request_" + datetime.strftime(datetime.now(), "%Y.%m.%d %H.%M.%S.%f") + ".txt"
    f = open("./log/" + request_file_name, "w")
    f.write(req_body)
    f.close()

    request_file_name = request_file_name.replace(" ", "%20")

    request = json.loads(req_body)

    if "type" not in request and request[type] != "call_result":
        raise Exception("Неизвестный метод")

    org_data = request["lead"]

    if not org_data:
        raise Exception("Отсутствует информация о лиде")

    org_id = org_data["external_id"]
    ext_org_id = org_data["id"]

    if not org_id:
        raise Exception("Отсутствует идентификатор в нашей системе")

    sql = """SELECT
             ContactPerson.ID as CPID,
             ContactPerson.`Name` as CName,
             ContactPerson.Surname,
             ContactPerson.MiddleName,
             Organization.ID,
             Organization.`Name`,
             Organization.INN,
             Organization.Address
             FROM Organization
             LEFT JOIN ContactPerson ON Organization.LastContactPerson = ContactPerson.ID
             WHERE Organization.ID = {};""".format(org_id)

    cur.execute(sql)
    db_our_data = cur.fetchall()

    if len(db_our_data) == 0:
        raise Exception("По идентификатору в нашей системе не найдена организация")

    db_our_data = db_our_data[0]

    if request["type"] == "call_project_case_failed":
        call_result = {"result_id": "Не дозвонились"}
    else:
        call_result = request["call_result"]

    result_id = call_result["result_id"]

    call_info = None
    call_project = None

    if "call" in request:
        call_info = request["call"]
    else:
        call_project = request["call_project"]

    project_list = Project.list_in_work()

    #Обработка по проектам
    try:
        for project in project_list:
            project_params = project.params()["Скорозвон"]

            if call_info and project_params["scenario_id"] != call_info["scenario_id"] or \
               call_project and project_params["project_id"] != call_project["id"]:
                continue

            call_state = str(result_id)
            comment = ""
            result_params = None

            project_bank = project.bank()

            if str(result_id) in project_params["results"]:
                result_params = project_params["results"][str(result_id)]
                call_state = result_params["name"]

            call_info = {"scenario_id": 0}

            cur.execute("""SELECT Result, ID FROM Call_
                           WHERE ExtID = {};""".format(ext_org_id))

            call = cur.fetchall()

            if len(call) == 0:
                cur.execute("INSERT INTO Call_ SET Bank = {}, Project = {}, ExtID = {};".
                            format(project_bank.id(), project.id(), ext_org_id))
                call_id = cur.lastrowid

                cur.execute("INSERT INTO Call_Organization SET Call_ = %s, Organization = %s;",
                            (call_id, org_id))

                controller.save_changes()

                cur.execute("""SELECT Result, ID FROM Call_
                                          WHERE ID = {};""".format(call_id))

                call = cur.fetchall()

            if len(call) == 0:
                log.write("Не найден звонок " + str(org_id) + "\n")
                continue

            call = call[0]

            if call["Result"] == call_state:
                log.write("Не грузим повторно, статус " + str(result_id) + "\n")
                continue

            if result_params and "send" in result_params:

                cur.execute("""UPDATE Call_ Set Result = %s WHERE ID = %s;""", (call_state, call["ID"]))
                controller.save_changes()

                is_hot = None

                if result_params["send"] == "cold":
                    is_hot = False
                    temperature = "COLD"

                elif result_params["send"] == "hot":
                    is_hot = True
                    temperature = "HOT"

                params = None

                try:
                    comment = call_result["comment"]
                    start_pos = comment.find("#")

                    if start_pos != -1:
                        comment = comment[start_pos + 1:]
                        end_pos = comment.find("#")

                        if end_pos != -1:
                            comment = comment[:end_pos]
                            params = json.loads(comment)
                except:
                    pass

                if params:
                    organization = Organization.get_by_id(controller, db_our_data["ID"])

                    if "add_phone" in params:
                        contact_person = ContactPerson.get_by_id(controller, db_our_data["CPID"])
                        new_phone = params["add_phone"]

                        contact_info = ContactInfo.create(controller, contact_person, "Телефон", new_phone)
                        org_data["phones"] = new_phone

                        organization.link_contact(contact_person, contact_info)

                    if "add_contact_person" in params:
                        contact_person_info = params["add_contact_person"]

                        contact_person = ContactPerson.create(controller, contact_person_info["surname"],
                                                              contact_person_info["name"],
                                                              contact_person_info["middlename"], None)

                        contact_info = ContactInfo.create(controller, contact_person,
                                                          "Телефон", contact_person_info["phone"])

                        organization.link_contact(contact_person, contact_info)

                        db_our_data["CName"] = contact_person_info["name"]
                        db_our_data["MiddleName"] = contact_person_info["middlename"]
                        db_our_data["Surname"] = contact_person_info["surname"]
                        org_data["phones"] = ContactInfo.normalize_phone(contact_person_info["phone"])

                    cur = controller.get_cursor()

                log.write("Грузим {} в {} по проекту {}\n".format(call["ID"], project_bank.name(), project.name()))

                data_to_send = {
                    "Имя": db_our_data["CName"],
                    "Отчество": db_our_data["MiddleName"],
                    "Фамилия": db_our_data["Surname"],
                    "Телефон": request["call"]["phone"],
                    "Горячий": is_hot,
                    "Название": db_our_data["Name"],
                    "ИНН": db_our_data["INN"],
                    "Адрес": db_our_data["Address"],
                    "Комментарий": call_result["comment"],
                    "temperature": temperature
                }

                try:
                    id = project_bank.get_score_id(db_our_data["INN"])

                    log.write("ID проверки: {}\n".format(id))

                    if id is not None:
                        res = project_bank.get_odp_status(id)

                        log.write("Результат проверки: {}\n".format(res))

                        if res != "APPROVED":
                            odp = open("odp.txt", "a", encoding="cp1251")
                            log.write("Контакт в ОДП\n")

                            message = "Отправляемый контакт в ОДП банка {}\n{} {}\n".format(
                                       project_bank.name(), datetime.strftime(datetime.now(), "%Y.%m.%d %H.%M.%S"),
                                       db_our_data["INN"])

                            odp.write(message)
                            odp.close()

                            send_to_telega(message)
                        else:
                            log.write("Контакт не в ОДП\n")
                except:
                    pass

                try:
                    cur.execute("SELECT * FROM Call_Bank WHERE Call_ = {} ORDER BY ID DESC".format(call["ID"]))
                    block_rec = cur.fetchone()

                    if block_rec is None:
                        log.write("Нет информации о блокировке, блокируем\n")
                        lock_res = project_bank.lock_org(data_to_send["ИНН"], data_to_send["Телефон"])
                        data_to_send["block_data"] = lock_res
                    else:
                        data_to_send["block_data"] = json.loads(block_rec["Data"])

                        if datetime.strptime(data_to_send["block_data"]['block_until'][:19],
                             "%Y-%m-%dT%H:%M:%S") < datetime.now():

                            log.write("На момент отправки блокировка {} истекла, блокируем\n".format(data_to_send["block_data"]))
                            lock_res = project_bank.lock_org(data_to_send["ИНН"], data_to_send["Телефон"])
                            data_to_send["block_data"] = lock_res

                            if lock_res is None:
                                # возможно поток блокировки уже получил новую блокировку
                                cur.execute(
                                    "SELECT * FROM Call_Bank WHERE Call_ = {} ORDER BY ID DESC".format(call["ID"]))
                                block_rec = cur.fetchone()
                                data_to_send["block_data"] = json.loads(block_rec["Data"])

                        else:
                            log.write("На момент отправки есть актуаьлная блокировка {}\n".format(
                                data_to_send["block_data"]))

                except Exception as e:
                    log.write("Ошибка при получении данных о блокировке: {}\n".format(e))
                    pass

                log.write("Предыдущее состояние: {}\n".format(call["Result"]))

                res_list = project_bank.send_org(data_to_send, log, project_params=project.params())

                for res in res_list:
                    if res is not None and 'ID' in res:
                        res_id = res['ID']
                        cur.execute("""INSERT INTO Call_Bank Set Call_ = %s, Bank = %s, ExtID = %s;""",
                                    (call["ID"], 7, res_id))
                        controller.save_changes()

                    if res is not None and 'error' in res:
                        comment = res['error'] + "<br/>"
                        send_to_telega("Ошибка при отправке {} в {}: {}\n"
                                       "Лог: {}\n"
                                       "Запрос: {}".format(db_our_data["INN"],
                                                          project_bank.name(),
                                                          comment,
                                                          "https://194.32.76.107:8001/log/{}".format(log_file_name.replace(" ", "%20")),
                                                          "https://194.32.76.107:8001/log/{}".format(request_file_name.replace(" ", "%20"))))

                comment += '<a href="https://194.32.76.107:8001/log/{}">Лог</a><br/>\n'.format(log_file_name)
                comment += '<a href="https://194.32.76.107:8001/log/{}">Запрос</a><br/>\n'.format(request_file_name)
            else:
                log.write("Не грузим, статус " + str(result_id) + "\n")

            if result_params and "operations" in result_params and len(result_params["operations"]) > 0:
                log.write("Выполняем список операций результата {}\n".format(result_params["operations"]))

                for operation in result_params["operations"]:

                    # Не хотим больше обрабатывать контакт ни по одному проекту.
                    # Для этого необходимо удалить все звонки из скорозвона.
                    if operation == "Прекратить обработку контакта":
                        skz_client = Skorozvon()
                        # Сначала найдем все звонки данного контакта
                        org = Organization.get_by_id(controller, org_id)
                        call_list = org.call_list()

                        # удалим все звонки из скорозвона, кроме текущего
                        for exist_call in call_list:

                            if exist_call.id() == call["ID"]:
                                continue

                            del_res = skz_client.set_odp_state({"ExtID": exist_call.ext_id()})

                            if del_res == "Internal Server Error":
                                skz_client.re_auth()
                                del_res = skz_client.set_odp_state({"ExtID": exist_call.ext_id()})

                            log.write("Результат удаления: {}\n".format(del_res))

                            # Поставим состояние звонку, если текщее состояние не отправлено и не ОДП
                            if exist_call.result() is None or \
                               exist_call.result().upper().find("ЛИД") == -1 and \
                               exist_call.result().upper().find("ОДП") == -1:

                                exist_call.set_result("{} (по другому проекту)".format(call_state))

                    elif operation == "Отложить обработку контакта":

                        try:

                            comment = call_result["comment"]

                            if comment and comment.find("#Время перезвона:") != -1:

                                cur.execute("SELECT * FROM Call_Bank WHERE Call_ = {}".format(call["ID"]))
                                block_rec = cur.fetchone()
                                block_data = json.loads(block_rec["Data"])

                                recall_date = comment[20:]
                                recall_date = recall_date[:16]

                                recall_date = "{}-{}-{}T{}:{}:00.000+03:00".\
                                    format(recall_date[6:10], recall_date[3:5], recall_date[0:2],
                                           recall_date[11:13], recall_date[14:16])

                                project_bank.set_lock_state(block_data["id"], "call_back", recall_date)

                                recall_date = "{} {}".format(recall_date[:10], recall_date[11:19])

                                cur.execute("""UPDATE Call_ 
                                               SET RecallDateTime = '{}', InernalResult = '{}', TryCount = TryCount + 1
                                               WHERE ID = %s;""", (recall_date, call_state, call["ID"]))
                                controller.save_changes()
                                call_state = None

                        except Exception as e:
                            log.write("Ошщибка при откладывании звонка в банке: {}\n".format(e))

                    elif operation == "Отложить обработку на 15 мин":
                        skz_client = Skorozvon()

                        del_res = skz_client.set_odp_state({"ExtID": ext_org_id})

                        if del_res == "Internal Server Error":
                            skz_client.re_auth()
                            del_res = skz_client.set_odp_state({"ExtID": ext_org_id})

                        #cur.execute("""UPDATE Call_ SET Duration = NOW() + INTERVAL 15 MINUTE, ExtID = NULL WHERE ID = %s;""", (call["ID"],))
                        cur.execute("""UPDATE Call_ SET RecallDateTime = NOW() + INTERVAL 15 MINUTE, ExtID = NULL, 
                                                        InernalResult = '{}', TryCount = TryCount + 1
                                                        WHERE ID = %s;""",
                                    (call_state, call["ID"]))

                        controller.save_changes()
                        call_state = None

                    elif operation == "Заблокировать контакт":
                        cur.execute("SELECT * FROM Call_Bank WHERE Call_ = {}".format(call["ID"]))
                        block_rec = cur.fetchone()
                        block_data = json.loads(block_rec["Data"])

                        project_bank.set_lock_state(block_data["id"], "disagree")

                    cur = controller.get_cursor()

            cur.execute("""UPDATE Call_ Set Result = %s, Comment = CONCAT(%s,COALESCE(Comment,'')) WHERE ID = %s;""",
                        (call_state, comment, call["ID"]))
            controller.save_changes()

            break
    except Exception as e:
        message = "Ошибка обработки статуса {} https://194.32.76.107:8001/log/{}".format(
            log_file_name.replace(" ", "%20"))

        send_to_telega(message)

    if call_info is None:
        call_info = {"scenario_id": 0}

    # Обработка результатов по банку Тинькофф
    if call_info["scenario_id"] == 20000000783:

        is_hot = None

        if result_id == "20000010368" or result_id == 20000010368:
            is_hot = False
            temperature = "COLD"
        elif result_id == "20000010367" or result_id == 20000010367:
            is_hot = True
            temperature = "HOT"

        log.write("Результат по контакту " + org_id + "\n")

        if is_hot is not None:

            cur.execute("""SELECT Result, ID FROM Call_
                           WHERE ExtID = {};""".format(ext_org_id))

            empty_call = cur.fetchall()

            if len(empty_call) == 0:
                cur.execute("INSERT INTO Call_ SET Bank = {}, ExtID = {};".format(7, ext_org_id))
                call_id = cur.lastrowid

                cur.execute("INSERT INTO Call_Organization SET Call_ = %s, Organization = %s;",
                            (call_id, org_id))

                controller.save_changes()

                cur.execute("""SELECT Result, ID FROM Call_
                               WHERE ID = {};""".format(call_id))

                empty_call = cur.fetchall()

            if len(empty_call) != 0:

                empty_call = empty_call[0]

                if empty_call["Result"] != "ОтправленоХолодный" and empty_call["Result"] != "ОтправленоГорячий":
                    log.write("Грузим в ТФ {}\n".format(empty_call["ID"]))

                    result = "ОтправленоХолодный"

                    if is_hot:
                        result = "ОтправленоГорячий"

                    cur.execute("""UPDATE Call_ Set Result = %s WHERE ID = %s;""", (result, empty_call["ID"]))
                    controller.save_changes()

                    data_to_send = {
                        "Имя": db_our_data["CName"],
                        "Отчество": db_our_data["MiddleName"],
                        "Фамилия": db_our_data["Surname"],
                        "Телефон": org_data["phones"],
                        "Горячий": is_hot,
                        "Название": db_our_data["Name"],
                        "ИНН": db_our_data["INN"],
                        "Комментарий": "",
                        "temperature": temperature
                    }

                    try:
                        sender = BankList.get_by_name("Тинькофф")

                        id = sender.get_score_id(db_our_data["INN"])

                        if id is not None:
                            res = sender.get_odp_status(id)

                            if res != "APPROVED":
                                odp = open("odp.txt", "a", encoding="cp1251")
                                log.write("Контакт в ОДП\n")

                                message = "Отправляемый контакт в ОДП банка Тинькофф\n{} {}\n".format(
                                    datetime.strftime(datetime.now(), "%Y.%m.%d %H.%M.%S"),
                                    db_our_data["INN"])

                                odp.write(message)
                                odp.close()

                                send_to_telega(message)
                            else:
                                log.write("Контакт не в ОДП\n")
                    except:
                        pass

                    log.write("Предыдущее состояние: {}\n".format(empty_call["Result"]))

                    res = sender.send_org(data_to_send, log)

                    if res is not None and 'ID' in res:
                        res_id = res['ID']
                        cur.execute("""INSERT INTO Call_Bank Set Call_ = %s, Bank = %s, ExtID = %s;""", (empty_call["ID"], 7, res_id))
                        controller.save_changes()

                    comment = ""
                    if res is not None and 'error' in res:
                        comment = res['error'] + "<br/>"

                    comment += '<a href="https://194.32.76.107:8001/log/{}">Лог</a><br/>\n'.format(log_file_name)
                    comment += '<a href="https://194.32.76.107:8001/log/{}">Запрос</a><br/>\n'.format(request_file_name)

                    cur.execute("""UPDATE Call_ Set Result = %s, Comment = CONCAT(%s,COALESCE(Comment,'')) WHERE ID = %s;""",
                                (result, comment, empty_call["ID"]))
                    controller.save_changes()
                else:
                    log.write("Не грузим повторно, статус " + str(result_id) + "\n")
            else:
                log.write("Не грузим повторно, статус " + str(result_id) + "\n")
        else:
            log.write("Не грузим, статус " + str(result_id) + "\n")

            res_dict = {
                        20000010364: "Больше не звоним",
                        20000009968: "Занято (системный)",
                        20000010367: "Лид (горячий)",
                        20000010363: "Не взяли трубку",
                        20000009977: "Неопознанная ошибка (системный)",
                        20000009972: "Несуществующий номер",
                        20000009971: "Нет ответа (системный)",
                        20000017411: "Номер из черного списка (системный)",
                        20000009967: "Обнаружен автоответчик (системный)",
                        20000009976: "Оператор занят (системный)",
                        20000009973: "Оператор не принял вызов (системный)",
                        20000010365: "Отказ",
                        20000009974: "Отклонен оператором (системный)",
                        20000009975: "Отклонен (системный)",
                        20000009979: "Отложен (системный)",
                        20000009807: "Перезвонить",
                        20000009980: "Повторный вызов (системный)",
                        20000009966: "Потерян (cистемный)",
                        20000009978: "Системная ошибка (системный)",
                        20000009969: "Соединен (системный)",
                        20000009970: "Сообщение не проиграно (системный)",
                        20000010366: "Уже звонили"
                       }

            call_state = str(result_id)
            if result_id in res_dict:
                call_state = res_dict[result_id]

            cur.execute("""SELECT Result, ID FROM Call_
                           WHERE ExtID = {};""".format(ext_org_id))

            bank_call = cur.fetchall()

            cur.execute("""UPDATE Call_ Set Result = %s WHERE ID = %s;""", (call_state, bank_call[0]["ID"]))
            controller.save_changes()
    # Обработка результатов по банку Сбер
    elif call_info["scenario_id"] == 20000001128:

        if result_id == 20000014499:
            cur.execute("""SELECT Result, ID FROM Call_
                           WHERE ExtID = {};""".format(ext_org_id))

            empty_call = cur.fetchall()

            if len(empty_call) == 0:
                cur.execute("INSERT INTO Call_ SET Bank = {}, ExtID = {};".format(8, ext_org_id))
                call_id = cur.lastrowid

                cur.execute("INSERT INTO Call_Organization SET Call_ = %s, Organization = %s;",
                            (call_id, org_id))

                controller.save_changes()

                cur.execute("""SELECT Result, ID FROM Call_
                               WHERE ID = {};""".format(call_id))

                empty_call = cur.fetchall()

            if len(empty_call) != 0:

                empty_call = empty_call[0]

                if empty_call["Result"] != "Отправлено":

                    log.write("Грузим в СБ\n")

                    data_to_send = {
                        "Имя": db_our_data["CName"],
                        "Отчество": db_our_data["MiddleName"],
                        "Фамилия": db_our_data["Surname"],
                        "Телефон": org_data["phones"],
                        "Название": db_our_data["Name"],
                        "ИНН": db_our_data["INN"],
                        "Адрес": db_our_data["Address"],
                        "Комментарий": call_result["comment"]
                    }

                    sender = BankList.get_by_name("Сбербанк")

                    comment = sender.send_org(data_to_send, log)
                    result = "Отправлено"

                    if comment:
                        send_to_telega("Ошибка при отправке заявки в Сбербанк: {}".format(comment))
                        comment += '\n'
                    else:
                        comment = ""

                    comment += '<a href="https://194.32.76.107:8001/log/{}">Лог</a><br/>\n'.format(log_file_name)
                    comment += '<a href="https://194.32.76.107:8001/log/{}">Запрос</a><br/>\n'.format(request_file_name)

                    cur.execute("""UPDATE Call_ Set Result = %s, Comment = %s WHERE ID = %s;""",
                                (result, comment, empty_call["ID"]))
                    controller.save_changes()
                else:
                    log.write("Не грузим повторно, статус " + str(result_id) + "\n")
        else:
            log.write("Не грузим, статус " + str(result_id) + "\n")

            res_dict = {
                20000014496: "Больше не звоним",
                20000014502: "Занято (системный)",
                20000014499: "Лид",
                20000014495: "Не взяли трубку",
                20000014511: "Неопознанная ошибка (системный)",
                20000014506: "Несуществующий номер",
                20000014505: "Нет ответа (системный)",
                20000017464: "Номер из черного списка (системный)",
                20000014501: "Обнаружен автоответчик (системный)",
                20000014510: "Оператор занят (системный)",
                20000014507: "Оператор не принял вызов (системный)",
                20000014498: "Отказ",
                20000014508: "Отклонен оператором (системный)",
                20000014509: "Отклонен (системный)",
                20000014513: "Отложен (системный)",
                20000014494: "Перезвонить",
                20000014514: "Повторный вызов (системный)",
                20000014500: "Потерян (cистемный)",
                20000014512: "Системная ошибка (системный)",
                20000014503: "Соединен (системный)",
                20000014504: "Сообщение не проиграно (системный)",
                20000014497: "Уже звонили"
            }

            call_state = str(result_id)
            if result_id in res_dict:
                call_state = res_dict[result_id]

            cur.execute("""SELECT Result, ID FROM Call_
                           WHERE ExtID = {};""".format(ext_org_id))

            bank_call = cur.fetchall()

            cur.execute("""UPDATE Call_ Set Result = %s WHERE ID = %s;""", (call_state, bank_call[0]["ID"]))
            controller.save_changes()
    # Обработка результатов по банку ВТБ
    elif call_info["scenario_id"] == 20000001410:

        if result_id == 20000018436:
            cur.execute("""SELECT Result, ID FROM Call_
                           WHERE ExtID = {};""".format(ext_org_id))

            empty_call = cur.fetchall()

            if len(empty_call) == 0:
                cur.execute("INSERT INTO Call_ SET Bank = {}, ExtID = {};".format(9, ext_org_id))
                call_id = cur.lastrowid

                cur.execute("INSERT INTO Call_Organization SET Call_ = %s, Organization = %s;",
                            (call_id, org_id))

                controller.save_changes()

                cur.execute("""SELECT Result, ID FROM Call_
                               WHERE ID = {};""".format(call_id))

                empty_call = cur.fetchall()

            if len(empty_call) != 0:

                empty_call = empty_call[0]

                if empty_call["Result"] != "Отправлено":
                    log.write("Грузим в ВТБ\n")

                    result = "Отправлено"

                    cur.execute("""UPDATE Call_ Set Result = %s WHERE ID = %s;""", (result, empty_call["ID"]))
                    controller.save_changes()

                    data_to_send = {
                        "Имя": db_our_data["CName"],
                        "Отчество": db_our_data["MiddleName"],
                        "Фамилия": db_our_data["Surname"],
                        "Телефон": org_data["phones"],
                        "Название": db_our_data["Name"],
                        "ИНН": db_our_data["INN"],
                        "Адрес": db_our_data["Address"],
                        "Комментарий": call_result["comment"]
                    }

                    sender = BankList.get_by_name("ВТБ24")

                    comment = sender.send_org(data_to_send, log)

                    if comment:
                        send_to_telega("Ошибка при отправке заявки в ВТБ24: {}".format(comment))
                        comment += '\n'
                    else:
                        comment = ""

                    comment += '<a href="https://194.32.76.107:8001/log/{}">Лог</a><br/>\n'.format(log_file_name)
                    comment += '<a href="https://194.32.76.107:8001/log/{}">Запрос</a><br/>\n'.format(request_file_name)

                    cur.execute("""UPDATE Call_ Set Result = %s, Comment = %s WHERE ID = %s;""",
                                (result, comment, empty_call["ID"]))
                    controller.save_changes()
                else:
                    log.write("Не грузим повторно, статус " + str(result_id) + "\n")
            else:
                log.write("Выгрузки контакта по банку не было " + str(result_id) + "\n")
        else:
            log.write("Не грузим, статус " + str(result_id) + "\n")

            res_dict = {
                20000018433: "Больше не звоним",
                20000018439: "Занято (системный)",
                20000018436: "Лид",
                20000018432: "Не взяли трубку",
                20000018448: "Неопознанная ошибка (системный)",
                20000018443: "Несуществующий номер",
                20000018442: "Нет ответа (системный)",
                20000018452: "Номер из черного списка (системный)",
                20000018438: "Обнаружен автоответчик (системный)",
                20000018447: "Оператор занят (системный)",
                20000018444: "Оператор не принял вызов (системный)",
                20000018435: "Отказ",
                20000018445: "Отклонен оператором (системный)",
                20000018446: "Отклонен (системный)",
                20000018450: "Отложен (системный)",
                20000018431: "Перезвонить",
                20000018451: "Повторный вызов (системный)",
                20000018437: "Потерян (cистемный)",
                20000018449: "Системная ошибка (системный)",
                20000018440: "Соединен (системный)",
                20000018441: "Сообщение не проиграно (системный)",
                20000018434: "Уже звонили"
            }

            call_state = str(result_id)
            if result_id in res_dict:
                call_state = res_dict[result_id]

            cur.execute("""SELECT Result, ID FROM Call_
                           WHERE ExtID = {};""".format(ext_org_id))

            bank_call = cur.fetchall()

            cur.execute("""UPDATE Call_ Set Result = %s WHERE ID = %s;""", (call_state, bank_call[0]["ID"]))
            controller.save_changes()
    # Обработка результатов по банку МТС
    elif call_info["scenario_id"] == 20000001780:

        if result_id == 20000023527:
            cur.execute("""SELECT Result, ID FROM Call_
                           WHERE ExtID = {};""".format(ext_org_id))

            empty_call = cur.fetchall()

            if len(empty_call) == 0:
                cur.execute("INSERT INTO Call_ SET Bank = {}, ExtID = {};".format(11, ext_org_id))
                call_id = cur.lastrowid

                cur.execute("INSERT INTO Call_Organization SET Call_ = %s, Organization = %s;",
                            (call_id, org_id))

                controller.save_changes()

                cur.execute("""SELECT Result, ID FROM Call_
                               WHERE ID = {};""".format(call_id))

                empty_call = cur.fetchall()

            if len(empty_call) != 0:

                empty_call = empty_call[0]

                if empty_call["Result"] != "Отправлено":
                    log.write("Грузим в МТС\n")

                    result = "Отправлено"

                    cur.execute("""UPDATE Call_ Set Result = %s WHERE ID = %s;""", (result, empty_call["ID"]))
                    controller.save_changes()

                    cur.execute("SELECT count(*) + 1 AS CNT FROM Call_ WHERE Bank = 11 AND Result = 'Отправлено';")
                    count = cur.fetchall()[0]["CNT"]


                    data_to_send = {
                        "Имя": db_our_data["CName"],
                        "Отчество": db_our_data["MiddleName"],
                        "Фамилия": db_our_data["Surname"],
                        "Телефон": org_data["phones"],
                        "Название": db_our_data["Name"],
                        "ИНН": db_our_data["INN"],
                        "Адрес": db_our_data["Address"],
                        "Комментарий": call_result["comment"],
                        "Номер": str(count)
                    }

                    sender = BankList.get_by_name("МТС")

                    comment = sender.send_org(data_to_send, log)

                    comment += '<a href="https://194.32.76.107:8001/log/{}">Лог</a><br/>\n'.format(log_file_name)
                    comment += '<a href="https://194.32.76.107:8001/log/{}">Запрос</a><br/>\n'.format(request_file_name)

                    cur.execute(
                        """UPDATE Call_ Set Result = %s, Comment = CONCAT(%s,COALESCE(Comment,'')) WHERE ID = %s;""",
                        (result, comment, empty_call["ID"]))

                    controller.save_changes()
                else:
                    log.write("Не грузим повторно, статус " + str(result_id) + "\n")
            else:
                log.write("Выгрузки контакта по банку не было " + str(result_id) + "\n")
        #else:
        #    log.write("Не грузим, статус " + str(result_id) + "\n")

        #    res_dict = {
        #        20000018433: "Больше не звоним",
        #        20000018439: "Занято (системный)",
        #        20000018436: "Лид",
        #        20000018432: "Не взяли трубку",
        #        20000018448: "Неопознанная ошибка (системный)",
        #        20000018443: "Несуществующий номер",
        #        20000018442: "Нет ответа (системный)",
        #        20000018452: "Номер из черного списка (системный)",
        #        20000018438: "Обнаружен автоответчик (системный)",
        #        20000018447: "Оператор занят (системный)",
        #        20000018444: "Оператор не принял вызов (системный)",
        #        20000018435: "Отказ",
        #        20000018445: "Отклонен оператором (системный)",
        #        20000018446: "Отклонен (системный)",
        #        20000018450: "Отложен (системный)",
        #        20000018431: "Перезвонить",
        #        20000018451: "Повторный вызов (системный)",
        #        20000018437: "Потерян (cистемный)",
        #        20000018449: "Системная ошибка (системный)",
        #        20000018440: "Соединен (системный)",
        #        20000018441: "Сообщение не проиграно (системный)",
        #        20000018434: "Уже звонили"
        #    }

        #    call_state = str(result_id)
        #    if result_id in res_dict:
        #        call_state = res_dict[result_id]

        #    cur.execute("""SELECT Call_.ID FROM Call_Organization
        #                           INNER JOIN Call_ ON Call_Organization.Call_ = Call_.ID
        #                           WHERE Call_Organization.Organization = {} AND
        #                           Call_.Bank = 7;""".format(org_id))

        #    bank_call = cur.fetchall()

        #    cur.execute("""UPDATE Call_ Set Result = %s WHERE ID = %s;""", (call_state, bank_call[0]["ID"]))
        #    controller.save_changes()

except Exception as e:

    message = "Ошибка обработки статуса {} https://194.32.76.107:8001/log/{}".format(log_file_name.replace(" ", "%20"))

    send_to_telega(message)

    log.write("Ошибка обработки " + str(e))
    print('''Status:500\r\n''')
else:
    print("Content-type: application/json")
    print()
    print(json.JSONEncoder().encode(res))
