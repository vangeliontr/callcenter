import time
import traceback
from threading import Thread, current_thread, active_count
from DataBase.DBController import DBController
from DataBase.Task import Task
from DataBase.TaskDescriptor import TaskDescriptor
from concurrent.futures.thread import ThreadPoolExecutor

SLEEP_DURATION = 1


def handle_task_impl(task_rec):

    controller = None

    while controller is None:
        try:
            controller = DBController()
        except Exception as e:
            print(e)

    task = Task(task_rec, controller)

    task.set_res({})

    print("Поток {}: Получил задачу {}".format(current_thread().ident, task.id()))

    task_handler = TaskDescriptor.get_by_task(controller, task.id())

    if task_handler is None:
        print("Поток {}: Не найден обработчик для задачи {}".format(current_thread.ident, task.id()))
        task.set_res({"Ошибка": "Не найден обработчик"})
        task.set_done()
        return

    try:
        res = task_handler.handle(task)

        if res == "WORK":
            task.set_on_work()
            return

    except Exception as e:
        res = task.get_res()

        if res is None:
            res = {}

        res["Ошибка"] = str(e)
        res["Описание"] = traceback.format_exc()
        task.set_res(res)

    task.set_done()


def handle_task_pool():

    controller = DBController()
    executor = ThreadPoolExecutor(max_workers=1500)

    while True:

        task_rec_list = Task.get_unprocessed_task_list(controller)

        print("Кол-во задач на выполнение {}".format(len(task_rec_list)))

        for task_rec in task_rec_list:
            executor.submit(handle_task_impl, task_rec)

            #new_thread = Thread(target=handle_task_impl, args=(task_rec,))
            #new_thread.start()
            #thread_list.append([new_thread, task_rec])

        #for thread_pair in thread_list:
        #    if not thread_pair[0].is_alive():
        #        thread_list.remove(thread_pair)
        #        print("Поток {} завершил работу".format(thread_pair[0].ident))

        #if len(task_rec_list) == 0:
        #    print("Активных потоков {}".format(active_count()))
        #    time.sleep(SLEEP_DURATION)
        #    continue


if __name__ == "__main__":
    handle_task_pool()