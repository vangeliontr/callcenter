import traceback

from DataBase.DBController import DBController
from DataBase.Task import Task
from DataBase.TaskDescriptor import TaskDescriptor

TASK_ID = 8396850

controller = DBController()
task = Task.get_by_id(controller, TASK_ID)

#task.set_res({})

task_handler = TaskDescriptor.get_by_task(controller, task.id())

if task_handler is None:
    task.set_res({"Ошибка": "Не найден обработчик"})
    task.set_done()
else:
    try:
        res = task_handler.handle(task)

        #if res == "WORK":
        #    task.set_on_work()

    except Exception as e:
        #res = task.get_res()

        #if res is None:
            res = {}

        #res["Ошибка"] = str(e)
        #res["Описание"] = traceback.format_exc()
        #task.set_res(res)

    #task.set_done()
