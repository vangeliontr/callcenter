#!/usr/bin/env python3

import sys
import json

sys.path.append("../")

from TaskHandler import handle_task_pool
from Tools.Daemon import Daemon


conf = json.loads(open("task_daemon_settings.txt", "r").read())
Daemon.run(handle_task_pool, conf)
