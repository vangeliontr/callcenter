#!/usr/bin/python3

import os
import sys
import json
sys.path.append("..")
from DataBase.DBController import DBController
from DataBase.Task import Task

content_len = int(os.environ["CONTENT_LENGTH"])
data = sys.stdin.read(content_len)

data = json.loads(data)

controller = DBController()
task_id = Task.set_task_impl(controller, data["name"], json.dumps(data["params"]))
print("Content-type: application/json")
print()
print(json.JSONEncoder().encode({"task_id": task_id}))
