#!/usr/bin/env python3

import sys
import json
import functools

sys.path.append("../")

from RunAdminServer import run_server
from Tools.Daemon import Daemon


conf = json.loads(open("admin_server_settings.txt", "r").read())
run_server_with_port = functools.partial(run_server, conf["port"])

Daemon.run(run_server_with_port, conf)
