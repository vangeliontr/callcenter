function appenToTable( tr, text ) {
    var td = document.createElement("td");
    var value = document.createTextNode(text);
    td.appendChild(value);
    tr.appendChild(td)
}

function appenLinkToTable( tr, href, text ) {
    var td = document.createElement("td");
    var link = document.createElement("a");
    var value = document.createTextNode(text);
    link.href = href;
    link.appendChild(value);
    td.appendChild(link);
    tr.appendChild(td)
}

function SelectMenu(id) {
    var menu = document.getElementById( id );
    menu.classList.add("active");
    var span = document.createElement('span');
    span.classList.add("sr-only");
    span.value = '(current)';
    menu.children[0].appendChild(span);
}
