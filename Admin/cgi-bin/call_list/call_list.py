#!/usr/bin/env python3

import sys
sys.path.append("..")

from Bank.BankList import BankList

print("Content-type: text/html")
print()
html_template = open("./cgi-bin/template.html", "r", encoding="utf-8").read()
html_template = html_template.replace("{{STYLE}}", """<link href="../../admin.css" rel="stylesheet">""")
html_template = html_template.replace("{{SCRIPT}}", """{{SCRIPT}}"""
                                      """<script>window.jQuery || document.write('<script src="../../assets/js/"""
                                      """vendor/jquery.min.js"><\/script>')</script>\n"""
                                      """<script src="../../admin.js"></script>\n"""
                                      """<script src="call_list.js"></script>\n"""
                                      """<script>startMonitoringCallList()</script>\n"""
                                      """<script>SelectMenu('menu_call_list')</script>\n""")

download_content = open("./cgi-bin/call_list/call_list.html", "r", encoding="utf-8").read()

bank_list = BankList.get()

bank_filter = ""

for bank in bank_list:
    bank_filter += """<button type="button" class="btn btn-secondary" >
                         <input type="radio" name="BankFilter" id="{}" autocomplete="off">
                         {}
                      </button>""".format(bank.id(), bank.name())

download_content = download_content.replace("{{BANK_FILTER}}", bank_filter)
html_template = html_template.replace("{{CONTENT}}", download_content)
print(html_template)
