#!/usr/bin/python3

import json
import sys
import cgi
sys.path.append("..")
from DataBase.DBController import DBController
from DataBase.Call import Call
from Bank.BankList import BankList
from DataBase.Project import Project

controller = DBController()


def create_last_call_stat():

    #form = cgi.FieldStorage()

    #start_date = form.getvalue('start_date')
    #end_date = form.getvalue('end_date')

    bank_list = BankList.get()
    project_list = Project.list()

    res = {}
    res["callListHeader"] = ""
    res["callListContent"] = ""

    call_list = Call.get_last_list(controller)

    for call in call_list:
        res["callListContent"] += "<tr>"
        format = ""

        for field in call:

            if res["callListHeader"] == "":
                if field == "Bank":
                    format += "<th>Банк</th>"
                elif field == "Project":
                    format += "<th>Проект</th>"
                else:
                    format += "<th>{}</th>".format(field)

            if field == "Bank":
                name = ""

                for bank in bank_list:
                    if bank.id() == call[field]:
                        name = bank.name()

                res["callListContent"] += "<td>{}</td>".format(name)

            elif field == "Project":
                name = ""

                for project in project_list:
                    if project.id() == call[field]:
                        name = project.name()

                res["callListContent"] += "<td>{}</td>".format(name)
            else:
                res["callListContent"] += "<td>{}</td>".format(call[field])

        if res["callListHeader"] == "":
            res["callListHeader"] = format

        res["callListContent"] += "</tr>"

    return res

print("Content-type: application/json")
print()
print(json.JSONEncoder().encode(create_last_call_stat()))