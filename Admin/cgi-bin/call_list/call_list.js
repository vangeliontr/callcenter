function startMonitoringCallList() {
    var xhr = new XMLHttpRequest();

    var body = '';

    xhr.open("POST", 'get_last_call_info.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var contact_list = JSON.parse( this.responseText );
        var callListContent = document.getElementById( "callListContent" );
        var callListHeader = document.getElementById( "callListHeader" );

        callListContent.innerHTML = contact_list["callListContent"];
        callListHeader.innerHTML = contact_list["callListHeader"];

        startMonitoringCallList();
    }

    xhr.send(body);
}