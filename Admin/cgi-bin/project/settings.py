#!/usr/bin/env python3

import sys
sys.path.append("..")
from DataBase.DBController import DBController
from DataBase.Project import Project

print("Content-type: text/html")
print()

html_template = open("./cgi-bin/template.html", "r", encoding="utf-8").read()
html_template = html_template.replace("{{STYLE}}", """<link href="../../admin.css" rel="stylesheet">""")

content = open("./cgi-bin/project/settings.html", "r", encoding="utf-8").read()

html_template = html_template.replace("{{CONTENT}}", content)
html_template = html_template.replace("{{SCRIPT}}",
                                      """<script src="../../admin.js"></script>\n"""
                                      """<script src="settings.js"></script>\n"""
                                      """<script>SelectMenu('menu_settings')</script>\n""")

controller = DBController()
project_list = Project.list(controller)

project_list_content = ""

for project in project_list:
    project_list_content += "<tr class='clickable-row'>"

    project_list_content += "<td>"
    project_list_content += str(project.id())
    project_list_content += "</td>"

    project_list_content += "<td>"
    project_list_content += project.name()
    project_list_content += "</td>"

    bank_name = ""
    bank = project.bank()

    if bank:
        bank_name = bank.name()

    project_list_content += "<td>"
    project_list_content += bank_name
    project_list_content += "</td>"

    project_list_content += "<td>"
    project_list_content += project.state()
    project_list_content += "</td>"

    project_list_content += "</tr>"

html_template = html_template.replace("{{PROJECT_LIST}}", project_list_content)

print(html_template)
