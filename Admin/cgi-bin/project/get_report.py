#!/usr/bin/python3

import cgi
import sys

from shutil import copyfileobj

sys.path.append("..")

from Bank.BankList import BankList

form = cgi.FieldStorage()


def get_report_file(form):
    bank_name = form.getvalue('bank_name')
    month = form.getvalue('month')
    year = form.getvalue('year')

    if year is None:
        year = "2020"

    bank = BankList.get_by_name(bank_name)

    wb = bank.get_month_report(month, year)
    wb.save("temp.xlsx")

    print("""Content-Type: application/vnd.ms-excel;base64;""")
    print("""Content-Disposition: attachment; filename = {}""".format("{} {}.xlsx".format(bank_name, month)))
    print("""Content-Transfer-Encoding: UTF-8""")
    print("""Content-Encoding: UTF-8""")
    print()
    copyfileobj(open("temp.xlsx", "rb"), sys.stdout.buffer)

get_report_file(form)
