$('#project_table').on('click', '.clickable-row', function(event) {
   $(this).addClass('active').siblings().removeClass('active');
   getStatusForProject($(this)[0].children[0].innerText);
   var project_info = document.getElementById("project_info")
   project_info.hidden = false
});

var report_div = document.getElementById("report")
var bankNameContainer = document.getElementById("bankName")

if(bankNameContainer.innerHTML == "ВТБ24" || bankNameContainer.innerHTML == "МТС")
    report_div.hidden = false

function SetView(view_name, raw_name){
    var view = document.getElementById(view_name)
    view.hidden = !view.hidden

    var raw = document.getElementById(raw_name)

    if(view.hidden)
        raw.src = "https://emojio.ru/images/apple-b/2b07.png"
    else
        raw.src = "https://emojio.ru/images/apple-b/2b06.png"
}

function getReport(bank_name) {
    var date = $('#mounthPicker').datepicker('getDate');
    document.location.href = "./get_report.py?bank_name=" + bank_name + "&month=" + (date.getMonth() + 1) + "&year=" + date.getUTCFullYear();
}

function setBankRegionListHandler(){
    $('#chaneRegionStateModal').modal('hide')

    $('table[id="region_table"] td').on('dblclick', function(event) {
        reg_id = event.currentTarget.attributes["reg_id"].value;

        if(!reg_id)
            return;

        $('#chaneRegionStateModal').modal('show')

        var xhr = new XMLHttpRequest();

        bankNameContainer = document.getElementById("bankName")

        var body = 'id=' + reg_id;
        body += "&bank=" + bankNameContainer.innerHTML

        xhr.open("POST", '../component/region_selector/select_region_to_bank.py', true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

        xhr.onreadystatechange = function() {
            if( this.readyState != 4 )
                return;

            getBankRegionList()
        }

        xhr.send(body);
    });
}

function getBankRegionList() {
    var xhr = new XMLHttpRequest();

    bankNameContainer = document.getElementById("bankName")

    var body = 'bank=' + bankNameContainer.innerHTML

    xhr.open("POST", '../component/region_selector/get_region_list_to_bank.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var bankRegionList = document.getElementById( "bankRegionList" );
        bankRegionList.innerHTML = this.responseText;

        setBankRegionListHandler()
    }

    xhr.send(body);
}

function getStatusForProject(project_id) {
    var xhr = new XMLHttpRequest();

    var dayPicker = document.getElementById( "dayPickerProject" );
    day_value = dayPicker.children[0].value

    var body = 'project_id=' + project_id;
    body += "&date=" + day_value

    xhr.open("POST", 'get_project_info.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res = JSON.parse( this.responseText );
        var statusProjectDownloadContent = document.getElementById( "statusProjectDownloadContent" );
        var statusProjectDownloadHeader = document.getElementById( "statusProjectDownloadHeader" );
        var statusProjectContent = document.getElementById( "statusProjectContent" );
        var statusProjectHeader = document.getElementById( "statusProjectHeader" );
        var statusProjectListContent = document.getElementById( "statusProjectListContent" );

        statusProjectDownloadContent.innerHTML = res["downloadProjectStatContent"];
        statusProjectDownloadHeader.innerHTML = res["downloadProjectStatHeader"];

        if(res["statusProjectHeader"] != "<tr></tr>"){
            statusProjectContent.innerHTML = res["statusProjectContent"];
            statusProjectHeader.innerHTML = res["statusProjectHeader"];
        }

        statusProjectListContent.innerHTML = res["statusProjectListContent"];

        var downloadContent = document.getElementById( "updateProject" );
        downloadContent.innerHTML = "Обновлено: " + new Date();

        setTimeout( getStatusForProject(project_id), 3000 );
    }

    xhr.send(body);
}

function getDownloadStatusForBank(bank_name) {
    var xhr = new XMLHttpRequest();

    var dayPicker = document.getElementById( "dayPicker" );
    day_value = dayPicker.children[0].value

    var body = 'bank_name=' + bank_name;
    body += "&date=" + day_value

    xhr.open("POST", 'get_download_status_info_for_project.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res = JSON.parse( this.responseText );
        var statusDownloadContent = document.getElementById( "statusDownloadContent" );
        var statusDownloadHeader = document.getElementById( "statusDownloadHeader" );

        statusDownloadContent.innerHTML = res["downloadStatContent"];
        statusDownloadHeader.innerHTML = res["downloadStatHeader"];

        var downloadContent = document.getElementById( "update" );
        downloadContent.innerHTML = "Обновлено: " + new Date();

        setTimeout( getDownloadStatusForBank(bank_name), 3000 );
    }

    xhr.send(body);
}

function getUploadStatusForBank(bank_name) {
    var xhr = new XMLHttpRequest();

    var dayPicker = document.getElementById( "dayPicker" );
    day_value = dayPicker.children[0].value

    var body = 'bank_name=' + bank_name;
    body += "&date=" + day_value

    xhr.open("POST", 'get_upload_status_info_for_project.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res = JSON.parse( this.responseText );
        var statusUploadContent = document.getElementById( "statusUploadContent" );
        var statusUploadHeader = document.getElementById( "statusUploadHeader" );

        statusUploadContent.innerHTML = res["uploadStatContent"];
        statusUploadHeader.innerHTML = res["uploadStatHeader"];

        var downloadContent = document.getElementById( "update" );
        downloadContent.innerHTML = "Обновлено: " + new Date();

        setTimeout( getUploadStatusForBank(bank_name), 3000 );
    }

    xhr.send(body);
}

function getStatusForBank(bank_name) {
    var xhr = new XMLHttpRequest();

    var dayPicker = document.getElementById( "dayPicker" );
    day_value = dayPicker.children[0].value

    var body = 'bank_name=' + bank_name;
    body += "&date=" + day_value

    xhr.open("POST", 'get_status_info_for_project.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res = JSON.parse( this.responseText );
        var statusDownloadContent = document.getElementById( "statusContent" );
        var statusDownloadHeader = document.getElementById( "statusHeader" );

        statusDownloadContent.innerHTML = res["downloadStatContent"];
        statusDownloadHeader.innerHTML = res["downloadStatHeader"];

        var downloadContent = document.getElementById( "update" );
        downloadContent.innerHTML = "Обновлено: " + new Date();

        setTimeout( getStatusForBank(bank_name), 3000 );
    }

    xhr.send(body);
}

function getCompareAct(bank_name){
    var xhr = new XMLHttpRequest();

    var startDayPicker = document.getElementById( "startDayPicker" );
    start_day_value = startDayPicker.children[0].value

    var endDayPicker = document.getElementById( "endDayPicker" );
    end_day_value = endDayPicker.children[0].value

    var body = 'bank_name=' + bank_name;
    body += "&start_date=" + start_day_value
    body += "&end_date=" + end_day_value

    xhr.open("POST", 'get_compare_act.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res = JSON.parse( this.responseText );
        var cmpActHeader = document.getElementById( "cmpActHeader" );
        var cmpActContent = document.getElementById( "cmpActContent" );

        cmpActHeader.innerHTML = res["cmpActHeader"];
        cmpActContent.innerHTML = res["cmpActContent"];
    }

    xhr.send(body);
}

function getProjectCompareAct(){
    var xhr = new XMLHttpRequest();

    var startDayPicker = document.getElementById( "projectStartDayPicker" );
    start_day_value = startDayPicker.children[0].value

    var endDayPicker = document.getElementById( "projectEndDayPicker" );
    end_day_value = endDayPicker.children[0].value

    var project_list = document.getElementById( "project_list" );

    var project_id = null

    for(project in project_list.children){
        if(project_list.children[project].classList.contains("active")) {
            project_id = project_list.children[project].children[0].innerText
            break
        }
    }

    var body = 'project_id=' + project_id;
    body += "&start_date=" + start_day_value
    body += "&end_date=" + end_day_value

    xhr.open("POST", 'get_project_compare_act.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res = JSON.parse( this.responseText );
        var cmpActHeader = document.getElementById( "cmpProjectActHeader" );
        var cmpActContent = document.getElementById( "cmpProjectActContent" );

        cmpActHeader.innerHTML = res["cmpActHeader"];
        cmpActContent.innerHTML = res["cmpActContent"];
    }

    xhr.send(body);
}

function ApplyAllows(allows) {

    if("Projects" in allows && !allows["Projects"]){
        var project_allows = document.getElementById("project_allows")
        project_allows.hidden = true
    }

    if("Regions" in allows && !allows["Regions"]){
        var regions_allows = document.getElementById("regions_allows")
        regions_allows.hidden = true
    }
}