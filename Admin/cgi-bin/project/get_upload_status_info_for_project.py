#!/usr/bin/python3

import json
import sys
import cgi
sys.path.append("..")
from DataBase.DBController import DBController
from Bank.BankList import BankList
from DataBase.Upload import Upload

form = cgi.FieldStorage()
bank_name = form.getvalue('bank_name')
date = form.getvalue('date')

bank = BankList.get_by_name(bank_name)

controller = DBController()
cur = controller.get_cursor()


def sort_status(dict_to_srt, priority):

    res_dict = {}

    for elem in priority:

        if elem in dict_to_srt:
            res_dict[elem] = dict_to_srt[elem]
            dict_to_srt.pop(elem)

    while(len(dict_to_srt) > 0):

        min_elem = None

        for elem in dict_to_srt:

            if elem is None:
                min_elem = elem
                break

            if min_elem is None:
                min_elem = elem
                continue

            if elem < min_elem:
                min_elem = elem

        res_dict[min_elem] = dict_to_srt[min_elem]
        dict_to_srt.pop(min_elem)

    return res_dict


def create_download_stat(bank, date):

    res = {}

    def get_table_val(val):
        if val is None:
            return ""
        else:
            return str(val)

    download_stat_content = ""

    upload_list = Upload.list(limit=None, start_date=date, end_date=date)

    lead_name = "Лид"

    if bank_name == "Тинькофф":
        lead_name = "Лид (горячий)"
    elif bank_name == "Сбербанк" or bank_name == "ВТБ24":
        lead_name = "Отправлено"

    stat_dict = {None: {}, lead_name: {}, "ОДП": {}, "ОДП при подгрузке": {}}
    count_sum = 0

    for upload in upload_list:

        call_list = upload.call_list([{"field": "Call_.Bank", "operation": "=", "value": bank.id()}])

        if not call_list:
            continue

        download_stat_content += "<tr>"
        download_stat_content += "<td>" + get_table_val(upload.id()) + "</td>"
        download_stat_content += "<td>" + upload.datetime().strftime("%H:%M:%S") + "</td>"
        download_stat_content += "<td>" + get_table_val(upload.contact_count()) + "</td>"

        count_sum += upload.contact_count()

        for call in call_list:
            if call.result() not in stat_dict:
                stat_dict[call.result()] = {}

            if upload.id() not in stat_dict[call.result()]:
                stat_dict[call.result()][upload.id()] = 0

            stat_dict[call.result()][upload.id()] += 1

        download_stat_content += "{{STATCONTENT" + str(upload.id()) + "}}"

        download_stat_content += "</tr>"

    download_stat_header = """<tr><th>Номер подгрузки</th><th>Время подгрузки</th><th>Кол-во контактов</th>"""

    res_line = """<b>
                      <tr>
                          <td>Итого:</td><td></td><td>{}</td>""".format(count_sum)

    res_stat_res = {None: 0, lead_name: 0, "ОДП": 0, "ОДП при подгрузке": 0}

    stat_dict = sort_status(stat_dict, [None, "ОДП при подгрузке", lead_name, "ОДП", "Отказ", "Уже звонили"])

    for upload in upload_list:
        stat_content_download = ""

        for status in stat_dict:
            count = 0

            if upload.id() in stat_dict[status]:
                count = stat_dict[status][upload.id()]

            # stat_content_download += """<td>{}</td>""".format(count)
            stat_content_download += """<td><a href="../organization_list/get_organization_list_file.py?upload_id={}&status={}">{}</a></td>""" \
                .format(upload.id(), status, count)

            if status not in res_stat_res:
                res_stat_res[status] = 0

            res_stat_res[status] += count

        download_stat_content = download_stat_content.replace("{{STATCONTENT" + str(upload.id()) + "}}",
                                                              stat_content_download)

    for status in stat_dict:
        if status is None:
            download_stat_header += "<th>{}</th>".format("Не обработано")
        else:
            download_stat_header += "<th>{}</th>".format(status)

        res_line += """<td>{}</td>""".format(res_stat_res[status])

    download_stat_header += "</tr>"
    res_line += """</tr></b>"""

    res["uploadStatContent"] = download_stat_content + res_line
    res["uploadStatHeader"] = download_stat_header
    return res


print("Content-type: application/json")
print()
print(json.JSONEncoder().encode(create_download_stat(bank, date)))
