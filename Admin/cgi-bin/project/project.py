#!/usr/bin/env python3

import sys
sys.path.append("..")
from Bank.BankList import BankList
import cgi
import json

form = cgi.FieldStorage()
bank_name = form.getvalue('name')
bank_alias = form.getvalue('alias')
allows = form.getvalue('allows')

#try:
#    allows = json.loads(allows)
#except:
#    allows = None

print("Content-type: text/html")
print()

html_template = open("./cgi-bin/template.html", "r", encoding="utf-8").read()
html_template = html_template.replace("{{STYLE}}", """<link href="../../admin.css" rel="stylesheet">""")

tinkoff_content = open("./cgi-bin/project/project.html", "r", encoding="utf-8").read()

apply_allows = ""

if allows:
   apply_allows = """<script>ApplyAllows({})</script>\n""".format(allows)

html_template = html_template.replace("{{CONTENT}}", tinkoff_content)
html_template = html_template.replace("{{SCRIPT}}",
                                      """<script src="../../admin.js"></script>\n"""
                                      """<script src="project.js"></script>\n""" +
                                      """<script>getUploadStatusForBank('{}')</script>\n""".format(bank_name) +
                                      """<script>getBankRegionList()</script>\n""" +
                                      """<script>getStatusForBank('{}')</script>\n""".format(bank_name) +
                                      """<script>SelectMenu('menu_{}')</script>\n""".format(bank_alias) +
                                      apply_allows)

bank = BankList.get_by_name(bank_name)

main_acc_data = bank.main_acc_data()

main_acc_header = "<tr>"
main_acc_cont = "<tr>"

for field in main_acc_data:
    main_acc_header += "<th>{}</th>".format(field)
    main_acc_cont += "<td>{}</td>".format(main_acc_data[field])

main_acc_header += "</tr>"
main_acc_cont += "</tr>"

html_template = html_template.replace("{{bankName}}", bank_name)

acc_list = bank.acc_list()

acc_header = "<tr>"
acc_cont = ""

first_line = True

for acc in acc_list:
    acc_cont += "<tr>"

    for field in acc["data"]:

        if first_line:
            acc_header += "<th>{}</th>".format(field)

        acc_cont += "<td>{}</td>".format(acc["data"][field])

    if first_line:
        acc_header += "<th>Последнее использование</th>".format(field)

    acc_cont += "<td>{}</td>".format(acc["LastUsing"])

    first_line = False
    acc_cont += "</tr>"

acc_header += "</tr>"

project_list = bank.get_projects()

project_list_content = ""

for project in project_list:
    project_list_content += "<tr class='clickable-row'>"

    project_list_content += "<td>"
    project_list_content += str(project.id())
    project_list_content += "</td>"

    project_list_content += "<td>"
    project_list_content += project.name()
    project_list_content += "</td>"

    bank_name = ""
    bank = project.bank()

    if bank:
        bank_name = bank.name()

    project_list_content += "<td>"
    project_list_content += bank_name
    project_list_content += "</td>"

    project_list_content += "<td>"
    project_list_content += project.state()
    project_list_content += "</td>"

    project_list_content += "</tr>"

html_template = html_template.replace("{{PROJECT_LIST}}", project_list_content)

print(html_template)
