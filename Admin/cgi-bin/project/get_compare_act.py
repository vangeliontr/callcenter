#!/usr/bin/python3

test_ca = open("test_ca.txt", "w")

try:
    import json
    import sys
    import cgi
    sys.path.append("..")

    from Tools.settings import GLOBAL_URL
    from DataBase.DBController import DBController
    from Bank.BankList import BankList
    from CallCenter.Skorozvon import Skorozvon

    form = cgi.FieldStorage()
    bank_name = form.getvalue('bank_name')
    start_date = form.getvalue('start_date')
    end_date = form.getvalue('end_date')

    bank = BankList.get_by_name(bank_name)

    bank_list = []

    try:
        bank_list = bank.get_org_list(start_date, end_date)
    except:
        pass

    client = Skorozvon()
    skorozvon_list = client.get_list(start_date, end_date, bank.get_results_ids(), bank.get_scenario_id())

    start_date = start_date[6:] + "-" + start_date[3:5] + "-" + start_date[:2] + " 00:00:00+03:00"
    end_date = end_date[6:] + "-" + end_date[3:5] + "-" + end_date[:2] + " 59:59:59+03:00"

    SQL = """SELECT Call_.*, Call_Organization.*, ContactPerson.*, Organization.OpenDate, Organization.INN, Organization.ID as ORGID FROM Call_ 
             INNER JOIN Call_Organization ON Call_Organization.Call_ = Call_.ID
             INNER JOIN Organization ON Call_Organization.Organization = Organization.ID
             INNER JOIN ContactPerson ON Organization.LastContactPerson = ContactPerson.ID
             WHERE Bank = {} AND (Result = 'Отправлено' OR Result = 'ОтправленоГорячий' OR Result = 'ОтправленоХолодный') AND
             Call_.Created > '{}' AND Call_.Created < '{}';""".format(bank.id(), start_date, end_date)

    controller = DBController()
    cur = controller.get_cursor()
    cur.execute(SQL)

    crm_list = cur.fetchall()

    res_list = {}

    for org in skorozvon_list:

        name = org["lead_name"]

        if name not in res_list:
            res_list[name] = {"Скорозвон": None, "CRM": None, "Банк": None}

        res_list[name]["Скорозвон"] = name

    for org in bank_list:

        name = bank.get_fio_by_contact(org, cur)
        name = name.strip()

        if name not in res_list:
            res_list[name] = {"Скорозвон": None, "CRM": None, "Банк": None}

        res_list[name]["Банк"] = name

    for org in crm_list:

        name = org["Surname"] + " " + org["Name"] + " " + org["MiddleName"]
        name = name.strip()

        if name not in res_list:
            res_list[name] = {"Скорозвон": None, "CRM": None, "Банк": None}

        res_list[name]["CRM"] = name

        res_list[name][
            "CRM"] += '</td><td><a href="{}/cgi-bin/organization/organization.py?id={}">{}</a>'. \
            format(GLOBAL_URL, str(org["ORGID"]), str(org["INN"]))

        if org["Comment"] is None:
            res_list[name]["CRM"] += "</td><td>"
        else:
            res_list[name]["CRM"] += "</td><td>" + org["Comment"]

        res_list[name]["CRM"] += "</td><td>" + str(org["OpenDate"])


    res = {}
    res["cmpActHeader"] = "<th>Скорозвон {}</th><th>Банк {}</th><th>CRM {}</th><th>ИНН</th><th>Комментарий</th><th>Дата регистрации</th>".format(len(skorozvon_list), len(bank_list), len(crm_list))
    res["cmpActContent"] = ""

    for elem in res_list:
        res["cmpActContent"] += "<tr>"

        if res_list[elem]["Скорозвон"] is None:
            res["cmpActContent"] += "<td></td>"
        else:
            res["cmpActContent"] += "<td>{}</td>".format(res_list[elem]["Скорозвон"])

        if res_list[elem]["Банк"] is None:
            res["cmpActContent"] += "<td></td>"
        else:
            res["cmpActContent"] += "<td>{}</td>".format(res_list[elem]["Банк"])

        if res_list[elem]["CRM"] is None:
            res["cmpActContent"] += "<td></td><td></td><td></td><td></td>"
        else:
            res["cmpActContent"] += "<td>{}</td>".format(res_list[elem]["CRM"])

        res["cmpActContent"] += "</tr>"

    print("Content-type: application/json")
    print()
    print(json.JSONEncoder().encode(res))
except Exception as e:
    test_ca.write(str(e))