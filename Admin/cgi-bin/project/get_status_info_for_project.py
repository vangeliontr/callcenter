#!/usr/bin/python3

import json
import sys
import cgi
sys.path.append("..")
from DataBase.DBController import DBController
from Bank.BankList import BankList

form = cgi.FieldStorage()
bank_name = form.getvalue('bank_name')
date = form.getvalue('date')

bank = BankList.get_by_name(bank_name)

controller = DBController()
cur = controller.get_cursor()


def sort_status(dict_to_srt, priority):

    res_dict = {}

    for elem in priority:

        if elem in dict_to_srt:
            res_dict[elem] = dict_to_srt[elem]
            dict_to_srt.pop(elem)

    while(len(dict_to_srt) > 0):

        min_elem = None

        for elem in dict_to_srt:

            if elem is None:
                min_elem = elem
                break

            if min_elem is None:
                min_elem = elem
                continue

            if elem < min_elem:
                min_elem = elem

        res_dict[min_elem] = dict_to_srt[min_elem]
        dict_to_srt.pop(min_elem)

    return res_dict


def create_download_stat(bank, date):

    res = {}

    def get_table_val(val):
        if val is None:
            return ""
        else:
            return str(val)

    lead_name = "Лид"

    if bank.name() == "Тинькофф":
        lead_name = "Лид (горячий)"
    elif bank.name() == "Сбербанк" or bank_name == "ВТБ24":
        lead_name = "Отправлено"

    download_stat_content = "<tr>"
    download_stat_header = "<tr>"

    SQL = """SELECT count(*) as CNT, Result FROM Call_ WHERE DATE(Created) = DATE(NOW()) AND Bank = {} AND Result is not NULL GROUP BY Result;""".format(bank.id())

    cur.execute(SQL)
    status_list = cur.fetchall()

    status_dict = {}

    for status in status_list:
        status_dict[status["Result"]] = status["CNT"]

    status_dict = sort_status(status_dict, [None, "ОДП при подгрузке", lead_name, "ОДП", "Отказ", "Уже звонили"])

    for elem in status_dict:
        download_stat_content += """<td>{}</td>""".format(status_dict[elem])
        download_stat_header += """<th>{}</th>""".format(elem)

    res["downloadStatContent"] = download_stat_content + "<tr>"
    res["downloadStatHeader"] = download_stat_header + "<tr>"
    return res


print("Content-type: application/json")
print()
print(json.JSONEncoder().encode(create_download_stat(bank, date)))
