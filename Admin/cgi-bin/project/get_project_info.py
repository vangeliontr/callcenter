#!/usr/bin/python3

import json
import sys
import cgi
sys.path.append("..")
from DataBase.DBController import DBController

form = cgi.FieldStorage()
project_id = form.getvalue('project_id')
date = form.getvalue('date')

controller = DBController()
cur = controller.get_cursor()


def create_download_stat(date):

    res = {}

    def get_table_val(val):
        if val is None:
            return ""
        else:
            return str(val)

    download_stat_content = ""

    SQL = """SELECT * FROM Download WHERE DATE(Created) = DATE({}) ORDER BY ID DESC LIMIT 15"""

    if date:
        test = date[6:10] + "-" + date[3:5] + "-" + date[0:2]
        SQL = SQL.format("'" + test + "'")
    else:
        SQL = SQL.format("NOW()")

    cur.execute(SQL)
    download_list = cur.fetchall()

    stat_dict = {}

    res_stat = {"CNT": 0, "ADR": 0, "UPL": 0}

    for download in download_list:
        download_stat_content += "<tr>"
        download_stat_content += "<td>" + get_table_val(download["ID"]) + "</td>"
        download_stat_content += "<td>" + get_table_val(download["RegDate"]) + "</td>"

        load_stat = {"CNT": 0, "ADR": 0}

        cur.execute("""SELECT count(*) as CNT
                       FROM Download_Organization
                       WHERE Download_Organization.Download = {}
                       """.format(download["ID"]))

        load_stat["CNT"] = cur.fetchall()[0]["CNT"]

        cur.execute("""SELECT count(*) as CNT FROM Organization
                       INNER JOIN Call_Organization ON Call_Organization.Organization = Organization.ID
                       INNER JOIN Call_ ON Call_Organization.Call_ = Call_.ID
                       WHERE Organization.Download = {} AND Call_.Project = {}
                       """.format(download["ID"], project_id))

        upload_count = cur.fetchall()[0]["CNT"]
        load_stat["ADR"] = load_stat["CNT"] - upload_count

        res_stat["CNT"] += load_stat["CNT"]
        res_stat["ADR"] += load_stat["ADR"]

        download_stat_content += """<td>{}</td>""".format(load_stat["CNT"])
        download_stat_content += """<td>{}</td>""".format(load_stat["ADR"])

        res_stat["UPL"] += upload_count
        download_stat_content += """<td>{}</td>""".format(upload_count)

        cur.execute("""SELECT count(*) as CNT, Call_.Result FROM Organization
                       INNER JOIN Call_Organization ON Call_Organization.Organization = Organization.ID
                       INNER JOIN Call_ ON Call_Organization.Call_ = Call_.ID
                       WHERE Organization.Download = {} AND Call_.Project = {}
                       GROUP BY Call_.Result
                       """.format(download["ID"], project_id))

        stat_list = cur.fetchall()

        for stat in stat_list:
            if stat["Result"] not in stat_dict:
                stat_dict[stat["Result"]] = {}

            stat_dict[stat["Result"]][download["ID"]] = stat["CNT"]

        download_stat_content += "{{STATCONTENT" + str(download["ID"]) + "}}"

        download_stat_content += "</tr>"

    download_stat_header = """<tr>
                                  <th>Номер загрузки</th>
                                  <th>Дата регистрации</th>
                                  <th>Кол-во контактов</th>
                                  <th>Не подходящий регион</th>
                                  <th>Запущено в работу</th>"""

    res_line = """<b>
                      <tr>
                          <td colspan="1">Итого:<td>"""

    res_line += """<td>{}</td>""".format(res_stat["CNT"])
    res_line += """<td>{}</td>""".format(res_stat["ADR"])
    res_line += """<td>{}</td>""".format(res_stat["UPL"])

    res_stat_res = {}

    for download in download_list:
        stat_content_download = ""

        for status in stat_dict:
            count = 0

            if download["ID"] in stat_dict[status]:
                count = stat_dict[status][download["ID"]]

            href = "../organization_list/get_organization_list_file.py?dl={}&project={}&status={}".format(download["ID"], project_id, status)
            stat_content_download += """<td><a href={}>{}</a></td>""".format(href, count)

            if status not in res_stat_res:
                res_stat_res[status] = 0

            res_stat_res[status] += count

        download_stat_content = download_stat_content.replace("{{STATCONTENT" + str(download["ID"]) + "}}",
                                                              stat_content_download)
    for status in stat_dict:
        if status is None:
            download_stat_header += "<th>{}</th>".format("Не обработано")
        else:
            download_stat_header += "<th>{}</th>".format(status)

        res_line += """<td>{}</td>""".format(res_stat_res[status])

    download_stat_header += "</tr>"
    res_line += """</tr></b>"""

    res["downloadProjectStatContent"] = download_stat_content + res_line
    res["downloadProjectStatHeader"] = download_stat_header

    download_stat_content = "<tr>"
    download_stat_header = "<tr>"

    SQL = """SELECT count(*) as CNT, Result FROM Call_ 
             WHERE DATE(Created) = DATE(NOW()) AND Project = {} AND 
             Result is not NULL GROUP BY Result;""".format(project_id)

    cur.execute(SQL)
    status_list = cur.fetchall()

    for status in status_list:
        download_stat_content += """<td>{}</td>""".format(status["CNT"])
        download_stat_header += """<th>{}</th>""".format(status["Result"])

    res["statusProjectContent"] = download_stat_content + "</tr>"
    res["statusProjectHeader"] = download_stat_header + "</tr>"

    SQL = """SELECT TIME(Call_.Created), Call_.Result, Call_.Comment, Organization.Name as OrgName, 
                    Organization.INN, Organization.Address, 
                    ContactPerson.Surname, ContactPerson.Name as CName, ContactPerson.MiddleName,
                    ContactInfo.Value ,Region.Name
             FROM Call_ 
             LEFT JOIN Call_Organization ON Call_Organization.Call_ = Call_.ID
             LEFT JOIN Organization ON Call_Organization.Organization = Organization.ID
             LEFT JOIN ContactPerson ON Organization.LastContactPerson = ContactPerson.ID
             LEFT JOIN ContactInfo ON Organization.LastPhoneInfo = ContactInfo.ID
             LEFT JOIN Organization_Region ON Organization_Region.Organization = Organization.ID
             LEFT JOIN Region ON Organization_Region.Region = Region.ID
             WHERE DATE(Call_.Created) = DATE(NOW()) AND Call_.Project = {} AND 
             Result is not NULL ORDER BY Call_.Created DESC;""".format(project_id)

    cur.execute(SQL)
    status_list = cur.fetchall()
    download_stat_list_content = ""

    for status in status_list:
        download_stat_list_content += "<tr> "

        for field in status:
            download_stat_list_content += "<td>{}<td>".format(status[field])

        download_stat_list_content += "</tr>"

    res["statusProjectListContent"] = download_stat_list_content

    return res


print("Content-type: application/json")
print()
print(json.JSONEncoder().encode(create_download_stat(date)))
