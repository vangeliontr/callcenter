#!/usr/bin/env python3

import cgi
import sys
sys.path.append("..")
from DataBase.DBController import DBController

form = cgi.FieldStorage()
id = form.getvalue('id')

controller = DBController()
cur = controller.get_cursor()

cur.execute("""SELECT *, Download.Created as DownloadCreated FROM Organization INNER JOIN Download ON Download.ID = 
               Organization.Download WHERE Organization.ID = """ + str(id))

organization = cur.fetchall()[0]

cur.execute("""SELECT * FROM Project""" )
projects = cur.fetchall()
project_block_list = organization["ProjectBlok"]

cur.execute("""SELECT *, ContactPerson.ID as ContactPersonID
FROM
ContactPerson_Organization
INNER JOIN ContactPerson ON ContactPerson_Organization.ContactPerson = ContactPerson.ID
WHERE
ContactPerson_Organization.Organization = """ + str(id))
contact_person_list = cur.fetchall()

cur.execute("""SELECT
DATE_FORMAT(Call_.Created, "%d.%m %H:%i") AS DateTime,
Call_.Result,
Call_.`Comment`,
Call_.Priority,
Call_.ExtID,
Call_.ID,
DATE_FORMAT( Call_.RecallDateTime, "%d.%m %H:%i") AS RecallDateTime,
Employee.`Name`,
Employee.Surname,
Bank.`Name` as BankName,
Project.`Name` as ProjectName
FROM
Call_Organization
INNER JOIN Call_ ON Call_Organization.Call_ = Call_.ID
LEFT JOIN Operator ON Call_.Operator = Operator.ID
LEFT JOIN Bank ON Call_.Bank = Bank.ID
LEFT JOIN Project ON Call_.Project = Project.ID
LEFT JOIN Employee ON Operator.Employee = Employee.ID
WHERE
Call_Organization.Organization =  """ + str( id ) )
call_list = cur.fetchall()

print("Content-type: text/html")
print()
html_template = open("./cgi-bin/card_template.html", "r", encoding="utf-8").read()
html_template = html_template.replace("{{STYLE}}", """<link href="../../admin.css" rel="stylesheet">""")

body = open("./cgi-bin/organization/organization.html", "r", encoding="utf-8").read()

if organization["Address"] is None:
   organization["Address"] = ""

body = body.replace("{{Organization.Name}}", str(organization["Name"]))
body = body.replace("{{Organization.Type}}", str(organization["Type"]))
body = body.replace("{{Organization.INN}}", str(organization["INN"]))
body = body.replace("{{Organization.Address}}", str(organization["Address"]))
body = body.replace("{{Organization.OpenDate}}", str(organization["OpenDate"]))
body = body.replace("{{Organization.TimeZone}}", str(organization["TimeZone"]))
body = body.replace("{{Organization.DownloadTime}}", str(organization["DownloadCreated"]))
body = body.replace("{{Organization.DownloadName}}", str(organization["Download"]))
body = body.replace("{{Organization.OKVED}}", str(organization["OKVED"]))

contact_person_content = ""

for contact_person in contact_person_list:
   contact_person_content += "<tr>"
   contact_person_content += '<td><a href="../contact_person/contact_person.py?id=' + str( contact_person["ContactPersonID"] ) + '">' + contact_person["Surname"] + "</a></td>"
   contact_person_content += "<td>" + contact_person["Name"] + "</td>"
   contact_person_content += "<td>" + contact_person["MiddleName"] + "</td>"
   contact_person_content += "<td>" + contact_person["Position"] + "</td>"
   contact_person_content += "</tr>"

body = body.replace("{{Organization.ContactPerson}}", contact_person_content)

call_content = ""

def get_table_val( val ):
   if val is None:
      return "";
   else:
      return str(val)

for call in call_list:
   call_content += "<tr>"
   call_content += "<td>" + get_table_val(call["ID"]) + "</td>"
   call_content += "<td>" + get_table_val(call["ExtID"]) + "</td>"
   call_content += "<td>" + get_table_val(call["DateTime"]) + "</td>"
   call_content += "<td>" + get_table_val(call["Result"]) + "</td>"
   call_content += "<td>" + get_table_val(call["Comment"]) + "</td>"
   call_content += "<td>" + get_table_val(call["Surname"]) + "</td>"
   call_content += "<td>" + get_table_val(call["Name"]) + "</td>"
   call_content += "<td>" + get_table_val(call["Priority"]) + "</td>"
   call_content += "<td>" + get_table_val(call["RecallDateTime"]) + "</td>"
   call_content += "<td>" + get_table_val(call["BankName"]) + "</td>"
   call_content += "<td>" + get_table_val(call["ProjectName"]) + "</td>"
   call_content += "<td></td>"
   call_content += "</tr>"

body = body.replace("{{Organization.Call_}}", call_content)

block_content = ""

for block in project_block_list:

   for project in projects:
      if str(project["ID"]) == block:
         block_content += "<tr>"
         block_content += "<td>" + project["Name"] + "</td>"
         block_content += "</tr>"

body = body.replace("{{Organization.ProjectBlock}}", block_content)

html_template = html_template.replace("{{CONTENT}}", body)
html_template = html_template.replace("{{SCRIPT}}",
                                      """<script>window.jQuery || document.write('<script src="../../assets/js/"""
                                      """vendor/jquery.min.js"><\/script>')</script>"""
                                      """<script src="../admin.js"></script>"""
                                      """""""<script>loadLastCalls()</script>""")

print(html_template)
