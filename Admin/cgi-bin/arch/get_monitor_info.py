#!/usr/bin/python3

import json
from DBController import DBController

controller = DBController()
cur = controller.GetCur()

res = {}

def get_table_val( val ):
   if val is None:
      return "";
   else:
      return str(val)

download_content = ""

cur.execute("""SELECT * FROM Download ORDER BY ID DESC LIMIT 4""")
download_list = cur.fetchall()

for download in download_list:
   cur.execute("SELECT count(*) AS CNT FROM Organization WHERE Download = " + str(download["ID"]))
   count = cur.fetchall()[0]["CNT"]
   download_content += "<tr>"
   download_content += "<td>" + get_table_val( download["FileName"] ) + "</td>"
   download_content += "<td>" + get_table_val( download["Created"] ) + "</td>"
   download_content += "<td>" + get_table_val( download["State"] ) + "</td>"
   download_content += """<td><a href="organization/organization.py?downloadId={}&downloadName={}">"""\
                          .format(download["ID"],download["FileName"]) + get_table_val( count ) + "</a></td>"
   download_content += "<td>" + get_table_val( download["Repeat"] ) + "</td>"
   download_content += "<td>" + get_table_val( download["Duration"] ) + "</td>"

   cur.execute( """SELECT Result, count(*) as CNT
                   FROM Organization
                   INNER JOIN Call_Organization ON Call_Organization.Organization = Organization.ID
                   INNER JOIN Call_ ON Call_Organization.Call_ = Call_.ID
                   WHERE ( Project = 1 or Project is Null ) AND Download = {} GROUP BY Result """.format( str(download["ID"]) )  )

   state_list = {}
   for state in cur.fetchall():
      state_list[state["Result"]] = state["CNT"]

   load_odp_cnt = 0

   if 'ОДПТФЗагрузка' in state_list:
      load_odp_cnt += state_list['ОДПТФЗагрузка'];

   download_content += """<td><a href="organization/organization.py?downloadId={}&downloadName={}&Status={}">"""\
                          .format(download["ID"],download["FileName"],'ОДПТФЗагрузка') + str(load_odp_cnt) + "</a></td>"

   odp_cnt = 0

   if 'ОДПРоботТФ' in state_list:
      odp_cnt += state_list['ОДПРоботТФ'];

   if 'ОДП' in state_list:
      odp_cnt += state_list['ОДП'];

   download_content += """<td><a href="organization/organization.py?downloadId={}&downloadName={}&Status={}">"""\
                          .format(download["ID"],download["FileName"],'ОДПРоботТФ,ОДП') + str(odp_cnt) + "</a></td>"

   send_cnt = 0

   if "Отправлено" in state_list:
      send_cnt += state_list['Отправлено']

   download_content += """<td><a href="organization/organization.py?downloadId={}&downloadName={}&Status={}">"""\
                          .format(download["ID"],download["FileName"],'Отправлено') + str(send_cnt) + "</a></td>"

   download_content += "</tr>"

res["downloadContent"] = download_content

cur.execute("""SELECT Result, IF(PrevCall_ IS NULL, true, false) as Uniq, count(*) as CNT FROM Call_
WHERE DATE(DateTime) = DATE(NOW()) AND Result <> 'ОДПРоботТФ'
GROUP BY Result, IF(PrevCall_ IS NULL, true, false)""")
processed_list = cur.fetchall()

processed = {"Всего":0,"ВсегоУникальных":0,"Отправлено":0,"ОтправленоУникальных":0,
             "ПовторнаяОбработка":0, "ПлохиеДанные":0, "Отказ":0 }

for data in processed_list:
   processed["Всего"] += data["CNT"]

   if data["Uniq"]:
      processed["ВсегоУникальных"] += data["CNT"]

   if data["Result"] == 'Отправлено':
      processed["Отправлено"] += data["CNT"]

      if data["Uniq"]:
         processed["ОтправленоУникальных"] += data["CNT"]
   elif data["Result"] in ('Недозвон','Повторный звонок','Отказ. Пока не актуально','Дальний регион'):
      processed["ПовторнаяОбработка"] += data["CNT"]
   elif data["Result"] in ('Неактивен','Неправильный номер','ОДП','Дубль','Отказ. Крым'):
      processed["ПлохиеДанные"] += data["CNT"]
   else:
      processed["Отказ"] += data["CNT"]

all = str(processed["Всего"])

if processed["Всего"] == 0:
   all_uniq = "0"
else:
   all_uniq = str(processed["ВсегоУникальных"]) + " ({:.1f}%)".format(processed["ВсегоУникальных"]/processed["Всего"] * 100),

if processed["Всего"] == 0:
   all_except = "0"
else:
   all_except = str(processed["Всего"] - processed["ВсегоУникальных"]) + " ({:.1f}%)".format((processed["Всего"] - processed["ВсегоУникальных"])/processed["Всего"] * 100),

if processed["Всего"] == 0:
   all_send = "0"
else:
   all_send = str(processed["Отправлено"]) + " ({:.1f}%)".format(processed["Отправлено"]/processed["Всего"] * 100),

if processed["ВсегоУникальных"] == 0:
   send_uniq = "0"
else:
   send_uniq = str(processed["ОтправленоУникальных"])  + " ({:.1f}%)".format(processed["ОтправленоУникальных"]/processed["ВсегоУникальных"] * 100),

if (processed["Всего"] - processed["ВсегоУникальных"]) == 0:
   send_except = "0"
else:
   send_except = str(processed["Отправлено"] - processed["ОтправленоУникальных"])  + " ({:.1f}%)".format((processed["Отправлено"] - processed["ОтправленоУникальных"])/(processed["Всего"] - processed["ВсегоУникальных"]) * 100),


res["processedContent"] = """
<tr>
<td>{}</td>
<td>{}</td>
<td>{}</td>1
<td>{}</td>
<td>{}</td>
<td>{}</td>
<td>{}</td>
<td>{}</td>
<td>{}</td>
</tr>
""".format(all,
           all_uniq,
           all_except,
           all_send,
           send_uniq,
           send_except,
           str(processed["ПовторнаяОбработка"]), str(processed["ПлохиеДанные"]), str(processed["Отказ"]))

ODP_content = """
               <tr>
                  <td>Операторами: {{odpOperCalls}}</td>
                  <td>Уникальных: {{odpUniq}} из них менее 1 часа назад {{thodpUniq}}</td>
                  <td>Уникальных: {{tdOdpUniq}}</td>
               </tr>
               <tr>
                  <td>Роботом: {{odpRobotCalls}}</td>
                  <td>Перезвонов: {{odpRecall}} из них менее 1 часа назад {{thodpRecall}}</td>
                  <td>Перезвонов: {{tdOdpRecall}}</td>
               </tr>
               <tr>
                  <td>
                  </td>
                  <td>Недозвонов: {{odpNotCalled}} из них менее 1 часа назад {{thodpNotCalled}}</td>
                  <td>Недозвонов: {{tdOdpNotCalled}}</td>
               </tr>
               <tr>
                  <td>
                    <h4 class="sub-header">Итого: {{odpCalls}}</h4>
                  </td>
                  <td>
                    <h4 class="sub-header">Итого: {{odpBuffer}}</h4>
                  </td>
                  <td>
                    <h4 class="sub-header">Итого: {{odpCheked}}</h4>
                  </td>
                </tr>"""

cur.execute("SELECT count(*) AS CNT, Result FROM Call_ WHERE DateTime >= DATE( NOW() ) AND (Result = 'ОДП' OR Result = 'ОДПРоботТФ') GROUP BY Result;")
ODP_cnt_list = cur.fetchall()

ODP_sum = 0

for ODP_cnt in ODP_cnt_list:
   if ODP_cnt["Result"] == 'ОДП':
      ODP_content = ODP_content.replace( "{{odpOperCalls}}", str(ODP_cnt["CNT"]) )
      ODP_sum = ODP_sum + ODP_cnt["CNT"]

   if ODP_cnt["Result"] == 'ОДПРоботТФ':
      ODP_content = ODP_content.replace( "{{odpRobotCalls}}", str(ODP_cnt["CNT"]) )
      ODP_sum = ODP_sum + ODP_cnt["CNT"]

ODP_content = ODP_content.replace( "{{odpOperCalls}}", "0" )
ODP_content = ODP_content.replace( "{{odpRobotCalls}}", "0" )
ODP_content = ODP_content.replace( "{{odpCalls}}", str(ODP_sum) )

cur.execute("""SELECT count(*) as CNT, Priority
FROM Call_
INNER JOIN ODP ON ODP.Call_ = Call_.ID
INNER JOIN Call_Organization ON Call_Organization.Call_ = Call_.ID
INNER JOIN Organization ON Call_Organization.Organization = Organization.ID
WHERE Result is NULL AND FIND_IN_SET( 1, ProjectBlok ) = 0
AND (Call_.RecallDateTime < NOW() OR Call_.RecallDateTime is NULL )
AND (Call_.Project = 1 OR Call_.Project is NULL)
AND ODP.Project = 1
GROUP BY Priority""")
ODP_cnt_list = cur.fetchall()

ODP_sum = 0

for ODP_cnt in ODP_cnt_list:
   if ODP_cnt["Priority"] == '':
      ODP_content = ODP_content.replace( "{{odpUniq}}", str(ODP_cnt["CNT"]) )
      ODP_sum = ODP_sum + ODP_cnt["CNT"]

   if ODP_cnt["Priority"] == 'Перезвон':
      ODP_content = ODP_content.replace( "{{odpRecall}}", str(ODP_cnt["CNT"]) )
      ODP_sum = ODP_sum + ODP_cnt["CNT"]

   if ODP_cnt["Priority"] == 'Недозвон':
      ODP_content = ODP_content.replace( "{{odpNotCalled}}", str(ODP_cnt["CNT"]) )
      ODP_sum = ODP_sum + ODP_cnt["CNT"]

ODP_content = ODP_content.replace( "{{odpUniq}}", "0" )
ODP_content = ODP_content.replace( "{{odpRecall}}", "0" )
ODP_content = ODP_content.replace( "{{odpNotCalled}}", "0" )
ODP_content = ODP_content.replace( "{{odpBuffer}}", str(ODP_sum) )

cur.execute("""SELECT count(*) as CNT, Priority
FROM Call_
INNER JOIN ODP ON ODP.Call_ = Call_.ID
INNER JOIN Call_Organization ON Call_Organization.Call_ = Call_.ID
INNER JOIN Organization ON Call_Organization.Organization = Organization.ID
WHERE ODP.Project = 1 AND Date(ODP.Created) = Date(NOW())
GROUP BY Priority""")
ODP_cnt_list = cur.fetchall()

ODP_sum = 0

for ODP_cnt in ODP_cnt_list:
   if ODP_cnt["Priority"] == '':
      ODP_content = ODP_content.replace( "{{tdOdpUniq}}", str(ODP_cnt["CNT"]) )
      ODP_sum = ODP_sum + ODP_cnt["CNT"]

   if ODP_cnt["Priority"] == 'Перезвон':
      ODP_content = ODP_content.replace( "{{tdOdpRecall}}", str(ODP_cnt["CNT"]) )
      ODP_sum = ODP_sum + ODP_cnt["CNT"]

   if ODP_cnt["Priority"] == 'Недозвон':
      ODP_content = ODP_content.replace( "{{tdOdpNotCalled}}", str(ODP_cnt["CNT"]) )
      ODP_sum = ODP_sum + ODP_cnt["CNT"]

ODP_content = ODP_content.replace( "{{tdOdpUniq}}", "0" )
ODP_content = ODP_content.replace( "{{tdOdpRecall}}", "0" )
ODP_content = ODP_content.replace( "{{tdOdpNotCalled}}", "0" )
ODP_content = ODP_content.replace( "{{odpCheked}}", str(ODP_sum) )

cur.execute("""SELECT count(*) as CNT, Priority
FROM Call_
INNER JOIN ODP ON ODP.Call_ = Call_.ID
INNER JOIN Call_Organization ON Call_Organization.Call_ = Call_.ID
INNER JOIN Organization ON Call_Organization.Organization = Organization.ID
WHERE Result is NULL AND FIND_IN_SET( 1, ProjectBlok ) = 0
AND (Call_.RecallDateTime < NOW() OR Call_.RecallDateTime is NULL )
AND (Call_.Project = 1 OR Call_.Project is NULL)
AND ODP.Project = 1 AND ODP.LastTry > NOW() - interval 1 HOUR
GROUP BY Priority""")

ODP_cnt_list = cur.fetchall()

for ODP_cnt in ODP_cnt_list:
   if ODP_cnt["Priority"] == '':
      ODP_content = ODP_content.replace( "{{thodpUniq}}", str(ODP_cnt["CNT"]) )

   if ODP_cnt["Priority"] == 'Перезвон':
      ODP_content = ODP_content.replace( "{{thodpRecall}}", str(ODP_cnt["CNT"]) )

   if ODP_cnt["Priority"] == 'Недозвон':
      test = ODP_cnt["CNT"]
      ODP_content = ODP_content.replace( "{{thodpNotCalled}}", str(test) )

ODP_content = ODP_content.replace( "{{thodpUniq}}", "0" )
ODP_content = ODP_content.replace( "{{thodpRecall}}", "0" )
ODP_content = ODP_content.replace( "{{thodpNotCalled}}", "0" )

res["ODPContent"] = ODP_content

def getWorkInfoByProject( cur, project ):

   sql = """SELECT TimeZone, count(*) as CNT, Priority, ODP.ID is NULL as ODP FROM Call_
   INNER JOIN Call_Organization on Call_Organization.Call_ = Call_.ID
   INNER JOIN Organization ON Organization.ID = Call_Organization.Organization
   LEFT JOIN ODP ON ODP.Call_ = Call_.ID AND ODP.Project = """ + project + """
   WHERE """

   if project == '2':
      sql += " Organization.OpenDate < (NOW() - interval 14 day) AND  "

   sql += """Result is NULL AND FIND_IN_SET( '""" + project + """', ProjectBlok ) = 0 AND
   (RecallDateTime < NOW() or RecallDateTime is NULL ) AND
   (Call_.Project = """ + project + """ or Call_.Project is Null) AND
   SUBSTR(Organization.Address,1,3) not in ( "295", "296", "297", "298", "299" ) AND
   MissedCall < 10
   GROUP BY Priority, TimeZone, ODP.ID is NULL"""

   cur.execute(sql)

   work_info_list = cur.fetchall()

   work_info_table = {}

   res_uniq = 0
   res_recall = 0
   res_notcalled = 0

   for work_info in work_info_list:
      if work_info["TimeZone"] not in work_info_table.keys():
         work_info_table[work_info["TimeZone"]] = {}

      if work_info["Priority"] == '':
         work_info_table[work_info["TimeZone"]]["uniq"] = {}

         if work_info["ODP"] == 0:
            work_info_table[work_info["TimeZone"]]["uniq"]["noODP"] = work_info["CNT"]
         else:
            work_info_table[work_info["TimeZone"]]["uniq"]["ODP"] = work_info["CNT"]

         res_uniq = res_uniq + work_info["CNT"]

      if work_info["Priority"] == 'Перезвон':
         work_info_table[work_info["TimeZone"]]["recall"] = {}

         if work_info["ODP"] == 0:
            work_info_table[work_info["TimeZone"]]["recall"]["noODP"] = work_info["CNT"]
         else:
            work_info_table[work_info["TimeZone"]]["recall"]["ODP"] = work_info["CNT"]

         res_recall = res_recall + work_info["CNT"]

      if work_info["Priority"] == 'Недозвон':
         work_info_table[work_info["TimeZone"]]["notcalled"] = {}

         if work_info["ODP"] == 0:
            work_info_table[work_info["TimeZone"]]["notcalled"]["noODP"] = work_info["CNT"]
         else:
            work_info_table[work_info["TimeZone"]]["notcalled"]["ODP"] = work_info["CNT"]

         res_notcalled += work_info["CNT"]

   work_content = ""

   def AddData(  elem ):
      res = 0
      res_ODP = 0

      if elem is not None:
         res_ODP = elem.get("noODP")
         if res_ODP is None:
            res_ODP = 0

         res = elem.get("ODP")
         if res is None:
            res = 0

         return "<td>{}({})</td>".format(res + res_ODP, res_ODP)
      else:
         return "<td></td>"

   for timezone in sorted(work_info_table):
      row = work_info_table[timezone]
      work_content += "<tr>"
      work_content += "<td>" + get_table_val( timezone ) + "</td>"
      work_content += AddData(row.get("uniq"))
      work_content += AddData(row.get("recall"))
      work_content += AddData(row.get("notcalled"))
      work_content += "</tr>"

   work_content += "<tr>"
   work_content += "<td><b>Итого:</b></td>"
   work_content += "<td><b>" + get_table_val( res_uniq ) + "</b></td>"
   work_content += "<td><b>" + get_table_val( res_recall ) + "</b></td>"
   work_content += "<td><b>" + get_table_val( res_notcalled ) + "</b></td>"
   work_content += "</tr>"

   return work_content

res["workContentTF"] = getWorkInfoByProject( cur, "1" )
res["workContentAlfa"] = getWorkInfoByProject( cur, "4" )
res["workContentSber"] = getWorkInfoByProject( cur, "2" )

print("Content-type: application/json")
print()
print(json.JSONEncoder().encode(res))