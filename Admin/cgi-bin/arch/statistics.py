#!/usr/bin/env python3

import datetime
from DBController import DBController

print("Content-type: text/html")
print()
head = open("./cgi-bin/head.tmpl", "r").read()
head = head.replace("{{slyle}}", """<link href="../admin.css" rel="stylesheet">""")
print(head)
menu_content = open("./cgi-bin/menu.html", "r", encoding="utf-8").read()
print(menu_content)
statistics_content = open("./cgi-bin/statistics.html", "r", encoding="utf-8").read()

cur_date = datetime.datetime.now()
start_date = cur_date.replace(day = 1)

statistics_content = statistics_content.replace("{{startDate}}", start_date.strftime("%m.%d.%Y"))
statistics_content = statistics_content.replace("{{endDate}}", cur_date.strftime("%m.%d.%Y"))

controller = DBController()
cur = controller.GetCur()

cur.execute("""SELECT * FROM Project WHERE IsWork <> 0;""" )
project_list = cur.fetchall()
project_res = ""

for project in project_list:
   checked = ""
   active = ""

   if project["Name"] == "Тинькофф":
      checked = "checked"
      active = "active"

   project_res +="""<label class="btn btn-secondary {}">
                       <input type="radio" name="project" id="{}" autocomplete="off"{}> {}
                    </label>""".format( active, project["ID"], checked, project["Name"])

statistics_content = statistics_content.replace( "{{PROJECT_LIST}}", project_res)
print(statistics_content)
print(open("./cgi-bin/foot.html", "r").read())