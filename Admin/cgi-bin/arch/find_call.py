#!/usr/bin/python3

import json
import cgi
from DBController import DBController

form = cgi.FieldStorage()
id = form.getvalue('call_id')

controller = DBController()
cur = controller.GetCur()
cur.execute("""
SELECT
Organization.ID as OrganizationID,
Organization.`Name`,
Organization.INN,
Organization.Address,
DATE_FORMAT( Organization.OpenDate, "%d.%m" ) as OpenDate,
Organization.TimeZone,
ContactPerson.ID as ContactPersonID,
ContactPerson.Surname,
ContactPerson.`Name` as ContactPersonName,
Call_.ID,
DATE_FORMAT(Call_.DateTime, "%d.%m %H:%i") as DateTime,
Call_.Result,
Project.`Name` as ProjectName,
Operator.ID,
Employee.Surname as EmployeeSurname,
Employee.`Name` as EmployeeName
FROM
(
SELECT
Call_.Result,
Call_.`Comment`,
Call_.DateTime,
Call_.ID,
Call_.Operator,
Call_.ContactPerson,
Call_.Project
FROM
Call_
WHERE Call_.ID = """ + id + """
ORDER BY Call_.DateTime DESC ) AS Call_
LEFT JOIN ContactPerson ON Call_.ContactPerson = ContactPerson.ID
LEFT JOIN Project ON Call_.Project = Project.ID
LEFT JOIN Operator ON Call_.Operator = Operator.ID
LEFT JOIN Employee ON Employee.ID = Operator.Employee
LEFT JOIN Call_Organization ON Call_.ID = Call_Organization.Call_
LEFT JOIN Organization ON Organization.ID = Call_Organization.Organization
""" )

res = cur.fetchall()

print("Content-type: application/json")
print()
print(json.JSONEncoder().encode(res))