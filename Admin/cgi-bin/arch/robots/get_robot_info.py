#!/usr/bin/python3

import json
from collections import deque

res = {}

def getActivity( path ):
   res = None

   try:
      with open(path) as f:
         row_list = (list(deque(f, 10)))
         res = ""

         for elem in row_list:
            if res != "":
               res += "<br>\n"

            res += elem

   except:
      pass

   if res == "":
      return None

   return res

activityTF = None
activitySB = None
activityAL = "Не реализовано"

activityTF = getActivity("../odp/tf/log.txt")
activitySB = getActivity("../odp/sb/log.txt")

if activityTF is None:
   activityTF = "Не работает"

if activitySB is None:
   activitySB = "Не работает"

res["activityTF"] = activityTF
res["activitySB"] = activitySB
res["activityAL"] = "Не реализовано"

print("Content-type: application/json")
print()
print(json.JSONEncoder().encode(res))