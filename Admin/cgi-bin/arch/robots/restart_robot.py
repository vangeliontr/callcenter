#!/usr/bin/python3

import os
import cgi
import time

form = cgi.FieldStorage()
robot = form.getvalue('robot')

os.chdir("../odp/" + robot + "/")
os.system("python3 " + robot + ".py restart")