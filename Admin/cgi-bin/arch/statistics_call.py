#!/usr/bin/env python3

import datetime

print("Content-type: text/html")
print()
head = open("./cgi-bin/head.tmpl", "r").read()
head = head.replace("{{slyle}}", """<link href="../admin.css" rel="stylesheet">""")
print(head)
menu_content = open("./cgi-bin/menu.html", "r", encoding="utf-8").read()
print(menu_content)
statistics_content = open("./cgi-bin/statistics_call.html", "r", encoding="utf-8").read()

cur_date = datetime.datetime.now()
start_date = cur_date.replace(day = 1)

statistics_content = statistics_content.replace("{{startDate}}", start_date.strftime("%m.%d.%Y"))
statistics_content = statistics_content.replace("{{endDate}}", cur_date.strftime("%m.%d.%Y"))
print(statistics_content)
print(open("./cgi-bin/foot.html", "r").read())