#!/usr/bin/python3

import json
import sys
sys.path.append(".\\cgi-bin\\")
from DBController import DBController
import cgi

res = {}

form = cgi.FieldStorage()
pilot_id = form.getvalue('pilot_id')

controller = DBController()
cur = controller.GetCur()

cur.execute("""SELECT Result, count(*) AS cnt
FROM Call_
INNER JOIN Note_Call_ ON Note_Call_.Call_ = Call_.ID
WHERE Note_Call_.Note = """ + pilot_id + """
GROUP BY Result""" )

pilot_info_content = ""
pilot_info_list = cur.fetchall()
all_cnt = 0
uniq_cnt = 0

for pilot_info in pilot_info_list:

   if pilot_info["Result"] is not None:
      pilot_info_content += "<tr><td>" + str(pilot_info["Result"]) + "</td><td>" + str(pilot_info["cnt"]) + "</td></tr>"
   else:
      uniq_cnt = pilot_info["cnt"]

   all_cnt += pilot_info["cnt"]

pilot_info_content = "<tr><td>ВСЕГО</td><td>" + str(all_cnt) + "</td></tr>" + "<tr><td>Не обработано</td><td>" + str(uniq_cnt) + "</td></tr>" + pilot_info_content

res["PilotInfo"] = pilot_info_content

print("Content-type: application/json")
print()
print(json.JSONEncoder().encode(res))