#!/usr/bin/env python3

import sys
sys.path.append(".\\cgi-bin\\")
from DBController import DBController

print("Content-type: text/html")
print()
head = open("./cgi-bin/head.tmpl", "r").read()
head = head.replace("{{slyle}}", """<link href="../../admin.css" rel="stylesheet">""")
print(head)
menu_content = open("./cgi-bin/menu.html", "r", encoding="utf-8").read()
print(menu_content)
pilot_content = open("./cgi-bin/pilot/pilot.html", "r", encoding="utf-8").read()

pilot_list_content = ""

controller = DBController()
cur = controller.GetCur()

cur.execute("""SELECT * FROM Note;""" )
pilot_list = cur.fetchall()

for pilot in pilot_list:
   pilot_list_content += """<li role="presentation"><a role="menuitem" tabindex="-1" onclick="getPilotInfo( """  + str(pilot["ID"]) + ", '" + pilot["Value"] + """' )" href="#">""" + pilot["Value"] + """</a></li>"""

pilot_content = pilot_content.replace("{{PILOT_LIST}}", pilot_list_content)

print(pilot_content)
print(open("./cgi-bin/foot.html", "r").read())