#!/usr/bin/python3

import json
import cgi
import traceback
from DBController import DBController
import re


def get_resp():
   form = cgi.FieldStorage()
   mode = form.getvalue('mode')
   start_date = form.getvalue('start_date')
   end_date = form.getvalue('end_date')
   detail = form.getvalue('detail')
   project = form.getvalue('project')
   controller = DBController()
   cur = controller.GetCur()

   main_sql = """SELECT """

   if detail == "Month":
      main_sql += 'DATE_FORMAT(OpenDate, "%m.%Y") as Detail,'
   else:
      main_sql += 'DATE_FORMAT(OpenDate, "%d.%m.%Y") as Detail,'

   main_sql += """ IF(Call_.ID IS NULL, false, true) as SEND,
IF(Bid.State = 'Счет открыт', true, false) as OPEN, count(*) as CNT"""

   if mode == 'TimeZoneGroup':
      main_sql += ', TimeZone as grp'

   if mode == 'RegionGroup':
      main_sql += ', TimeZone.Region as grp'

   main_sql += """
FROM Organization
LEFT JOIN Bid ON Bid.Organization = Organization.ID
LEFT JOIN Call_ ON Call_.ID = Bid.Call_ AND Project = {} """.format(project)

   if mode == 'RegionGroup':
      main_sql += ' LEFT JOIN TimeZone ON SUBSTR(Organization.Address,1,3) >= TimeZone.StartValue AND ' \
                  'SUBSTR(Organization.Address,1,3) <= TimeZone.EndValue'

   main_sql += """
WHERE OpenDate >= '{}' AND OpenDate <= '{}'
GROUP BY """

   if detail == "Month":
      main_sql += 'MONTH( OpenDate ), YEAR(openDate)'
   else:
      main_sql += 'OpenDate'

   main_sql += """
, IF(Call_.ID IS NULL, false, true), IF(Bid.State = 'Счет открыт', true, false)"""

   if mode == 'TimeZoneGroup':
      main_sql += ', TimeZone'

   if mode == 'RegionGroup':
      main_sql += ', TimeZone.Region'

   cur.execute(main_sql.format(start_date,end_date))

   res = cur.fetchall()

   if len(res) == 0:
      res_none = {}
      res_none["statContent"] = "Нет организаций с датой регистрации в указанном периоде"
      print("Content-type: application/json")
      print()
      print(json.JSONEncoder().encode(res_none))
      return

   info = {}
   row_res = None

   REGIONS = ["01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16",
              "17","18","19","21","22","23","24","25","26","27","28","29","30","31","32","33",
              "34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49",
              "50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65",
              "66","67","68","69","70","71","72","73","74","75","76","77","78","79","83","86",
              "87","89","91","92","95",None]

   TIMEZONES = [9,8,7,6,5,4,3,2,1,0,-1,None]

   TEMPLATE = {"COUNT":0, "SEND":0,"OPEN":0, "ODP":0, "ODP_LOAD":0, "UNIQ_SEND":0 }

   if mode == "NonGroup":
      row_res = TEMPLATE.copy()

   if mode == 'TimeZoneGroup':
      row_res = {}

      for TimeZone in TIMEZONES:
         row_res[TimeZone]= TEMPLATE.copy()

   if mode == 'RegionGroup':
      row_res = {}

      for Region in REGIONS:
         row_res[Region]= TEMPLATE.copy()

   glob_info = TEMPLATE.copy()

   for data in res:
      cur_row = None

      if not data["Detail"] in info:
         info[data["Detail"]] = {}
         cur_row = info[data["Detail"]]
         cur_row["res"] = TEMPLATE.copy()

         if mode == 'TimeZoneGroup':
            for TimeZone in TIMEZONES:
               cur_row[TimeZone]= TEMPLATE.copy()

         if mode == 'RegionGroup':
            for Region in REGIONS:
               cur_row[Region]= TEMPLATE.copy()
      else:
         cur_row = info[data["Detail"]]

      cur_row["res"]["COUNT"] += data["CNT"]
      glob_info["COUNT"] += data["CNT"]

      if "grp" in data:
         row_res[data["grp"]]["COUNT"] += data["CNT"]
         cur_row[data["grp"]]["COUNT"] += data["CNT"]

      if data["SEND"]:
         cur_row["res"]["SEND"] += data["CNT"]
         glob_info["SEND"] += data["CNT"]

         if "grp" in data:
            row_res[data["grp"]]["SEND"] += data["CNT"]
            cur_row[data["grp"]]["SEND"] += data["CNT"]

      if data["OPEN"]:
         cur_row["res"]["OPEN"] += data["CNT"]
         glob_info["OPEN"] += data["CNT"]

         if "grp" in data:
            row_res[data["grp"]]["OPEN"] += data["CNT"]
            cur_row[data["grp"]]["OPEN"] += data["CNT"]

   odp_sql = """SELECT Result"""

   if detail == "Month":
      odp_sql += ', DATE_FORMAT(OpenDate, "%m.%Y") as Detail,'
   else:
      odp_sql += ', DATE_FORMAT(OpenDate, "%d.%m.%Y") as Detail,'

   odp_sql += """ count(*) as CNT """

   if mode == 'TimeZoneGroup':
      odp_sql += ', TimeZone as grp'

   if mode == 'RegionGroup':
      odp_sql += ', TimeZone.Region as grp'

   odp_sql += """
FROM Organization
INNER JOIN Call_Organization ON Call_Organization.Organization = Organization.ID
INNER JOIN Call_ ON Call_Organization.Call_ = Call_.ID"""

   if mode == 'RegionGroup':
      odp_sql += ' LEFT JOIN TimeZone ON SUBSTR(Organization.Address,1,3) >= TimeZone.StartValue AND ' \
                  'SUBSTR(Organization.Address,1,3) <= TimeZone.EndValue'

   ODP_Robot = 'ОДПРоботТФ'
   ODP_Load = 'ОДПТФЗагрузка'

   if  project == '2':
      ODP_Robot = 'ОДПРоботСбер'
      ODP_Load = 'ОДПСберЗагрузка'

   if  project == '4':
      ODP_Robot = 'ОДПРоботАльфа'
      ODP_Load = 'ОДПАльфаЗагрузка'

   odp_sql += """
WHERE OpenDate >= '{}' AND OpenDate <= '{}' AND (Result = 'ОДП' AND Project = """ + project + """ OR Result = '"""+ODP_Robot+"""' OR Result = '"""+ODP_Load+"""'
or Result = 'Отправлено' and Project = """+project+""" and PrevCall_ is Null)
GROUP BY Result """

   if detail == "Month":
      odp_sql += ', MONTH( OpenDate ), YEAR(openDate)'
   else:
      odp_sql += ', OpenDate'

   if mode == 'TimeZoneGroup':
      odp_sql += ', TimeZone'

   if mode == 'RegionGroup':
      odp_sql += ', TimeZone.Region'

   cur.execute(odp_sql.format(start_date, end_date))

   odp_list = cur.fetchall()

   for odp in odp_list:
      key = "ODP"

      if odp["Result"] == 'ОДПТФЗагрузка':
         key = 'ODP_LOAD'

      if odp["Result"] == 'Отправлено':
         key = 'UNIQ_SEND'

      info[odp["Detail"]]["res"][key] += odp["CNT"]
      if "grp" in odp:
         row_res[odp["grp"]][key] += odp["CNT"]
         info[odp["Detail"]][odp["grp"]][key] += odp["CNT"]

      glob_info[key] += odp["CNT"]

   res = {}
   res["statContent"] = """
<thead class="thead-inverse">
<tr>
<th width="10%">Дата регистрации</th>
<th></th>"""

   if mode == 'TimeZoneGroup':
      for TimeZone in TIMEZONES:
         res["statContent"] += """<th>{}</th>""".format(TimeZone)

   if mode == 'RegionGroup':
      for Region in REGIONS:
         res["statContent"] += """<th>{}</th>""".format(Region)

   res["statContent"] += """<th>Итого</th>
<th></th>
</tr>
</thead>
<tbody>"""

   for data in info:
      res["statContent"] += """
<tr>
   <td rowspan="6" style="vertical-align: middle;">{}</td>
   <td>Всего организаций</td>""".format(data)

      if mode == 'TimeZoneGroup':
         for TimeZone in TIMEZONES:
            res["statContent"] += """<td>{}</td>""".format(info[data][TimeZone]["COUNT"])

      if mode == 'RegionGroup':
         for Region in REGIONS:
            res["statContent"] += """<td>{}</td>""".format(info[data][Region]["COUNT"])

      res["statContent"] += """
   <td>{}</td>
</tr>""".format(str(info[data]["res"]["COUNT"]))

      res["statContent"] += """
<tr>
   <td>ОДП при загрузке</td>"""

      if mode == 'TimeZoneGroup':
         for TimeZone in TIMEZONES:
            if info[data][TimeZone]["COUNT"] != 0:
               res["statContent"] += """<td>{}</td>""".format(str(info[data][TimeZone]["ODP_LOAD"]) +
                                  " ({:.1f}%)".format(info[data][TimeZone]["ODP_LOAD"]/info[data][TimeZone]["COUNT"] * 100))
            else:
               res["statContent"] += """<td>0</td>"""

      if mode == 'RegionGroup':
         for Region in REGIONS:
            if info[data][Region]["COUNT"] != 0:
               res["statContent"] += """<td>{}</td>""".format(str(info[data][Region]["ODP_LOAD"]) +
                                  " ({:.1f}%)".format(info[data][Region]["ODP_LOAD"]/info[data][Region]["COUNT"] * 100))
            else:
               res["statContent"] += """<td>0</td>"""

      res["statContent"] += """
   <td>{}</td>
</tr>""".format(str(info[data]["res"]["ODP_LOAD"]) + " ({:.1f}%)".format(info[data]["res"]["ODP_LOAD"]/info[data]["res"]["COUNT"] * 100))

      res["statContent"] += """
<tr>
   <td>ОДП</td>"""

      if mode == 'TimeZoneGroup':
         for TimeZone in TIMEZONES:
            if info[data][TimeZone]["COUNT"] != 0:
               res["statContent"] += """<td>{}</td>""".format(str(info[data][TimeZone]["ODP"]) +
                                  " ({:.1f}%)".format(info[data][TimeZone]["ODP"]/info[data][TimeZone]["COUNT"] * 100))
            else:
               res["statContent"] += """<td>0</td>"""

      if mode == 'RegionGroup':
         for Region in REGIONS:
            if info[data][Region]["COUNT"] != 0:
               res["statContent"] += """<td>{}</td>""".format(str(info[data][Region]["ODP"]) +
                                  " ({:.1f}%)".format(info[data][Region]["ODP"]/info[data][Region]["COUNT"] * 100))
            else:
               res["statContent"] += """<td>0</td>"""

      res["statContent"] += """
   <td>{}</td>
</tr>""".format(str(info[data]["res"]["ODP"]) + " ({:.1f}%)".format(info[data]["res"]["ODP"]/info[data]["res"]["COUNT"] * 100))

      res["statContent"] += """
<tr>
   <td>Отправлено заявок</td>"""

      if mode == 'TimeZoneGroup':
         for TimeZone in TIMEZONES:
            if info[data][TimeZone]["COUNT"] != 0:
               res["statContent"] += """<td>{}</td>""".format(str(info[data][TimeZone]["SEND"]) +
                                  " ({:.1f}%)".format(info[data][TimeZone]["SEND"]/info[data][TimeZone]["COUNT"] * 100))
            else:
               res["statContent"] += """<td>0</td>"""

      if mode == 'RegionGroup':
         for Region in REGIONS:
            if info[data][Region]["COUNT"] != 0:
               res["statContent"] += """<td>{}</td>""".format(str(info[data][Region]["SEND"]) +
                                  " ({:.1f}%)".format(info[data][Region]["SEND"]/info[data][Region]["COUNT"] * 100))
            else:
               res["statContent"] += """<td>0</td>"""

      res["statContent"] += """
   <td>{}</td>
</tr>""".format( str(info[data]["res"]["SEND"]) + " ({:.1f}%)".format(info[data]["res"]["SEND"]/info[data]["res"]["COUNT"] * 100) )

      res["statContent"] += """
<tr>
   <td>Отправлено заявок по первому звонку</td>"""

      if mode == 'TimeZoneGroup':
         for TimeZone in TIMEZONES:
            if info[data][TimeZone]["COUNT"] != 0:
               res["statContent"] += """<td>{}</td>""".format(str(info[data][TimeZone]["UNIQ_SEND"]) +
                                  " ({:.1f}%)".format(info[data][TimeZone]["UNIQ_SEND"]/info[data][TimeZone]["COUNT"] * 100))
            else:
               res["statContent"] += """<td>0</td>"""

      if mode == 'RegionGroup':
         for Region in REGIONS:
            if info[data][Region]["COUNT"] != 0:
               res["statContent"] += """<td>{}</td>""".format(str(info[data][Region]["UNIQ_SEND"]) +
                                  " ({:.1f}%)".format(info[data][Region]["UNIQ_SEND"]/info[data][Region]["COUNT"] * 100))
            else:
               res["statContent"] += """<td>0</td>"""

      res["statContent"] += """
   <td>{}</td>
</tr>""".format( str(info[data]["res"]["UNIQ_SEND"]) + " ({:.1f}%)".format(info[data]["res"]["UNIQ_SEND"]/info[data]["res"]["COUNT"] * 100) )

      res["statContent"] += """
<tr>
   <td>Заключено сделок</td>"""

      if mode == 'TimeZoneGroup':
         for TimeZone in TIMEZONES:

            if info[data][TimeZone]["SEND"] == 0:
               res["statContent"] += """<td>0</td>"""
            else:
               res["statContent"] += """<td>{}</td>""".format(str(info[data][TimeZone]["OPEN"]) +
                                  " ({:.1f}%)".format(info[data][TimeZone]["OPEN"]/info[data][TimeZone]["SEND"] * 100))

      if mode == 'RegionGroup':
         for Region in REGIONS:

            if info[data][Region]["SEND"] == 0:
               res["statContent"] += """<td>0</td>"""
            else:
               res["statContent"] += """<td>{}</td>""".format(str(info[data][Region]["OPEN"]) +
                                  " ({:.1f}%)".format(info[data][Region]["OPEN"]/info[data][Region]["SEND"] * 100))

      if info[data]["res"]["SEND"]== 0:
         res["statContent"] += """<td>0</td></tr>"""
      else:
         res["statContent"] += """
   <td>{}</td>
</tr>
""".format( str(info[data]["res"]["OPEN"]) + " ({:.1f}%)".format(info[data]["res"]["OPEN"]/info[data]["res"]["SEND"] * 100) )

   res["statContent"] += """
<tr>
   <td rowspan="6" style="vertical-align: middle;"><h3>Итого :</h5></td>
   <td><h4>Всего организаций</h4></td>"""

   if mode == 'TimeZoneGroup':
      for TimeZone in TIMEZONES:
         res["statContent"] += """<td>{}</td>""".format(str(row_res[TimeZone]["COUNT"]))

   if mode == 'RegionGroup':
      for Region in REGIONS:
         res["statContent"] += """<td>{}</td>""".format(str(row_res[Region]["COUNT"]))

   res["statContent"] += """
   <td><h4>{}</h4></td>
</tr>""".format(str(glob_info["COUNT"]))

   res["statContent"] += """
<tr>
   <td><h4>ОДП при загрузке</h4></td>"""

   if mode == 'TimeZoneGroup':
      for TimeZone in TIMEZONES:
         if row_res[TimeZone]["COUNT"] != 0:
            res["statContent"] += """<td>{}</td>""".format(str(row_res[TimeZone]["ODP_LOAD"]) +
                               " ({:.1f}%)".format(row_res[TimeZone]["ODP_LOAD"]/row_res[TimeZone]["COUNT"] * 100))
         else:
            res["statContent"] += """<td>0</td>"""

   if mode == 'RegionGroup':
      for Region in REGIONS:
         if row_res[Region]["COUNT"]:
            res["statContent"] += """<td>{}</td>""".format(str(row_res[Region]["ODP_LOAD"]) +
                               " ({:.1f}%)".format(row_res[Region]["ODP_LOAD"]/row_res[Region]["COUNT"] * 100))
         else:
            res["statContent"] += """<td>0</td>"""

   res["statContent"] += """
   <td><h4>{}</h4></td>
</tr>""".format(str(glob_info["ODP_LOAD"]) + " ({:.1f}%)".format(glob_info["ODP_LOAD"]/glob_info["COUNT"] * 100))

   res["statContent"] += """
<tr>
   <td><h4>ОДП</h4></td>"""

   if mode == 'TimeZoneGroup':
      for TimeZone in TIMEZONES:
         if row_res[TimeZone]["COUNT"] != 0:
            res["statContent"] += """<td>{}</td>""".format(str(row_res[TimeZone]["ODP"]) +
                               " ({:.1f}%)".format(row_res[TimeZone]["ODP"]/row_res[TimeZone]["COUNT"] * 100))
         else:
            res["statContent"] += """<td>0</td>"""

   if mode == 'RegionGroup':
      for Region in REGIONS:
         if row_res[Region]["COUNT"]:
            res["statContent"] += """<td>{}</td>""".format(str(row_res[Region]["ODP"]) +
                               " ({:.1f}%)".format(row_res[Region]["ODP"]/row_res[Region]["COUNT"] * 100))
         else:
            res["statContent"] += """<td>0</td>"""

   res["statContent"] += """
   <td><h4>{}</h4></td>
</tr>""".format(str(glob_info["ODP"]) + " ({:.1f}%)".format(glob_info["ODP"]/glob_info["COUNT"] * 100))

   res["statContent"] += """
<tr>
   <td><h4>Отправлено заявок</h4></td>"""

   if mode == 'TimeZoneGroup':
      for TimeZone in TIMEZONES:
         if row_res[TimeZone]["COUNT"] != 0:
            res["statContent"] += """<td>{}</td>""".format(str(row_res[TimeZone]["SEND"]) +
                               " ({:.1f}%)".format(row_res[TimeZone]["SEND"]/row_res[TimeZone]["COUNT"] * 100))
         else:
            res["statContent"] += """<td>{}</td>"""

   if mode == 'RegionGroup':
      for Region in REGIONS:
         if row_res[Region]["COUNT"] != 0:
            res["statContent"] += """<td>{}</td>""".format(str(row_res[Region]["SEND"]) +
                               " ({:.1f}%)".format(row_res[Region]["SEND"]/row_res[Region]["COUNT"] * 100))
         else:
            res["statContent"] += """<td>{}</td>"""

   res["statContent"] += """
   <td><h4>{}</h4></td>
</tr>""".format(str(glob_info["SEND"]) + " ({:.1f}%)".format(glob_info["SEND"]/glob_info["COUNT"] * 100))

   res["statContent"] += """
<tr>
   <td><h4>Отправлено заявок по первому звонку</h4></td>"""

   if mode == 'TimeZoneGroup':
      for TimeZone in TIMEZONES:
         if row_res[TimeZone]["COUNT"] != 0:
            res["statContent"] += """<td>{}</td>""".format(str(row_res[TimeZone]["UNIQ_SEND"]) +
                               " ({:.1f}%)".format(row_res[TimeZone]["UNIQ_SEND"]/row_res[TimeZone]["COUNT"] * 100))
         else:
            res["statContent"] += """<td>{}</td>"""

   if mode == 'RegionGroup':
      for Region in REGIONS:
         if row_res[Region]["COUNT"] != 0:
            res["statContent"] += """<td>{}</td>""".format(str(row_res[Region]["UNIQ_SEND"]) +
                               " ({:.1f}%)".format(row_res[Region]["UNIQ_SEND"]/row_res[Region]["COUNT"] * 100))
         else:
            res["statContent"] += """<td>{}</td>"""

   res["statContent"] += """
   <td><h4>{}</h4></td>
</tr>""".format(str(glob_info["UNIQ_SEND"]) + " ({:.1f}%)".format(glob_info["UNIQ_SEND"]/glob_info["COUNT"] * 100))

   res["statContent"] += """
<tr>
   <td><h4>Заключено сделок</h4></td>"""

   if mode == 'TimeZoneGroup':
      for TimeZone in TIMEZONES:
         if row_res[TimeZone]["SEND"] == 0:
            res["statContent"] += """<td>0</td>"""
         else:
            res["statContent"] += """<td>{}</td>""".format(str(row_res[TimeZone]["OPEN"]) +
                               " ({:.1f}%)".format(row_res[TimeZone]["OPEN"]/row_res[TimeZone]["SEND"] * 100))

   if mode == 'RegionGroup':
      for Region in REGIONS:
         if row_res[Region]["SEND"] == 0:
            res["statContent"] += """<td>0</td>"""
         else:
            res["statContent"] += """<td>{}</td>""".format(str(row_res[Region]["OPEN"]) +
                               " ({:.1f}%)".format(row_res[Region]["OPEN"]/row_res[Region]["SEND"] * 100))

   if info[data]["res"]["SEND"]== 0:
      res["statContent"] += """<td><h4>0</h4></td>
</tr>"""
   else:
      res["statContent"] += """
   <td><h4>{}</h4></td>
</tr>
""".format(str(glob_info["OPEN"]) + " ({:.1f}%)".format(glob_info["OPEN"]/glob_info["SEND"] * 100),)

   res["statContent"] += "</tbody>"
   print("Content-type: application/json")
   print()
   print(json.JSONEncoder().encode(res))

try:
   get_resp()
except Exception as e:
   res = {}
   res["statContent"] = "\nОшибка:\n" + str(e) + "\n" + traceback.format_exc()
   print("Content-type: application/json")
   print()
   print(json.JSONEncoder().encode(res))