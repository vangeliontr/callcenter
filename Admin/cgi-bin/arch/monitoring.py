#!/usr/bin/env python3

from DBController import DBController

controller = DBController()
cur = controller.GetCur()

print("Content-type: text/html")
print()
head = open("./cgi-bin/head.tmpl", "r").read()
head = head.replace("{{slyle}}", """<link href="../admin.css" rel="stylesheet">""")
print(head)
menu_content = open("./cgi-bin/menu.html", "r", encoding="utf-8").read()
print(menu_content)
monitoring_content = open("./cgi-bin/monitoring.html", "r", encoding="utf-8").read()

print(monitoring_content)
print(open("./cgi-bin/foot.html", "r").read())