#!/usr/bin/python3

import json
from DBController import DBController

controller = DBController()
cur = controller.GetCur()
cur.execute("""
SELECT
Organization.ID as OrganizationID,
Organization.`Name`,
Organization.INN,
Organization.Address,
DATE_FORMAT( Organization.OpenDate, "%d.%m" ) as OpenDate,
Organization.TimeZone,
ContactPerson.ID as ContactPersonID,
ContactPerson.Surname,
ContactPerson.`Name` as ContactPersonName,
Call_.ID,
DATE_FORMAT(Call_.DateTime, "%d.%m %H:%i") as DateTime,
Call_.Result,
Project.`Name` as ProjectName,
Operator.ID,
Employee.Surname as EmployeeSurname,
Employee.`Name` as EmployeeName
FROM
(
SELECT
Call_.Result,
Call_.`Comment`,
Call_.DateTime,
Call_.ID,
Call_.Operator,
Call_.ContactPerson,
Call_.Project
FROM
Call_
WHERE Result != 'ОДПРоботТФ'
ORDER BY Call_.DateTime DESC
LIMIT 100 ) AS Call_
INNER JOIN ContactPerson ON Call_.ContactPerson = ContactPerson.ID
INNER JOIN Project ON Call_.Project = Project.ID
INNER JOIN Operator ON Call_.Operator = Operator.ID
INNER JOIN Employee ON Employee.ID = Operator.Employee
INNER JOIN Call_Organization ON Call_.ID = Call_Organization.Call_
INNER JOIN Organization ON Organization.ID = Call_Organization.Organization
""" )

res = cur.fetchall()

print("Content-type: application/json")
print()
print(json.JSONEncoder().encode(res))