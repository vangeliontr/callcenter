#!/usr/bin/python3

import json
import cgi
import traceback
from DBController import DBController
import re


def get_resp():

   def PrintRow(name, data, col, mode, REGIONS, TIMEZONES, OPERATORS):
      res = "<td>{}</td>".format(name)

      if mode == 'TimeZoneGroup':
         for TimeZone in TIMEZONES:
            res += """<td>{}</td>""".format(data[TimeZone][col])

      if mode == 'RegionGroup':
         for Region in REGIONS:
            res += """<td>{}</td>""".format(data[Region][col])

      if mode == 'OperatorGroup':
         for operator in OPERATORS:
            res += """<td>{}</td>""".format(data[operator][col])

      res += """<td style="font-weight: bold;font-size: larger;">{}</td>
                </tr>""".format(str(data["res"][col]))

      return res

   def PrintRowDiv(name, data, col, base, mode, REGIONS, TIMEZONES, OPERATORS):
      res = "<td>{}</td>".format(name)

      if mode == 'TimeZoneGroup':
         for TimeZone in TIMEZONES:
            if data[TimeZone][base] != 0:
               res += """<td>{}</td>""".format(str(data[TimeZone][col]) +
                      " ({:.1f}%)".format(data[TimeZone][col]/data[TimeZone][base] * 100))
            else:
               res += """<td>0</td>"""

      if mode == 'RegionGroup':
         for Region in REGIONS:
            if data[Region][base] != 0:
               res += """<td>{}</td>""".format(str(data[Region][col]) +
                      " ({:.1f}%)".format(data[Region][col]/data[Region][base] * 100))
            else:
               res += """<td>0</td>"""

      if mode == 'OperatorGroup':
         for operator in OPERATORS:
            if data[operator][base] != 0:
               res += """<td>{}</td>""".format(str(data[operator][col]) +
                      " ({:.1f}%)".format(data[operator][col]/data[operator][base] * 100))
            else:
               res += """<td>0</td>"""

      if data["res"][base] != 0:
         res += """<td style="font-weight: bold;font-size: larger;">{}</td>
                   </tr>""".format(str(data["res"][col]) + " ({:.1f}%)".format(data["res"][col]/data["res"][base] * 100))
      else:
         res += """<td style="font-weight: bold;font-size: larger;">0</td>"""

      return res

   def PrintDetail(data, info, TEMPLATE, mode, REGIONS, TIMEZONES, OPERATORS, is_bold):

      style = ""

      if is_bold:
         style = "font-weight: bold;font-size: larger;"

      res = """<tr style="{}">
               <td rowspan="{}" style="vertical-align: middle;">{}</td>""".format(style,str(len(TEMPLATE)),data)

      res += PrintRow("Всего звонков", info[data], "COUNT", mode, REGIONS, TIMEZONES, OPERATORS)
      res += '<tr style="{}">'.format(style) + PrintRowDiv("Отправлено", info[data], "SEND", "COUNT", mode, REGIONS, TIMEZONES, OPERATORS)
      res += '<tr style="{}">'.format(style) + PrintRowDiv("Сделок", info[data], "BID", "SEND", mode, REGIONS, TIMEZONES, OPERATORS)
      res += '<tr style="{}">'.format(style) + PrintRowDiv("Повторная обработка", info[data], "RECALL", "COUNT", mode, REGIONS, TIMEZONES, OPERATORS)
      res += '<tr style="{}">'.format(style) + PrintRowDiv("Плохие данные", info[data], "BADDATA", "COUNT", mode, REGIONS, TIMEZONES, OPERATORS)
      res += '<tr style="{}">'.format(style) + PrintRowDiv("Отказ", info[data], "NEGATIV", "COUNT", mode, REGIONS, TIMEZONES, OPERATORS)
      res += '<tr style="{}">'.format(style) + PrintRowDiv("ОДП", info[data], "ODP", "COUNT", mode, REGIONS, TIMEZONES, OPERATORS)
      res += '<tr style="{}">'.format(style) + PrintRowDiv("Уникальных контактов", info[data], "UNIQ_COUNT", "COUNT", mode, REGIONS, TIMEZONES, OPERATORS)
      res += '<tr style="{}">'.format(style) + PrintRowDiv("Отправлено уникальных", info[data], "UNIQ_SEND", "UNIQ_COUNT", mode, REGIONS, TIMEZONES, OPERATORS)

      return res

   form = cgi.FieldStorage()
   mode = form.getvalue('mode')
   start_date = form.getvalue('start_date')
   end_date = form.getvalue('end_date')
   detail = form.getvalue('detail')
   controller = DBController()
   cur = controller.GetCur()

   main_sql = """SELECT """

   if detail == "Month":
      main_sql += 'DATE_FORMAT(DateTime, "%m.%Y") as Detail,'
   elif detail == "Hours":
      main_sql += 'DATE_FORMAT(DateTime, "%d.%m.%Y %H") as Detail,'
   else:
      main_sql += 'DATE_FORMAT(DateTime, "%d.%m.%Y") as Detail,'

   main_sql += """ Result, IF(PrevCall_ IS NULL, false, true) as uniq, IF(Bid.State = 'Счет открыт', true, false) as BID, count(*) as CNT"""

   if mode == 'OperatorGroup':
      main_sql += ", CONCAT(Employee.Name, ' ', Employee.Surname) as grp"

   if mode == 'TimeZoneGroup':
      main_sql += ', TimeZone as grp'

   if mode == 'RegionGroup':
      main_sql += ', TimeZone.Region as grp'

   main_sql += """
FROM Call_ LEFT JOIN Bid ON Call_.ID = Bid.Call_ """

   if mode == 'OperatorGroup':
      main_sql += """INNER JOIN Operator ON Call_.Operator = Operator.ID
                     INNER JOIN Employee ON Operator.Employee = Employee.ID"""

   if mode == 'TimeZoneGroup' or mode == 'RegionGroup':
      main_sql += """INNER JOIN Call_Organization ON Call_Organization.Call_ = Call_.ID
                     INNER JOIN Organization ON Call_Organization.Organization = Organization.ID"""

   if mode == 'RegionGroup':
      main_sql += ' LEFT JOIN TimeZone ON SUBSTR(Organization.Address,1,3) >= TimeZone.StartValue AND ' \
                  'SUBSTR(Organization.Address,1,3) <= TimeZone.EndValue'

   main_sql += """
WHERE DateTime >= '{}' AND DateTime <= '{}' AND Result is NOT NULL AND Result <> 'ОДПРоботТФ'
GROUP BY """

   if detail == "Month":
      main_sql += 'MONTH( DateTime ), YEAR(DateTime)'
   elif detail == "Hours":
      main_sql += 'Date(DateTime), HOUR(DateTime)'
   else:
      main_sql += 'Date(DateTime)'

   main_sql += """, Result, IF(PrevCall_ IS NULL, false, true), IF(Bid.State = 'Счет открыт', true, false)"""

   if mode == 'OperatorGroup':
      main_sql += ", Employee.Name, Employee.Surname"

   if mode == 'TimeZoneGroup':
      main_sql += ', TimeZone'

   if mode == 'RegionGroup':
      main_sql += ', TimeZone.Region'

   cur.execute(main_sql.format(start_date,end_date))

   res = cur.fetchall()

   OPERATORS = []
   if mode == 'OperatorGroup':
      cur.execute("""SELECT Employee.Surname, Employee.`Name`
                     FROM Operator
                     INNER JOIN Employee ON Operator.Employee = Employee.ID
                     WHERE Dismissed = FALSE""")

      op_list = cur.fetchall()

      for data in res:
         if not data['grp'] in OPERATORS:
            OPERATORS.append( data['grp'] )

   info = {}
   row_res = None

   REGIONS = ["01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16",
              "17","18","19","21","22","23","24","25","26","27","28","29","30","31","32","33",
              "34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49",
              "50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65",
              "66","67","68","69","70","71","72","73","74","75","76","77","78","79","83","86",
              "87","89","91","92","95",None]

   TIMEZONES = [9,8,7,6,5,4,3,2,1,0,-1,None]

   TEMPLATE = {"COUNT":0, "SEND":0, "RECALL":0, "BADDATA":0, "NEGATIV":0, "ODP":0, "UNIQ_COUNT":0, "UNIQ_SEND":0, "BID":0}

   if mode == "NonGroup":
      row_res = TEMPLATE.copy()

   if mode == 'TimeZoneGroup':
      row_res = {}

      for TimeZone in TIMEZONES:
         row_res[TimeZone]= TEMPLATE.copy()

   if mode == 'RegionGroup':
      row_res = {}

      for Region in REGIONS:
         row_res[Region]= TEMPLATE.copy()

   if mode == 'OperatorGroup':
      row_res = {}

      for operator in OPERATORS:
         row_res[operator]= TEMPLATE.copy()

   glob_info = TEMPLATE.copy()

   for data in res:
      cur_row = None

      if not data["Detail"] in info:
         info[data["Detail"]] = {}
         cur_row = info[data["Detail"]]
         cur_row["res"] = TEMPLATE.copy()

         if mode == 'TimeZoneGroup':
            for TimeZone in TIMEZONES:
               cur_row[TimeZone]= TEMPLATE.copy()

         if mode == 'RegionGroup':
            for Region in REGIONS:
               cur_row[Region]= TEMPLATE.copy()

         if mode == 'OperatorGroup':
            for operator in OPERATORS:
               cur_row[operator]= TEMPLATE.copy()
      else:
         cur_row = info[data["Detail"]]

      cur_row["res"]["COUNT"] += data["CNT"]
      glob_info["COUNT"] += data["CNT"]

      if "grp" in data:
         row_res[data["grp"]]["COUNT"] += data["CNT"]
         cur_row[data["grp"]]["COUNT"] += data["CNT"]

      if data["Result"] == 'Отправлено':
         cur_row["res"]["SEND"] += data["CNT"]
         glob_info["SEND"] += data["CNT"]

         if "grp" in data:
            row_res[data["grp"]]["SEND"] += data["CNT"]
            cur_row[data["grp"]]["SEND"] += data["CNT"]

      if data["Result"] in ('Недозвон', 'Повторный звонок', 'Отказ. Пока не актуально', 'Дальний регион'):
         cur_row["res"]["RECALL"] += data["CNT"]
         glob_info["RECALL"] += data["CNT"]

         if "grp" in data:
            row_res[data["grp"]]["RECALL"] += data["CNT"]
            cur_row[data["grp"]]["RECALL"] += data["CNT"]

      if data["Result"] in ('Неактивен', 'Неправильный номер', 'Дубль', 'Отказ. Крым'):
         cur_row["res"]["BADDATA"] += data["CNT"]
         glob_info["BADDATA"] += data["CNT"]

         if "grp" in data:
            row_res[data["grp"]]["BADDATA"] += data["CNT"]
            cur_row[data["grp"]]["BADDATA"] += data["CNT"]

      if data["Result"] in ('ОДП'):
         cur_row["res"]["ODP"] += data["CNT"]
         glob_info["ODP"] += data["CNT"]

         if "grp" in data:
            row_res[data["grp"]]["ODP"] += data["CNT"]
            cur_row[data["grp"]]["ODP"] += data["CNT"]

      if data["Result"] in ('Отказ. Сбер', 'Отказ. Точка', 'Отказ. ТФ', 'Отказ. Др.банк', 'Отказ. Открыл', 'Отказ. Звонили', 'Отказ. Нет потребности', 'Отказ. Услуга не нужна', 'Отказ. Мск'):
         cur_row["res"]["NEGATIV"] += data["CNT"]
         glob_info["NEGATIV"] += data["CNT"]

         if "grp" in data:
            row_res[data["grp"]]["NEGATIV"] += data["CNT"]
            cur_row[data["grp"]]["NEGATIV"] += data["CNT"]

      if data["uniq"]:
         cur_row["res"]["UNIQ_COUNT"] += data["CNT"]
         glob_info["UNIQ_COUNT"] += data["CNT"]

         if "grp" in data:
            row_res[data["grp"]]["UNIQ_COUNT"] += data["CNT"]
            cur_row[data["grp"]]["UNIQ_COUNT"] += data["CNT"]

      if data["uniq"] and data["Result"] == 'Отправлено':
         cur_row["res"]["UNIQ_SEND"] += data["CNT"]
         glob_info["UNIQ_SEND"] += data["CNT"]

         if "grp" in data:
            row_res[data["grp"]]["UNIQ_SEND"] += data["CNT"]
            cur_row[data["grp"]]["UNIQ_SEND"] += data["CNT"]

      if data["BID"]:
         cur_row["res"]["BID"] += data["CNT"]
         glob_info["BID"] += data["CNT"]

         if "grp" in data:
            row_res[data["grp"]]["BID"] += data["CNT"]
            cur_row[data["grp"]]["BID"] += data["CNT"]

   res = {}
   res["statContent"] = """
<thead class="thead-inverse">
<tr>
<th width="10%">Дата звонка</th>
<th></th>"""

   if mode == 'TimeZoneGroup':
      for TimeZone in TIMEZONES:
         res["statContent"] += """<th>{}</th>""".format(TimeZone)

   if mode == 'RegionGroup':
      for Region in REGIONS:
         res["statContent"] += """<th>{}</th>""".format(Region)

   if mode == 'OperatorGroup':
      for operator in OPERATORS:
         res["statContent"] += """<th>{}</th>""".format(operator)

   res["statContent"] += """<th>Итого</th>
<th></th>
</tr>
</thead>
<tbody>"""

   for data in info:
      res["statContent"] += PrintDetail(data, info, TEMPLATE, mode, REGIONS, TIMEZONES, OPERATORS, False)

   row_res['res'] = glob_info
   info['Итого'] = row_res
   res["statContent"] += PrintDetail('Итого', info, TEMPLATE, mode, REGIONS, TIMEZONES, OPERATORS, True)

   res["statContent"] += "</tbody>"
   print("Content-type: application/json")
   print()
   print(json.JSONEncoder().encode(res))

try:
   get_resp()
except Exception as e:
   res = {}
   res["statContent"] = "\nОшибка:\n" + str(e) + "\n" + traceback.format_exc()
   print("Content-type: application/json")
   print()
   print(json.JSONEncoder().encode(res))