function SelectMenu(id) {
    var menu = document.getElementById( id );
    menu.classList.add("active");
    var span = document.createElement('span');
    span.classList.add("sr-only");
    span.value = '(current)';
    menu.children[0].appendChild(span);
}

function appenToTable( tr, text ) {
    var td = document.createElement("td");
    var value = document.createTextNode(text);
    td.appendChild(value);
    tr.appendChild(td)
}

function appenLinkToTable( tr, href, text ) {
    var td = document.createElement("td");
    var link = document.createElement("a");
    var value = document.createTextNode(text);
    link.href = href;
    link.appendChild(value);
    td.appendChild(link);
    tr.appendChild(td)
}

function refreshLastCalls() {
    var table = document.getElementById( "last_contact_content" )
    table.innerHTML = ""
    loadLastCalls()
}

function findOrganization(){
    inn = document.getElementById("find_organization_inn").value
    id = document.getElementById("find_organization_id").value

    var xhr = new XMLHttpRequest();

    var body = 'find_organization_inn=' + inn;
    body += '&find_organization_id=' + id;

    xhr.open("POST", 'find_organization.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res_json = this.responseText.substring( this.responseText.search("{") - 1)
        var contact_list = JSON.parse( res_json );
        var table = document.getElementById( "find_organization_content" )

        for( var i = 0; i < contact_list.length; i++ ) {
            var tr = document.createElement("tr");
            var row = contact_list[i];

            appenToTable(tr, row["Type"])
            appenLinkToTable(tr, 'organization.py?id=' + row["ID"], row["Name"])
            appenToTable(tr, row["INN"])
            appenToTable(tr, row["OpenDate"])
            appenToTable(tr, row["Address"])
            appenToTable(tr, row["TimeZone"])

            table.appendChild(tr)
        }
    }

    xhr.send(body);
}

function findContactPerson(){
    tel = document.getElementById("find_contact_person_tel").value
    surname = document.getElementById("find_contact_person_surname").value
    name = document.getElementById("find_contact_person_name").value
    middlename = document.getElementById("find_contact_person_middlename").value
    email = document.getElementById("find_contact_person_email").value

    var xhr = new XMLHttpRequest();

    var body = 'contact_person_tel=' + tel;
    body += '&contact_person_surname=' + surname;
    body += '&contact_person_name=' + name;
    body += '&contact_person_middlename=' + middlename;
    body += '&contact_person_email=' + email;

    xhr.open("POST", 'find_contact_person.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res_json = this.responseText.substring( this.responseText.search("{") - 1)
        var contact_list = JSON.parse( res_json );
        var table = document.getElementById( "find_contact_person_content" )
        table.innerHTML = ""

        if(contact_list.length == 0){
          alert("Не удалось найти контактное лицо")
        }

        for( var i = 0; i < contact_list.length; i++ ) {
            var tr = document.createElement("tr");
            var row = contact_list[i];

            appenLinkToTable(tr, 'contact_person.py?id=' + row["ContactPersonID"], row["Surname"])
            appenToTable(tr, row["Name"])
            appenToTable(tr, row["MiddleName"])

            table.appendChild(tr)
        }
    }

    xhr.send(body);
}

function findCall() {
    call_id = document.getElementById("find_call_id").value

    var xhr = new XMLHttpRequest();

    var body = 'call_id=' + call_id;

    xhr.open("POST", 'find_call.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res_json = this.responseText.substring( this.responseText.search("{") - 1)
        var contact_list = JSON.parse( res_json );
        var table = document.getElementById( "find_contact_content" )

        for( var i = 0; i < contact_list.length; i++ ) {
            var tr = document.createElement("tr");
            var row = contact_list[i];

            appenLinkToTable(tr, 'organization.py?id=' + row["OrganizationID"], row["Name"])
            appenToTable(tr, row["INN"])
            appenToTable(tr, row["Address"])
            appenToTable(tr, row["OpenDate"])
            appenToTable(tr, row["TimeZone"])
            appenLinkToTable(tr, 'contact_person.py?id=' + row["ContactPersonID"], row["Surname"])
            appenToTable(tr, row["ContactPersonName"])
            appenToTable(tr, row["DateTime"])
            appenToTable(tr, row["Result"])
            appenToTable(tr, row["ProjectName"])
            appenToTable(tr, row["EmployeeName"])
            appenToTable(tr, row["EmployeeSurname"])

            table.appendChild(tr)
        }
    }

    xhr.send(body);
}

function restartRobort(robot) {
    var xhr = new XMLHttpRequest();

    xhr.open("POST", 'restart_robot.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send("robot=" + robot);
}

function startActivityMonitoring() {
    var xhr = new XMLHttpRequest();

    xhr.open("POST", 'get_robot_info.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var contact_list = JSON.parse( this.responseText );
        var activityTF = document.getElementById( "activityTF" );
        var activitySB = document.getElementById( "activitySB" );
        var activityAL = document.getElementById( "activityAL" );

        activityTF.innerHTML = contact_list["activityTF"];
        activitySB.innerHTML = contact_list["activitySB"];
        activityAL.innerHTML = contact_list["activityAL"];

        var downloadContent = document.getElementById( "update" );
        downloadContent.innerHTML = "Обновлено: " + new Date();

        //setTimeout(startActivityMonitoring, 3000);
        startActivityMonitoring()
    }

    xhr.send("page=0");
}

function startMonitoring() {
    var xhr = new XMLHttpRequest();

    var body = 'page=0';

    xhr.open("POST", 'get_monitor_info.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var contact_list = JSON.parse( this.responseText );
        var downloadContent = document.getElementById( "downloadContent" );
        var ODPContent = document.getElementById( "ODPContent" );
        var workContentAlfa = document.getElementById( "workContentAlfa" );
        var workContentSber = document.getElementById( "workContentSber" );
        var workContentTF = document.getElementById( "workContentTF" );
        var processedContent = document.getElementById( "processedContent" );

        downloadContent.innerHTML = contact_list["downloadContent"];
        ODPContent.innerHTML = contact_list["ODPContent"];
        workContentTF.innerHTML = contact_list["workContentTF"];
        workContentAlfa.innerHTML = contact_list["workContentAlfa"];
        workContentSber.innerHTML = contact_list["workContentSber"];
        processedContent.innerHTML = contact_list["processedContent"];

        var downloadContent = document.getElementById( "update" );
        downloadContent.innerHTML = "Обновлено: " + new Date();

        startMonitoring()
    }

    xhr.send(body);
}

function startMonitoringDownload() {
    var xhr = new XMLHttpRequest();

    var body = 'page=0';

    xhr.open("POST", 'get_download_info.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var contact_list = JSON.parse( this.responseText );
        var downloadContent = document.getElementById( "downloadContent" );

        downloadContent.innerHTML = contact_list["downloadContent"];

        var downloadContent = document.getElementById( "update" );
        downloadContent.innerHTML = "Обновлено: " + new Date();

        startMonitoringDownload()
    }

    xhr.send(body);
}

function InitStat() {
    var start_date = $("#datetimepickerstart").data("DateTimePicker").date();
    var end_date = $("#datetimepickerend").data("DateTimePicker").date();

    start_date = start_date.year() + "-" + (start_date.month() + 1) + "-" + start_date.date()
    end_date = end_date.year() + "-" + (end_date.month() + 1) + "-" + end_date.date()

    mode = "NonGroup"

    if( $("#TimeZoneGroup")[0].checked ){
        mode = "TimeZoneGroup"
    }

    if( $("#RegionGroup")[0].checked ){
        mode = "RegionGroup"
    }

    detail = "Days"

    if( $("#Month")[0].checked ){
        detail = "Month"
    }

    project_list = document.getElementsByName("project")
    project_id = "1"

    for( var i = 0; i < project_list.length; i++ )
        if( project_list[i].checked )
            project_id = project_list[i].id

    var xhr = new XMLHttpRequest();

    var body = 'mode=' + mode;
    body += '&start_date=' + start_date;
    body += '&end_date=' + end_date;
    body += '&detail=' + detail;
    body += '&project=' + project_id;

    xhr.open("POST", 'get_statistics.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res_json = this.responseText.substring( this.responseText.search("{") - 1)
        var res = JSON.parse( res_json );
        var table = document.getElementById( "StatBody" )

        table.innerHTML = res["statContent"];
    }

    xhr.send(body);
}

function loadLastCalls() {
    var xhr = new XMLHttpRequest();

    var body = 'page=0';

    xhr.open("POST", 'get_last_contacts.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res_json = this.responseText.substring( this.responseText.search("{") - 1)
        var contact_list = JSON.parse( res_json );
        var table = document.getElementById( "last_contact_content" )

        for( var i = 0; i < contact_list.length; i++ ) {
            var tr = document.createElement("tr");
            var row = contact_list[i];

            appenLinkToTable(tr, 'organization.py?id=' + row["OrganizationID"], row["Name"])
            appenToTable(tr, row["INN"])
            appenToTable(tr, row["Address"])
            appenToTable(tr, row["OpenDate"])
            appenToTable(tr, row["TimeZone"])
            appenLinkToTable(tr, 'contact_person.py?id=' + row["ContactPersonID"], row["Surname"])
            appenToTable(tr, row["ContactPersonName"])
            appenToTable(tr, row["DateTime"])
            appenToTable(tr, row["Result"])
            appenToTable(tr, row["ProjectName"])
            appenToTable(tr, row["EmployeeName"])
            appenToTable(tr, row["EmployeeSurname"])

            table.appendChild(tr)
        }
    }

    xhr.send(body);
}

function GetCallStat() {
    var start_date = $("#datetimepickerstart").data("DateTimePicker").date();
    var end_date = $("#datetimepickerend").data("DateTimePicker").date();

    start_date = start_date.year() + "-" + (start_date.month() + 1) + "-" + start_date.date() + " " + start_date.hour() + ":00:00"
    end_date = end_date.year() + "-" + (end_date.month() + 1) + "-" + end_date.date() + " " + end_date.hour() + ":00:00"

    mode = "NonGroup"

    if( $("#OperatorGroup")[0].checked ){
        mode = "OperatorGroup"
    }

    if( $("#TimeZoneGroup")[0].checked ){
        mode = "TimeZoneGroup"
    }

    if( $("#RegionGroup")[0].checked ){
        mode = "RegionGroup"
    }

    detail = "Days"

    if( $("#Month")[0].checked ){
        detail = "Month"
    }

    if( $("#Hours")[0].checked ){
        detail = "Hours"
    }

    var xhr = new XMLHttpRequest();

    var body = 'mode=' + mode;
    body += '&start_date=' + start_date;
    body += '&end_date=' + end_date;
    body += '&detail=' + detail;

    xhr.open("POST", 'get_call_statistics.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res_json = this.responseText.substring( this.responseText.search("{") - 1)
        var res = JSON.parse( res_json );
        var table = document.getElementById( "StatBody" )

        table.innerHTML = res["statContent"];
    }

    xhr.send(body);
}

function setOdpCheck(){
    var odpCheck = document.getElementById( "odpCheck" );
    odpCheck.checked = true
}

function getOrganizationList(){
    var downloadList = document.getElementById( "downloadList" );
    var downloadId = downloadList.getAttribute("downloadId");

    var operationList = document.getElementById( "operationList" );
    var operationId = operationList.getAttribute("operationId");

    var validList = document.getElementById( "validList" );
    var validId = validList.getAttribute("validId");

    var odpCheck = document.getElementById( "odpCheck" );
    var firstNumberCheck = document.getElementById( "firstNumberCheck" );

    var xhr = new XMLHttpRequest();

    var body = 'downloadId=' + downloadId;

    if($("#StateData")[0].checked)
        body += "&status=" + $("#StateDataInput")[0].value;

    body += "&operation=" + operationId
    body += "&valid=" + validId

    if( odpCheck.checked )
        body += "&odpCheck=Да"

    if( firstNumberCheck.checked )
        body += "&firstNumberCheck=Да"

    xhr.open("POST", 'get_organization_list.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res_json = this.responseText.substring( this.responseText.search("{") - 1)
        var contact_list = JSON.parse( res_json );
        var table = document.getElementById( "organization_content" )
        table.innerHTML = ""

        for( var i = 0; i < contact_list.length; i++ ) {
            var tr = document.createElement("tr");
            var row = contact_list[i];

            appenToTable(tr, row["Type"])
            appenLinkToTable(tr, '../organization.py?id=' + row["ID"], row["Name"])
            appenToTable(tr, row["INN"])
            appenToTable(tr, row["OpenDate"])
            appenToTable(tr, row["Address"])
            appenToTable(tr, row["TimeZone"])

            table.appendChild(tr)
        }

        var count_head = document.getElementById( "count_head" );

        count_head.innerHTML = "Количество: " + contact_list.length
    }

    xhr.send(body);
}

function setStatus(status) {
    $("#StateData").click();
    $("#StateDataInput")[0].value  = status
}

function setDownload(id,name){
    var downloadList = document.getElementById( "downloadList" )
    downloadList.innerHTML = name
    downloadList.setAttribute("downloadId", id)
}

function setOperation(id,name){
    var operationList = document.getElementById( "operationList" )
    operationList.innerHTML = name
    operationList.setAttribute("operationId", id)
}

function setValid(name){
    var validList = document.getElementById( "validList" )
    validList.innerHTML = name
    validList.setAttribute("validId", name)
}

function getPilotInfo( pilot_id, pilot_name ) {
    var pilot_head = document.getElementById( "pilot_head" )
    pilot_head.innerHTML = "Пилот: " + pilot_name

    var update = document.getElementById( "update" )
    update.innerHTML = "Построение отчета"

    var body = 'pilot_id=' + pilot_id;

    var xhr = new XMLHttpRequest();
    xhr.open("POST", 'get_pilot_info.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res = JSON.parse( this.responseText );
        var table = document.getElementById( "PilotInfo" )

        table.innerHTML = res["PilotInfo"];
    }

    xhr.send(body);
}