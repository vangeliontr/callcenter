#!/usr/bin/env python3

import cgi
import sys
sys.path.append("..")
from DataBase.DBController import DBController

form = cgi.FieldStorage()
id = form.getvalue('id')

controller = DBController()
cur = controller.get_cursor()

cur.execute("""SELECT * FROM ContactPerson WHERE ContactPerson.ID = """ + str( id ) )
contactPerson = cur.fetchall()[0]

cur.execute("""SELECT * FROM ContactInfo WHERE ContactPerson = """ + str( id ) )
contactPersonInfoList = cur.fetchall()

cur.execute("""SELECT
Organization.ID,
Organization.Type,
Organization.`Name`,
Organization.INN,
Organization.OpenDate,
Organization.Address,
Organization.TimeZone
FROM
ContactPerson_Organization
INNER JOIN Organization ON ContactPerson_Organization.Organization = Organization.ID
WHERE
ContactPerson_Organization.ContactPerson = """ + str( id ) )
organizationList = cur.fetchall()

print("Content-type: text/html")
print()
html_template = open("./cgi-bin/card_template.html", "r", encoding="utf-8").read()
html_template = html_template.replace("{{STYLE}}", """<link href="../../admin.css" rel="stylesheet">""")

body = open("./cgi-bin/contact_person/contact_person.html", "r", encoding="utf-8").read()

body = body.replace("{{ContactPerson.Surname}}", contactPerson["Surname"])
body = body.replace("{{ContactPerson.Name}}", contactPerson["Name"])
body = body.replace("{{ContactPerson.MiddleName}}", contactPerson["MiddleName"])

contact_person_info_content = ""

def get_table_val( val ):
   if val is None:
      return "";
   else:
      return str(val)

for info in contactPersonInfoList:
   contact_person_info_content += "<tr>"
   contact_person_info_content += "<td>" + get_table_val( info["Type"] ) + "</td>"
   contact_person_info_content += "<td>" + get_table_val( info["SubType"] ) + "</td>"
   contact_person_info_content += "<td>" + get_table_val( info["Value"] ) + "</td>"
   contact_person_info_content += "</tr>"

body = body.replace("{{ContactInfo}}", contact_person_info_content)

organization_info_content = ""

for organization in organizationList:
   organization_info_content += "<tr>"
   organization_info_content += "<td>" + get_table_val( organization["Type"] ) + "</td>"
   organization_info_content += '<td><a href="../organization/organization.py?id=' + str( organization["ID"] ) + '">' + organization["Name"] + "</a></td>"
   organization_info_content += "<td>" + get_table_val( organization["INN"] ) + "</td>"
   organization_info_content += "<td>" + get_table_val( organization["OpenDate"] ) + "</td>"
   organization_info_content += "<td>" + get_table_val( organization["Address"] ) + "</td>"
   organization_info_content += "<td>" + get_table_val( organization["TimeZone"] ) + "</td>"
   organization_info_content += "</tr>"

body = body.replace("{{OrganizationInfo}}", organization_info_content)

html_template = html_template.replace("{{CONTENT}}", body)
html_template = html_template.replace("{{SCRIPT}}", "")

print(html_template)
