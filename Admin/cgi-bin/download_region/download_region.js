function startMonitoringDownloadRegion() {
    var xhr = new XMLHttpRequest();

    var body = 'page=0';

    xhr.open("POST", 'get_download_region_info.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    bank_filter = null

    var bank_filter_elem = document.getElementById("BankFilter")

    for(child_id in bank_filter_elem.children) {
        children = bank_filter_elem.children[child_id]

        try{
            if($("#" + children.children[0].id)[0].checked)
                bank_filter = children.children[0].id
        }
        catch(err) {}
    }

    if( bank_filter !== "" && bank_filter !== null )
        body += "&BankFilter=" + bank_filter

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var contact_list = JSON.parse( this.responseText );
        var downloadContent = document.getElementById( "downloadRegionContent" );
        var downloadContentHead = document.getElementById( "downloadRegionContentHead" );

        downloadContent.innerHTML = contact_list["downloadContent"];
        downloadContentHead.innerHTML = contact_list["downloadContentHead"];

        var downloadContent = document.getElementById( "update" );
        downloadContent.innerHTML = "Обновлено: " + new Date();

        startMonitoringDownloadRegion();
    }

    xhr.send(body);
}