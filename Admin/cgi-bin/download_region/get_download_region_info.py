#!/usr/bin/python3

import json
import sys
import cgi
sys.path.append("..")
from DataBase.DBController import DBController

controller = DBController()
cur = controller.get_cursor()

form = cgi.FieldStorage()

bank_filter = form.getvalue('BankFilter')

def create_download_stat(bank_filter):

    res = {}

    def get_table_val(val):
        if val is None:
            return ""
        else:
            return str(val)

    download_region_info = {}

    if bank_filter is not None:
        cur.execute("""SELECT * FROM Bank_Region 
                       INNER JOIN Region ON Region.ID = Bank_Region.Region 
                       WHERE Bank = {}""".format(bank_filter))

        bank_region_list = cur.fetchall()
        bank_filter = ""

        for bank_region in bank_region_list:
            if bank_region["Number"] is None:
                continue

            if bank_filter != "":
                bank_filter += ","

            bank_filter += bank_region["Number"]

        bank_filter = "(" + bank_filter + ")"

    cur.execute("""SELECT * FROM Download ORDER BY ID DESC LIMIT 30""")
    download_list = cur.fetchall()

    for download in download_list:

        sql = """SELECT SUBSTRING(INN,1,2) as Region, count(*) as Count
                 FROM Organization
                 WHERE Download = {} BANK_FILTER
                 GROUP BY SUBSTRING(INN,1,2)""".format(download["ID"])

        if bank_filter:
            sql = sql.replace("BANK_FILTER", "AND SUBSTRING(INN,1,2) in {}".format(bank_filter))
        else:
            sql = sql.replace("BANK_FILTER", "")

        cur.execute(sql)

        region_list = cur.fetchall()

        for reg_info in region_list:

            if reg_info["Region"] not in download_region_info:
                download_region_info[reg_info["Region"]] = {}

            download_region_info[reg_info["Region"]][download["ID"]] = reg_info["Count"]

    download_content = ""
    download_content_head = "<tr><th>ID</th><th>Тип</th>"

    for reg_id in download_region_info:
        download_content_head += "<th>{}</th>".format(reg_id)

    for download in download_list:
        download_id = download["ID"]
        download_content += "<tr>"
        download_content += "<td>{}</td>".format(download_id)
        download_content += "<td>{}</td>".format(download["Type"])

        for reg_id in download_region_info:
            count = 0

            if download_id in download_region_info[reg_id]:
                count = download_region_info[reg_id][download_id]

            download_content += "<td>{}</td>".format(count)

        download_content += "<tr>"

    download_content_head += "</tr>"

    res["downloadContent"] = download_content
    res["downloadContentHead"] = download_content_head
    return res

print("Content-type: application/json")
print()
print(json.JSONEncoder().encode(create_download_stat(bank_filter)))