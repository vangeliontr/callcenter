#!/usr/bin/python3

import sys
import cgi

sys.path.append("..")

from DataBase.DBController import DBController
from DataBase.Task import Task

form = cgi.FieldStorage()
id = form.getvalue('id')

controller = DBController()
task = Task.get_by_id(controller, id)

task.stop()
