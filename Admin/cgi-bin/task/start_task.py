#!/usr/bin/env python3

import sys
import json
sys.path.append("..")
from DataBase.DBController import DBController
from DataBase.TaskDescriptor import TaskDescriptor

import cgi
form = cgi.FieldStorage()
name = form.getvalue('name')

print("Content-type: text/html")
print()

html_template = open("./cgi-bin/template.html", "r", encoding="utf-8").read()
html_template = html_template.replace("{{STYLE}}", """<link href="../../admin.css" rel="stylesheet">""")

start_task_content = open("./cgi-bin/task/start_task.html", "r", encoding="utf-8").read()
start_task_content = start_task_content.replace("{{TASK_NAME}}", name)

controller = DBController()
task_discriptor = TaskDescriptor.get_by_name(controller, name)

task_params_content = ""

task_params = task_discriptor.get_task_params()

for param in task_params:
    task_params_content += "<tr>"

    task_params_content += "<td>{}</td>".format(task_params[param]["name"])

    control = "Неизвестный тип параметра"

    if task_params[param]["type"] == "Текст":
        control = """<input id="{}" type="text">""".format(param)
    if task_params[param]["type"] == "Целое":
        control = """<input id="{}" type="number">""".format(param)
    if task_params[param]["type"] == "Файл":
        control = """<div class="input-group mb-3">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="{}" aria-describedby="inputGroupFileAddon01">
                      </div>
                    </div>""".format(param)

    task_params_content += "<td>{}</td>".format(control)

    task_params_content += "</tr>"

start_task_content = start_task_content.replace("{{TASK_PARAMS_CONTENT}}", task_params_content)
start_task_content = start_task_content.replace("{{START_TASK_PARAMS}}", """'{}', {}""".format(name, json.dumps(task_params).replace('"', "'")))

html_template = html_template.replace("{{CONTENT}}", start_task_content)
html_template = html_template.replace("{{SCRIPT}}",
                                      """<script>window.jQuery || document.write('<script src="../../assets/js/"""
                                      """vendor/jquery.min.js"><\/script>')</script>\n"""
                                      """<script src="../../admin.js"></script>\n"""
                                      """<script src="start_task.js"></script>\n"""
                                      """<script>SelectMenu('menu_task')</script>\n""")

print(html_template)
