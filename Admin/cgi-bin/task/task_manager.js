getTaskStat()

function setOperation(id,name){
    var operationList = document.getElementById( "stateList" )
    operationList.innerHTML = name
    operationList.setAttribute("stateId", id)
}

function restartTask(task_id) {
    var xhr = new XMLHttpRequest();

    var body = '';

    xhr.open("POST", 'restart_task.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.send("id="+task_id);
}

function setFilter(task_type, state){
    setOperation(state, state)
    var taskTypeFilter = document.getElementById( "task_type_filter" );
    taskTypeFilter.value = task_type
}

function startMonitoringTask() {
    var xhr = new XMLHttpRequest();

    var taskTypeFilter = document.getElementById( "task_type_filter" );
    var taskIdFilter = document.getElementById( "task_id_filter" );
    var stateList = document.getElementById( "stateList" );
    var stateId = stateList.getAttribute("stateId")

    var is_duration = null
    var state = null

    if(stateId == 'Ожидает в очереди' || stateId == 'Отложены')
        state = 0
    else if(stateId == 'В работе')
        state = 1
    else if(stateId == 'Завершены')
        state = 2

    if(stateId == 'Отложены')
        is_duration = true

    if(stateId == 'Ожидает в очереди')
        is_duration = false

    var body = '';

    body += "task_type_filter=" + taskTypeFilter.value
    body += "&task_id_filter=" + taskIdFilter.value
    body += "&state_filter=" + state
    body += "&is_duration_filter=" + is_duration

    xhr.open("POST", 'get_tasks_info.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var task_list = JSON.parse( this.responseText );
        var downloadContent = document.getElementById( "taskContent" );

        downloadContent.innerHTML = task_list["taskContent"];

        startMonitoringTask()
    }

    xhr.send(body);
}

function getTaskStat() {
    var xhr = new XMLHttpRequest();

    var body = '';

    xhr.open("POST", 'get_tasks_stat.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var task_stat = JSON.parse( this.responseText );
        var taskStatHead = document.getElementById( "taskStatHead" );
        var taskStatBody = document.getElementById( "taskStatBody" );

        taskStatHead.innerHTML = task_stat["taskStatHead"];
        taskStatBody.innerHTML = task_stat["taskStatBody"];

        getTaskStat()
    }

    xhr.send(body);
}