#!/usr/bin/python3

import sys
import json
import cgi

sys.path.append("..")

from DataBase.DBController import DBController
from DataBase.Task import Task

form = cgi.FieldStorage()
task_type_filter = form.getvalue('task_type_filter')
task_id_filter = form.getvalue('task_id_filter')
state_filter = form.getvalue('state_filter')
is_duration_filter = form.getvalue('is_duration_filter')

if state_filter == 'null':
    state_filter = None

if is_duration_filter == 'null':
    is_duration_filter = None

if is_duration_filter == 'true':
    is_duration_filter = True

if is_duration_filter == 'false':
    is_duration_filter = False

controller = DBController()

if task_id_filter:
    task_list = [Task.get_by_id(controller, task_id_filter)]
else:
    task_list = Task.get_list(controller, 20, task_type_filter, state_filter, is_duration_filter)

task_content = ""

for task in task_list:
    task_content += "<tr>"

    task_content += "<td><a href='./task.py?id={}'>{}</a></td>".format(task.id(), task.id())
    task_content += "<td>{}</td>".format(task.type())
    task_content += "<td>{}</td>".format(task.state())

    params_content = """<ul class="list-group list-group-horizontal">"""
    params = task.get_params()

    for param in params:
        params_content += """<li class="list-group-item">{}: {}</li>""".format(param, params[param])

    params_content += "</ul>"

    task_content += "<td>{}</td>".format(params_content)

    res_content = """<ul class="list-group list-group-horizontal">"""
    res_list = task.get_res()

    for res in res_list:
        value = ""

        try:
            value = res_list[res]
        except:
            pass

        res_content += """<li class="list-group-item">{}: {}</li>""".format(res, value)

    res_content += "</ul>"

    task_content += "<td>{}</td>".format(res_content)

    if task.state() == 0:
        task_content += "<td>{}</td>".format(res_content)
    else:
        task_content += """<td style="vertical-align: middle">
                                <a onclick="restartTask({})" class="badge badge-pill badge-success">
                                    <h5>Перезапустить</h5>
                                </a>
                            </td>""".format(task.id())

    task_content += "</tr>"

print("Content-type: application/json")
print()
print(json.JSONEncoder().encode({"taskContent": task_content}))
