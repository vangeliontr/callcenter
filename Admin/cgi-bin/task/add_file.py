#!/usr/bin/python3

import sys
import os
import json
import traceback
import base64

sys.path.append("..")

pre_path = "../Files/"

try:

    content_len = int(os.environ["CONTENT_LENGTH"])

    #open("log", "a").write("cl {}\n".format(content_len))

    file = sys.stdin.read(content_len)

    #open("log", "a").write("file {}\n".format(len(file)))
    content_len = content_len - len(file)

    #open("log", "a").write("cl {}\n".format(content_len))

    while content_len > 0:
        part = sys.stdin.read(content_len)
        file += part
        content_len = content_len - len(part)

    #form = cgi.FieldStorage()
    #file = form.getvalue('file')

    #open("log", "a").write("file {}\n".format(file[:100]))

    file_name = file[:file.find(",")]
    file_content = file[file.find(",")+1:]


    try:
        file_content = base64.b64decode(file_content)
    except:
        pass

    open(pre_path + file_name, "wb").write(file_content)

except Exception as e:
    print("Content-type: application/json")
    print()
    print(json.JSONEncoder().encode({"error": str(e) + "\n" + traceback.format_exc()}))