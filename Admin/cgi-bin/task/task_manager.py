#!/usr/bin/env python3

import sys
sys.path.append("..")
from DataBase.DBController import DBController
from DataBase.TaskDescriptor import TaskDescriptor

print("Content-type: text/html")
print()

html_template = open("./cgi-bin/template.html", "r", encoding="utf-8").read()
html_template = html_template.replace("{{STYLE}}", """<link href="../../admin.css" rel="stylesheet">""")

task_manager_content = open("./cgi-bin/task/task_manager.html", "r", encoding="utf-8").read()

controller = DBController()
task_discriptor_list = TaskDescriptor.get_list(controller)
task_discriptor_content = ""

for task_discriptor in task_discriptor_list:
    task_discriptor_content += "<tr>"

    task_discriptor_content += """<td style="vertical-align: middle;"><h4>{}</h4></td>""".format(task_discriptor.name())

    params_content = """<ul class="list-group list-group-horizontal">"""
    params = task_discriptor.get_handle_params()

    for param in params:
        params_content += """<li class="list-group-item">{}: {}</li>""".format(param, params[param])

    params_content += "</ul>"

    task_discriptor_content += "<td>{}</td>".format(params_content)

    params_content = """<ul class="list-group list-group-horizontal">"""
    params = task_discriptor.get_params()

    for param in params:
        params_content += """<li class="list-group-item">{}: {}</li>""".format(param, params[param])

    params_content += "</ul>"

    task_discriptor_content += "<td>{}</td>".format(params_content)

    params_content = """<ul class="list-group list-group-horizontal">"""
    params = task_discriptor.get_task_params()

    for param in params:
        params_content += """<li class="list-group-item">{}: {}</li>""".format(params[param]["name"], params[param]["type"])

    params_content += "</ul>"

    task_discriptor_content += "<td>{}</td>".format(params_content)

    task_discriptor_content += """<td style="vertical-align: middle"><a href="./start_task.py?name={}" class="badge badge-pill badge-success"><h4>Запустить</h4></a></td>""".format(task_discriptor.name())

    task_discriptor_content += "</tr>"

task_manager_content = task_manager_content.replace("{{TASK_DESCRIPTOR_CONTENT}}", task_discriptor_content)

html_template = html_template.replace("{{CONTENT}}", task_manager_content)
html_template = html_template.replace("{{SCRIPT}}",
                                      """<script>window.jQuery || document.write('<script src="../../assets/js/"""
                                      """vendor/jquery.min.js"><\/script>')</script>\n"""
                                      """<script src="../../admin.js"></script>\n"""
                                      """<script src="task_manager.js"></script>\n"""
                                      """<script>startMonitoringTask()</script>\n"""
                                      """<script>SelectMenu('menu_task')</script>\n""")

print(html_template)
