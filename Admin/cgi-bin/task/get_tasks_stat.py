#!/usr/bin/python3

import sys
import json

sys.path.append("..")

from DataBase.DBController import DBController
from DataBase.Task import Task

controller = DBController()

task_stat_head = ""
task_stat_body = ""


stat_info = Task.get_stat(controller)

stat_struct = {}

for stat_row in stat_info:

    if stat_row["Type"] not in stat_struct:
        stat_struct[stat_row["Type"]] = {"Ожидает в очереди": 0, "Отложены": 0, "В работе": 0, "Завершены": 0}

    if stat_row["Suspended"] == 1:
        stat_struct[stat_row["Type"]]["Отложены"] += stat_row["cnt"]
    elif stat_row["State"] == 0:
        stat_struct[stat_row["Type"]]["Ожидает в очереди"] += stat_row["cnt"]
    elif stat_row["State"] == 1:
        stat_struct[stat_row["Type"]]["В работе"] += stat_row["cnt"]
    elif stat_row["State"] == 2:
        stat_struct[stat_row["Type"]]["Завершены"] += stat_row["cnt"]

task_stat_head = """<tr>
                        <th>Название</th>
                        <th>Ожидает в очереди</th>
                        <th>Отложены</th>
                        <th>В работе</th>
                        <th>Завершены</th>
                    </tr>"""

for task in stat_struct:
    task_stat_body += "<tr>"
    task_stat_body += "<td>{}</td>".format(task)
    task_stat_body += """<td><a href="#task_list" onclick="setFilter('{}', 'Ожидает в очереди')">{}</a></td>""".format(task, stat_struct[task]["Ожидает в очереди"])
    task_stat_body += """<td><a href="#task_list" onclick="setFilter('{}', 'Отложены')">{}</a></td>""".format(task, stat_struct[task]["Отложены"])
    task_stat_body += """<td><a href="#task_list" onclick="setFilter('{}', 'В работе')">{}</a></td>""".format(task, stat_struct[task]["В работе"])
    task_stat_body += """<td><a href="#task_list" onclick="setFilter('{}', 'Завершены')">{}</a></td>""".format(task, stat_struct[task]["Завершены"])
    task_stat_body += "</tr>"

print("Content-type: application/json")
print()
print(json.JSONEncoder().encode({"taskStatHead": task_stat_head, "taskStatBody": task_stat_body}))
