#!/usr/bin/python3

import sys
import cgi
import json
import traceback

sys.path.append("..")

from DataBase.DBController import DBController
from DataBase.TaskDescriptor import TaskDescriptor
from DataBase.Task import Task

try:
    form = cgi.FieldStorage()
    name = form.getvalue('name')

    controller = DBController()
    task_discriptor = TaskDescriptor.get_by_name(controller, name)

    task_params = task_discriptor.get_task_params()

    task_params_to_execute = {}

    for param in task_params:
        if task_params[param]["type"] == "Файл":
            value = form.getvalue(param)
            value = value.split("$")
            task_params_to_execute[param] = {"Имя": value[0], "Идентификатор": value[1]}
        else:
            task_params_to_execute[param] = form.getvalue(param)

    Task.set_task_impl(controller, name, json.dumps(task_params_to_execute))

    print("Content-type: application/json")
    print()
    print(json.JSONEncoder().encode({"res": "ok"}))
except Exception as e:
    print("Content-type: application/json")
    print()
    print(json.JSONEncoder().encode({"error": str(e) + "\n" + traceback.format_exc()}))