function restartTask(task_id) {
    var xhr = new XMLHttpRequest();

    var body = '';

    xhr.open("POST", 'restart_task.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        location.reload();
    }

    xhr.send("id="+task_id);
}

function stopTask(task_id) {
    var xhr = new XMLHttpRequest();

    var body = '';

    xhr.open("POST", 'stop_task.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        location.reload();
    }

    xhr.send("id="+task_id);
}

function delayTask(task_id, minutes) {
    var xhr = new XMLHttpRequest();

    var body = '';

    xhr.open("POST", 'delay_task.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        location.reload();
    }
    xhr.send("id="+task_id+"&minutes="+minutes);
}