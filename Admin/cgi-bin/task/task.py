#!/usr/bin/env python3

import cgi
import sys
sys.path.append("..")
from DataBase.DBController import DBController
from DataBase.Task import Task

form = cgi.FieldStorage()
task_id = form.getvalue('id')
controller = DBController()

task = Task.get_by_id(controller, task_id)

print("Content-type: text/html")
print()
html_template = open("./cgi-bin/card_template.html", "r", encoding="utf-8").read()
html_template = html_template.replace("{{STYLE}}", """<link href="../../admin.css" rel="stylesheet">""")

body = open("./cgi-bin/task/task.html", "r", encoding="utf-8").read()

body = body.replace("{{TaskID}}", str(task.id()))
body = body.replace("{{TaskType}}", str(task.type()))
body = body.replace("{{TaskState}}", str(task.state()))
body = body.replace("{{TaskDelay}}", task.suspend().strftime("%H:%M:%S %d.%m.%Y"))

params_content = ""
params = task.get_params()

for param in params:
    params_content += """<li class="list-group-item">{}: {}</li>""".format(param, params[param])

body = body.replace("{{Params}}", params_content)

res_content = ""
res_list = task.get_res()

for res in res_list:
    value = ""

    try:
        value = res_list[res]
    except:
        pass

    res_content += """<li class="list-group-item">{}: {}</li>""".format(res, value)

body = body.replace("{{Result}}", res_content)

html_template = html_template.replace("{{CONTENT}}", body)
html_template = html_template.replace("{{SCRIPT}}",
                                      """<script>window.jQuery || document.write('<script src="../../assets/js/"""
                                      """vendor/jquery.min.js"><\/script>')</script>"""
                                      """<script src="task.js"></script>\n"""
                                      """<script src="../admin.js"></script>""")

print(html_template)
