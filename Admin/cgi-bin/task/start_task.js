const uuid =()=>([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g,c=>(c^crypto.getRandomValues(new Uint8Array(1))[0]&15 >> c/4).toString(16));

load_count = 0

function loadFileToServer(file) {
   var reader = new FileReader();
   var id = uuid()
   res = null

   load_count += 1

   reader.onload = function () {
        res = reader.result;

        res = res.substring(res.indexOf("base64,")+7)

        res = id + "," + res

        var body = res;

        var xhr = new XMLHttpRequest();

        xhr.open("POST", 'add_file.py', true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

        var handler = function() {
            if( this.readyState != 4 )
                return;

            load_count -= 1
        }

        xhr.onreadystatechange = handler;
        xhr.send(encodeURI(body));
   };

   reader.onerror = function (error) {
        alert('Ошибка: ' + error);
   };

   reader.readAsDataURL(file);

   return id
}

function startTask( name, params ){

    var body = "" ;
    body += 'name=' + name;

    for( param in params) {
        var param_container = document.getElementById(param)
        var param_value = param_container.value;

        if(params[param]['type'] == "Текст" || params[param]['type'] == "Целое"){
            if( param_value === "" ) {
                alert("Параметр '" + params[param]['name'] + "' должен быть заполнен")
                return
            }

            body += "&" + param + "=" + param_value
        }

        if(params[param]['type'] == "Файл"){
            if( param_value === "" ) {
                alert("Параметр '" + params[param]['name'] + "' должен быть заполнен")
                return
            }

            id = loadFileToServer(param_container.files[0]);

            body += "&" + param + "=" + param_container.files[0].name + "$" + id
        }
    }

    var xhr = new XMLHttpRequest();

    xhr.open("POST", 'execute_task.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    var handler = function() {
        if( this.readyState != 4 )
            return;

        var res_json = this.responseText.substring( this.responseText.search("{") - 1)

        if( res_json != "" ) {
            var res = JSON.parse( res_json );

            if( res.error !== undefined ){
                alert("Ошибка при старте задачи:\n" + res.error)
                return
            }
        }

        window.location.href = "./task_manager.py";
    }

    xhr.onreadystatechange = handler;
    xhr.send(body);
}