
function getOrganizationList(){

    var name = document.getElementById( "NameInput" ).value;
    var inn = document.getElementById( "INNInput" ).value;
    var address = document.getElementById( "AddressInput" ).value;
    var startRegDate = document.getElementById( "startDayPicker" ).children[0].value;
    var endRegDate = document.getElementById( "endDayPicker" ).children[0].value;
    var sortRegDate = ""

    if($("#RegDateAscSort")[0].checked) sortRegDate = "ASC";
    if($("#RegDateDescSort")[0].checked) sortRegDate = "DESC";

    var phone = document.getElementById( "PhoneInput" ).value;
    var cp_family = document.getElementById( "FInput" ).value;
    var cp_name = document.getElementById( "IInput" ).value;
    var cp_surname = document.getElementById( "OInput" ).value;
    var closeFilter = ""

    if($("#ClosedHidden")[0].checked) closeFilter = "ClosedHidden";
    if($("#NotClosedHidden")[0].checked) closeFilter = "NotClosedHidden";
    if($("#ClosedMark")[0].checked) closeFilter = "ClosedMark";

    var odpFilter = ""

    if($("#TinkoffODP")[0].checked) odpFilter = "Тинькофф";

    var count = document.getElementById( "CountInput" ).value;
    var view = "monitor"

    if($("#ViewFile")[0].checked) view = "file";

    var download_id = document.getElementById( "DownloadId" ).value;
    var okved = document.getElementById( "OKVED" ).value;

    var body = "" ;
    body += 'name=' + name;
    body += '&inn=' + inn;
    body += '&address=' + address;
    body += '&startRegDate=' + startRegDate;
    body += '&endRegDate=' + endRegDate;
    body += '&sortRegDate=' + sortRegDate;
    body += '&phone=' + phone;
    body += '&cp_family=' + cp_family;
    body += '&cp_name=' + cp_name;
    body += '&cp_surname=' + cp_surname;
    body += '&closeFilter=' + closeFilter;
    body += '&odpFilter=' + odpFilter;
    body += '&count=' + count;
    body += '&view=' + view;
    body += '&download_id=' + download_id;
    body += '&okved=' + okved;

    var xhr = new XMLHttpRequest();

    xhr.open("POST", 'get_organization_list.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    var progress = document.getElementById( "progress" )
    progress.hidden = false
    progress.children[1].textContent="0%"
    progress.children[0].style.width="0%"

    var table = document.getElementById( "organization_content" )
    table.innerHTML = ""

    var count_head = document.getElementById( "count_head" );

    count_head.innerHTML = "Количество: 0"

    var head = document.getElementById( "resTableHead" )

    head.innerHTML = "<th>Тип</th><th>Название</th><th>ИНН</th><th>Дата регистрации</th><th>Адрес</th>" +
                     "<th>Телефон</th><th>Фамилия</th><th>Имя</th><th>Отчество</th>"

    if( closeFilter === "ClosedMark" )
        head.innerHTML += "<th>Актуально</th>"

    if( odpFilter !== "" )
        head.innerHTML += "<th>ОДП</th>"

    if( okved !== "" )
        head.innerHTML += "<th>ОКВЭД</th>"

    var handler = function() {
        if( this.readyState != 4 )
            return;

        var res_json = this.responseText.substring( this.responseText.search("{") - 1)
        var res = JSON.parse( res_json );

        if( res.error !== undefined ){
            alert("Ошибка при выполнении запроcа:\n" + res.error)
            return
        }

        var contact_list = res.contact_list;

        if( view == "monitor" ) {
            for( var i = 0; i < contact_list.length; i++ ) {
                var tr = document.createElement("tr");
                var row = contact_list[i];

                appenToTable(tr, row["Тип"])
                appenLinkToTable(tr, '../organization/organization.py?id=' + row["ID"], row["Название"])
                appenToTable(tr, row["ИНН"])
                appenToTable(tr, row["ДатаРегистрации"])
                appenToTable(tr, row["Адрес"])
                appenToTable(tr, row["Телефон"])
                appenToTable(tr, row["Фамилия"])
                appenToTable(tr, row["Имя"])
                appenToTable(tr, row["Отчество"])

                if( closeFilter === "ClosedMark" )
                    appenToTable(tr, row["Актуально"])

                if( odpFilter !== "" )
                    appenToTable(tr, row["ОДП"])

                if( okved !== "" )
                    appenToTable(tr, row["ОКВЭД"])

                table.appendChild(tr)
            }

            count_head.innerHTML = "Количество: " + res.res_cnt
        }

        if(res.is_complete) {

            if( view == "file") {
                var file_link = document.getElementById( "fileLink" )
                file_link.href = "../../" + res.file_id + ".csv"
                file_link.innerText="Скачать файл"
            }

            progress.hidden = true
            return
        }
        else {

            if(res.max){
                progress.children[1].textContent=res.res_cnt + " из " + res.max
                progress.children[0].style.width=(res.res_cnt/res.max*100) + "%"
            }

            var xhr = new XMLHttpRequest();

            xhr.open("POST", 'get_organization_list.py', true);
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.onreadystatechange = handler;
            request_data = body + "&offset=" + res.offset + "&max=" + res.max + "&res_cnt=" + res.res_cnt

            if(res.file_id !== undefined)
                request_data += "&file_id=" + res.file_id

            xhr.send(request_data);
        }
    }

    xhr.onreadystatechange = handler;
    xhr.send(body);
}

function SetView(view_name, raw_name){
    var view = document.getElementById(view_name)
    view.hidden = !view.hidden

    var raw = document.getElementById(raw_name)

    if(view.hidden)
        raw.src = "https://emojio.ru/images/apple-b/2b07.png"
    else
        raw.src = "https://emojio.ru/images/apple-b/2b06.png"
}