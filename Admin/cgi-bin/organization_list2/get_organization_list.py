#!/usr/bin/python3

import uuid
import time
import traceback
import requests
import json
import sys
sys.path.append("..")
from DataBase.DBController import DBController
from Bank.BankList import BankList
import cgi

try:
    form = cgi.FieldStorage()

    name = form.getvalue('name')
    inn = form.getvalue('inn')
    address = form.getvalue('address')
    startRegDate = form.getvalue('startRegDate')
    endRegDate = form.getvalue('endRegDate')
    sortRegDate = form.getvalue('sortRegDate')
    phone = form.getvalue('phone')
    cp_family = form.getvalue('cp_family')
    cp_name = form.getvalue('cp_name')
    cp_surname = form.getvalue('cp_surname')
    closeFilter = form.getvalue('closeFilter')
    odpFilter = form.getvalue('odpFilter')
    count = form.getvalue('count')
    view = form.getvalue('view')
    download_id = form.getvalue('download_id')
    okved = form.getvalue('okved')
    file_id = form.getvalue('file_id')
    offset = form.getvalue('offset')
    res_cnt = form.getvalue('res_cnt')
    max = form.getvalue('max')

    bank = None

    if odpFilter is not None and len(odpFilter) > 0:
        bank = BankList.get_by_name(odpFilter)

    res_file = None

    if view == 'file':
        if file_id is None:
            file_id = str(uuid.uuid4())

        res_file = open(file_id + ".csv", "a")

        if offset is None:
            title = "Название;ИНН;ДатаРегистрации;Адрес;Фамилия;Имя;Отчество;Телефон\n"

            if closeFilter == "ClosedMark":
                title = title.replace("\n", ";Актуален\n")

            if bank is not None:
                title = title.replace("\n", ";ОДП\n")

            res_file.write(title)

    LIMIT = 100

    controller = DBController()
    cur = controller.get_cursor()
    sql = """SELECT *, Organization.Name as OrgName, Organization.Type as OrgType
             FROM Organization 
             LEFT JOIN ContactPerson ON ContactPerson.ID = Organization.LastContactPerson
             LEFT JOIN ContactInfo ON ContactInfo.ID = Organization.LastPhoneInfo """

    where = []
    order = []

    if name is not None:
        where.append("Organization.Name like '%{}%'".format(name))

    if inn is not None:
        where.append("INN like '%{}%'".format(inn))

    if address is not None:
        where.append("Address like '%{}%'".format(address))

    if phone is not None:
        where.append("Value like '%{}%'".format(phone))

    if cp_surname is not None:
        where.append("MiddleName like '%{}%'".format(cp_surname))

    if cp_name is not None:
        where.append("ContactPerson.Name like '%{}%'".format(cp_name))

    if cp_family is not None:
        where.append("Surname like '%{}%'".format(cp_family))

    if startRegDate is not None:
       where.append("OpenDate >= '{}'".format(startRegDate[6:10] + "-" + startRegDate[3:5] + "-" + startRegDate[0:2]))

    if endRegDate is not None:
       where.append("OpenDate <= '{}'".format(endRegDate[6:10] + "-" + endRegDate[3:5] + "-" + endRegDate[0:2]))

    if sortRegDate is not None:
        order.append( "OpenDate " + sortRegDate )

    if download_id is not None:
        where.append("Download = {}".format(download_id))

    if okved is not None:
        where.append("OKVED like '{}%'".format(okved))

    if len(where) > 0:
       build = None

       for case in where:
          if build is None:
             build = case
          else:
             build += " AND " + case + " "

       sql = sql + " WHERE " + build

    if len(order) > 0:
        sql = sql + " ORDER BY"
        for case in order:
            sql += " " + case

    if count is not None:
        max = int(count)

    if max is None:
        cur.execute(sql.replace("SELECT *, Organization.Name as OrgName, Organization.Type as OrgType", "SELECT COUNT(*) as CNT"))
        cnt = cur.fetchall()
        max = cnt[0]["CNT"]
    else:
        max = int(max)

    sql += " LIMIT {}".format(LIMIT)

    if offset is not None:
        sql += " OFFSET {}".format(offset)
        offset = int(offset)
    else:
        offset = 0

    if res_cnt is not None:
        res_cnt = int(res_cnt)
    else:
        res_cnt = 0

    cur.execute(sql)
    org_list = cur.fetchall()

    bank_list = {}
    check_list = None

    if bank is not None:
        for org in org_list:
            bank_list[org["INN"]] = bank.get_score_id(org["INN"])

    if closeFilter is not None:

        query = None
        for org in org_list:

            if query:
                query += "%2C+" + org["INN"]
            else:
                query = org["INN"]

        is_success = False

        while not is_success:
            url_request = "https://egrul.nalog.ru/"

            r = requests.post(url_request, data="vyp3CaptchaToken=&query={}&region=&PreventChromeAutocomplete=".
                              format(query), headers={"Content-Type": "application/x-www-form-urlencoded"})

            res = json.loads(r.text)

            if "ERRORS" in res:
                print(r.text)
                time.sleep(30)
                continue

            is_success = True

        url_res = "https://egrul.nalog.ru/search-result/" + res["t"]

        status = "wait"

        while status == "wait":
            r = requests.get(url_res)

            res = json.loads(r.text)

            if "rows" in res:
                status = "ready"
                check_list = res["rows"]

    res = []

    for org in org_list:

        #f = ""
        #i = ""
        #o = ""

        #phone = ""

        #SQL = "SELECT * FROM ContactPerson_Organization " \
        #      "INNER JOIN ContactPerson ON ContactPerson.ID = ContactPerson_Organization.ContactPerson " \
        #      "WHERE Organization = {} ORDER BY ContactPerson.ID DESC;".format(org["ID"])
        #cur.execute(SQL)
        #contact_person_list = cur.fetchall()

        #if len(contact_person_list) > 0:
        #    f = contact_person_list[0]["Surname"]
        #    i = contact_person_list[0]["Name"]
        #    o = contact_person_list[0]["MiddleName"]

        #    SQL = "SELECT * FROM ContactInfo WHERE ContactPerson = {} AND Type = 'Телефон' ORDER BY ID DESC;".format(
        #        contact_person_list[0]["ID"])
        #    cur.execute(SQL)
        #    contact_info_list = cur.fetchall()

        #    if len(contact_info_list) > 0:
        #        phone = contact_info_list[0]["Value"]

        openDate = str(org["OpenDate"])
        openDate = openDate[8:10] + "." + openDate[5:7] + "." + openDate[0:4]

        org_data= {"ID": org["ID"],
                   "Тип": org["OrgType"],
                   "Название": org["OrgName"],
                   "ИНН": org["INN"],
                   "ДатаРегистрации": openDate,
                   "Адрес": org["Address"],
                   "Фамилия": org["Surname"],
                   "Имя": org["Name"],
                   "Отчество": org["MiddleName"],
                   "Телефон": org["Value"]}

        if closeFilter is not None:
            is_close = False

            for row in check_list:
                if row["r"] == openDate and row["i"] == org["INN"]:
                    if "e" in row:
                        is_close = True

            if is_close and closeFilter == 'ClosedHidden':
                if count is None:
                    max = max - 1
                continue

            if not is_close and closeFilter == 'NotClosedHidden':
                if count is None:
                    max = max - 1
                continue

            org_data["Актуально"] = "Нет" if is_close else "Да"

        if bank is not None:
            org_data["ОДП"] = bank.get_odp_status(bank_list[org["INN"]])

        if okved is not None:
            org_data["ОКВЭД"] = org["OKVED"]

        res.append(org_data)

        if view == 'file':
            data_to_file = str(org_data["Название"]) + ";" + str(org_data["ИНН"]) + ";" + str(org_data["ДатаРегистрации"]) + ";" + str(org_data["Адрес"]) + ";" \
                           + str(org_data["Фамилия"]) + ";" + str(org_data["Имя"]) + ";" + str(org_data["Отчество"]) + ";" \
                           + str(org_data["Телефон"]) + "\n"

            if closeFilter == "ClosedMark":
                data_to_file = data_to_file.replace("\n",  ";" + str(org_data["Актуально"]) + "\n")

            if bank is not None:
                data_to_file = data_to_file.replace("\n", ";" + str(org_data["ОДП"]) + "\n")

            if okved is not None:
                data_to_file = data_to_file.replace("\n", ";" + str(org_data["ОКВЭД"]) + "\n")

            res_file.write(data_to_file)

        if (res_cnt + len(res)) >= max:
            break

    data_to_send = {"contact_list": res,
                    "is_complete": (len(org_list) < LIMIT) or (res_cnt + len(res)) >= max,
                    "offset": offset + LIMIT,
                    "max": max,
                    "res_cnt": res_cnt + len(res)}

    if file_id is not None:
        data_to_send["file_id"] = file_id

    print("Content-type: application/json")
    print()
    print(json.JSONEncoder().encode(data_to_send))
except Exception as e:
    print("Content-type: application/json")
    print()
    print(json.JSONEncoder().encode({"error": str(e) + "\n" + traceback.format_exc()}))
