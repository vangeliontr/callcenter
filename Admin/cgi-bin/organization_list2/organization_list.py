#!/usr/bin/env python3

import sys
sys.path.append("..")
from DataBase.DBController import DBController
import cgi

form = cgi.FieldStorage()
downloadId = form.getvalue('downloadId')
downloadName = form.getvalue('downloadName')
operation = form.getvalue('operation')
operationName = form.getvalue('operationName')
valid = form.getvalue('valid')
odp = form.getvalue('odp')
Status = form.getvalue('Status')

print("Content-type: text/html")
print()

html_template = open("./cgi-bin/template.html", "r", encoding="utf-8").read()
html_template = html_template.replace("{{STYLE}}", """<link href="../../admin.css" rel="stylesheet">""")

organization_content = open("./cgi-bin/organization_list2/organization_list.html", "r", encoding="utf-8").read()

download_list_content = ""

controller = DBController()
cur = controller.get_cursor()

cur.execute("""SELECT * FROM Download ORDER BY ID DESC;""" )
download_list = cur.fetchall()
download_list_content += """<li role="presentation"><a role="menuitem" tabindex="-1" onclick="setDownload( null, 'Любая' )" href="#">Любая</a></li>"""

for download in download_list:
   download_list_content += """<li role="presentation"><a role="menuitem" tabindex="-1" onclick="setDownload( """  + str(download["ID"]) + ", '" + str(download["ID"])  + """' )" href="#">""" + str(download["ID"])  + """</a></li>"""

organization_content = organization_content.replace("{{DOWNLOAD_LIST}}", download_list_content)

script_on_run = ""

if downloadId:
   script_on_run += "setDownload({},'{}');".format(downloadId, downloadName)

if Status:
   script_on_run += "setStatus('{}');".format(Status)

if odp:
   script_on_run += "setOdpCheck();"

if operation:
   script_on_run += "setOperation('{}','{}');".format(operation, operationName)

if valid:
   script_on_run += "setValid('{}');".format(valid)

if script_on_run != "":
    script_on_run = "<script>" + script_on_run + "getOrganizationList();</script>\n"


html_template = html_template.replace("{{CONTENT}}", organization_content)
html_template = html_template.replace("{{SCRIPT}}",
                                      """<script>window.jQuery || document.write('<script src="../../assets/js/"""
                                      """vendor/jquery.min.js"><\/script>')</script>\n"""
                                      """<script src="../../admin.js"></script>\n"""
                                      """<script src="organization_list.js"></script>\n"""
                                      """<script>SelectMenu('menu_organization2')</script>\n""" +
                                      script_on_run)

print(html_template)
