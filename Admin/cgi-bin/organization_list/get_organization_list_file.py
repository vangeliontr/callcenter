#!/usr/bin/python3

import cgi
import sys
from shutil import copyfileobj
from openpyxl import Workbook

sys.path.append("..")

from DataBase.DBController import DBController


form = cgi.FieldStorage()
download = form.getvalue('dl')
project = form.getvalue('project')
upload_id = form.getvalue('upload_id')
status = form.getvalue('status')
load_state = form.getvalue('load_state')
have_phone = form.getvalue('have_phone')


def get_file():

    res_struct = {
        "Название": "OrgName",
        "ИНН": "INN",
        "Имя": "Name",
        "Фамилия": "Surname",
        "Отчество": "MiddleName",
        "Телефон": "Value"
    }

    sql = """SELECT
             {}
             FROM
             Organization
             LEFT JOIN ContactPerson ON ContactPerson.ID = Organization.LastContactPerson
             LEFT JOIN ContactInfo ON ContactInfo.ID = Organization.LastPhoneInfo
             {}
             WHERE {}
             """

    WHERE = []
    JOIN = []
    FIELDS = ["Organization.`Name` as OrgName",
              "Organization.INN",
              "ContactPerson.`Name`",
              "ContactPerson.Surname",
              "ContactPerson.MiddleName",
              "ContactInfo.`Value`"]

    if download and not load_state:
        WHERE.append("Organization.Download = {}".format(download))

    if project:
        JOIN.append("""INNER JOIN Call_Organization ON Call_Organization.Organization = Organization.ID
                                         INNER JOIN Call_ ON Call_Organization.Call_ = Call_.ID""")

        WHERE.append("Call_.Project = {}".format(project))

        if status:
            WHERE.append("Call_.Result = '{}'".format(status))

    if load_state:
        JOIN.append("""INNER JOIN Download_Organization ON Download_Organization.Organization = Organization.ID""")

        WHERE.append("Download_Organization.Operation = '{}'".format(load_state))

        WHERE.append("Download_Organization.Download = {}".format(download))

        FIELDS.append("Organization.Download")

        res_struct["Загрузка"] = "Download"

    if upload_id:
        JOIN.append("""INNER JOIN Upload_Organization ON Upload_Organization.Organization = Organization.ID""")

        WHERE.append("Upload_Organization.Upload = {}".format(upload_id))

        if status:
            JOIN.append("""INNER JOIN Call_ ON Call_.ID = Upload_Organization.`Call`""")
            WHERE.append("Call_.Result = '{}'".format(status))

            FIELDS.append("Call_.ExtID")
            res_struct["Идентификатор скорозвона"] = "ExtID"

    if have_phone == "True":
        WHERE.append("ContactInfo.`Value` is not NULL".format(project))

    sql = sql.format(",\n".join(FIELDS), "\n".join(JOIN), " AND ".join(WHERE))

    controller = DBController()
    cur = controller.get_cursor()

    cur.execute(sql)

    wb = Workbook()
    ws = wb.active
    ws.title = "Список"

    col = 0

    for elem in res_struct:

        col += 1

        ws.cell(row=1, column=col, value=elem)

    row = 1

    org_list = cur.fetchall()

    for org in org_list:

        row += 1
        col = 0

        for elem in res_struct:
            col += 1

            ws.cell(row=row, column=col, value=str(org[res_struct[elem]]))

    wb.save('temp.xlsx')

    name = "unnamed"

    if download:
        name = str(download)

    if upload_id:
        name = str(upload_id)

    print("""Content-Type: application/vnd.ms-excel;base64;""")
    print("""Content-Disposition: attachment; filename = {}""".format(name + ".xlsx"))
    print("""Content-Transfer-Encoding: UTF-8""")
    print("""Content-Encoding: UTF-8""")
    print()
    #print(res.encode("cp866"))
    copyfileobj(open("temp.xlsx", "rb"), sys.stdout.buffer)
    #print(base64.b64encode(file.read()).decode())
    #sys.stdout.flush()

    #bstdout = open(sys.stdout.fileno(), 'wb', closefd=False)
    #bstdout.write(file.read())
    #bstdout.flush()


get_file()
