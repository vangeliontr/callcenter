#!/usr/bin/python3

import json
import sys
sys.path.append("..")
from DataBase.DBController import DBController
import cgi

form = cgi.FieldStorage()
downloadId = form.getvalue('downloadId')
valid = form.getvalue('valid')
operation = form.getvalue('operation')
odpCheck = form.getvalue('odpCheck')
firstNumberCheck = form.getvalue('firstNumberCheck')
status = form.getvalue('status')

controller = DBController()
cur = controller.get_cursor()
sql = """SELECT
Organization.ID,
Organization.Type,
Organization.Name,
Organization.INN,
DATE_FORMAT( Organization.OpenDate, "%d.%m" ) as OpenDate,
Organization.Address,
Organization.TimeZone
FROM
Organization """

where = ""
join = ""

if downloadId and downloadId != 'null':
   join += "INNER JOIN Download_Organization ON Download_Organization.Organization = Organization.ID"

   where += " Download_Organization.Download = " + downloadId

   if operation is not None and operation != 'null':
      where += " AND Download_Organization.Operation = '{}' ".format(operation)

   if valid is not None and valid != 'null':
      where += " AND Download_Organization.PhoneValidate = '{}' ".format(valid)

   if odpCheck is not None:
      where += " AND Download_Organization.ODP = 'Да' "

   if firstNumberCheck is not None:
      where += " AND Download_Organization.Operation = '{}'".format(operation)

if status and status != 'all':
   status_list = status.split(',')
   join += " INNER JOIN Call_Organization ON Organization.ID = Call_Organization.Organization "
   join += " INNER JOIN Call_ ON Call_.ID = Call_Organization.Call_ "
   if len( where ) != 0: where += " AND "
   where += " Call_.Result in ("

   is_first = True
   for s in status_list:
      if not is_first:
         where += ','

      where += "'" + s + "'"
      is_first = False
   where += ")"

sql = sql + " " + join + " WHERE " + where

cur.execute(sql)
res = cur.fetchall()

print("Content-type: application/json")
print()
print(json.JSONEncoder().encode(res))