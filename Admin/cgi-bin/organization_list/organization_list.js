
function setOdpCheck(){
    var odpCheck = document.getElementById( "odpCheck" );
    odpCheck.checked = true
}

function getOrganizationList(){
    var downloadList = document.getElementById( "downloadList" );
    var downloadId = downloadList.getAttribute("downloadId");

    var operationList = document.getElementById( "operationList" );
    var operationId = operationList.getAttribute("operationId");

    var validList = document.getElementById( "validList" );
    var validId = validList.getAttribute("validId");

    var odpCheck = document.getElementById( "odpCheck" );
    var firstNumberCheck = document.getElementById( "firstNumberCheck" );

    var xhr = new XMLHttpRequest();

    var body = 'downloadId=' + downloadId;

    if($("#StateData")[0].checked)
        body += "&status=" + $("#StateDataInput")[0].value;

    body += "&operation=" + operationId
    body += "&valid=" + validId

    if( odpCheck.checked )
        body += "&odpCheck=Да"

    if( firstNumberCheck.checked )
        body += "&firstNumberCheck=Да"

    xhr.open("POST", 'get_organization_list.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res_json = this.responseText.substring( this.responseText.search("{") - 1)
        var contact_list = JSON.parse( res_json );
        var table = document.getElementById( "organization_content" )
        table.innerHTML = ""

        for( var i = 0; i < contact_list.length; i++ ) {
            var tr = document.createElement("tr");
            var row = contact_list[i];

            appenToTable(tr, row["Type"])
            appenLinkToTable(tr, '../organization/organization.py?id=' + row["ID"], row["Name"])
            appenToTable(tr, row["INN"])
            appenToTable(tr, row["OpenDate"])
            appenToTable(tr, row["Address"])
            appenToTable(tr, row["TimeZone"])

            table.appendChild(tr)
        }

        var count_head = document.getElementById( "count_head" );

        count_head.innerHTML = "Количество: " + contact_list.length
    }

    xhr.send(body);
}

function setStatus(status) {
    $("#StateData").click();
    $("#StateDataInput")[0].value  = status
}

function setDownload(id,name){
    var downloadList = document.getElementById( "downloadList" )
    downloadList.innerHTML = name
    downloadList.setAttribute("downloadId", id)
}

function setOperation(id,name){
    var operationList = document.getElementById( "operationList" )
    operationList.innerHTML = name
    operationList.setAttribute("operationId", id)
}

function setValid(name){
    var validList = document.getElementById( "validList" )
    validList.innerHTML = name
    validList.setAttribute("validId", name)
}