#!/usr/bin/python3

import json
import sys
import cgi
sys.path.append("..")
from DataBase.DBController import DBController
from DataBase.Task import Task

form = cgi.FieldStorage()
start_date = form.getvalue('start_date')
end_date = form.getvalue('end_date')

controller = DBController()

start_date = start_date[6:] + "-" + start_date[3:5] + "-" + start_date[:2] + " 00:00:00+03:00"
end_date = end_date[6:] + "-" + end_date[3:5] + "-" + end_date[:2] + " 23:59:59+03:00"


def get_simple_safe(param):
    try:
        return int(param)
    except:
        pass

    return 0


def get_safe(params, name):

    if name not in params:
        return ""

    return params[name]


def get_safe_num(params, name):

    if name not in params:
        return 0

    try:
        return int(params[name])
    except:
        return 0


def get_task_id(task_str):

    task_str = str(task_str)

    if task_str.find(">") != -1:
        task_str = task_str[task_str.find(">"):]

    return ''.join([i for i in task_str if i.isdigit()])


def get_uploads_task(controller, task_list_str):

    if task_list_str == 0:
        return [None, None, None, None, None]

    task_list = task_list_str.split(",")

    if len(task_list) < 5:
        return [None, None, None, None, None]

    res = []

    for task_str in task_list:
        task_id = get_task_id(task_str)

        res.append(Task.get_by_id(controller, task_id))

    return res


def create_download_stat():
    task_list = Task.get_by_type(controller, "Загрузка файла в БД", start_date, end_date)

    res = ""

    sum = {
        "id": "Итого",
        "date": "",
        "in_file": 0,
        "not_uniq": 0,
        "double": 0,
        "to_load": 0,
        "to_reach": 0,
        "tf_odp": 0,
        "tf_loaded": 0,
        "sber_odp": 0,
        "sber_loaded": 0,
        "vtb_odp":0,
        "vtb_loaded": 0,
        "mts_odp": 0,
        "mts_loaded": 0,
        "tfk_odp": 0,
        "tfk_opd_loaded": 0,
        "reach_from_db": 0,
        "not_rf_pass":0,
        "t_in_odp": 0,
        "reach": 0,
        "tf_reach_odp": 0,
        "tf_reach_loaded": 0,
        "sber_reach_odp": 0,
        "sber_reach_loaded": 0,
        "vtb_reach_odp": 0,
        "vtb_reach_loaded": 0,
        "mts_reach_odp": 0,
        "mts_reach_loaded": 0,
        "tfk_reach_odp": 0,
        "tfk_reach_opd_loaded": 0
    }

    for task in task_list:
        task_res = task.get_res()

        if "Обрабатывается" not in task_res:
            continue

        res += "<tr>"

        res += """<td><a href="../task/task.py?id={}">{}</a></td>""".format(task.id(), task.id())
        res += "<td>{}</td>".format(str(task.suspend()))

        in_file = get_simple_safe(task_res["Обрабатывается"][7:task_res["Обрабатывается"].find(" из")])
        res += "<td>{}</td>".format(in_file)
        sum["in_file"] += in_file

        not_uniq = get_safe_num(task_res, "Не уникальных")
        res += "<td>{}</td>".format(not_uniq)
        sum["not_uniq"] += not_uniq

        double = get_safe_num(task_res, "Дублей")
        res += "<td>{}</td>".format(double)
        sum["double"] += double

        to_load = get_safe_num(task_res, "На подгрузку")
        res += "<td>{}</td>".format(to_load)
        sum["to_load"] += to_load

        to_reach = get_safe_num(task_res, "На обогащение")
        res += "<td>{}</td>".format(to_reach)
        sum["to_reach"] += to_reach

        uploads_task_list = get_uploads_task(controller, get_safe(task_res, "Задачи подгрузки"))

        for uploads_task in uploads_task_list:

            if uploads_task is None:
                res += "<td></td><td></td>"
                continue

            uploads_task_res = uploads_task.get_res()

            res += "<td>{}</td>".format(get_safe_num(uploads_task_res, "Осталось контактов") -
                                        get_safe_num(uploads_task_res, "Подгружено в скорозвон"))
            res += "<td>{}</td>".format(get_safe_num(uploads_task_res, "Подгружено в скорозвон"))

        rich_task_id = get_task_id(get_safe(task_res, "Задача обогащения"))

        if rich_task_id == "":
            continue

        rich_task = Task.get_by_id(controller, rich_task_id)

        rich_task_res = rich_task.get_res()

        reach_from_db = get_safe_num(rich_task_res, "Обогащено из БД")
        res += "<td>{}</td>".format(reach_from_db)
        sum["reach_from_db"] += reach_from_db

        not_rf_pass = get_safe_num(rich_task_res, "Контактов не с паспортом РФ")
        res += "<td>{}</td>".format(not_rf_pass)
        sum["not_rf_pass"] += not_rf_pass

        t_in_odp = get_safe_num(rich_task_res, "В ОДП более чем в 2х банках")
        res += "<td>{}</td>".format(t_in_odp)
        sum["t_in_odp"] += t_in_odp

        reach = get_safe_num(rich_task_res, "Обогащено")
        res += """<td><a href="{}?dl={}&load_state={}&have_phone={}">{}</a></td>""".\
            format("../organization_list/get_organization_list_file.py", get_safe(task_res, "Номер загрузки"),
                   "Обогащение", "True", reach)
        sum["reach"] += reach

        uploads_task_list = get_uploads_task(controller, get_safe(rich_task_res, "Задачи подгрузки"))

        for uploads_task in uploads_task_list:

            if uploads_task is None:
                res += "<td></td><td></td>"
                continue

            uploads_task_res = uploads_task.get_res()

            res += "<td>{}</td>".format(get_safe_num(uploads_task_res, "Осталось контактов") -
                                        get_safe_num(uploads_task_res, "Подгружено в скорозвон"))
            res += "<td>{}</td>".format(get_safe_num(uploads_task_res, "Подгружено в скорозвон"))

        res += "</tr>"

        res + "<tr>"

    for elem in sum:
        res += f"<td>{sum[elem]}</td>"

    res + "</tr>"

    return {"downloadContent": res}


print("Content-type: application/json")
print()
print(json.JSONEncoder().encode(create_download_stat()))