#!/usr/bin/env python3

import sys
sys.path.append("..")
from DataBase.DBController import DBController

print("Content-type: text/html")
print()
html_template = open("./cgi-bin/template.html", "r", encoding="utf-8").read()
html_template = html_template.replace("{{STYLE}}", """<link href="../../admin.css" rel="stylesheet">""")
html_template = html_template.replace("{{SCRIPT}}", """{{SCRIPT}}"""
                                      """<script>window.jQuery || document.write('<script src="../../assets/js/"""
                                      """vendor/jquery.min.js"><\/script>')</script>\n"""
                                      """<script src="../../admin.js"></script>\n"""
                                      """<script src="rich_download_list.js"></script>\n"""
                                      """<script>startMonitoringRichDownload()</script>\n"""
                                      """<script>SelectMenu('menu_rich_download')</script>\n""")

download_content = open("./cgi-bin/rich_download/rich_download_list.html", "r", encoding="utf-8").read()

html_template = html_template.replace("{{CONTENT}}", download_content)
print(html_template)
