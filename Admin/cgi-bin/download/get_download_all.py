#!/usr/bin/env python3

import cgi
import sys
sys.path.append("..")
from DataBase.DBController import DBController
from Bank.BankList import BankList
from DataBase.Project import Project

form = cgi.FieldStorage()
id = form.getvalue('id')

controller = DBController()
cur = controller.get_cursor()

print("Content-type: text/html")
print()
html_template = open("./cgi-bin/card_template.html", "r", encoding="utf-8").read()
html_template = html_template.replace("{{STYLE}}", """<link href="../../admin.css" rel="stylesheet">""")

body = open("./cgi-bin/download/download.html", "r", encoding="utf-8").read()

body = body.replace("{{ID}}", id)

errors = ""

cur.execute("SELECT * FROM DownloadError WHERE Download = {}".format(id))
error_rec_list = cur.fetchall()

for error_rec in error_rec_list:
   errors += "<tr><td>{}</td></tr>".format(error_rec["Text"])

body = body.replace("{{ERROR_COUNT}}", str(len(error_rec_list)))
body = body.replace("{{ERRORS}}", errors)

bank_list_content = ""
bank_list = BankList.get()

for bank in bank_list:

    bank_content = """<table>
        <tr>
            <td width="30px">
                <img id="{}ListRaw" style="margin-bottom: 10px;" width="70%" 
                     src="https://emojio.ru/images/apple-b/2b07.png" onclick="SetView('{}List','{}ListRaw')"/>
            </td>
            <td>
                <h3 class="sub-header">Загружено в {}: {{COUNT}}</h3>
            </td>
        </tr>
    </table>
    <div id="{}List" hidden="true" class="table-responsive">
        <table class="table table-striped table-hover">
            <thead class="thead-inverse">
                <tr>
                    <th>ID</th>
                    <th>ИНН</th>
                    <th>Название</th>
                    <th>ОГРН</th>
                    <th>Адрес</th>
                    <th>Регион</th>
                </tr>
            </thead>
            <tbody>
                {{LIST}}
            </tbody>
        </table>
    </div>""".format(bank.id(), bank.id(), bank.id(), bank.name(), bank.id())

    cur.execute("""SELECT
                   Organization.ID,
                   Organization.`Name`,
                   Organization.INN,
                   Organization.Address,
                   Organization.OGRN,
                   Organization_Region.`Comment`,
                   Region.Number,
                   Region.`Name` as RegionName
                   FROM
                   Organization
                   INNER JOIN Call_Organization ON Call_Organization.Organization = Organization.ID
                   INNER JOIN Call_ ON Call_Organization.Call_ = Call_.ID
                   LEFT JOIN Organization_Region ON Organization.ID = Organization_Region.Organization
                   LEFT JOIN Region ON Region.ID = Organization_Region.Region
                   WHERE Organization.Download = {} AND Call_.Bank = {} AND Project is NULL
                """.format(id, bank.id()))

    org_rec_list = cur.fetchall()

    table_content = ""

    for org in org_rec_list:
        table_content += "<tr>"
        table_content += "<td>{}</td>".format(org["ID"])
        table_content += "<td>{}</td>".format(org["INN"])
        table_content += "<td>{}</td>".format(org["Name"])
        table_content += "<td>{}</td>".format(org["OGRN"])
        table_content += "<td>{}</td>".format(org["Address"])
        table_content += "<td>По {}: {} {}</td>".format(org["Comment"], org["Number"], org["RegionName"])
        table_content += "</tr>"

    bank_content = bank_content.replace("{COUNT}", str(len(org_rec_list)))
    bank_content = bank_content.replace("{LIST}", table_content)

    bank_list_content += bank_content

body = body.replace("{{BANK_LIST}}", bank_list_content)

project_list_content = ""
project_list = Project.list_in_work()

for project in project_list:

    project_content = """<table>
            <tr>
                <td width="30px">
                    <img id="p{}ListRaw" style="margin-bottom: 10px;" width="70%" 
                         src="https://emojio.ru/images/apple-b/2b07.png" onclick="SetView('p{}List','p{}ListRaw')"/>
                </td>
                <td>
                    <h3 class="sub-header">Загружено в {}: {{COUNT}}</h3>
                </td>
            </tr>
        </table>
        <div id="p{}List" hidden="true" class="table-responsive">
            <table class="table table-striped table-hover">
                <thead class="thead-inverse">
                    <tr>
                        <th>ID</th>
                        <th>ИНН</th>
                        <th>Название</th>
                        <th>ОГРН</th>
                        <th>Адрес</th>
                        <th>Регион</th>
                    </tr>
                </thead>
                <tbody>
                    {{LIST}}
                </tbody>
            </table>
        </div>""".format(project.id(), project.id(), project.id(), project.name(), project.id())

    cur.execute("""SELECT
                       Organization.ID,
                       Organization.`Name`,
                       Organization.INN,
                       Organization.Address,
                       Organization.OGRN,
                       Organization_Region.`Comment`,
                       Region.Number,
                       Region.`Name` as RegionName
                       FROM
                       Organization
                       INNER JOIN Call_Organization ON Call_Organization.Organization = Organization.ID
                       INNER JOIN Call_ ON Call_Organization.Call_ = Call_.ID
                       LEFT JOIN Organization_Region ON Organization.ID = Organization_Region.Organization
                       LEFT JOIN Region ON Region.ID = Organization_Region.Region
                       WHERE Organization.Download = {} AND Call_.Project = {}
                    """.format(id, project.id()))

    org_rec_list = cur.fetchall()

    table_content = ""

    for org in org_rec_list:
        table_content += "<tr>"
        table_content += "<td>{}</td>".format(org["ID"])
        table_content += "<td>{}</td>".format(org["INN"])
        table_content += "<td>{}</td>".format(org["Name"])
        table_content += "<td>{}</td>".format(org["OGRN"])
        table_content += "<td>{}</td>".format(org["Address"])
        table_content += "<td>По {}: {} {}</td>".format(org["Comment"], org["Number"], org["RegionName"])
        table_content += "</tr>"

    project_content = project_content.replace("{COUNT}", str(len(org_rec_list)))
    project_content = project_content.replace("{LIST}", table_content)

    project_list_content += project_content

body = body.replace("{{PROJECT_LIST}}", project_list_content)

cur.execute(""" SELECT
                Organization.`Name`,
                Organization.ID,
                Organization.INN,
                Organization.Address,
                Organization.OGRN,
                Organization.Download,
                Download_Organization.Operation,
                Download_Organization.PhoneValidate,
                Download_Organization_Bank.ODP,
                Download_Organization_Bank.AllowAddress,
                Bank.`Name` as BankName,
                Organization_Region.`Comment`,
                Region.Number,
                Region.`Name` as RegionName
                FROM
                Download_Organization
                INNER JOIN Organization ON Download_Organization.Organization = Organization.ID
                INNER JOIN Download_Organization_Bank ON Download_Organization_Bank.Download_Organization = Download_Organization.ID
                INNER JOIN Bank ON Download_Organization_Bank.Bank = Bank.ID
                LEFT JOIN Organization_Region ON Organization.ID = Organization_Region.Organization
                LEFT JOIN Region ON Region.ID = Organization_Region.Region
                WHERE
                Download_Organization.Download = {}
                """.format(id))

all_org_raw_list = cur.fetchall()

org_list = {}

for raw_org_data in all_org_raw_list:

    id = raw_org_data["ID"]

    if id not in org_list:
        org_list[id] = {}

        org_list[id]["ИНН"] = raw_org_data["INN"]
        org_list[id]["Называние"] = raw_org_data["Name"]
        org_list[id]["ОГРН"] = raw_org_data["OGRN"]
        org_list[id]["Адрес"] = raw_org_data["Address"]
        org_list[id]["Регион"] = "По {}: {} {}</td>".format(raw_org_data["Comment"],
                                                            raw_org_data["Number"],
                                                            raw_org_data["RegionName"])

        created = "Уже был в загрузке {}".format(raw_org_data["Download"])

        if raw_org_data["Operation"] == 'Создано':
            created = "Новый контакт"

        valid = "Не валидный телефон"

        if raw_org_data["PhoneValidate"] == 'Валидный':
            valid = "Правильный телефон"

        org_list[id]["Комментарий"] = "{}<br>{}".format(created, valid)

    odp = "Не в ОДП"

    if raw_org_data["ODP"] == 'Да':
        odp = "В ОДП"

    address = "Не подходит по адресу"

    if raw_org_data["AllowAddress"] == 'Да':
        address = "Подходит по адресу"

    bank_state = "<br>{}: {}, {}".format(raw_org_data["BankName"], odp, address)

    org_list[id]["Комментарий"] += bank_state


all_content = ""

for org_id in org_list:
    all_content += """
        <tr>
            <td>{}</td>
            <td>{}</td>
            <td>{}</td>
            <td>{}</td>
            <td>{}</td>
            <td>{}</td>
            <td>{}</td>
        </tr>
    """.format(
        org_id,
        org_list[org_id]["ИНН"],
        org_list[org_id]["Называние"],
        org_list[org_id]["ОГРН"],
        org_list[org_id]["Адрес"],
        org_list[org_id]["Регион"],
        org_list[org_id]["Комментарий"]
    )

body = body.replace("{{ALL_COUNT}}", str(len(org_list)))
body = body.replace("{{ALL_LIST}}", all_content)

html_template = html_template.replace("{{CONTENT}}", body)
html_template = html_template.replace("{{SCRIPT}}",
                                      """<script>window.jQuery || document.write('<script src="../../assets/js/"""
                                      """vendor/jquery.min.js"><\/script>')</script>"""
                                      """<script src="../admin.js"></script>"""
                                      """<script src="./download.js"></script>""")

print(html_template)
