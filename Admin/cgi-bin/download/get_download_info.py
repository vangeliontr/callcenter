#!/usr/bin/python3

import json
import sys
import cgi
sys.path.append("..")
from DataBase.DBController import DBController

controller = DBController()
cur = controller.get_cursor()


def create_download_stat():

    form = cgi.FieldStorage()

    start_date = form.getvalue('start_date')
    end_date = form.getvalue('end_date')

    res = {}

    def get_table_val(val):
        if val is None:
            return ""
        else:
            return str(val)

    start_date = start_date[6:] + "-" + start_date[3:5] + "-" + start_date[:2] + " 00:00:00+03:00"
    end_date = end_date[6:] + "-" + end_date[3:5] + "-" + end_date[:2] + " 23:59:59+03:00"

    download_content = ""

    cur.execute("""SELECT * FROM Download 
                   WHERE ReceiveDateTime >= '{}' AND ReceiveDateTime <= '{}'
                   ORDER BY ID DESC""".format(start_date, end_date))
    download_list = cur.fetchall()

    #Пример <td><a href="organization/organization.py?downloadId={}&downloadName={}">

    for download in download_list:

        sql = """SELECT * FROM DownloadFile WHERE Download = {};""".format(str(download["ID"]))
        cur.execute(sql)
        download_file_list = cur.fetchall()

        download_content += "<tr>"
        download_content += "<td>"

        for download_file in download_file_list:
            download_content += """<a href="./get_download_file.py?id={}&name={}" class="badge badge-pill badge-primary">Скачать</a>""".format(download_file["ID"], download_file["Name"])

        download_content += """<a onclick="restartDownload('{}')" class="badge badge-pill badge-primary"">Перезапустить</a>""".format(str(download["ID"]))

        download_content += "</td>"

        download_content += """<td><a href="./download.py?id={}">{}</a></td>""".format(get_table_val(download["ID"]),
                                                                              get_table_val(download["ID"]))
        download_content += "<td>" + get_table_val( download["Source"] ) + "</td>"
        download_content += "<td>" + get_table_val( download["ReceiveDateTime"] ) + "</td>"
        download_content += "<td>" + get_table_val(download["Type"]) + "</td>"
        download_content += "<td>" + get_table_val(download["RegDate"]) + "</td>"
        download_content += "<td>" + get_table_val( download["State"] ) + "</td>"
        download_content += "<td>" + get_table_val(download["Comment"]) + "</td>"

        info = {
           "count": 0,
           "odp": 0,
           "uniq":{
              "count": 0,
              "valid": 0,
              "invalid": 0,
              "empty": 0,
              "notodp": 0,
              "odp": 0
           },
           "ununiq":{
              "count": 0,
              "valid": 0,
              "invalid": 0,
              "empty": 0,
              "first_valid_contact": 0,
              "notodp": 0,
              "odp": 0
           }
        }


        cur.execute( """SELECT Operation,
                        PhoneValidate,
                        ODP,
                        FirstNumber,
                        count(*) as CNT
                        FROM
                        Download_Organization
                        WHERE
                        Download_Organization.Download = {}
                        GROUP BY
                        Download_Organization.Operation,
                        Download_Organization.PhoneValidate,
                        Download_Organization.ODP,
                        Download_Organization.FirstNumber
                        """.format( str(download["ID"]) )  )

        row_data = cur.fetchall()

        for row in row_data:
            section_name = ""

            if row["Operation"] == "Создано":
                section_name = "uniq"
            elif row["Operation"] == "Дубль":
                section_name = "double"
            else:
                section_name = "ununiq"

            info["count"] += row["CNT"]
            info[section_name]["count"] += row["CNT"]

            if row["ODP"] == "Да":
                info["odp"] += row["CNT"]
                info[section_name]["odp"] += row["CNT"]
            else:
                info[section_name]["notodp"] += row["CNT"]

            if row["PhoneValidate"] == "Валидный":
                info[section_name]["valid"] += row["CNT"]

            if row["PhoneValidate"] == "Не валидный":
                info[section_name]["invalid"] += row["CNT"]

            if row["PhoneValidate"] == "Пустой":
                info[section_name]["empty"] += row["CNT"]

            if section_name == "ununiq":
                if row["FirstNumber"] == "Да":
                    info[section_name]["first_valid_contact"] += row["CNT"]

        cur.execute("""SELECT counts.*, Bank.Name FROM Bank
                       LEFT JOIN (
                       SELECT count(*) as cnt, Download_Organization.Download, Download_Organization_Bank.Bank
                       FROM Download_Organization_Bank
                       INNER JOIN Download_Organization ON Download_Organization_Bank.Download_Organization = Download_Organization.ID
                       WHERE Download_Organization.Download = {} AND Download_Organization_Bank.ODP = 'Да'
                       GROUP BY Download_Organization.Download, Download_Organization_Bank.Bank ) as counts
                       ON counts.Bank = Bank.ID
                       """.format( str(download["ID"])))

        bank_list = cur.fetchall()
        bank_stat = {}

        for bank in bank_list:

            for attr in bank:
                if bank[attr] is None:
                    bank[attr] = 0

            bank_stat[bank["Name"]] = bank

        cur.execute("""SELECT * FROM Bank ORDER BY ID""")
        bank_list = cur.fetchall()

        cur.execute("""SELECT * FROM CallCenter ORDER BY ID""")
        call_center_list = cur.fetchall()

        cur.execute("""SELECT count(*) as CNT, Bank, CallCenter, Download
FROM Organization_Bank_CallCenter
INNER JOIN Organization ON Organization_Bank_CallCenter.Organization = Organization.ID
WHERE Download = {}
GROUP BY Bank, CallCenter, Download
""".format(str(download["ID"])))
        call_center_bank_info = cur.fetchall()

        cur.execute("""SELECT count(*) as cnt FROM DownloadError WHERE Download = {};""".format(str(download["ID"])))
        error_cnt = cur.fetchall()[0]["cnt"]

        download_content += """<td>{}</td>""".format(info["count"])
        download_content += """<td>{}</td>""".format(error_cnt)
        download_content += """<td>{}</td>""".format(bank_stat["Тинькофф"]["cnt"])
        download_content += """<td>{}</td>""".format(bank_stat["Сбербанк"]["cnt"])
        download_content += """<td>{}</td>""".format(bank_stat["ВТБ24"]["cnt"])

        for call_center in call_center_list:
            for bank in bank_list:
                count = 0
                for ccb_info in call_center_bank_info:
                    if ccb_info["Bank"] == bank["ID"] and ccb_info["CallCenter"] == call_center["ID"]:
                        count = ccb_info["CNT"]

                download_content += """<td>{}</td>""".format(count)

        href = "../organization_list/get_organization_list_file.py?dl={}".format(download["ID"])
        href += "&load_state={}"

        download_content += """<td><a href="{}">{}</a></td>""".format(href.format("Создано"), info["uniq"]["count"])
        download_content += """<td>{}</td>""".format(info["uniq"]["valid"])
        download_content += """<td>{}</td>""".format(info["uniq"]["invalid"])
        download_content += """<td>{}</td>""".format(info["uniq"]["empty"])
        download_content += """<td>{}</td>""".format(info["uniq"]["notodp"])
        download_content += """<td>{}</td>""".format(info["uniq"]["odp"])

        download_content += """<td><a href="{}">{}</a></td>""".format(href.format("Обновлено"), info["ununiq"]["count"])
        download_content += """<td>{}</td>""".format(info["ununiq"]["valid"])
        download_content += """<td>{}</td>""".format(info["ununiq"]["invalid"])
        download_content += """<td>{}</td>""".format(info["ununiq"]["empty"])
        download_content += """<td>{}</td>""".format(info["ununiq"]["first_valid_contact"])
        download_content += """<td>{}</td>""".format(info["ununiq"]["notodp"])
        download_content += """<td>{}</td>""".format(info["ununiq"]["odp"])

        download_content += "</tr>"

    res["downloadContent"] = download_content
    return res

print("Content-type: application/json")
print()
print(json.JSONEncoder().encode(create_download_stat()))