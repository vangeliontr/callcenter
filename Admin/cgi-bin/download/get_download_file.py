#!/usr/bin/python3

import cgi
import sys

from shutil import copyfileobj


sys.path.append("..")

form = cgi.FieldStorage()


def get_file(form):
    id = form.getvalue('id')
    name = form.getvalue('name')

    try:
        file = open("../Files/{}".format(id), "rb")
    except:

        try:
            file = open("../Files/{}.xlsx".format(id), "rb")
        except:

            #pre_path = """../../PersonalOperatorAcc/auto_load/"""

            #try:
            #    file = open(pre_path + "Успешно обработано//" + name, "rb")
            #except:
            #    try:
            #        file = open(pre_path + "Ошибки//" + name, "rb")
            #    except:
            print("Content-type: text/html")
            print()
            print("File not found")
            return

    print("""Content-Type: application/vnd.ms-excel;base64;""")
    print("""Content-Disposition: attachment; filename = {}""".format(name))
    print("""Content-Transfer-Encoding: UTF-8""")
    print("""Content-Encoding: UTF-8""")
    print()
    copyfileobj(file, sys.stdout.buffer)


get_file(form)
