#!/usr/bin/python3

import cgi
import sys
import shutil

sys.path.append("..")

from DataBase.DBController import DBController
from DataBase.Download import Download

pre_path = "../../PersonalOperatorAcc/auto_load/"

form = cgi.FieldStorage()


def restart_download():
    controller = DBController()
    id = form.getvalue('id')

    exists_download = Download.get_by_id(controller, id)

    for file in exists_download.get_file_list():

        filename = file["Name"]

        try:
            shutil.move( pre_path + "Успешно обработано/" + filename, pre_path + filename)
        except:
            pass

        try:
            shutil.move( pre_path + "Ошибки/" + filename, pre_path + filename)
        except:
            pass

    exists_download.set_state_full("Ожидает загрузки", "")

try:
    restart_download()
except Exception as e:
    open("test.txt", "w").write(str(e))