function restartDownload(id) {
    var xhr = new XMLHttpRequest();

    var body = 'id=0' + id;

    xhr.open("POST", 'restart_download.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.send(body);
}

function startMonitoringDownload() {
    var xhr = new XMLHttpRequest();

    var startDayPicker = document.getElementById( "startDayPicker" );
    var start_day_value = startDayPicker.children[0].value

    var endDayPicker = document.getElementById( "endDayPicker" );
    var end_day_value = endDayPicker.children[0].value

    var body = 'page=0';

    body += "&start_date=" + start_day_value
    body += "&end_date=" + end_day_value

    xhr.open("POST", 'get_download_info.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var contact_list = JSON.parse( this.responseText );
        var downloadContent = document.getElementById( "downloadContent" );

        downloadContent.innerHTML = contact_list["downloadContent"];

        var downloadContent = document.getElementById( "update" );
        downloadContent.innerHTML = "Обновлено: " + new Date();

        startMonitoringDownload()
    }

    xhr.send(body);
}