function SetView(view_name, raw_name){
    var view = document.getElementById(view_name)
    view.hidden = !view.hidden

    var raw = document.getElementById(raw_name)

    if(view.hidden)
        raw.src = "https://emojio.ru/images/apple-b/2b07.png"
    else
        raw.src = "https://emojio.ru/images/apple-b/2b06.png"
}