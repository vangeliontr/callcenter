#!/usr/bin/env python3

import sys
sys.path.append("..")
from Bank.BankList import BankList
import cgi

form = cgi.FieldStorage()
downloadId = form.getvalue('downloadId')
downloadName = form.getvalue('downloadName')
operation = form.getvalue('operation')
operationName = form.getvalue('operationName')
valid = form.getvalue('valid')
odp = form.getvalue('odp')
Status = form.getvalue('Status')

print("Content-type: text/html")
print()

html_template = open("./cgi-bin/template.html", "r", encoding="utf-8").read()
html_template = html_template.replace("{{STYLE}}", """<link href="../../admin.css" rel="stylesheet">""")

tinkoff_content = open("./cgi-bin/tinkoff/tinkoff.html", "r", encoding="utf-8").read()

html_template = html_template.replace("{{CONTENT}}", tinkoff_content)
html_template = html_template.replace("{{SCRIPT}}",
                                      """<script src="tinkoff.js"></script>\n"""
                                      """<script>getDownloadStatusForBank('Тинькофф')</script>\n""")

bank = BankList.get_by_name("Тинькофф")

main_acc_data = bank.main_acc_data()

main_acc_header = "<tr>"
main_acc_cont = "<tr>"

for field in main_acc_data:
    main_acc_header += "<th>{}</th>".format(field)
    main_acc_cont += "<td>{}</td>".format(main_acc_data[field])

main_acc_header += "</tr>"
main_acc_cont += "</tr>"

html_template = html_template.replace("{{mainAccountDataHeader}}", main_acc_header)
html_template = html_template.replace("{{mainAccountDataContent}}", main_acc_cont)

acc_list = bank.acc_list()

acc_header = "<tr>"
acc_cont = ""

first_line = True

for acc in acc_list:
    acc_cont += "<tr>"

    for field in acc:

        if first_line:
            acc_header += "<th>{}</th>".format(field)

        acc_cont += "<td>{}</td>".format(acc[field])

    first_line = False
    acc_cont += "</tr>"

acc_header += "</tr>"

html_template = html_template.replace("{{accountDataHeader}}", acc_header)
html_template = html_template.replace("{{accountDataContent}}", acc_cont)

print(html_template)
