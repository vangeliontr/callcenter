#!/usr/bin/python3

import json
import sys
import cgi
sys.path.append("..")
from DataBase.DBController import DBController
from Bank.BankList import BankList

form = cgi.FieldStorage()
bank_name = form.getvalue('bank_name')
date = form.getvalue('date')

bank = BankList.get_by_name(bank_name)

controller = DBController()
cur = controller.get_cursor()


def create_download_stat(bank, date):

    res = {}

    def get_table_val(val):
        if val is None:
            return ""
        else:
            return str(val)

    download_stat_content = ""

    SQL = """SELECT * FROM Download WHERE DATE(Created) = DATE({}) ORDER BY ID DESC LIMIT 5"""

    if date:
        test = date[6:10] + "-" + date[3:5] + "-" + date[0:2]
        SQL = SQL.format("'" + test + "'")
    else:
        SQL = SQL.format("NOW()")

    cur.execute(SQL)
    download_list = cur.fetchall()

    stat_dict = {}

    res_stat = {"CNT": 0, "ODP": 0, "ADR": 0, "UPL": 0}

    for download in download_list:
        download_stat_content += "<tr>"
        download_stat_content += "<td>" + get_table_val(download["ID"]) + "</td>"
        download_stat_content += "<td>" + get_table_val(download["RegDate"]) + "</td>"

        cur.execute("""SELECT count(*) as CNT, Download_Organization_Bank.ODP as ODP,
                              Download_Organization_Bank.AllowAddress as ADR
                       FROM Download_Organization
                       INNER JOIN Download_Organization_Bank ON 
                           Download_Organization_Bank.Download_Organization = Download_Organization.ID
                       WHERE
                           Download_Organization.Download = {} AND
                           Download_Organization_Bank.Bank = {}
                       GROUP BY
                           Download_Organization_Bank.ODP,
                           Download_Organization_Bank.AllowAddress
                       """.format(download["ID"], bank.id()))

        load_list = cur.fetchall()
        load_stat = {"CNT": 0, "ODP": 0, "ADR": 0}

        for load_info in load_list:

            load_stat["CNT"] += load_info["CNT"]

            if load_info["ODP"] == "Да":
                load_stat["ODP"] += load_info["CNT"]

            if load_info["ADR"] != "Да":
                load_stat["ADR"] += load_info["CNT"]

        res_stat["CNT"] += load_stat["CNT"]
        res_stat["ODP"] += load_stat["ODP"]
        res_stat["ADR"] += load_stat["ADR"]

        download_stat_content += """<td>{}</a></td>""".format(load_stat["CNT"])
        download_stat_content += """<td>{}</a></td>""".format(load_stat["ODP"])
        download_stat_content += """<td>{}</a></td>""".format(load_stat["ADR"])

        cur.execute("""SELECT count(*) as CNT
                               FROM Organization_Bank_CallCenter
                               INNER JOIN Organization ON Organization_Bank_CallCenter.Organization = Organization.ID
                               WHERE Download = {} AND Bank = {}
                               """.format(download["ID"], bank.id()))

        upload_list = cur.fetchall()

        upload_count = 0
        if upload_list is not None and len(upload_list) > 0:
            upload_count += upload_list[0]["CNT"]

        res_stat["UPL"] += upload_count
        download_stat_content += """<td>{}</a></td>""".format(upload_count)

        cur.execute("""SELECT count(*) as CNT, Call_.Result
                       FROM
                       Download_Organization
                       INNER JOIN Organization ON Download_Organization.Organization = Organization.ID
                       INNER JOIN Organization_Bank_CallCenter ON Organization_Bank_CallCenter.Organization = Organization.ID
                       INNER JOIN Call_Organization ON Call_Organization.Organization = Organization.ID
                       INNER JOIN Call_ ON Call_Organization.Call_ = Call_.ID AND Call_.Bank = {}
                       WHERE
                       Download_Organization.Download = {} AND
                       Organization_Bank_CallCenter.Bank = {}
                       GROUP BY Call_.Result
                       """.format(bank.id(), download["ID"], bank.id()))

        stat_list = cur.fetchall()

        for stat in stat_list:
            if stat["Result"] not in stat_dict:
                stat_dict[stat["Result"]] = {}

            stat_dict[stat["Result"]][download["ID"]] = stat["CNT"]

        download_stat_content += "{{STATCONTENT" + str(download["ID"]) + "}}"

        download_stat_content += "</tr>"

    download_stat_header = """<tr>
                                  <th>Номер загрузки</th>
                                  <th>Дата регистрации</th>
                                  <th>Кол-во контактов</th>
                                  <th>ОДП</th>
                                  <th>Не подходящий регион</th>
                                  <th>Запущено в работу</th>"""

    res_line = """<b>
                      <tr>
                          <td colspan="1">Итого:<td>"""

    res_line += """<td>{}</td>""".format(res_stat["CNT"])
    res_line += """<td>{}</td>""".format(res_stat["ODP"])
    res_line += """<td>{}</td>""".format(res_stat["ADR"])
    res_line += """<td>{}</td>""".format(res_stat["UPL"])

    res_stat_res = {}

    for download in download_list:
        stat_content_download = ""

        for status in stat_dict:
            count = 0

            if download["ID"] in stat_dict[status]:
                count = stat_dict[status][download["ID"]]

            stat_content_download += """<td>{}</td>""".format(count)

            if status not in res_stat_res:
                res_stat_res[status] = 0

            res_stat_res[status] += count

        download_stat_content = download_stat_content.replace("{{STATCONTENT" + str(download["ID"]) + "}}",
                                                              stat_content_download)
    for status in stat_dict:
        if status is None:
            download_stat_header += "<th>{}</th>".format("Не обработано")
        else:
            download_stat_header += "<th>{}</th>".format(status)

        res_line += """<td>{}</td>""".format(res_stat_res[status])

    download_stat_header += "</tr>"
    res_line += """</tr></b>"""

    res["downloadStatContent"] = download_stat_content + res_line
    res["downloadStatHeader"] = download_stat_header
    return res


print("Content-type: application/json")
print()
print(json.JSONEncoder().encode(create_download_stat(bank, date)))
