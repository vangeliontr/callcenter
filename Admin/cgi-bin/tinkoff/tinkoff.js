function getDownloadStatusForBank(bank_name) {
    var xhr = new XMLHttpRequest();

    var dayPicker = document.getElementById( "dayPicker" );
    day_value = dayPicker.children[0].value

    var body = 'bank_name=' + bank_name;
    body += "&date=" + day_value

    xhr.open("POST", 'get_download_status_info_for_bank.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res = JSON.parse( this.responseText );
        var statusDownloadContent = document.getElementById( "statusDownloadContent" );
        var statusDownloadHeader = document.getElementById( "statusDownloadHeader" );

        statusDownloadContent.innerHTML = res["downloadStatContent"];
        statusDownloadHeader.innerHTML = res["downloadStatHeader"];

        var downloadContent = document.getElementById( "update" );
        downloadContent.innerHTML = "Обновлено: " + new Date();

        setTimeout( getDownloadStatusForBank(bank_name), 3000 );
    }

    xhr.send(body);
}