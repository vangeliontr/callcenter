#!/usr/bin/python3

import json
import cgi
import sys
sys.path.append("..")
from DataBase.DBController import DBController
import re

def get_resp():
    form = cgi.FieldStorage()
    tel = form.getvalue('contact_person_tel')
    surname = form.getvalue('contact_person_surname')
    name = form.getvalue('contact_person_name')
    middlename = form.getvalue('contact_person_middlename')
    email = form.getvalue('contact_person_email')

    if tel is None and surname is None and name is None and middlename is None and email is None:
        print("Content-type: application/json")
        print()
        print(json.JSONEncoder().encode([]))
        return

    controller = DBController()
    cur = controller.get_cursor()

    if tel is not None:
        tel = "".join(re.findall('\d+', tel ) )

        def insert_char(txt, chr):
            out = ""
            for t in txt:
                out += t + chr
            return chr + out

        tel = insert_char(tel, '%')

        cur.execute("""SELECT ContactPerson FROM ContactInfo WHERE `Value` like '""" + tel +  "';")

        person_list = cur.fetchall()

        tel = ""

        if len(person_list) == 0:
            print("Content-type: application/json")
            print()
            print(json.JSONEncoder().encode([]))
            return

        for person in person_list:
            if len(tel) == 0:
                tel = str(person["ContactPerson"])
                continue

            tel = tel + ", " + str(person["ContactPerson"])

        tel = "(" + tel + ")"

    sql = """SELECT
    Name,
    Surname,
    MiddleName,
    ContactPerson.ID as ContactPersonID
    FROM
    ContactInfo
    INNER JOIN ContactPerson ON ContactInfo.ContactPerson = ContactPerson.ID WHERE """

    is_first = True

    if tel is not None:
        sql = sql + """ ContactPerson.ID in """ + tel
        is_first = False

    if email is not None:
        if not is_first:
           sql += " AND "
        sql = sql + """ `Value` = '""" + email + "'"
        is_first = False

    if surname is not None:
        if not is_first:
           sql += " AND "
        sql = sql + """ Surname = '""" + surname + "'"
        is_first = False

    if name is not None:
        if not is_first:
           sql += " AND "
        sql = sql + """ Name = '""" + name + "'"
        is_first = False

    if middlename is not None:
        if not is_first:
           sql += " AND "
        sql = sql + """ Middlename = '""" + middlename + "'"
        is_first = False

    sql = sql + """
    GROUP BY ContactPerson.ID
    ORDER BY ContactPerson.ID DESC
    LIMIT 10
    """

    cur.execute(sql)

    res = cur.fetchall()

    print("Content-type: application/json")
    print()
    print(json.JSONEncoder().encode(res))

get_resp()