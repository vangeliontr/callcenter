#!/usr/bin/python3

import json
import cgi
import sys
sys.path.append("..")
from DataBase.DBController import DBController

form = cgi.FieldStorage()
inn = form.getvalue('find_organization_inn')
id = form.getvalue('find_organization_id')

controller = DBController()
cur = controller.get_cursor()
sql = """SELECT
ID,
Type,
Name,
INN,
DATE_FORMAT( Organization.OpenDate, "%d.%m" ) as OpenDate,
Address,
TimeZone
FROM
Organization
WHERE """

if id is not None:
   sql += "Organization.ID = " + id
elif inn is not None:
   sql += """Organization.INN = '""" + inn + "'"

cur.execute(sql)
res = cur.fetchall()

print("Content-type: application/json")
print()
print(json.JSONEncoder().encode(res))