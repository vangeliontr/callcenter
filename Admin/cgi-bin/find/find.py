#!/usr/bin/env python3

print("Content-type: text/html")
print()

html_template = open("./cgi-bin/template.html", "r", encoding="utf-8").read()
html_template = html_template.replace("{{STYLE}}", """<link href="../../admin.css" rel="stylesheet">""")

admin_content = open("./cgi-bin/find/find.html", "r", encoding="utf-8").read()

html_template = html_template.replace("{{CONTENT}}", admin_content)
html_template = html_template.replace("{{SCRIPT}}",
                                      """<script>window.jQuery || document.write('<script src="../../assets/js/"""
                                      """vendor/jquery.min.js"><\/script>')</script>\n"""
                                      """<script src="../../admin.js"></script>\n"""
                                      """<script src="find.js"></script>\n"""
                                      """<script>SelectMenu('menu_find')</script>\n""")

print(html_template)
