function findCall() {
    call_id = document.getElementById("find_call_id").value

    var xhr = new XMLHttpRequest();

    var body = 'call_id=' + call_id;

    xhr.open("POST", 'find_call.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res_json = this.responseText.substring( this.responseText.search("{") - 1)
        var contact_list = JSON.parse( res_json );
        var table = document.getElementById( "find_contact_content" )

        for( var i = 0; i < contact_list.length; i++ ) {
            var tr = document.createElement("tr");
            var row = contact_list[i];

            appenLinkToTable(tr, '../organization/organization.py?id=' + row["OrganizationID"], row["Name"])
            appenToTable(tr, row["INN"])
            appenToTable(tr, row["Address"])
            appenToTable(tr, row["OpenDate"])
            appenToTable(tr, row["TimeZone"])
            appenLinkToTable(tr, 'contact_person.py?id=' + row["ContactPersonID"], row["Surname"])
            appenToTable(tr, row["ContactPersonName"])
            appenToTable(tr, row["DateTime"])
            appenToTable(tr, row["Result"])
            appenToTable(tr, row["ProjectName"])
            appenToTable(tr, row["EmployeeName"])
            appenToTable(tr, row["EmployeeSurname"])

            table.appendChild(tr)
        }
    }

    xhr.send(body);
}

function findContactPerson(){
    tel = document.getElementById("find_contact_person_tel").value
    surname = document.getElementById("find_contact_person_surname").value
    name = document.getElementById("find_contact_person_name").value
    middlename = document.getElementById("find_contact_person_middlename").value
    email = document.getElementById("find_contact_person_email").value

    var xhr = new XMLHttpRequest();

    var body = 'contact_person_tel=' + tel;
    body += '&contact_person_surname=' + surname;
    body += '&contact_person_name=' + name;
    body += '&contact_person_middlename=' + middlename;
    body += '&contact_person_email=' + email;

    xhr.open("POST", 'find_contact_person.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res_json = this.responseText.substring( this.responseText.search("{") - 1)
        var contact_list = JSON.parse( res_json );
        var table = document.getElementById( "find_contact_person_content" )
        table.innerHTML = ""

        if(contact_list.length == 0){
          alert("Не удалось найти контактное лицо")
        }

        for( var i = 0; i < contact_list.length; i++ ) {
            var tr = document.createElement("tr");
            var row = contact_list[i];

            appenLinkToTable(tr, '../contact_person/contact_person.py?id=' + row["ContactPersonID"], row["Surname"])
            appenToTable(tr, row["Name"])
            appenToTable(tr, row["MiddleName"])

            table.appendChild(tr)
        }
    }

    xhr.send(body);
}

function findOrganization(){
    inn = document.getElementById("find_organization_inn").value
    id = document.getElementById("find_organization_id").value

    var xhr = new XMLHttpRequest();

    var body = 'find_organization_inn=' + inn;
    body += '&find_organization_id=' + id;

    xhr.open("POST", 'find_organization.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res_json = this.responseText.substring( this.responseText.search("{") - 1)
        var contact_list = JSON.parse( res_json );
        var table = document.getElementById( "find_organization_content" )

        for( var i = 0; i < contact_list.length; i++ ) {
            var tr = document.createElement("tr");
            var row = contact_list[i];

            appenToTable(tr, row["Type"])
            appenLinkToTable(tr, '../organization/organization.py?id=' + row["ID"], row["Name"])
            appenToTable(tr, row["INN"])
            appenToTable(tr, row["OpenDate"])
            appenToTable(tr, row["Address"])
            appenToTable(tr, row["TimeZone"])

            table.appendChild(tr)
        }
    }

    xhr.send(body);
}
