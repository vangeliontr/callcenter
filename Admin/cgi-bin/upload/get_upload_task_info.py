#!/usr/bin/python3

import json
import sys
import cgi
sys.path.append("..")
from DataBase.Task import Task
from DataBase.DBController import DBController


def get_val_by_dict(params):
    res = """<br/><ul class="list-group list-group-horizontal">"""

    for param in params:
        res += """<li class="list-group-item">{}: {}</li>""".format(param, params[param])

    res += "</ul>"

    return res


def create_upload_task_stat():
    controller = DBController()

    task_list = Task.get_by_reestr(controller, "Подгрузка")

    task_content = ""
    res = {}

    for task in task_list:

        task_content += "<tr>"
        task_content += "<td><a href='../task/task.py?id={}'>{}</a></td>".format(task.id(), task.id())
        task_content += "<td>{}</td>".format(task.type())
        task_content += "<td>{}</td>".format(task.suspend())
        task_content += "<td>{}</td>".format(task.state())

        params_content = """<ul class="list-group list-group-horizontal">"""
        params = task.get_params()

        for param in params:
            params_content += """<li class="list-group-item">{}: {}</li>""".format(param, params[param])

        params_content += "</ul>"

        task_content += "<td>{}</td>".format(params_content)

        res_content = """<ul class="list-group list-group-horizontal">"""
        task_res_list = task.get_res()

        for task_res in task_res_list:

            if isinstance(task_res_list[task_res], dict):
                val = get_val_by_dict(task_res_list[task_res])
            else:
                val = task_res_list[task_res]

            res_content += """<li class="list-group-item">{}: {}</li>""".format(task_res, val)
            pass

        res_content += "</ul>"

        task_content += "<td>{}</td>".format(res_content)
        task_content += "</tr>"

    res["taskContent"] = task_content
    return res


print("Content-type: application/json")
print()
print(json.JSONEncoder().encode(create_upload_task_stat()))
