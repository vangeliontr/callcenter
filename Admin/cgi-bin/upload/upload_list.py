#!/usr/bin/env python3

import sys
sys.path.append("..")
from DataBase.DBController import DBController

print("Content-type: text/html")
print()
html_template = open("./cgi-bin/template.html", "r", encoding="utf-8").read()
html_template = html_template.replace("{{STYLE}}", """<link href="../../admin.css" rel="stylesheet">""")
html_template = html_template.replace("{{SCRIPT}}", """{{SCRIPT}}"""
                                      """<script>window.jQuery || document.write('<script src="../../assets/js/"""
                                      """vendor/jquery.min.js"><\/script>')</script>\n"""
                                      """<script src="../../admin.js"></script>\n"""
                                      """<script src="upload_list.js"></script>\n"""
                                      """<script>startMonitoringUpload()</script>\n"""
                                      """<script>startMonitoringUploadState()</script>\n"""
                                      """<script>startMonitoringUploadTZ()</script>\n"""
                                      """<script>startMonitoringUploadTask()</script>\n"""
                                      """<script>SelectMenu('menu_upload')</script>\n""")

download_content = open("./cgi-bin/upload/upload_list.html", "r", encoding="utf-8").read()

CALL_CENTER_LIST = ""
CALL_CENTER_BANK_LIST = ""

controller = DBController()
cur = controller.get_cursor()

cur.execute("""SELECT * FROM Bank ORDER BY ID""")
bank_list = cur.fetchall()

cur.execute("""SELECT * FROM CallCenter ORDER BY ID""")
call_center_list = cur.fetchall()

for call_center in call_center_list:
    CALL_CENTER_LIST += """<th colspan="{}">Загружено в {}</th>""".format(len(bank_list),call_center["Name"])

    for bank in bank_list:
        CALL_CENTER_BANK_LIST += "<th>{}</th>".format(bank["Name"])

download_content = download_content.replace("{{CALL_CENTER_LIST}}", CALL_CENTER_LIST)
download_content = download_content.replace("{{CALL_CENTER_BANK_LIST}}", CALL_CENTER_BANK_LIST)

html_template = html_template.replace("{{CONTENT}}", download_content)
print(html_template)
