function startMonitoringUpload() {
    var xhr = new XMLHttpRequest();

    var startDayPicker = document.getElementById( "startDayPicker" );
    var start_day_value = startDayPicker.children[0].value

    var endDayPicker = document.getElementById( "endDayPicker" );
    var end_day_value = endDayPicker.children[0].value

    var body = 'page=0';

    body += "&start_date=" + start_day_value
    body += "&end_date=" + end_day_value

    xhr.open("POST", 'get_upload_info.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var contact_list = JSON.parse( this.responseText );
        var downloadContent = document.getElementById( "uploadContent" );

        downloadContent.innerHTML = contact_list["uploadContent"];

        var downloadContent = document.getElementById( "update" );
        downloadContent.innerHTML = "Обновлено: " + new Date();

        startMonitoringUpload()
    }

    xhr.send(body);
}

function startMonitoringUploadTZ() {
    var xhr = new XMLHttpRequest();

    var startDayPicker = document.getElementById( "startDayPicker" );
    var start_day_value = startDayPicker.children[0].value

    var endDayPicker = document.getElementById( "endDayPicker" );
    var end_day_value = endDayPicker.children[0].value

    var body = 'page=0';

    body += "&start_date=" + start_day_value
    body += "&end_date=" + end_day_value

    xhr.open("POST", 'get_upload_tz_info.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var table_data = JSON.parse( this.responseText );
        var uploadTZContent = document.getElementById( "uploadTZContent" );
        var uploadTZHead = document.getElementById( "uploadTZHead" );

        uploadTZContent.innerHTML = table_data["uploadTZContent"];
        uploadTZHead.innerHTML = table_data["uploadTZHead"];

        startMonitoringUploadTZ()
    }

    xhr.send(body);
}

function startMonitoringUploadState() {
    var xhr = new XMLHttpRequest();

    var startDayPicker = document.getElementById( "startDayPicker" );
    var start_day_value = startDayPicker.children[0].value

    var endDayPicker = document.getElementById( "endDayPicker" );
    var end_day_value = endDayPicker.children[0].value

    var body = 'page=0';

    body += "&start_date=" + start_day_value
    body += "&end_date=" + end_day_value

    xhr.open("POST", 'get_upload_state_info.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var table_data = JSON.parse( this.responseText );
        var uploadContent = document.getElementById( "uploadStateContent" );
        var uploadHead = document.getElementById( "uploadStateHead" );

        uploadContent.innerHTML = table_data["uploadContent"];
        uploadHead.innerHTML = table_data["uploadHead"];

        startMonitoringUploadState()
    }

    xhr.send(body);
}

function startMonitoringUploadTask() {
    var xhr = new XMLHttpRequest();

    xhr.open("POST", 'get_upload_task_info.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var contact_list = JSON.parse( this.responseText );
        var downloadContent = document.getElementById( "taskContent" );

        downloadContent.innerHTML = contact_list["taskContent"];

        startMonitoringUploadTask()
    }

    xhr.send();
}