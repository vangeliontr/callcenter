#!/usr/bin/python3

import json
import sys
import cgi
sys.path.append("..")
from DataBase.Upload import Upload


def create_upload_stat():

    form = cgi.FieldStorage()

    start_date = form.getvalue('start_date')
    end_date = form.getvalue('end_date')

    upload_list = Upload.list(start_date=start_date, end_date=end_date, limit=None)

    upload_content = ""
    upload_head = ""
    upload_list_report = {}
    res = {}

    for upload in upload_list:
        report = upload.call_report()

        for elem in report:

            if elem["Result"] is None:
                elem["Result"] = "Не обработано"

            if not elem["Result"] in upload_list_report:
                upload_list_report[elem["Result"]] = {}

            upload_list_report[elem["Result"]][upload.id()] = elem["CNT"]

            if elem["Result"] == "Не обработано":
                if not "Обработано" in upload_list_report:
                    upload_list_report["Обработано"] = {}

                upload_list_report["Обработано"][upload.id()] = upload.contact_count() - elem["CNT"]

    upload_head += "<th>ID</th>"
    upload_head += "<th>Количество</th>"

    for result in upload_list_report:
        upload_head += "<th>{}</th>".format(result)

    all_count = 0

    for upload in upload_list:

        upload_content += "<tr>"
        upload_content += "<td>{}</td>".format(upload.id())
        upload_content += "<td>{}</td>".format(upload.contact_count())
        all_count += upload.contact_count()

        for result in upload_list_report:

            cnt = 0

            if upload.id() in upload_list_report[result]:
                cnt = upload_list_report[result][upload.id()]

            upload_content += """<td><a href="../organization_list/get_organization_list_file.py?upload_id={}&status={}">{}</a></td>"""\
                .format(upload.id(), result, cnt)

        upload_content += "</tr>"

    # Подведем итог

    upload_content += "<tr>"
    upload_content += """<td style="font-weight:bold">Итого</td>"""
    upload_content += """<td style="font-weight:bold">{}</td>""".format(all_count)

    for result in upload_list_report:

        cnt = 0

        for upload_id in upload_list_report[result]:
            cnt += upload_list_report[result][upload_id]

        upload_content += """<td style="font-weight:bold">{}</td>""".format(cnt)

    upload_content += "</tr>"

    res["uploadContent"] = upload_content
    res["uploadHead"] = upload_head
    return res


print("Content-type: application/json")
print()
print(json.JSONEncoder().encode(create_upload_stat()))
