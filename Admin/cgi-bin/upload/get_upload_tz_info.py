#!/usr/bin/python3

import json
import sys
import cgi
sys.path.append("..")
from DataBase.Upload import Upload


def create_upload_stat():

    form = cgi.FieldStorage()

    start_date = form.getvalue('start_date')
    end_date = form.getvalue('end_date')

    upload_list = Upload.list(start_date=start_date, end_date=end_date, limit=None)

    upload_content = ""
    upload_head = ""
    upload_list_tz = {}
    res = {}

    for upload in upload_list:
        report = upload.time_zone_report()

        for elem in report:

            if not elem["TZ"] in upload_list_tz:
                upload_list_tz[elem["TZ"]] = {}

            upload_list_tz[elem["TZ"]][upload.id()] = elem["CNT"]

    upload_head += "<th>ID</th>"
    upload_head += "<th>Количество</th>"

    for result in upload_list_tz:
        upload_head += "<th>{}</th>".format(result)

    all_count = 0

    for upload in upload_list:

        upload_content += "<tr>"
        upload_content += "<td>{}</td>".format(upload.id())
        upload_content += "<td>{}</td>".format(upload.contact_count())
        all_count += upload.contact_count()

        for result in upload_list_tz:

            cnt = 0

            if upload.id() in upload_list_tz[result]:
                cnt = upload_list_tz[result][upload.id()]

            upload_content += "<td>{}</td>".format(cnt)

        upload_content += "</tr>"

    # Подведем итог

    upload_content += "<tr>"
    upload_content += """<td style="font-weight:bold">Итого</td>"""
    upload_content += """<td style="font-weight:bold">{}</td>""".format(all_count)

    for result in upload_list_tz:

        cnt = 0

        for upload_id in upload_list_tz[result]:
            cnt += upload_list_tz[result][upload_id]

        upload_content += """<td style="font-weight:bold">{}</td>""".format(cnt)

    upload_content += "</tr>"

    res["uploadTZContent"] = upload_content
    res["uploadTZHead"] = upload_head
    return res


print("Content-type: application/json")
print()
print(json.JSONEncoder().encode(create_upload_stat()))
