#!/usr/bin/python3

import json
import sys
import cgi
sys.path.append("..")
from DataBase.Upload import Upload


def create_upload_stat():

    form = cgi.FieldStorage()

    start_date = form.getvalue('start_date')
    end_date = form.getvalue('end_date')

    upload_list = Upload.list(start_date=start_date, end_date=end_date, limit=None)

    upload_content = ""
    res = {}

    for upload in upload_list:

        upload_content += "<tr>"
        upload_content += "<td>{}</td>".format(upload.id())
        upload_content += "<td>{}</td>".format(upload.datetime())
        upload_content += "<td>{}</td>".format(upload.state())
        upload_content += "<td>{}</td>".format(upload.contact_count())
        upload_content += "<td>{}</td>".format(upload.params())
        upload_content += "</tr>"

    res["uploadContent"] = upload_content
    return res


print("Content-type: application/json")
print()
print(json.JSONEncoder().encode(create_upload_stat()))
