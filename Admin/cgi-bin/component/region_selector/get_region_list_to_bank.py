import json
import sys
import cgi
sys.path.append("..")

from Bank.BankList import BankList

form = cgi.FieldStorage()
bank_name = form.getvalue('bank')

bank = BankList.get_by_name(bank_name)


allow_regions = bank.regions
dis_allow_regions = bank.dis_allow_regions

region_content = ""

max = len(allow_regions)

if len(dis_allow_regions) > max:
    max = len(dis_allow_regions)

for i in range(0, max):
    allow_code = ""
    allow_name = ""
    allow_id = ""
    dis_allow_code = ""
    dis_allow_name = ""
    dis_allow_id = ""

    if i < len(allow_regions):
        allow_code = allow_regions[i].get_number()
        allow_name = allow_regions[i].get_name()
        allow_id = allow_regions[i].id()

    if i < len(dis_allow_regions):
        dis_allow_code = dis_allow_regions[i].get_number()
        dis_allow_name = dis_allow_regions[i].get_name()
        dis_allow_id = dis_allow_regions[i].id()

    region_content += "<tr><td reg_id='{}'>{} {}</td><td reg_id='{}'>{} {}</td></tr>".\
        format(allow_id, allow_code, allow_name, dis_allow_id, dis_allow_code, dis_allow_name)

print("Content-type: application/json")
print()
print(region_content)
