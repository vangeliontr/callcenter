import json
import sys
import cgi
sys.path.append("..")

from Bank.BankList import BankList

form = cgi.FieldStorage()
reg_id = form.getvalue('id')
bank_name = form.getvalue('bank')

bank = BankList.get_by_name(bank_name)

bank.change_region_state(reg_id)
