function select(area) {

    content_list = document.body.querySelectorAll('div.content')
    content_list.forEach(content => content.hidden = true)

    menu_list = document.body.querySelectorAll('li.cust_menu')
    menu_list.forEach(menu => menu.classList.remove('active'))

    menu = document.getElementById("menu_" + area)
    content = document.getElementById("content_" + area)

    menu.classList.add('active')
    content.hidden = false
}

function movePage(val) {
    cur_val = document.getElementById("cur_page_val").innerHTML
    document.getElementById("cur_page_val").innerHTML = (val + parseInt(cur_val)).toString()

    getHistory()
}

function setLimit(val) {
    if(document.getElementById("page_size").innerHTML == val)
        return

    document.getElementById("cur_page_val").innerHTML = "1"
    document.getElementById("page_size").innerHTML = val
    getHistory()
}

function getHistory() {
    var xhr = new XMLHttpRequest();

    var start_day_value = document.getElementById("startDTPickerInp").value
    var end_day_value = document.getElementById("endDTPickerInp").value

    var limit = document.getElementById("page_size").value
    var offset = document.getElementById("cur_page_val").innerHTML
    var site = document.getElementById("find_site").value
    var ip = document.getElementById("find_ip").value
    var method = document.getElementById("find_method").value
    var path = document.getElementById("find_path").value
    var user = document.getElementById("find_user").value

    //var startDayPicker = document.getElementById( "startDayPicker" );
    //var start_day_value = startDayPicker.children[0].value

    //var endDayPicker = document.getElementById( "endDayPicker" );
    //var end_day_value = endDayPicker.children[0].value

    var body = 'limit='+limit;

    body += "&start_date=" + start_day_value
    body += "&end_date=" + end_day_value
    body += "&offset=" + offset
    body += "&site=" + site
    body += "&ip=" + ip
    body += "&method=" + method
    body += "&path=" + path
    body += "&user=" + user

    xhr.open("POST", 'get_history.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    $('#selectWaitDiaog').modal('show')

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res = JSON.parse( this.responseText );
        var historyContent = document.getElementById( "historyContent" );

        historyContent.innerHTML = res["historyContent"];
        setPageVis("next_page_val", res["next"])
        setPageVis("prev_page_val", res["prev"])
        $('#selectWaitDiaog').modal('hide')
    }

    xhr.send(body);
}

function getStat() {
    var xhr = new XMLHttpRequest();

    var start_day_value = document.getElementById("startDTPickerInpStat").value
    var end_day_value = document.getElementById("endDTPickerInpStat").value

    var site = document.getElementById("find_site_stat").value
    var ip = document.getElementById("find_ip_stat").value
    var method = document.getElementById("find_method_stat").value
    var path = document.getElementById("find_path_stat").value
    var user = document.getElementById("find_user_stat").value
    var hStat = "None"
    var vStat = "None"

    var HStatList = document.getElementsByName("HStat");

    for(var i = 0; i < HStatList.length; i++) {
        if(HStatList[i].checked == true) {
            hStat = HStatList[i].value
            break
        }
    }

    var VStatList = document.getElementsByName("VStat");

    for(var i = 0; i < VStatList.length; i++) {
        if(VStatList[i].checked == true) {
            vStat = VStatList[i].value
            break
        }
    }

    var body = '';

    body += "start_date=" + start_day_value
    body += "&end_date=" + end_day_value
    body += "&site=" + site
    body += "&ip=" + ip
    body += "&method=" + method
    body += "&path=" + path
    body += "&user=" + user
    body += "&hStat=" + hStat
    body += "&vStat=" + vStat

    xhr.open("POST", 'get_history_stat.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    $('#selectWaitDiaog').modal('show')

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res = JSON.parse( this.responseText );
        var historyContentStat = document.getElementById( "historyContentStat" );
        var historyHeadStat = document.getElementById( "historyHeadStat" );

        historyContentStat.innerHTML = res["historyContentStat"];
        historyHeadStat.innerHTML = res["historyHeadStat"];
        $('#selectWaitDiaog').modal('hide')
    }

    xhr.send(body);
}

function getIPList() {
    var xhr = new XMLHttpRequest();

    var body = '';

    xhr.open("POST", 'get_ip_list.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res = JSON.parse( this.responseText );
        var historyContentIP = document.getElementById( "historyContentIP" );

        historyContentIP.innerHTML = res["historyContentIP"];
    }

    xhr.send(body);
}

function addIP() {
    var xhr = new XMLHttpRequest();

    add_ip
    var add_ip = document.getElementById("add_ip").value;

    var body = 'add_ip='+add_ip;

    xhr.open("POST", 'add_ip.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        getIPList()
    }

    xhr.send(body);
}

function setPageVis(id, val) {

    if(val)
        val = "";
    else
        val = "none"

    elem = document.getElementById(id).style.display = val
}

function refresh() {
    document.getElementById("cur_page_val").innerHTML = "1"
    getHistory()
}