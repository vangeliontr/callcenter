#!/usr/bin/python3

import json
import sys
sys.path.append("..")

from DataBase.DBController import DBController


def get_ip_list():

    res = {}
    history_content_ip = ""

    controller = DBController()
    cur = controller.get_cursor()

    cur.execute("SELECT * FROM IPList")
    rec_list = cur.fetchall()

    for rec in rec_list:
        history_content_ip += "<tr>"
        history_content_ip += "<td>{}</td>".format(rec["IP"])
        history_content_ip += "</tr>"


    res["historyContentIP"] = history_content_ip
    return res

print("Content-type: application/json")
print()
print(json.JSONEncoder().encode(get_ip_list()))