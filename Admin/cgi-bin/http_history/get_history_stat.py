#!/usr/bin/python3

import json
import sys
import cgi
sys.path.append("..")

from DataBase.DBController import DBController
from DataBase.HttpHistory import HttpHistory


def get_field_alias(field):

    if field == "User":
        return "Поьзователь"

    if field == "URL":
        return "Путь"

    if field == "ClientIP":
        return "IP Адрес"

    if field == "Method":
        return "Метод"

    if field == "Site":
        return "Сайт"

    return field


def get_history_stat():

    form = cgi.FieldStorage()

    start_date = form.getvalue('start_date')
    end_date = form.getvalue('end_date')

    site = form.getvalue('site', None)
    ip = form.getvalue('ip', None)
    method = form.getvalue('method', None)
    path = form.getvalue('path', None)
    user = form.getvalue('user', None)
    h_stat = form.getvalue('hStat', None)
    v_stat = form.getvalue('vStat', None)

    if h_stat == "None":
        h_stat = None

    if v_stat == "None":
        v_stat = None

    res = {}
    history_content = ""
    history_head = ""
    controller = DBController()

    rec_list = HttpHistory.get_stat(controller, start_date, end_date, site, ip, method, path, user, h_stat, v_stat)

    if h_stat and v_stat:

        table = {}

        v_list = []

        for rec in rec_list:

            if rec[h_stat] not in table:
                table[rec[h_stat]] = {}

            if rec[v_stat] not in v_list:
                v_list.append(rec[v_stat])

            table[rec[h_stat]][rec[v_stat]] = rec["Count"]

        history_head = "<tr><th></th>"

        for v in v_list:
            history_head += "<th>{}</th>".format(v)

        history_head += "</tr>"

        for h in table:
            history_content += "<tr>"
            history_content += "<td>{}</td>".format(h)

            for v in v_list:

                if v not in table[h]:
                    history_content += "<td>0</td>"
                else:
                    history_content += "<td>{}</td>".format(table[h][v])

            history_content += "</tr>"

    elif h_stat or v_stat:

        field = ""

        if h_stat:
            field = h_stat

        if v_stat:
            field = v_stat

        field_alias = get_field_alias(field)

        history_head = "<tr><th>{}</th><th>Количество</th></tr>".format(field_alias)

        for rec in rec_list:
            history_content += "<tr>"
            history_content += "<td>{}</td>".format(rec[field])
            history_content += "<td>{}</td>".format(rec["Count"])
            history_content += "</tr>"
    else:

        history_head = "<tr><th>Количество</th></tr>"

        for rec in rec_list:

            history_content += "<tr>"
            history_content += "<td>{}</td>".format(rec["Count"])
            history_content += "</tr>"

    res["historyContentStat"] = history_content
    res["historyHeadStat"] = history_head
    return res

print("Content-type: application/json")
print()
print(json.JSONEncoder().encode(get_history_stat()))