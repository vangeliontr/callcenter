#!/usr/bin/python3

import json
import sys
import cgi
sys.path.append("..")

from DataBase.DBController import DBController


def add_ip():

    form = cgi.FieldStorage()
    res = {}

    add_ip = form.getvalue('add_ip')
    controller = DBController()

    cur = controller.get_cursor()

    cur.execute("INSERT INTO IPList SET IP = '{}'".format(add_ip))

    return res

print("Content-type: application/json")
print()
print(json.JSONEncoder().encode(add_ip()))