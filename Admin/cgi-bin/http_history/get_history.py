#!/usr/bin/python3

import json
import sys
import cgi
sys.path.append("..")

from DataBase.DBController import DBController
from DataBase.HttpHistory import HttpHistory


def get_history():

    form = cgi.FieldStorage()

    start_date = form.getvalue('start_date')
    end_date = form.getvalue('end_date')

    limit = int(form.getvalue('limit'))
    offset = int(form.getvalue('offset'))
    site = form.getvalue('site', None)
    ip = form.getvalue('ip', None)
    method = form.getvalue('method', None)
    path = form.getvalue('path', None)
    user = form.getvalue('user', None)

    res = {}
    history_content = ""
    controller = DBController()

    rec_list = HttpHistory.log_list(controller, limit + 1, offset - 1, start_date, end_date, site, ip, method, path, user)

    count = 0
    for rec in rec_list:
        count += 1

        if count > limit:
            break

        history_content += "<tr>"
        history_content += "<td>{}</td>".format(rec["Site"])
        history_content += "<td>{}</td>".format(rec["DateTime"])
        history_content += "<td>{}</td>".format(rec["ClientIP"])
        history_content += "<td>{}</td>".format(rec["Method"])
        history_content += "<td>{}</td>".format(rec["URL"])
        history_content += "<td>{}</td>".format(rec["User"])
        history_content += "</tr>"

    res["next"] = True
    res["prev"] = True

    if int(limit) + 1 > len(rec_list):
        res["next"] = False

    if offset == 1:
        res["prev"] = False

    res["historyContent"] = history_content
    return res

print("Content-type: application/json")
print()
print(json.JSONEncoder().encode(get_history()))