#!/usr/bin/env python3
import cgi
import json
import sys
sys.path.append("..")
from Bank.BankList import BankList

form = cgi.FieldStorage()
bank_name = form.getvalue('bank')

bank = BankList.get_by_name(bank_name)

region_list = bank.get_work_region_list()

print("Content-type: application/json")
print()
print(json.JSONEncoder().encode(region_list))