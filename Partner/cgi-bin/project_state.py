#!/usr/bin/env python3

import cgi
import sys
sys.path.append("..")

form = cgi.FieldStorage()
project_name = form.getvalue('name')

print("Content-type: text/html")
print()
html_template = open("./cgi-bin/project_state.html", "r", encoding="utf-8").read()
html_template = html_template.replace("{{PROJECT_NAME}}", project_name)
print(html_template)
