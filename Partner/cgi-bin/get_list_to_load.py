import sys
import json
import cgi

sys.path.append("..")

from DataBase.DBController import DBController
from DataBase.Project import Project

controller = DBController()

form = cgi.FieldStorage()
project_name = form.getvalue('project_name')

project = Project.get_by_name(project_name)

SQL = """SELECT Organization.ID, Organization.INN, ContactInfo.Value as Phone, ContactPerson.Name,
                        ContactPerson.MiddleName, ContactPerson.Surname, Organization.Address, 
                        Organization.Name as OrgName, Call_.ID as CallID, Organization.Download, Call_.RecallDateTime
                 FROM Call_ 
                 INNER JOIN Call_Organization ON Call_Organization.Call_ = Call_.ID
                 INNER JOIN Organization ON Call_Organization.Organization = Organization.ID
                 INNER JOIN ContactInfo ON Organization.LastPhoneInfo = ContactInfo.ID
                 INNER JOIN ContactPerson ON Organization.LastContactPerson = ContactPerson.ID
                 WHERE Result is NULL and Project = {} AND Call_.ExtID is NULL AND Organization.Download <> 2372
                 ORDER BY Call_.ID DESC;""".format(project.id())

cur = controller.get_cursor()
cur.execute(SQL)

org_list = cur.fetchall()

res = ""

res += "<tr>"
res += "<th>Отложен до<th>"
res += "<th>Фамилия<th>"
res += "<th>Имя<th>"
res += "<th>Отчество<th>"
res += "</tr>"

for org in org_list:
    res += "<tr>"
    res += "<td>{}<td>".format(org["RecallDateTime"])
    res += "<td>{}<td>".format(org["Surname"])
    res += "<td>{}<td>".format(org["Name"])
    res += "<td>{}<td>".format(org["MiddleName"])
    res += "</tr>"

print("Content-type: application/json")
print()
print(json.JSONEncoder().encode({"listToLoad": res, "count": len(org_list)}))
