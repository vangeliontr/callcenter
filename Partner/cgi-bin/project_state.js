function getListToLoad(project_name) {
    var xhr = new XMLHttpRequest();

    xhr.open("POST", 'get_list_to_load.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    var body = 'project_name=' + project_name;

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res = JSON.parse( this.responseText );
        var listToLoad = document.getElementById( "listToLoad" );
        var listCount = document.getElementById( "listCount" );

        listToLoad.innerHTML = res["listToLoad"];
        listCount.innerHTML = res["count"]

        getListToLoad()
    }

    xhr.send(body);
}