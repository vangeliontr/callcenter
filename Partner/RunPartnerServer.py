#!/usr/bin/env python3

import sys

sys.path.append("../")
from Tools.AuthServer import start_server


def run_server(port=None):

    if port is None:
        port = int(sys.argv[6])

    start_server("0.0.0.0", port, "Partner")


if __name__ == "__main__":
    run_server(8102)
