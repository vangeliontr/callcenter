function showAddPhone() {
    phone_control.hidden = false
}

function showAddContactPerson() {
    contact_person_info.hidden = false
}

function SelectComment(add_info) {
    comment = document.getElementById("comment_input").value;
    tf_product_control = document.getElementById("tf_product_control");

    product_list = ""

    for( var i = 1; i < tf_product_control.childElementCount; i++) {

        if( tf_product_control.children[i].children[0].checked ) {

            if(product_list.length != 0)
                product_list += ", "

            product_list += '"' + tf_product_control.children[i].children[0].id + '"';
        }
    }

    var recallPickerInp = document.getElementById("recallPickerInp")
    var currentBankSelector = document.getElementById("CurrentBankSelector")

    var add_data = ""

    if(add_info == "phone"){
        add_data = ', "add_phone": "' + phone_input.value + '"'
    }
    else if(add_info == "contact_person"){
        add_data = ', "add_contact_person": { "name": "' + name_input.value + '", "surname": "' +
        surname_input.value + '", "middlename": "' + middlename_input.value + '", "phone": "' + cpi_phone_input.value + '" }'
    }

    var tmp_val = '#{"Комментарий":"' + comment + '", ' +
                    '"Список продуктов": [' + product_list + '], ' +
                    '"Время звонка": "' + recallPickerInp.value + '", ' +
                    '"Предыдущий банк": "' + currentBankSelector.value + '", '+
                    '"Промокод": "psoldreg"' + add_data + '}#';

    if ( recall_last_value == tmp_val )
        return

    recall_last_value = tmp_val

    const el = document.createElement('textarea');

    el.value = tmp_val

    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);

    alert("Данные скопированы в буфер");
}

function OnSetMonth() {
    month_count = document.getElementById("month_count");
    range = document.getElementById("formControlRange");
    month_count.innerText = range.value;
};

function SelectSum() {
    sum_input = document.getElementById("sum_input");
    value = sum_input.value;

    value = parseInt(value);

    if(isNaN(value)) {
        alert("Вееденное значение в сумму кредита не число");
        return;
    }

    sum_input.value = value;

    if(value > 2000000) {
        alert("Сумма кредита не может превышать 2.000.000 рублей");
        return;
    }

    range = document.getElementById("formControlRange");

    const el = document.createElement('textarea');

    el.value = '#{"СрокКредита":' + range.value + ', "СуммаКредита":' + value + '}#';

    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);

    alert("Срок и сумма кредита скопированы в буфер");
}

function SelectBank() {
    ProjectSelector = document.getElementById("ProjectSelector")
    project = ProjectSelector.selectedOptions[0].label

    email_control = document.getElementById("email_control")
    month_control = document.getElementById("month_control")
    sum_control = document.getElementById("sum_control")
    region_control = document.getElementById("region_control")
    city_control = document.getElementById("city_control")
    office_control = document.getElementById("office_control")
    tf_product_control = document.getElementById("tf_product_control")
    comment_control = document.getElementById("comment_control")
    recall_control = document.getElementById("recall_control")
    recall_help = document.getElementById("recall_help")
    current_bank = document.getElementById("current_bank")

    if( project === "МТС" ) {
        email_control.hidden = false
        month_control.hidden = true
        sum_control.hidden = true
        region_control.hidden = true
        city_control.hidden = false
        office_control.hidden = false
        recall_control.hidden = true
        current_bank.hidden = true
    }
    else if( project === "Тинькофф действующий" ) {
        email_control.hidden = true
        month_control.hidden = true
        sum_control.hidden = true
        region_control.hidden = true
        city_control.hidden = true
        office_control.hidden = true
        tf_product_control.hidden = false
        comment_control.hidden = false
        recall_control.hidden = false
        recall_help.innerText = "Выберете дату и время звонка"
        current_bank.hidden = false

        addButton.hidden = false
        //surname_control.hidden = false
        //name_control.hidden = false
        //middlename_control.hidden = false
        //phone_control.hidden = false
    }
    else if(project === "Сбербанк" || project == "ВТБ24"){
        email_control.hidden = true
        month_control.hidden = true
        sum_control.hidden = true
        region_control.hidden = false
        city_control.hidden = false
        office_control.hidden = false
        recall_control.hidden = true
        tf_product_control.hidden = true
        comment_control.hidden = true
        current_bank.hidden = true

        if(project === "Сбербанк")
            recall_control.hidden = false
            recall_help.innerText = "Или выберете дату и время перезвона"
    }
    else if( project === "ПромСвязьБанк" || project === "РосСельхозБанк" ) {
        email_control.hidden = false
        month_control.hidden = true
        sum_control.hidden = true
        region_control.hidden = false
        city_control.hidden = false
        office_control.hidden = true
        tf_product_control.hidden = true
        comment_control.hidden = true
        recall_control.hidden = true
        current_bank.hidden = true
    }
    else {
        email_control.hidden = true
        month_control.hidden = false
        sum_control.hidden = false
        region_control.hidden = true
        city_control.hidden = true
        office_control.hidden = true
        recall_control.hidden = true
        current_bank.hidden = true
    }

    if(region_control.hidden === false)
        ShowBankRegions(project)
    else if (city_control.hidden === false)
        ShowBankRegionCity(project, "")
}

function SelectRegion() {
    ProjectSelector = document.getElementById("ProjectSelector")
    Bank = ProjectSelector.selectedOptions[0].label
    RegionSelector = document.getElementById("RegionSelector")
    region = RegionSelector.selectedOptions[0].id
    region_name = RegionSelector.selectedOptions[0].label
    ShowBankRegionCity(Bank, region, region_name)
}

function SelectCity() {
    ProjectSelector = document.getElementById("ProjectSelector")
    Bank = ProjectSelector.selectedOptions[0].label
    RegionSelector = document.getElementById("RegionSelector")
    region = RegionSelector.selectedOptions[0].id
    CitySelector = document.getElementById("CitySelector")
    city = CitySelector.selectedOptions[0].id

    office_control = document.getElementById("office_control")

    if(office_control.hidden){

        ProjectSelector = document.getElementById("ProjectSelector")
        project = ProjectSelector.selectedOptions[0].label

        const el = document.createElement('textarea');
        email = document.getElementById("email_input").value

        if( project === "ПромСвязьБанк") {
            el.value = "#ОфисБанка:" + email + ":" + city + "#";
        }
        else if( project === "РосСельхозБанк"){
            region_name = RegionSelector.selectedOptions[0].innerText
            el.value = "#ОфисБанка:" + email + ":" + region + ":" + region_name + ":" + city + "#";
        }

        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);

        alert("Адрес скопирован в буффер")
    }
    else
        ShowBankRegionCityOffice(Bank, region, city)
}

function SelectOffice() {
    ProjectSelector = document.getElementById("ProjectSelector")
    Bank = ProjectSelector.selectedOptions[0].label
    RegionSelector = document.getElementById("RegionSelector")
    region = RegionSelector.selectedOptions[0].id
    region_name = RegionSelector.selectedOptions[0].value
    CitySelector = document.getElementById("CitySelector")
    city = CitySelector.selectedOptions[0].id
    city_name = CitySelector.selectedOptions[0].value
    OfficeSelector = document.getElementById("OfficeSelector")
    office = OfficeSelector.selectedOptions[0].id
    office_name = OfficeSelector.selectedOptions[0].value

    const el = document.createElement('textarea');

    if( Bank === "МТС" ) {
        email = document.getElementById("email_input").value
        el.value = "#ОфисБанка:" + Bank + ":" + city + ":" + office + ":" + email + "#";
    }
    else
        el.value = "#ОфисБанка:" + Bank + ":" + region + ":" + city + ":" + office + "#";

    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);

    alert("Адрес скопирован в буффер")
}

function ShowBankRegionCityOffice(Bank, region, city) {
    var xhr = new XMLHttpRequest();

    var body = 'bank=' + Bank + "&region=" + region + "&city=" + city;

    xhr.open("POST", 'cgi-bin/get_bank_regions_city_office.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res_json = this.responseText.substring( this.responseText.search("{") - 1)
        var row_reg_list = JSON.parse( res_json );
        var region_list = document.getElementById( "OfficeSelector" )
        region_list.innerHTML = '';

        var empty = document.createElement("option");
        empty.setAttribute("data-hidden", "true")
        region_list.appendChild(empty)

        for( var i = 0; i < row_reg_list.length; i++ ) {
            var region = document.createElement("option");
            var row_region = row_reg_list[i]

            region.setAttribute("id", row_region.ID)
            region.textContent = row_region.name

            region_list.appendChild(region)
        }

        $("#OfficeSelector").selectpicker('refresh');
    }

    xhr.send(body);
}

function ShowBankRegionCity(Bank, region, region_name) {
    var xhr = new XMLHttpRequest();

    var body = 'bank=' + Bank + "&region=" + region;

    if(region_name){
        body += "&region_name="+region_name
    }

    xhr.open("POST", 'cgi-bin/get_bank_regions_city.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res_json = this.responseText.substring( this.responseText.search("{") - 1)
        var row_reg_list = JSON.parse( res_json );
        var region_list = document.getElementById( "CitySelector" )
        region_list.innerHTML = '';

        var empty = document.createElement("option");
        empty.setAttribute("data-hidden", "true")
        region_list.appendChild(empty)

        for( var i = 0; i < row_reg_list.length; i++ ) {
            var region = document.createElement("option");
            var row_region = row_reg_list[i]

            region.setAttribute("id", row_region.ID)
            region.textContent = row_region.name

            region_list.appendChild(region)
        }

        $("#CitySelector").selectpicker('refresh');
    }

    xhr.send(body);
}

function ShowBankRegions(Bank) {
    var xhr = new XMLHttpRequest();

    var body = 'bank=' + Bank;

    xhr.open("POST", 'cgi-bin/get_bank_regions.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res_json = this.responseText.substring( this.responseText.search("{") - 1)
        var row_reg_list = JSON.parse( res_json );
        var region_list = document.getElementById( "RegionSelector" )
        region_list.innerHTML = '';

        var empty = document.createElement("option");
        empty.setAttribute("data-hidden", "true")
        region_list.appendChild(empty)

        for( var i = 0; i < row_reg_list.length; i++ ) {
            var region = document.createElement("option");
            var row_region = row_reg_list[i]

            region.setAttribute("id", row_region.ID)
            region.textContent = row_region.name

            region_list.appendChild(region)
        }

        $("#RegionSelector").selectpicker('refresh');
    }

    xhr.send(body);
}







function SelectMenu(id) {
    var menu = document.getElementById( id );
    menu.classList.add("active");
    var span = document.createElement('span');
    span.classList.add("sr-only");
    span.value = '(current)';
    menu.children[0].appendChild(span);
}

function appenToTable( tr, text ) {
    var td = document.createElement("td");
    var value = document.createTextNode(text);
    td.appendChild(value);
    tr.appendChild(td)
}

function appenLinkToTable( tr, href, text ) {
    var td = document.createElement("td");
    var link = document.createElement("a");
    var value = document.createTextNode(text);
    link.href = href;
    link.appendChild(value);
    td.appendChild(link);
    tr.appendChild(td)
}

function refreshLastCalls() {
    var table = document.getElementById( "last_contact_content" )
    table.innerHTML = ""
    loadLastCalls()
}

function findOrganization(){
    inn = document.getElementById("find_organization_inn").value
    id = document.getElementById("find_organization_id").value

    var xhr = new XMLHttpRequest();

    var body = 'find_organization_inn=' + inn;
    body += '&find_organization_id=' + id;

    xhr.open("POST", 'find_organization.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res_json = this.responseText.substring( this.responseText.search("{") - 1)
        var contact_list = JSON.parse( res_json );
        var table = document.getElementById( "find_organization_content" )

        for( var i = 0; i < contact_list.length; i++ ) {
            var tr = document.createElement("tr");
            var row = contact_list[i];

            appenToTable(tr, row["Type"])
            appenLinkToTable(tr, 'organization.py?id=' + row["ID"], row["Name"])
            appenToTable(tr, row["INN"])
            appenToTable(tr, row["OpenDate"])
            appenToTable(tr, row["Address"])
            appenToTable(tr, row["TimeZone"])

            table.appendChild(tr)
        }
    }

    xhr.send(body);
}

function findContactPerson(){
    tel = document.getElementById("find_contact_person_tel").value
    surname = document.getElementById("find_contact_person_surname").value
    name = document.getElementById("find_contact_person_name").value
    middlename = document.getElementById("find_contact_person_middlename").value
    email = document.getElementById("find_contact_person_email").value

    var xhr = new XMLHttpRequest();

    var body = 'contact_person_tel=' + tel;
    body += '&contact_person_surname=' + surname;
    body += '&contact_person_name=' + name;
    body += '&contact_person_middlename=' + middlename;
    body += '&contact_person_email=' + email;

    xhr.open("POST", 'find_contact_person.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res_json = this.responseText.substring( this.responseText.search("{") - 1)
        var contact_list = JSON.parse( res_json );
        var table = document.getElementById( "find_contact_person_content" )
        table.innerHTML = ""

        if(contact_list.length == 0){
          alert("Не удалось найти контактное лицо")
        }

        for( var i = 0; i < contact_list.length; i++ ) {
            var tr = document.createElement("tr");
            var row = contact_list[i];

            appenLinkToTable(tr, 'contact_person.py?id=' + row["ContactPersonID"], row["Surname"])
            appenToTable(tr, row["Name"])
            appenToTable(tr, row["MiddleName"])

            table.appendChild(tr)
        }
    }

    xhr.send(body);
}

function findCall() {
    call_id = document.getElementById("find_call_id").value

    var xhr = new XMLHttpRequest();

    var body = 'call_id=' + call_id;

    xhr.open("POST", 'find_call.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res_json = this.responseText.substring( this.responseText.search("{") - 1)
        var contact_list = JSON.parse( res_json );
        var table = document.getElementById( "find_contact_content" )

        for( var i = 0; i < contact_list.length; i++ ) {
            var tr = document.createElement("tr");
            var row = contact_list[i];

            appenLinkToTable(tr, 'organization.py?id=' + row["OrganizationID"], row["Name"])
            appenToTable(tr, row["INN"])
            appenToTable(tr, row["Address"])
            appenToTable(tr, row["OpenDate"])
            appenToTable(tr, row["TimeZone"])
            appenLinkToTable(tr, 'contact_person.py?id=' + row["ContactPersonID"], row["Surname"])
            appenToTable(tr, row["ContactPersonName"])
            appenToTable(tr, row["DateTime"])
            appenToTable(tr, row["Result"])
            appenToTable(tr, row["ProjectName"])
            appenToTable(tr, row["EmployeeName"])
            appenToTable(tr, row["EmployeeSurname"])

            table.appendChild(tr)
        }
    }

    xhr.send(body);
}

function restartRobort(robot) {
    var xhr = new XMLHttpRequest();

    xhr.open("POST", 'restart_robot.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send("robot=" + robot);
}

function startActivityMonitoring() {
    var xhr = new XMLHttpRequest();

    xhr.open("POST", 'get_robot_info.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var contact_list = JSON.parse( this.responseText );
        var activityTF = document.getElementById( "activityTF" );
        var activitySB = document.getElementById( "activitySB" );
        var activityAL = document.getElementById( "activityAL" );

        activityTF.innerHTML = contact_list["activityTF"];
        activitySB.innerHTML = contact_list["activitySB"];
        activityAL.innerHTML = contact_list["activityAL"];

        var downloadContent = document.getElementById( "update" );
        downloadContent.innerHTML = "Обновлено: " + new Date();

        //setTimeout(startActivityMonitoring, 3000);
        startActivityMonitoring()
    }

    xhr.send("page=0");
}

function startMonitoring() {
    var xhr = new XMLHttpRequest();

    var body = 'page=0';

    xhr.open("POST", 'get_monitor_info.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var contact_list = JSON.parse( this.responseText );
        var downloadContent = document.getElementById( "downloadContent" );
        var ODPContent = document.getElementById( "ODPContent" );
        var workContentAlfa = document.getElementById( "workContentAlfa" );
        var workContentSber = document.getElementById( "workContentSber" );
        var workContentTF = document.getElementById( "workContentTF" );
        var processedContent = document.getElementById( "processedContent" );

        downloadContent.innerHTML = contact_list["downloadContent"];
        ODPContent.innerHTML = contact_list["ODPContent"];
        workContentTF.innerHTML = contact_list["workContentTF"];
        workContentAlfa.innerHTML = contact_list["workContentAlfa"];
        workContentSber.innerHTML = contact_list["workContentSber"];
        processedContent.innerHTML = contact_list["processedContent"];

        var downloadContent = document.getElementById( "update" );
        downloadContent.innerHTML = "Обновлено: " + new Date();

        startMonitoring()
    }

    xhr.send(body);
}

function startMonitoringDownload() {
    var xhr = new XMLHttpRequest();

    var body = 'page=0';

    xhr.open("POST", 'get_download_info.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var contact_list = JSON.parse( this.responseText );
        var downloadContent = document.getElementById( "downloadContent" );

        downloadContent.innerHTML = contact_list["downloadContent"];

        var downloadContent = document.getElementById( "update" );
        downloadContent.innerHTML = "Обновлено: " + new Date();

        startMonitoringDownload()
    }

    xhr.send(body);
}

function InitStat() {
    var start_date = $("#datetimepickerstart").data("DateTimePicker").date();
    var end_date = $("#datetimepickerend").data("DateTimePicker").date();

    start_date = start_date.year() + "-" + (start_date.month() + 1) + "-" + start_date.date()
    end_date = end_date.year() + "-" + (end_date.month() + 1) + "-" + end_date.date()

    mode = "NonGroup"

    if( $("#TimeZoneGroup")[0].checked ){
        mode = "TimeZoneGroup"
    }

    if( $("#RegionGroup")[0].checked ){
        mode = "RegionGroup"
    }

    detail = "Days"

    if( $("#Month")[0].checked ){
        detail = "Month"
    }

    project_list = document.getElementsByName("project")
    project_id = "1"

    for( var i = 0; i < project_list.length; i++ )
        if( project_list[i].checked )
            project_id = project_list[i].id

    var xhr = new XMLHttpRequest();

    var body = 'mode=' + mode;
    body += '&start_date=' + start_date;
    body += '&end_date=' + end_date;
    body += '&detail=' + detail;
    body += '&project=' + project_id;

    xhr.open("POST", 'get_statistics.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res_json = this.responseText.substring( this.responseText.search("{") - 1)
        var res = JSON.parse( res_json );
        var table = document.getElementById( "StatBody" )

        table.innerHTML = res["statContent"];
    }

    xhr.send(body);
}

function loadLastCalls() {
    var xhr = new XMLHttpRequest();

    var body = 'page=0';

    xhr.open("POST", 'get_last_contacts.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res_json = this.responseText.substring( this.responseText.search("{") - 1)
        var contact_list = JSON.parse( res_json );
        var table = document.getElementById( "last_contact_content" )

        for( var i = 0; i < contact_list.length; i++ ) {
            var tr = document.createElement("tr");
            var row = contact_list[i];

            appenLinkToTable(tr, 'organization.py?id=' + row["OrganizationID"], row["Name"])
            appenToTable(tr, row["INN"])
            appenToTable(tr, row["Address"])
            appenToTable(tr, row["OpenDate"])
            appenToTable(tr, row["TimeZone"])
            appenLinkToTable(tr, 'contact_person.py?id=' + row["ContactPersonID"], row["Surname"])
            appenToTable(tr, row["ContactPersonName"])
            appenToTable(tr, row["DateTime"])
            appenToTable(tr, row["Result"])
            appenToTable(tr, row["ProjectName"])
            appenToTable(tr, row["EmployeeName"])
            appenToTable(tr, row["EmployeeSurname"])

            table.appendChild(tr)
        }
    }

    xhr.send(body);
}

function GetCallStat() {
    var start_date = $("#datetimepickerstart").data("DateTimePicker").date();
    var end_date = $("#datetimepickerend").data("DateTimePicker").date();

    start_date = start_date.year() + "-" + (start_date.month() + 1) + "-" + start_date.date() + " " + start_date.hour() + ":00:00"
    end_date = end_date.year() + "-" + (end_date.month() + 1) + "-" + end_date.date() + " " + end_date.hour() + ":00:00"

    mode = "NonGroup"

    if( $("#OperatorGroup")[0].checked ){
        mode = "OperatorGroup"
    }

    if( $("#TimeZoneGroup")[0].checked ){
        mode = "TimeZoneGroup"
    }

    if( $("#RegionGroup")[0].checked ){
        mode = "RegionGroup"
    }

    detail = "Days"

    if( $("#Month")[0].checked ){
        detail = "Month"
    }

    if( $("#Hours")[0].checked ){
        detail = "Hours"
    }

    var xhr = new XMLHttpRequest();

    var body = 'mode=' + mode;
    body += '&start_date=' + start_date;
    body += '&end_date=' + end_date;
    body += '&detail=' + detail;

    xhr.open("POST", 'get_call_statistics.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res_json = this.responseText.substring( this.responseText.search("{") - 1)
        var res = JSON.parse( res_json );
        var table = document.getElementById( "StatBody" )

        table.innerHTML = res["statContent"];
    }

    xhr.send(body);
}

function setOdpCheck(){
    var odpCheck = document.getElementById( "odpCheck" );
    odpCheck.checked = true
}

function getOrganizationList(){
    var downloadList = document.getElementById( "downloadList" );
    var downloadId = downloadList.getAttribute("downloadId");

    var operationList = document.getElementById( "operationList" );
    var operationId = operationList.getAttribute("operationId");

    var validList = document.getElementById( "validList" );
    var validId = validList.getAttribute("validId");

    var odpCheck = document.getElementById( "odpCheck" );
    var firstNumberCheck = document.getElementById( "firstNumberCheck" );

    var xhr = new XMLHttpRequest();

    var body = 'downloadId=' + downloadId;

    if($("#StateData")[0].checked)
        body += "&status=" + $("#StateDataInput")[0].value;

    body += "&operation=" + operationId
    body += "&valid=" + validId

    if( odpCheck.checked )
        body += "&odpCheck=Да"

    if( firstNumberCheck.checked )
        body += "&firstNumberCheck=Да"

    xhr.open("POST", 'get_organization_list.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res_json = this.responseText.substring( this.responseText.search("{") - 1)
        var contact_list = JSON.parse( res_json );
        var table = document.getElementById( "organization_content" )
        table.innerHTML = ""

        for( var i = 0; i < contact_list.length; i++ ) {
            var tr = document.createElement("tr");
            var row = contact_list[i];

            appenToTable(tr, row["Type"])
            appenLinkToTable(tr, '../organization.py?id=' + row["ID"], row["Name"])
            appenToTable(tr, row["INN"])
            appenToTable(tr, row["OpenDate"])
            appenToTable(tr, row["Address"])
            appenToTable(tr, row["TimeZone"])

            table.appendChild(tr)
        }

        var count_head = document.getElementById( "count_head" );

        count_head.innerHTML = "Количество: " + contact_list.length
    }

    xhr.send(body);
}

function setStatus(status) {
    $("#StateData").click();
    $("#StateDataInput")[0].value  = status
}

function setDownload(id,name){
    var downloadList = document.getElementById( "downloadList" )
    downloadList.innerHTML = name
    downloadList.setAttribute("downloadId", id)
}

function setOperation(id,name){
    var operationList = document.getElementById( "operationList" )
    operationList.innerHTML = name
    operationList.setAttribute("operationId", id)
}

function setValid(name){
    var validList = document.getElementById( "validList" )
    validList.innerHTML = name
    validList.setAttribute("validId", name)
}

function getPilotInfo( pilot_id, pilot_name ) {
    var pilot_head = document.getElementById( "pilot_head" )
    pilot_head.innerHTML = "Пилот: " + pilot_name

    var update = document.getElementById( "update" )
    update.innerHTML = "Построение отчета"

    var body = 'pilot_id=' + pilot_id;

    var xhr = new XMLHttpRequest();
    xhr.open("POST", 'get_pilot_info.py', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if( this.readyState != 4 )
            return;

        var res = JSON.parse( this.responseText );
        var table = document.getElementById( "PilotInfo" )

        table.innerHTML = res["PilotInfo"];
    }

    xhr.send(body);
}

recall_last_value = null

function SelectReCall(){

    var tf_product_control = document.getElementById("tf_product_control");

    if(tf_product_control.hidden == false)
        return

    var recallPickerInp = document.getElementById( "recallPickerInp" )

    if ( recall_last_value == '#{"Время перезвона":' + recallPickerInp.value + '}#' )
        return

    recall_last_value = '#{"Время перезвона":' + recallPickerInp.value + '}#'

    const el = document.createElement('textarea');
    el.value = recall_last_value;

    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);

    alert("Время перезвона скопировано в буффер")
}