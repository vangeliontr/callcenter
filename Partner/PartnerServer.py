#!/usr/bin/env python3

import sys
import json
import functools

sys.path.append("../")

from RunPartnerServer import run_server
from Tools.Daemon import Daemon


conf = json.loads(open("partner_server_settings.txt", "r").read())
run_server_with_port = functools.partial(run_server, conf["port"])

Daemon.run(run_server_with_port, conf)
