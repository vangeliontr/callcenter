import sys
from http.server import CGIHTTPRequestHandler, HTTPServer
from socketserver import ThreadingMixIn


def start_server(host, port, request_handler=CGIHTTPRequestHandler):
    class ThreadingSimpleServer(ThreadingMixIn, HTTPServer):
        pass

    server = ThreadingSimpleServer((host, port), request_handler)

    try:
        while 1:
            sys.stdout.flush()
            server.handle_request()
    except KeyboardInterrupt:
        print("\nShutting down server per users request.")
