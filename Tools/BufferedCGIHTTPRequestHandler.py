from http.server import CGIHTTPRequestHandler
from io import BytesIO, FileIO


class MyBytesIO(BytesIO):

    def __init__(self):
        self.tmp_file = FileIO("./tmp.tmp", "a")

    def fileno(self):
        return self.tmp_file.fileno()


class BufferedCGIHTTPRequestHandler(CGIHTTPRequestHandler):

    def run_cgi(self):
        """
        Post-process CGI script response before sending to client.
        Override HTTP status line with value of "Status:" header, if set.
        """

        self.original_wfile = self.wfile
        self.wfile = MyBytesIO()

        CGIHTTPRequestHandler.run_cgi(self)

        is_have_custom_error = False

        self.wfile.seek(0)
        body = None
        headers = []
        for line in self.wfile:
            headers.append(line)  # includes new line character
            if line.strip() == '':  # blank line signals end of headers
                body = self.wfile.read()
                break
            elif line.startswith(b'Status:'):
                # Use status header to override premature HTTP status line.
                # Header format is: "Status: code message"
                is_have_custom_error = True
                status = line.split(b':')[1].strip()
                headers[0] = b'%s %s' % (bytes(self.protocol_version, 'utf-8'), status)

        if is_have_custom_error:
            self.original_wfile.write(b''.join(headers))

            if body is not None:
                self.original_wfile.write(body)
        else:
            self.wfile.seek(0)
            for line in self.wfile:
                self.original_wfile.write(line)
