import requests
import random
import os

from datetime import datetime, timedelta


class ProxyHub:

    def __init__(self):

        self.proxy_list = []

        self.proxy_list.append({"Adress": "https://kotbakinskij7468:054248@37.203.242.244:10653", "Date": None})

        #try:
        #    requests.get("https://egrul.nalog.ru/", timeout=1, proxies={'https': "https://kotbakinskij7468:054248@37.203.242.244:10653"})
        #except Exception as e:
        #    pass

        #resp = requests.get("https://www.proxy-list.download/api/v1/get?type=https")
        #for proxy in resp.text.split("\r\n"):
        #    if proxy:
        #        print(f"Проверка прокси {proxy}")

        #        try:
        #            requests.get("https://egrul.nalog.ru/", timeout=1, proxies={'https': proxy})
        #        except Exception as e:
        #            error_text = str(e)

        #            if error_text.find("timeout") == -1 and error_text.find("timed out") == -1:
        #                print(f"Отсеяли {proxy} по причине {e}")
        #                continue

        #        self.proxy_list.append({"Adress": proxy, "Date": None})

        #params_file_name = os.path.dirname(__file__) + "/proxy_list.txt"
        #source_file = open(params_file_name, "r")

        #for line in source_file.readlines():
        #    self.proxy_list.append({"Adress": line.replace("\n", ""), "Date": None})

    def test_proxy(self, url, data=None, headers=None):

        num = 0

        for proxy in self.proxy_list:

            num += 1

            try:
                res = requests.post(url, data=data, headers=headers, proxies={'https': proxy["Adress"]})
            except Exception as e:
                res = None
                print("{} Прокси {} ошибка: {}".format(num, proxy, e))
            else:
                print("{} Прокси {} ок".format(num, proxy))

        return res

    def get(self, url, data=None, headers=None):

        res = None

        while res is None:

            num = random.randint(0, len(self.proxy_list) - 1)

            if self.proxy_list[num]["Date"] is not None:
                if self.proxy_list[num]["Date"] > datetime.now():
                    continue

            #print("Прокси {}".format(num))

            try:
                res = requests.get(url, data=data, headers=headers, proxies={'https': self.proxy_list[num]["Adress"]})
            except Exception as e:
                res = None
                #self.proxy_list[num]["Date"] = datetime.now() + timedelta(minutes = 10)
                print("Прокси {} ошибка {}".format(num, e))

        return res

    def post(self, url, data=None, headers=None):

        res = None

        while res is None:

            num = random.randint(0, len(self.proxy_list) - 1)

            if self.proxy_list[num]["Date"] is not None:
                if self.proxy_list[num]["Date"] > datetime.now():
                    print("Change proxy")
                    continue

            #print("Прокси {}".format(num))

            try:
                res = requests.post(url, data=data, headers=headers, proxies={'https': self.proxy_list[num]["Adress"]})
            except Exception as e:
                res = None
                #self.proxy_list[num]["Date"] = datetime.now() + timedelta(hours = 2)
                print("Прокси {} ошибка: {}".format(num, e))

        return res
