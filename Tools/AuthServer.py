import sys
import base64
import json
import cgi
import os
import copy
import select
import urllib.parse
import ssl

from datetime import datetime, timedelta
from random import randint
from ipaddress import ip_address
from ipaddress import ip_network

from http import HTTPStatus

from http.cookies import SimpleCookie
from http.server import CGIHTTPRequestHandler, HTTPServer, nobody_uid
from socketserver import ThreadingMixIn
from urllib.parse import urlparse, parse_qs

from DataBase.DBController import DBController
from DataBase.HttpHistory import HttpHistory
from DataBase.Task import Task


class CustomServerHandler(CGIHTTPRequestHandler):

    def send_to_telega(self, controller, message):
        Task.set_task_impl(controller, "Оповещение проблем", json.dumps({"message": message}))
        pass

    def do_HEAD(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def do_AUTHHEAD(self):
        self.send_response(401)
        self.send_header(
            'WWW-Authenticate', 'Basic realm="Demo Realm"')
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def do_GET(self):
        ''' Present frontpage with user authentication. '''
        if self.headers.get('Authorization') == None:
            self.do_AUTHHEAD()

            response = {
                'success': False,
                'error': 'No auth header received'
            }

            self.wfile.write(bytes(json.dumps(response), 'utf-8'))
            return

        user_rec = self.server.is_auth(self.headers.get('Authorization'), self)

        if user_rec is None:
            self.do_AUTHHEAD()

            response = {
                'success': False,
                'error': 'Invalid credentials'
            }

            self.wfile.write(bytes(json.dumps(response), 'utf-8'))
            return

        if not self.server.is_allow_ip(self.client_address[0]):

            cookies = self.headers.get('Cookie')

            if cookies is not None:
                cookies = [{"name": item.split("=")[0], "value": item.split("=")[1]} for item in cookies.split("; ")]
            else:
                cookies = []

            sid = None
            tc = b""

            for cookie in cookies:

                if cookie["name"] == "TempCode":
                    tc += b";" + cookie["value"].encode()

                    if cookie["value"] == user_rec["TempCode"]:
                        sid = user_rec["TempCode"]

            if sid is None:
                is_send = self.server.generate_and_send_temp_code(user_rec)

                self.send_response(200)
                self.end_headers()

                page = open("ip_error_page.html", "rb").read()

                comment = tc

                #page = page.replace(b"{comment}", comment)

                self.wfile.write(page)
                return

        controller = DBController()
        history = HttpHistory(controller)

        history.add_log_data(self.command, self.path, self.client_address[0], self.server.site,
                             self.server.user(self.headers.get('Authorization')))

        return super().do_GET()

    def do_POST(self):

        controller = DBController()

        ''' Present frontpage with user authentication. '''
        if self.server.site != 'SkzAPI' and self.headers.get('Authorization') == None:
            self.do_AUTHHEAD()

            response = {
                'success': False,
                'error': 'No auth header received'
            }

            self.wfile.write(bytes(json.dumps(response), 'utf-8'))
            return

        user_rec = self.server.is_auth(self.headers.get('Authorization'), self)

        if user_rec is None:
            #self.do_AUTHHEAD()

            response = {
                'success': False,
                'error': 'Invalid credentials'
            }

            self.wfile.write(bytes(json.dumps(response), 'utf-8'))
            return

        if not self.server.is_allow_ip(self.client_address[0]):

            if self.server.site == "SkzAPI":
                self.send_to_telega(controller, f"Отказ в доступе на API для IP {self.client_address[0]}")

            cookies = SimpleCookie(self.headers.get('Cookie'))

            for cookie in cookies:

                if cookie == "TempCode":
                    if cookies["TempCode"].value == user_rec["TempCode"]:
                        sid = user_rec["TempCode"]

            if sid is None:
                self.do_AUTHHEAD()

                response = {
                    'success': False,
                    'error': 'IP адрес не разрешен'
                }

                self.wfile.write(bytes(json.dumps(response), 'utf-8'))
                return

        history = HttpHistory(controller)

        history.add_log_data(self.command, self.path, self.client_address[0], self.server.site,
                             self.server.user(self.headers.get('Authorization')))

        return super().do_POST()

    def _parse_POST(self):
        ctype, pdict = cgi.parse_header(self.headers.getheader('content-type'))
        if ctype == 'multipart/form-data':
            postvars = cgi.parse_multipart(self.rfile, pdict)
        elif ctype == 'application/x-www-form-urlencoded':
            length = int(self.headers.getheader('content-length'))
            postvars = cgi.parse_qs(
                self.rfile.read(length), keep_blank_values=1)
        else:
            postvars = {}

        return postvars

    def _parse_GET(self):
        getvars = parse_qs(urlparse(self.path).query)

        return getvars

    def is_cgi(self):
        path = self.path

        separator_pos = path.find("?")

        if separator_pos != -1:
            path = path[0:separator_pos]

        filename, file_extension = os.path.splitext(path)

        if file_extension != ".py":
            return False

        return super().is_cgi()

    def run_cgi(self):
        """Execute a CGI script."""
        dir, rest = self.cgi_info
        path = dir + '/' + rest
        i = path.find('/', len(dir)+1)
        while i >= 0:
            nextdir = path[:i]
            nextrest = path[i+1:]

            scriptdir = self.translate_path(nextdir)
            if os.path.isdir(scriptdir):
                dir, rest = nextdir, nextrest
                i = path.find('/', len(dir)+1)
            else:
                break

        # find an explicit query string, if present.
        rest, _, query = rest.partition('?')

        # dissect the part after the directory name into a script name &
        # a possible additional path, to be stored in PATH_INFO.
        i = rest.find('/')
        if i >= 0:
            script, rest = rest[:i], rest[i:]
        else:
            script, rest = rest, ''

        scriptname = dir + '/' + script
        scriptfile = self.translate_path(scriptname)
        if not os.path.exists(scriptfile):
            self.send_error(
                HTTPStatus.NOT_FOUND,
                "No such CGI script (%r)" % scriptname)
            return
        if not os.path.isfile(scriptfile):
            self.send_error(
                HTTPStatus.FORBIDDEN,
                "CGI script is not a plain file (%r)" % scriptname)
            return
        ispy = self.is_python(scriptname)
        if self.have_fork or not ispy:
            if not self.is_executable(scriptfile):
                self.send_error(
                    HTTPStatus.FORBIDDEN,
                    "CGI script is not executable (%r)" % scriptname)
                return

        # Reference: http://hoohoo.ncsa.uiuc.edu/cgi/env.html
        # XXX Much of the following could be prepared ahead of time!
        env = copy.deepcopy(os.environ)
        env['SERVER_SOFTWARE'] = self.version_string()
        env['SERVER_NAME'] = self.server.server_name
        env['GATEWAY_INTERFACE'] = 'CGI/1.1'
        env['SERVER_PROTOCOL'] = self.protocol_version
        env['SERVER_PORT'] = str(self.server.server_port)
        env['REQUEST_METHOD'] = self.command
        uqrest = urllib.parse.unquote(rest)
        env['PATH_INFO'] = uqrest
        env['PATH_TRANSLATED'] = self.translate_path(uqrest)
        env['SCRIPT_NAME'] = scriptname
        if query:
            env['QUERY_STRING'] = query
        env['REMOTE_ADDR'] = self.client_address[0]
        authorization = self.headers.get("authorization")
        if authorization:
            authorization = authorization.split()
            if len(authorization) == 2:
                import base64, binascii
                env['AUTH_TYPE'] = authorization[0]
                if authorization[0].lower() == "basic":
                    try:
                        authorization = authorization[1].encode('ascii')
                        authorization = base64.decodebytes(authorization).\
                                        decode('ascii')
                    except (binascii.Error, UnicodeError):
                        pass
                    else:
                        authorization = authorization.split(':')
                        if len(authorization) == 2:
                            env['REMOTE_USER'] = authorization[0]
        # XXX REMOTE_IDENT
        if self.headers.get('content-type') is None:
            env['CONTENT_TYPE'] = self.headers.get_content_type()
        else:
            env['CONTENT_TYPE'] = self.headers['content-type']
        length = self.headers.get('content-length')
        if length:
            env['CONTENT_LENGTH'] = length
        referer = self.headers.get('referer')
        if referer:
            env['HTTP_REFERER'] = referer
        accept = []
        for line in self.headers.getallmatchingheaders('accept'):
            if line[:1] in "\t\n\r ":
                accept.append(line.strip())
            else:
                accept = accept + line[7:].split(',')
        env['HTTP_ACCEPT'] = ','.join(accept)
        ua = self.headers.get('user-agent')
        if ua:
            env['HTTP_USER_AGENT'] = ua
        co = filter(None, self.headers.get_all('cookie', []))
        cookie_str = ', '.join(co)
        if cookie_str:
            env['HTTP_COOKIE'] = cookie_str
        # XXX Other HTTP_* headers
        # Since we're setting the env in the parent, provide empty
        # values to override previously set values
        for k in ('QUERY_STRING', 'REMOTE_HOST', 'CONTENT_LENGTH',
                  'HTTP_USER_AGENT', 'HTTP_COOKIE', 'HTTP_REFERER'):
            env.setdefault(k, "")

        self.send_response(HTTPStatus.OK, "Script output follows")
        self.flush_headers()

        decoded_query = query.replace('+', ' ')

        if False: #self.have_fork:
            # Unix -- fork as we should
            args = [script]
            if '=' not in decoded_query:
                args.append(decoded_query)
            nobody = nobody_uid()
            self.wfile.flush() # Always flush before forking
            pid = os.fork()
            if pid != 0:
                # Parent
                pid, sts = os.waitpid(pid, 0)
                # throw away additional data [see bug #427345]
                while select.select([self.rfile], [], [], 0)[0]:
                    if not self.rfile.read(1):
                        break
                if sts:
                    self.log_error("CGI script exit status %#x", sts)
                return
            # Child
            try:
                try:
                    os.setuid(nobody)
                except OSError:
                    pass
                os.dup2(self.rfile.fileno(), 0)
                os.dup2(self.wfile.fileno(), 1)
                os.execve(scriptfile, args, env)
            except:
                self.server.handle_error(self.request, self.client_address)
                os._exit(127)

        else:
            # Non-Unix -- use subprocess
            import subprocess
            cmdline = [scriptfile]
            if self.is_python(scriptfile):
                interp = sys.executable
                if interp.lower().endswith("w.exe"):
                    # On Windows, use python.exe, not pythonw.exe
                    interp = interp[:-5] + interp[-4:]
                cmdline = [interp, '-u'] + cmdline
            if '=' not in query:
                cmdline.append(query)
            self.log_message("command: %s", subprocess.list2cmdline(cmdline))
            try:
                nbytes = int(length)
            except (TypeError, ValueError):
                nbytes = 0
            p = subprocess.Popen(cmdline,
                                 stdin=subprocess.PIPE,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE,
                                 env = env
                                 )
            if self.command.lower() == "post" and nbytes > 0:
                test_len = nbytes
                data = b''

                while test_len > 0:
                    part = self.rfile.read(test_len)
                    data += part
                    test_len = test_len - len(part)
            else:
                data = None
            # throw away additional data [see bug #427345]
            while select.select([self.rfile._sock], [], [], 0)[0]:
                if not self.rfile._sock.recv(1):
                    break
            stdout, stderr = p.communicate(data)
            self.wfile.write(stdout)
            if stderr:
                self.log_error('%s', stderr)
            p.stderr.close()
            p.stdout.close()
            status = p.returncode
            if status:
                self.log_error("CGI script exit status %#x", status)
            else:
                self.log_message("CGI script exited OK")


class CustomHTTPServer(HTTPServer):
    key = ''

    def __init__(self, address, handlerClass=CustomServerHandler):
        super().__init__(address, handlerClass)
        self.key = base64.b64encode(
            bytes('%s:%s' % ('demo', 'demo'), 'utf-8')).decode('ascii')

    def set_auth(self, username, password):
        self.key = base64.b64encode(
            bytes('%s:%s' % (username, password), 'utf-8')).decode('ascii')

    def get_auth_key(self):
        return self.key

    def user(self, auth_data):

        if auth_data is None:
            return "anonimous"

        b64_str = auth_data.replace('Basic ', "")
        login_pass = base64.b64decode(b64_str)
        login_pass = login_pass.decode()
        auth_data = login_pass.split(":")

        return auth_data[0]

    def is_allow_ip(self, ip):
        controller = DBController()
        cur = controller.get_cursor()

        cur.execute("SELECT * FROM IPList;")
        allow_ip_list = cur.fetchall()

        for allow_ip_rec in allow_ip_list:

            if ip_address(ip) in ip_network(allow_ip_rec["IP"]):
                return True

        return False

    def is_auth(self, auth_data, server=None):

        if self.site == "SkzAPI":
            return True

        b64_str = auth_data.replace('Basic ', "")
        login_pass = base64.b64decode(b64_str)
        login_pass = login_pass.decode()
        auth_data = login_pass.split(":")

        controller = DBController()
        cur = controller.get_cursor()

        cur.execute("SELECT * FROM Users WHERE Login = '{}' AND Password = '{}' AND Site = '{}';".
                    format(auth_data[0], auth_data[1], self.site))
        user_rec = cur.fetchone()

        if user_rec is None:
            return None

        if server is None or user_rec["Allows"] is None:
            return user_rec

        if server.path.find("cgi-bin") == -1:
            return user_rec

        try:
            allows = json.loads(user_rec["Allows"])
        except:
            return user_rec

        path_is_allow = False
        allow_elem = None

        for elem in allows:
            if server.path.find(elem) != -1:
                path_is_allow = True
                allow_elem = elem
                break

        if not path_is_allow:
            return None

        if allows[allow_elem]:
            server.path += "&allows={}".format(json.dumps(allows[allow_elem]))

        return user_rec

    def generate_and_send_temp_code(self, user_rec):

        # Если 5ти мин не прошло новый код не высылаем
        if datetime.now() - user_rec["TempCodeDate"] < timedelta(minutes=5):
            return False

        controller = DBController()
        tmp_value = randint(100000, 999999)

        cur = controller.get_cursor()

        cur.execute("""
            UPDATE Users SET TempCode = {}, TempCodeDate = NOW()
            WHERE ID = {}
        """.format(tmp_value, user_rec["ID"]))

        Task.set_task_impl(controller, "Отправка сообщения пользователю",
                           json.dumps({"message": tmp_value,
                                       "user": user_rec["TelegrammContact"]}))


def start_server(HOST, PORT, site="Admin"):
    class ThreadingSimpleServer(ThreadingMixIn, CustomHTTPServer):
        pass

    server = ThreadingSimpleServer((HOST, PORT), CustomServerHandler)
    server.have_fork = False
    server.site = site

    try:
        open("../{}_key.pem".format(site), "r")
        server.socket = ssl.wrap_socket(server.socket,
                                        keyfile="../{}_key.pem".format(site),
                                        certfile="../{}_cert.pem".format(site),
                                        server_side=True)
    except:
        pass

    CustomServerHandler.have_fork = False

    try:
        while 1:
            sys.stdout.flush()
            server.handle_request()
    except KeyboardInterrupt:
        print("\nShutting down server per users request.")
