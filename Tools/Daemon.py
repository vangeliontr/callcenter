#!/usr/bin/env python3

import os
import sys
from daemon import runner


class Daemon:

    @staticmethod
    def run(func, conf):
        cur_path = conf["path"]
        os.chdir(cur_path)

        class App:
            def __init__(self):
                self.stdin_path = '/dev/null'
                self.pidfile_timeout = 5
                self.stdout_path = conf["log"]
                self.stderr_path = conf["err"]
                self.pidfile_path = conf["pid"]

            def run(self):
                os.chdir(cur_path)
                func()

        app = App()
        daemon_runner = runner.DaemonRunner(app)
        daemon_runner.do_action()
