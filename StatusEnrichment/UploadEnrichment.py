from datetime import datetime, timedelta
from CallCenter.Skorozvon import Skorozvon
from DataBase.Upload import Upload
from DataBase.Project import Project
from DataBase.Call import Call

def enrich_status(params, set_res):

    skorozvon = Skorozvon()

    project_list = params["Список проектов"]

    for project_name in project_list:
        project = Project.get_by_name(project_name)
        project_params = project.params()

        status_list = skorozvon.get_status_by_project(project_params["Скорозвон"]["scenario_id"])

        for status in status_list:
            result_params = project_params["Скорозвон"]["results"][str(status["result_id"])]

            if "send" in result_params:
                continue

            call = Call.by_ext_id(ext_call_id=status["organization_id"])

            call.set_result(status["result_name"])

        pass

def enrich_upload_status(params, set_res):

    end_dt = datetime.now()
    start_dt = end_dt - timedelta(days=(int(params["Days"])-1))

    start_dt = start_dt.strftime("%d.%m.%Y")
    end_dt = end_dt.strftime("%d.%m.%Y")

    upload_list = Upload.list(start_date=start_dt, end_date=end_dt, limit=None, offset=None)

    skorozvon = Skorozvon()

    status_res = {}
    status_res["Обогащаются подгрузки"] = ""
    status_res["Получено статусов"] = 0

    for upload in upload_list:

        if status_res["Обогащаются подгрузки"]:
            status_res["Обогащаются подгрузки"] += ", "

        status_res["Обогащаются подгрузки"] += str(upload.id())

    set_res(status_res)

    for upload in upload_list:

        status_res["Текущая подгрузка"] = str(upload.id())
        set_res(status_res)

        call_list = upload.call_list([{"field": "Result", "operation": "is", "value": "NULL"}])

        count = 0

        for call in call_list:

            count += 1
            status_res["Обрабатываем звонок"] = "{} из {}".format(count, len(call_list))
            set_res(status_res)

            status = None

            try:
                status = skorozvon.get_last_status(call.ext_id())
            except:
                pass

            if status is None:
                continue

            call.set_result(status)
            status_res["Получено статусов"] += 1

    status_res.pop("Текущая подгрузка")
    status_res.pop("Обрабатываем звонок")
    set_res(status_res)

    return {"sleep": int(params["Periodicity"])*60, "params": params}

if __name__ == "__main__":
    enrich_status({"Список проектов": ["Эксклюзивный регион ТФ"]}, None)