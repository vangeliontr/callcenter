from CallCenter.Skorozvon import Skorozvon
from DataBase.DBController import DBController

skorozvon = Skorozvon()

controller = DBController()
cur = controller.get_cursor()

download_list = [1352,
1348,
1343,
1337,
1329,
1326,
1304,
1301,
1298,
1149,
1142,
1138,
1126,
1120,
1112,
1106,
1100]

for download_id in download_list:

    print(download_id)

    cur.execute("""SELECT
                   Call_.ID,
                   Call_.ExtID,
                   Call_.Result
                   FROM
                   Organization
                   INNER JOIN Call_Organization ON Call_Organization.Organization = Organization.ID
                   INNER JOIN Call_ ON Call_Organization.Call_ = Call_.ID
                   WHERE
                   Organization.Download = {} AND Result is NULL;""".format(download_id))

    call_list = cur.fetchall()

    count = 0

    for call in call_list:

        count += 1
        print("{} из {}".format(count, len(call_list)))

        status = skorozvon.get_last_status(call["ExtID"])

        if status is None:
            continue

        cur.execute("UPDATE Call_ SET Result = '{}' WHERE ID = {}".format(status, call["ID"]))

    controller.save_changes()

