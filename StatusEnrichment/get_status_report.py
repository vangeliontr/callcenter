from DataBase.DBController import DBController
from Bank.BankList import BankList
from DataBase.Project import Project

controller = DBController()
cur = controller.get_cursor()

download_list = [1352,
1348,
1343,
1337,
1329,
1326,
1304,
1301,
1298,
1149,
1142,
1138,
1126,
1120,
1112,
1106,
1100]

result_file = open("result.csv", "w")
bank_list = BankList.get()
project_list = Project.list_in_work()

result_file.write("Название;ИНН;Адрес;Имя;Фамилия;Отчество;Телефон")

for bank in bank_list:
    result_file.write(";{}".format(bank.name()))

for project in project_list:
    result_file.write(";{}".format(project.name()))

result_file.write("\n")

for download_id in download_list:

    print(download_id)

    cur.execute(""" SELECT
                    Organization.ID,
                    Organization.`Name` as OrgName, 
                    Organization.INN,
                    Organization.Address,
                    ContactPerson.`Name`,
                    ContactPerson.Surname,
                    ContactPerson.MiddleName,
                    ContactInfo.`Value`
                    FROM
                    Organization
                    LEFT JOIN ContactPerson ON ContactPerson.ID = Organization.LastContactPerson
                    LEFT JOIN ContactInfo ON ContactInfo.ID = Organization.LastPhoneInfo
                    WHERE
                    Organization.Download = {}
                    ;""".format(download_id))

    org_list = cur.fetchall()
    count = 0

    for org in org_list:

        count += 1
        print("{} из {}".format(count, len(org_list)))

        result_file.write("{};{};{};{};{};{};{}".format(org["OrgName"], org["INN"], org["Address"], org["Name"],
                                                        org["Surname"], org["MiddleName"], org["Value"]))

        cur.execute("""SELECT Call_.Result,
                        Call_.Bank,
                        Call_.Project
                        FROM
                        Call_
                        INNER JOIN Call_Organization ON Call_Organization.Call_ = Call_.ID
                        WHERE
                        Call_Organization.Organization = {}
                        """.format(org["ID"]))

        call_list = cur.fetchall()

        for bank in bank_list:

            bank_res = ""

            for call in call_list:
                if call["Project"] is None and call["Bank"] == bank.id():
                    bank_res = call["Result"]

            result_file.write(";{}".format(bank_res))

        for project in project_list:

            project_res = ""

            for call in call_list:
                if call["Project"] == project.id():
                    project_res = call["Result"]

            result_file.write(";{}".format(project_res))

        result_file.write("\n")
